const INIT_DATA = ({name}) => [
    {
        name: 'systemtypes',
        label: 'System type',
        url: `/api/wa/istsos/services/${name}/systemtypes`,
        mode: "buttons",
        selected: 'insitu-fixed-point',
        data: []
    },
    {
        name: 'observedproperties',
        label: 'Observed properties',
        url: `/api/wa/istsos/services/${name}/observedproperties`,
        mode: "list-selectable",
        selected: false,
        data: []
    },
    {
        name: 'uoms',
        label: 'Unit of measures',
        url: `/api/wa/istsos/services/${name}/uoms`,
        mode: "list-selectable",
        selected: false,
        data: []
    },
    {
        name: 'configsections',
        label: 'Configuration sections',
        url: `/api/wa/istsos/services/${name}/configsections`,
        mode: "urn",
        selected: false,
        data: []
    },
    {
        name: 'epsg',
        label: 'EPSG',
        url: `/api/wa/istsos/services/${name}/epsgs`,
        mode: "list-selectable",
        selected: false,
        data: []
    },
    {
        name: 'procedure',
        label: 'Procedures',
        url: `/api/wa/istsos/services/${name}/procedures/operations/geojson`,
        mode: "list-selectable",
        selected: false,
        data: []
    }
]

const fetchOptions = {
    method: 'GET',
    headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Basic YWRtaW46YmF0bWFu'
    }
}

export default {INIT_DATA, fetchOptions}