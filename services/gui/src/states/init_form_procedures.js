
const INIT_DATA  = {
    title: 'Procedures',
    service: '',
    base_url:`/istsos/wa/istsos/services`,
    api_get_list_url: 'procedures/operations/getlist',
    api_url: 'procedures',
    api_config_url: 'configsections',
    api_uom_url: 'uoms',
    api_observedproperty_url: 'observedproperties',
    api_systemtypes_url: `systemtypes`,
    to_edit: '',
    edit: false,
    isLoading: false,
    toasts: [],
    uom_tmp: [],
    checked: [],
    form: {
        "system_id": "",
        "assignedSensorId": "",
        "system": "",
        "description": "",
        "keywords": "",
        "identification": [
            {
                "name":"uniqueID",
                "definition":"urn:ogc:def:identifier:OGC:uniqueID",
                "value":"urn:ogc:def:procedure:x-istsos:1.0:"
            }
        ],
        "classification": [
            {
                "name": "System Type",
                "definition": "urn:ogc:def:classifier:x-istsos:1.0:systemType",
                "value": "insitu-fixed-point"
            },
            {
                "name": "Sensor Type",
                "definition": "urn:ogc:def:classifier:x-istsos:1.0:sensorType",
                "value": ""
            }
        ],
        "characteristics": "",
        "contacts": [{
        "administrativeArea": '',
        "city": '',
        "country": '',
        "deliveryPoint": '',
        "email": '',
        "fax": '',
        "individualName": '',
        "organizationName": '',
        "postalcode": '',
        "role": "urn:x-ogc:def:classifiers:x-istsos:1.0:contactType:owner",
        "voice": '',
        "web": ''
        }, {
        "administrativeArea": '',
        "city": '',
        "country": '',
        "deliveryPoint": '',
        "email": '',
        "fax": '',
        "individualName": '',
        "organizationName": '',
        "postalcode": '',
        "role": "urn:x-ogc:def:classifiers:x-istsos:1.0:contactType:manufacturer",
        "voice": '',
        "web": ''
        }, {
        "administrativeArea": '',
        "city": '',
        "country": '',
        "deliveryPoint": '',
        "email": '',
        "fax": '',
        "individualName": '',
        "organizationName": '',
        "postalcode": '',
        "role": "urn:x-ogc:def:classifiers:x-istsos:1.0:contactType:operator",
        "voice": '',
        "web": ''
        }],
        "documentation": [],
        "capabilities": [],
        "location": {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [
                    0, 0, 0
                ]
            },
            "crs": {
                "type": "name",
                "properties": {
                    "name": ""
                }
            },
            "properties": {
                "name": ""
            }
        },
        "interfaces": "",
        "inputs": [],
        "outputs": [
            {
                "name": "Time",
                "definition": "urn:ogc:def:parameter:x-istsos:1.0:time:iso8601",
                "uom": "iso8601",
                "description": "",
                "constraint": {}
            }
        ],
        "mqtt": "",
        "history": [],
        "error": "",
        "message": "",
        "procedures": []
    },
    default: {
        "system_id": "",
        "assignedSensorId": "",
        "system": "",
        "description": "",
        "keywords": "",
        "identification": [
            {
                "name":"uniqueID",
                "definition":"urn:ogc:def:identifier:OGC:uniqueID",
                "value":"urn:ogc:def:procedure:x-istsos:1.0:"
            }
        ],
        "classification": [
            {
                "name": "System Type",
                "definition": "urn:ogc:def:classifier:x-istsos:1.0:systemType",
                "value": "insitu-fixed-point"
            },
            {
                "name": "Sensor Type",
                "definition": "urn:ogc:def:classifier:x-istsos:1.0:sensorType",
                "value": ""
            }
        ],
        "characteristics": "",
        "contacts": [{
        "administrativeArea": '',
        "city": '',
        "country": '',
        "deliveryPoint": '',
        "email": '',
        "fax": '',
        "individualName": '',
        "organizationName": '',
        "postalcode": '',
        "role": "urn:x-ogc:def:classifiers:x-istsos:1.0:contactType:owner",
        "voice": '',
        "web": ''
        }, {
        "administrativeArea": '',
        "city": '',
        "country": '',
        "deliveryPoint": '',
        "email": '',
        "fax": '',
        "individualName": '',
        "organizationName": '',
        "postalcode": '',
        "role": "urn:x-ogc:def:classifiers:x-istsos:1.0:contactType:manufacturer",
        "voice": '',
        "web": ''
        }, {
        "administrativeArea": '',
        "city": '',
        "country": '',
        "deliveryPoint": '',
        "email": '',
        "fax": '',
        "individualName": '',
        "organizationName": '',
        "postalcode": '',
        "role": "urn:x-ogc:def:classifiers:x-istsos:1.0:contactType:operator",
        "voice": '',
        "web": ''
        }],
        "documentation": [],
        "capabilities": [],
        "location": {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [
                    "", "", ""
                ]
            },
            "crs": {
                "type": "name",
                "properties": {
                    "name": ""
                }
            },
            "properties": {
                "name": ""
            }
        },
        "interfaces": "",
        "inputs": [],
        "outputs": [
            {
                "name": "Time",
                "definition": "urn:ogc:def:parameter:x-istsos:1.0:time:iso8601",
                "uom": "iso8601",
                "description": "",
                "constraint": {}
            }
        ],
        "mqtt": "",
        "history": [],
        "error": "",
        "message": "",
        "procedures": []
    }
}

export default { INIT_DATA }