const initialProcedure = {
  init: [
    {
        name: 'systemtypes',
        label: 'System type',
        api_url: `/systemtypes`,
        mode: "buttons",
        selected: 'insitu-fixed-point',
        data: []
    },
    {
        name: 'observedproperties',
        label: 'Observed properties',
        api_url: `/observedproperties`,
        mode: "list-selectable",
        selected: false,
        data: []
    },
    {
        name: 'uoms',
        label: 'Unit of measures',
        api_url: `/uoms`,
        mode: "list-selectable",
        selected: false,
        data: []
    },
    {
        name: 'configsections',
        label: 'Configuration sections',
        api_url: `/configsections`,
        mode: "urn",
        selected: false,
        data: []
    },
    {
        name: 'epsg',
        label: 'EPSG',
        api_url: `/epsgs`,
        mode: "list-selectable",
        selected: false,
        data: []
    },
    {
        name: 'procedure',
        label: 'Procedures',
        api_url: `/procedures/operations/geojson`,
        mode: "list-selectable",
        selected: false,
        data: []
    }
  ],
  status: false,
  isLoading: true,
  legend: "Procedure",
  method: "POST",
  base_url: `${process.env.NEXT_PUBLIC_SIMILE_PROTOCOL}://${process.env.NEXT_PUBLIC_SIMILE_DOMAIN}/wa/istsos/services/`,
  api_url: `/procedures`,
  form: {
      "system_id": "",
      "assignedSensorId": "",
      "system": "",
      "description": "",
      "keywords": "",
      "identification": [
          {
              "name":"uniqueID",
              "definition":"urn:ogc:def:identifier:OGC:uniqueID",
              "value":"urn:ogc:def:procedure:x-istsos:1.0:"
          }
      ],
      "classification": [
          {
              "name": "System Type",
              "definition": "urn:ogc:def:classifier:x-istsos:1.0:systemType",
              "value": "insitu-fixed-point"
          },
          {
              "name": "Sensor Type",
              "definition": "urn:ogc:def:classifier:x-istsos:1.0:sensorType",
              "value": ""
          }
      ],
      "characteristics": "",
      "contacts": [],
      "documentation": [],
      "capabilities": [],
      "location": {
          "type": "Feature",
          "geometry": {
              "type": "Point",
              "coordinates": [
                  0, 0, 0
              ]
          },
          "crs": {
              "type": "name",
              "properties": {
                  "name": ""
              }
          },
          "properties": {
              "name": ""
          }
      },
      "interfaces": "",
      "inputs": [],
      "outputs": [
          {
              "name": "Time",
              "definition": "urn:ogc:def:parameter:x-istsos:1.0:time:iso8601",
              "uom": "iso8601",
              "description": "",
              "constraint": {}
          }
      ],
      "mqtt": "",
      "history": [],
      "error": "",
      "message": "",
      "procedures": []
  },
  default: {
      "system_id": "",
      "assignedSensorId": "",
      "system": "",
      "description": "",
      "keywords": "",
      "identification": [
          {
              "name":"uniqueID",
              "definition":"urn:ogc:def:identifier:OGC:uniqueID",
              "value":"urn:ogc:def:procedure:x-istsos:1.0:"
          }
      ],
      "classification": [
          {
              "name": "System Type",
              "definition": "urn:ogc:def:classifier:x-istsos:1.0:systemType",
              "value": ""
          },
          {
              "name": "Sensor Type",
              "definition": "urn:ogc:def:classifier:x-istsos:1.0:sensorType",
              "value": ""
          }
      ],
      "characteristics": "",
      "contacts": [],
      "documentation": [],
      "capabilities": [],
      "location": {
          "type": "Feature",
          "geometry": {
              "type": "Point",
              "coordinates": [
                  "", "", ""
              ]
          },
          "crs": {
              "type": "name",
              "properties": {
                  "name": ""
              }
          },
          "properties": {
              "name": ""
          }
      },
      "interfaces": "",
      "inputs": [],
      "outputs": [
          {
              "name": "Time",
              "definition": "urn:ogc:def:parameter:x-istsos:1.0:time:iso8601",
              "uom": "iso8601",
              "description": "",
              "constraint": {}
          }
      ],
      "mqtt": "",
      "history": []
  },
  uom_tmp: [],
  op_selected: [],
  uom_selected: [],
  modals: [
      {
          id: "op",
          open: false,
          legend: "Observed property",
          url: '/observedproperties',
          form: {
              name: "",
              definition: "urn:ogc:def:parameter:x-istsos:1.0:",
              description: "",
              constraint: {},
              // constraint: {
              //     role: "un:x-ogc:def:classifiers:x-istsos:1.0:qualityIndexCheck:level0",
              //     interval: ["", ""]
              // },
              error: "",
              message: ""
          },
          default: {
              name: "",
              definition: "urn:ogc:def:parameter:x-istsos:1.0:",
              description: "",
              constraint: {},
              // constraint: {
              //     role: "un:x-ogc:def:classifiers:x-istsos:1.0:qualityIndexCheck:level0",
              //     interval: ["", ""]
              // },
              error: "",
              message: ""
          },
      },
      {
        id: "uom",
        open: false,
        legend: "Unit of measure",
        url: '/uoms',
        form: {
            name: "",
            description: "",
            error: "",
            message: ""
        },
        default: {
            name: "",
            description: "",
            error: "",
            message: ""
        },
      }
  ]
}

const actionsReducer = (state, action) => {
  switch (action.type) {
    case 'LOAD_DATA':
      return {
        ...state,
        status: true,
        isLoading: false,
        init: action.data,
        form: {
          ...state.form,
          location: {
            ...state.form.location,
            crs: {
              ...state.form.location.crs,
              properties: {
                ...state.form.location.crs.properties,
                name: action.data[4].data[0]['name']
              }
            }
          }
        }
    };
    case 'UPDATE_INIT':
      return {
        ...state,
        init: action.data
      }
    case 'UPDATE_INIT_OUTPUT':
      return {
        ...state,
        init: action.data,
        form: {
          ...state.form,
          outputs: state.default.outputs
        }
      }
    case 'UPDATE_INIT_ADD_OUTPUTS':
      return {
        ...state,
        init: action.data.init,
        form: {
          ...state.form,
          outputs: [
            ...state.form.outputs,
            ...action.data.outputs
          ]
        }
      }
    case 'UPDATE_UOM_SIMPLE':
      return {
        ...state,
        uom_tmp: action.data
      }
    case 'UPDATE_UOM':
      return {
        ...state,
        form: {
          ...state.form,
          outputs: [
            ...state.form.outputs,
            state.uom_tmp[action.data.idx]
          ]
        },
        uom_tmp: state.uom_tmp.filter(
          item2 => item2.name != action.data.item.name
        )
      }
    case 'UPDATE_UOM_NULL':
      return {
        ...state,
        form: {
          ...state.form,
          outputs: [
            ...state.form.outputs,
            {
              ...action.data,
              uom: "null"
            }
          ]
        }
      }
    case 'UPDATE_UOM_0':
      return {
        ...state,
        form: {
          ...state.form,
          outputs: state.form.outputs.filter(item2 => item2.name != action.data.name)
        },
        uom_tmp: [
          ...state.uom_tmp,
          state.form.outputs.filter(item2 => item2.name == action.data.name)[0]
        ]
      }
    case 'UPDATE_OUTPUTS':
      return {
        ...state,
        form: {
          ...state.form,
          outputs: action.data
        }
      }
    case 'ADD_UOM':
      return {
        ...state,
        uom_tmp: [
          ...state.uom_tmp,
          action.data
        ]
      }
    case 'ADD_MODALS':
      return {
        ...state,
        modals: action.data
      }
    case 'UPDATE_CLASSIFICATION':
      return {
        ...state,
        form: {
          ...state.form,
          classification: action.data
        }
      }
    case 'UPDATE_FORM_LOCATION_PROPERTIES':
      return {
        ...state,
        form: {
          ...state.form,
          location: {
            ...state.form.location,
            properties: {
              ...state.form.location.properties,
              name: action.data
            }
          }
        }
      }
    case 'UPDATE_FORM_LOCATION_CRS_PROPERTIES':
     return {
        ...state,
        form: {
          ...state.form,
          location: {
            ...state.form.location,
            crs: {
              ...state.form.location.crs,
              properties: {
                ...state.form.location.crs.properties,
                name: action.data
              }
            }
          }
        }
      }
    case 'UPDATE_FORM_LOCATION_GEOMETRY_COORDINATES':
      return {
        ...state,
        form: {
          ...state.form,
          location: {
            ...state.form.location,
            geometry: {
              ...state.form.location.geometry,
              coordinates: action.data
            }
          }
        }
      }
    case 'UPDATE_FORM_SYSTEM':
      return {
        ...state,
        form: {
          ...state.form,
          system_id: action.data.value,
          system: action.data.value,
          identification: action.data.identification
        }
      }
    case 'UPDATE_FORM_SIMPLE':
      return {
        ...state,
        form: {
          ...state.form,
          [action.data.id]: action.data.value
        }
      }
    case 'UPDATE_FORM_CLASSIFICATION':
      return {
        ...state,
        init: action.data.init,
        form: {
          ...action.data.defaultForm,
          classification: action.data.classification,
          location: {
            ...state.form.location
          }
        }
      }
    case 'SET_MSG':
      return {
        ...state,
        form: {
          ...state.form,
          message: action.data,
          error: ""
        }
      }
    case 'SET_ERR_MSG':
      return {
        ...state,
        form: {
          ...state.form,
          message: "",
          error: action.data
        }
      }
    case 'UPDATE_INIT_MODALS':
      return {
        ...state,
        init: action.data.init,
        modals: action.data.modals
      }
    default:
      return state;
  }
};

export {initialProcedure, actionsReducer}