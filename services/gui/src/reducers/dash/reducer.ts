
const initialDash = {
  tag: 'status',
  title: 'Status',
  isLoading: true,
  loaded: 0,
  base_url: "/istsos/wa/istsos/services/",
  url: "/istsos/wa/istsos/operations/status",
  get_op: "/observedproperties",
  get_proc: "/procedures/operations/geojson",
  get_uom: "/uoms",
  services: [],
  filters: {
    services: [],
    temporal: [],
    observedproperties:[],
    procedures: []
  },
  alias: {
    service: 'Service',
    procedures: 'Procedures',
    observedproproties: 'Observed properties',
    uom: 'Unit of measures'
  }
}

const actionsReducer = (state, action) => {
  
  switch (action.type) {
    case 'ADD_SERVICE_DATA':
      return state
    case 'ADD_OP':
      var new_services = state.services.map((item, index) => {
        if (index === action.id) {
          return { ...item, observedproperties: action.data };
        } else {
          return item;
        }
      })
      return {
        ...state,
        services: new_services,
        loaded: state.loaded+1,
        isLoading: state.loaded == 2 ? false : true
      };
    case 'ADD_PROC':
      var new_services = state.services.map((item, index) => {
        if (index === action.id) {
          return { ...item, procedures: action.data };
        } else {
          return item;
        }
      })
      return {
        ...state,
        services: new_services,
        loaded: state.loaded+1,
        isLoading: state.loaded == 2 ? false : true
      };
    case 'ADD_UOM':
      var new_services = state.services.map((item, index) => {
        if (index === action.id) {
          return { ...item, uom: action.data };
        } else {
          return item;
        }
      })
      return {
        ...state,
        services: new_services,
        loaded: state.loaded+1,
        isLoading: state.loaded == 2 ? false : true
      };
    case 'ADD_SERVICES':
      return {
        ...state,
        services: action.data.map(
          item => ({
            service: item.service,
            get_op: `${state.base_url}${item.service}${state.get_op}`,
            get_proc: `${state.base_url}${item.service}${state.get_proc}`,
            get_uom: `${state.base_url}${item.service}${state.get_uom}`,
            procedures: [],
            observedproperties: [],
            uom: []
          })
        )
      };
    default:
      return state;
  }
};


export {initialDash, actionsReducer};