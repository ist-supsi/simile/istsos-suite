import {
  faServer, faDatabase,
  faWarehouse, faInfo,
  faMapMarkedAlt, faBroadcastTower
} from '@fortawesome/free-solid-svg-icons'

const initialServer = [
  {
    tag: 'status',
    title: 'Status',
    isLoading: true,
    url: `/istsos/wa/istsos/operations/status`,
    icon: faServer,
    type: 'status',
    data: [],
    alias: {
      service: "Service",
      featuresOfInterest: "Features Of Interests",
      offerings: "Offerings",
      procedures: "Procedures",
      observedProperties: "Observed Properties",
      availability: "Availability",
      database: "Database",
      getcapabilities: "GetCapabilities",
      describesensor: "DescribeSensor",
      getobservation: "GetObservation",
      getfeatureofinterest: "GetFeatureOfInterest",
      insertobservation: "InsertObservation",
      registersensor: "RegisterSensor",
    },
    test: false,
    post: {
      url: `/istsos/wa/istsos/services`,
      body: {
        "service":"",
        "customdb":"on",
        "user":"postgres",
        "password":"postgres",
        "host":"localhost",
        "port":"5432",
        "dbname":"istsos",
        "epsg": "4326"
      },
      form: [
        {
          title: "Sensor Observation Service",
          collapse: false,
          test: {
            active: false,
            message: null
          },
          items: [
            {
              id: 'service',
              label: 'Service name',
              default: '',
              type: 'text'
            },
            {
              id: 'epsg',
              label: 'EPSG',
              default: '4326',
              type: 'text'
            }
          ],
          error: "",
          message: ""
        },
        {
          title: 'Customize database',
          reference_tag: 'database', // tag to get default values already retrieve from another main element of the initialState list
          collapse: true,
          test: {
            active: true,
            message: null
          },
          items: [
            {
                id: 'user',
                label: 'User',
                default: 'postgres',
                type: 'text'
            },
            {
                id: 'password',
                label: 'Password',
                default: 'postgres',
                type: 'password'
            },
            {
                id: 'host',
                label: 'Host',
                default: 'localhost',
                type: 'text'
            },
            {
                id: 'port',
                label: 'Port',
                default: 5432,
                type: 'number'
            },
            {
                id: 'dbname',
                label: 'DB name',
                default: 'istsos',
                type: 'text'
            }
          ]
        }
      ],
    },
    put: false,
    delete: {
      url: `/istsos/wa/istsos/services`,
      body: {
        service: ''
      }
    },
    message: "",
    error: "",
    default: true
  },
  {
    tag: 'database',
    title: 'Database',
    isLoading: false,
    icon: faDatabase,
    type: 'database',
  },
  {
    tag: 'provider',
    title: 'Provider',
    isLoading: false,
    icon: faWarehouse,
    type: 'provider',
  },
  {
    tag: 'identification',
    title: 'Identification',
    isLoading: false,
    icon: faInfo,
    type: 'identification',
  },
  {
    tag: 'coordinates',
    title: 'Coordinates',
    isLoading: false,
    icon: faMapMarkedAlt,
    type: 'coordinates',
  },
  // {
  //   tag: 'mqtt',
  //   title: 'Mqtt publisher',
  //   isLoading: false,
  //   icon: faBroadcastTower,
  //   type: 'mqtt'
  // },
  
]


const actionsReducer = (state, action) => {
  switch (action.type) {
    case 'RELOAD':
      console.log('reload');
      
      return state.map((item, index) => {
        if (index === action.id) {
          return { ...item, isLoading: true };
        } else {
          return item;
        }
      });
    case 'LOAD':
      return state.map((item, index) => {
        return {
          ...item,
          data: action.data[index].data,
          isLoading: false
        }
      })
    case 'ADD_SERVICE':
      return state;
    default:
      return state;
  }
};
 
// const [todos, dispatch] = useReducer(actionsReducer, initialServer);

export {initialServer, actionsReducer};

// const todoReducer = (state, action) => {
//   switch (action.type) {
//     case 'DO_TODO':
//       return state.map(todo => {
//         if (todo.id === action.id) {
//           return { ...todo, complete: true };
//         } else {
//           return todo;
//         }
//       });
//     case 'UNDO_TODO':
//       return state.map(todo => {
//         if (todo.id === action.id) {
//           return { ...todo, complete: false };
//         } else {
//           return todo;
//         }
//       });
//     default:
//       return state;
//   }
// };