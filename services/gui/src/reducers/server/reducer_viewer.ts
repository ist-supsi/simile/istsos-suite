import {
  faServer, faDatabase,
  faWarehouse, faInfo,
  faMapMarkedAlt, faBroadcastTower
} from '@fortawesome/free-solid-svg-icons'

const initialServer = [
  {
    tag: 'status',
    title: 'Status',
    isLoading: true,
    url: `/istsos/wa/istsos/operations/status`,
    icon: faServer,
    type: 'status',
    data: [],
    alias: {
      service: "Service",
      featuresOfInterest: "Features Of Interests",
      offerings: "Offerings",
      procedures: "Procedures",
      observedProperties: "Observed Properties",
      availability: "Availability",
      database: "Database",
      getcapabilities: "GetCapabilities",
      describesensor: "DescribeSensor",
      getobservation: "GetObservation",
      getfeatureofinterest: "GetFeatureOfInterest",
      insertobservation: "InsertObservation",
      registersensor: "RegisterSensor",
    },
    message: "",
    error: "",
    default: true
  },  
]


const actionsReducer = (state, action) => {
  switch (action.type) {
    case 'RELOAD':
      console.log('reload');
      
      return state.map((item, index) => {
        if (index === action.id) {
          return { ...item, isLoading: true };
        } else {
          return item;
        }
      });
    case 'LOAD':
      return state.map((item, index) => {
        return {
          ...item,
          data: action.data[index].data,
          isLoading: false
        }
      })
    case 'ADD_SERVICE':
      return state;
    default:
      return state;
  }
};
 
// const [todos, dispatch] = useReducer(actionsReducer, initialServer);

export {initialServer, actionsReducer};

// const todoReducer = (state, action) => {
//   switch (action.type) {
//     case 'DO_TODO':
//       return state.map(todo => {
//         if (todo.id === action.id) {
//           return { ...todo, complete: true };
//         } else {
//           return todo;
//         }
//       });
//     case 'UNDO_TODO':
//       return state.map(todo => {
//         if (todo.id === action.id) {
//           return { ...todo, complete: false };
//         } else {
//           return todo;
//         }
//       });
//     default:
//       return state;
//   }
// };