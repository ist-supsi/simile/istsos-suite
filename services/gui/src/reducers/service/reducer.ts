import {
  faServer, faDatabase,
  faWarehouse, faInfo,
  faMapMarkedAlt, faBroadcastTower,
  faExchangeAlt, faThermometer,
  faTable, faAtlas, faChartLine,
  faFileCsv, faCogs, faStar, faTint,
  faLeaf, faTasks
} from '@fortawesome/free-solid-svg-icons'



const initialService = (name: string) => [
  {
    tag: 'status',
    title: 'Status',
    isLoading: true,
    url: `/istsos/wa/istsos/operations/status`,
    icon: faServer,
    type: 'hidden',
    data: []
  },
  {
    tag: 'procedures',
    title: 'Procedures',
    isLoading: true,
    icon: faThermometer,
    type: 'procedure',
  },
  {
    tag: 'dashboard',
    title: 'Dashboard',
    isLoading: false,
    icon: faChartLine,
    type: 'dashboard',
  },
  {
    tag: 'analysis',
    title: 'Data Analysis',
    isLoading: true,
    icon: faCogs,
    type: 'analysis',
  },
  {
    tag: 'importer',
    title: 'Data importer',
    isLoading: false,
    icon: faFileCsv ,
    type: 'importer',
  },
  {
    tag: 'process',
    title: 'Processes',
    isLoading: true,
    icon: faTasks,
    type: 'process',
    data: []
  },
  // {
  //   tag: 'database',
  //   title: 'Database',
  //   isLoading: true,
  //   icon: faDatabase,
  //   type: 'database'
  // },
  {
    tag: 'provider',
    title: 'Provider',
    isLoading: true,
    icon: faWarehouse,
    type: 'provider',
    data: {},
  },
  {
    tag: 'identification',
    title: 'Identification',
    isLoading: true,
    icon: faInfo,
    type: 'identification'
  },
  {
    tag: 'coordinates',
    title: 'Coordinates',
    isLoading: true,
    icon: faMapMarkedAlt,
    type: 'coordinates',
  },
  // {
  //   tag: 'mqtt',
  //   title: 'Mqtt publisher',
  //   isLoading: true,
  //   icon: faAtlas,
  //   type: 'mqtt',
  // },
  {
    tag: 'getobservation',
    title: 'GetObservation configuration',
    isLoading: true,
    icon: faBroadcastTower,
    type: 'getobservation',
  },
  {
    tag: 'observedproperties',
    title: 'Observed properties',
    isLoading: true,
    icon: faLeaf,
    type: 'observedproperty',
  },
  {
    tag: 'uom',
    title: 'Unit of measures',
    isLoading: true,
    icon: faTint,
    type: 'uoms',
  },
  {
    tag: 'dataquality',
    title: 'Data quality',
    isLoading: true,
    icon: faStar,
    type: 'dataqualities',
  }
  // {
  //   tag: 'serviceurl',
  //   title: 'Proxy Configuration',
  //   isLoading: true,
  //   icon: faExchangeAlt,
  //   type: 'serviceurl',
  // },
]


const actionsReducer = (state, action) => {
  switch (action.type) {
    case 'RELOAD':
      console.log('reload');
      
      return state.map((item, index) => {
        if (index === action.id) {
          return { ...item, isLoading: true };
        } else {
          return item;
        }
      });
    case 'LOAD':
      return state.map((item, index) => {
        return {
          ...item,
          data: action.data[index].data,
          isLoading: false
        }
      })
    case 'CHANGE_SERVICE':
      let new_state =  action.data
      return state.map(
        (item, index)=>{
          return {
            ...item,
            url: new_state[index].url,
            data: action.data[index].data,
            isLoading: false
          }
        }
      )
    case 'ADD_SERVICE':
      return state;
    default:
      return state;
  }
};
 
// const [todos, dispatch] = useReducer(actionsReducer, initialServer);

export {initialService, actionsReducer};