

import { NextPage } from 'next'
import { withKeycloak } from '@react-keycloak/ssr'

// containers
import DashContainer from '../containers/dash/DashContainer'

// components
import Layout from '../components/layout/layout'
import dynamic from "next/dynamic";
const Auth = dynamic(() => import("./auth"));


const DashPage: NextPage<{keycloak: any}> = ({ keycloak }) => {
  if (keycloak?.authenticated) {
    return (
      <Layout authenticated={keycloak?.authenticated} user={keycloak?.idTokenParsed} logout={keycloak?.logout}>
        <DashContainer token={keycloak.token} service='demo'/>
      </Layout>
    )
  } else {
    return <Auth />
  }
}

export default withKeycloak(DashPage)