import '../styles/index.css'
import '../styles/react-datetime.css'
import cookie from 'cookie'
import * as React from 'react'
import type { IncomingMessage } from 'http'
import type { AppProps, AppContext } from 'next/app'

import { SSRKeycloakProvider, SSRCookies } from '@react-keycloak/ssr'

const keycloakCfg = {
  realm: `${process.env.NEXT_PUBLIC_KEYCLOAK_REALM}`,
  url: '/auth',
  clientId: process.env.NEXT_PUBLIC_KEYCLOAK_CLIENT_ID,
  checkLoginIframe: false,
  "ssl-required": false
  //"ssl-required": "external",
}


interface InitialProps {
  cookies: unknown
}

function MyApp({ Component, pageProps, cookies }: AppProps & InitialProps) {
  return (
    <SSRKeycloakProvider
      keycloakConfig={keycloakCfg}
      persistor={SSRCookies(cookies)}
    >
      <Component {...pageProps} />
    </SSRKeycloakProvider>
  )
}

function parseCookies(req?: IncomingMessage) {
  if (!req || !req.headers) {
    return {}
  }
  return cookie.parse(req.headers.cookie || '')
}

MyApp.getInitialProps = async (context: AppContext) => {
  return {
    cookies: parseCookies(context?.ctx?.req)
  }
}

export default MyApp