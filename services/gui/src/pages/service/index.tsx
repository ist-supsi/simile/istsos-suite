import { NextPage } from 'next'
import { useRouter } from 'next/router'
import { withKeycloak } from '@react-keycloak/ssr'

// containers
import IndexContainer from '../../containers/service/IndexContainer'
import IndexViewerContainer from '../../containers/service/IndexViewerContainer'

// components
import Loader from 'react-loader-spinner'
import Layout from '../../components/layout/layout'
import dynamic from "next/dynamic";
const Auth = dynamic(() => import("../auth"));


const IndexPage: NextPage<{keycloak: any}> = ({ keycloak }) => {
  const router = useRouter()
  var service_selected: boolean = false;
  var service_name: string;
  
  if (router.query) {
    if (router.query.id && typeof(router.query.id) === 'string' ) {
      service_name = router.query.id.toLowerCase();
      service_selected = true
    }
  }
  if (keycloak?.authenticated && keycloak?.idTokenParsed) {
    if (keycloak?.realmAccess?.roles?.indexOf('editor') > -1) {
      return (
        <Layout authenticated={keycloak?.authenticated} user={keycloak?.idTokenParsed} logout={keycloak?.logout}>
          <div>Sei un data manager</div>
          <div>Sei un data manager</div>
          <div>Sei un data manager</div>
        </Layout>
      )
    } else if (keycloak?.realmAccess?.roles?.indexOf('admin') > -1) {
      return (
        <IndexContainer
          authenticated={keycloak?.authenticated}
          user={keycloak?.idTokenParsed}
          role={'admin'}
          logout={keycloak?.logout}
          token={keycloak.token}
          service={service_name}
        />
      )
    }  else if (keycloak?.realmAccess?.roles?.indexOf('viewer') > -1) {
      return <IndexViewerContainer
        authenticated={keycloak?.authenticated}
        user={keycloak?.idTokenParsed}
        role={'viewer'}
        logout={keycloak?.logout}
        token={keycloak.token}
        service={service_name}
      />
    } else {
      return (
        <div>
          Per ora non sono disponibile contenuti aperti al pubblico
          <Loader
            type="Bars"
            color="#000"
            height={100}
            width={100}
          />
        </div>
      )
    }
    
  } else if (keycloak?.authenticated) {
    return (
      <div>
        <Loader
          type="Bars"
          color="#000"
          height={100}
          width={100}
        />
      </div>
    )
  } else {
    return <Auth />
  }
}

export default withKeycloak(IndexPage)