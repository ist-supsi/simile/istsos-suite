


// components
import Layout from '../components/layout/layout'
import { withKeycloak } from '@react-keycloak/ssr'
import { NextPage } from 'next'
import styles from '../styles/utils.module.css'


const Auth: NextPage<{keycloak: any}> = ({keycloak}) => {
  return (
    <Layout auth={true} authenticated={keycloak?.authenticated}  logout={keycloak?.logout}>
      <div>
        {
          keycloak?.authenticated
          ? <button onClick={() => keycloak.logout()}>Log out</button>
          : <div className={styles.loader}>
              <div className={styles.centered}>
                <div className={styles.content_loader}>
                  <div>
                    <div className={styles.header}>
                      <h1 className={styles.title}>Disclaimer</h1>
                      <img src={process.env.NEXT_PUBLIC_LOGO_EXT} width={'500'} />
                    </div>
                    <div className={styles.content}>
                      {/* <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                      <p> Fugiat pariatur quam dolores, id aut, quasi quidem voluptatem </p>
                      <p>repellat temporibus libero recusandae. Error nesciunt exercitationem </p>
                      <p>quod repellendus accusamus necessitatibus minima eligendi?</p> */}
                    </div>
                    <button className={styles.button1} onClick={() => keycloak.login()}>➲ Sign in with OAuth</button>
                    <button
                      className={styles.button2}
                      onClick={() => keycloak.login()}
                    >
                        Enter as a guest using<br/><i>u: guest, p: guest</i>
                    </button>
                  </div>
                </div>
              </div>
          </div>
        }
      </div>
    </Layout>
  )
}

export default withKeycloak(Auth)