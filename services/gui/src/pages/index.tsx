
import { NextPage } from 'next'
import { withKeycloak } from '@react-keycloak/ssr'

// containers
import IndexContainer from '../containers/index/IndexContainer'
import IndexViewerContainer from '../containers/index/IndexViewerContainer'

// components
import Loader from 'react-loader-spinner'
import Layout from '../components/layout/layout'
import dynamic from "next/dynamic";
const Auth = dynamic(() => import("./auth"));


const IndexPage: NextPage<{keycloak: any}> = ({ keycloak }) => {
  if (keycloak?.authenticated && keycloak?.idTokenParsed) {
    if (keycloak?.realmAccess?.roles?.indexOf('editor') > -1) {
      return (
        <Layout authenticated={keycloak?.authenticated} user={keycloak?.idTokenParsed} logout={keycloak?.logout}>
          <div></div>
          <div>Menu</div>
          <div>Sei un data editor2</div>
        </Layout>
      )
    } else if (keycloak?.realmAccess?.roles?.indexOf('admin') > -1) {
      return (
        <IndexContainer
          authenticated={keycloak?.authenticated}
          user={keycloak?.idTokenParsed}
          role={'admin'}
          logout={keycloak?.logout}
          token={keycloak.token}
        />
      )
    } else if (keycloak?.realmAccess?.roles?.indexOf('viewer') > -1) {
      return <IndexViewerContainer
        authenticated={keycloak?.authenticated}
        user={keycloak?.idTokenParsed}
        role={'viewer'}
        logout={keycloak?.logout}
        token={keycloak.token}
      />
    } else {
      return <div>Per ora non sono disponibile contenuti aperti al pubblico</div>
      // return (
      //   <Layout authenticated={keycloak?.authenticated} user={keycloak?.idTokenParsed} logout={keycloak?.logout}>
      //     Per ora non sono disponibile contenuti aperti al pubblico
      //   </Layout>
      // )
    }
    
  } else if (keycloak?.authenticated) {
    return (
      <Layout authenticated={keycloak?.authenticated} user={keycloak?.idTokenParsed} logout={keycloak?.logout}>
        <div></div>
        <div>Menu</div>
        <div>
          <div className='loading-gif'>
            <Loader
              type="Bars"
              color="#000"
              height={100}
              width={100}
            />
          </div>
          Loading user
        </div>
      </Layout>
    )
  } else {
    return <Auth />
  }
}

export default withKeycloak(IndexPage)