#[macro_use]
extern crate serde_derive;
extern crate chrono;

mod utils;
mod istsos;
use chrono::prelude::*;
use std::convert::*;
use std::collections::HashMap;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use wasm_bindgen_futures::JsFuture;
use web_sys::{Request, RequestInit, RequestMode, Response};
use serde_json;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
extern {
    fn alert(s: &str);
}

#[wasm_bindgen]
extern "C" {
    // Use `js_namespace` here to bind `console.log(..)` instead of just
    // `log(..)`
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);

    #[wasm_bindgen(js_namespace = console, js_name = log)]
    fn log_string(s: String);

    #[wasm_bindgen(js_namespace = console, js_name = log)]
    fn log_js(s: JsValue);

    // #[wasm_bindgen(js_namespace = console, js_name = log)]
    // fn log_string_list(s: Vec<&str>);

    // The `console.log` is quite polymorphic, so we can bind it with multiple
    // signatures. Note that we need to use `js_name` to ensure we always call
    // `log` in JS.
    #[wasm_bindgen(js_namespace = console, js_name = log)]
    fn log_u16(a: u16);

    #[wasm_bindgen(js_namespace = console, js_name = log)]
    fn log_u32(a: u32);

    #[wasm_bindgen(js_namespace = console, js_name = log)]
    fn log_usize(a: usize);

    #[wasm_bindgen(js_namespace = console, js_name = log)]
    fn log_f32(a: f32);

    #[wasm_bindgen(js_namespace = console, js_name = log)]
    fn log_f64(a: f64);

    // Multiple arguments too!
    #[wasm_bindgen(js_namespace = console, js_name = log)]
    fn log_many(a: &str, b: &str);
}


#[derive(Serialize, Deserialize, Debug)]
pub struct FileInfo<'a> {
    name: &'a str,
    datetime: Vec<String>,
    observedproperties: Vec<&'a str>,
    data:  Vec<Vec<String>>
}


fn parse_datetime_string(date_time_string: &str) -> Result<istsos::BasicResult, istsos::BasicResult> {
    let from = NaiveDateTime::parse_from_str(date_time_string, "%d/%m/%Y %H:%M:%S");
    let from: istsos::BasicResult = match from {
        Ok(r) => istsos::BasicResult{
            success: true,
            message: r.format("%Y-%m-%dT%H:%M:%SZ").to_string()
        },
        Err(_) => istsos::BasicResult{
            success: false,
            message: "Can't parse datetime type1".to_string()
        }
    };
    if from.success==false {
        let from = DateTime::parse_from_rfc2822(date_time_string);
        let from: istsos::BasicResult = match from {
            Ok(r) => istsos::BasicResult{
                success: true,
                message: r.to_rfc3339()
            },
            Err(_) => istsos::BasicResult{
                success: false,
                message: "Can't parse datetime type2".to_string()
            }
        };
        if from.success==false {
            let from = DateTime::parse_from_rfc3339(date_time_string);
            let from: istsos::BasicResult = match from {
                Ok(r) => istsos::BasicResult{
                    success: true,
                    message: r.to_rfc3339()
                },
                Err(_) =>istsos::BasicResult{
                    success: false,
                    message: "Can't parse datetime type3".to_string()
                }
            };

            if from.success==false {
                let from = DateTime::parse_from_str(date_time_string, "%Y-%m-%dT%H:%M:%S%z");
                return match from {
                    Ok(r) => Ok(istsos::BasicResult{
                        success: true,
                        message: r.to_rfc3339()
                    }),
                    Err(_) => Err(istsos::BasicResult{
                        success: false,
                        message: "Can't parse datetime type4".to_string()
                    })
                };
            } else {
                return Ok(from)
            }
        } else {
            return Ok(from)
        }
    } else {
        return Ok(istsos::BasicResult{
            success: true,
            message: from.message
        })
    }
}


#[wasm_bindgen]
pub fn load(content: JsValue, delimiter:JsValue) -> Result<JsValue, JsValue> {
    
    let content = content.as_string().unwrap();
    let delimiter = delimiter.as_string().unwrap();
    let content: Vec<&str> = content.split("\n").collect();
    let mut check_header: Vec<&str> = content[0].trim().split(&delimiter).collect();
    check_header.retain(|&x| x != "");
    if check_header.len() >= 2 {
        let name: &str = "";
        let mut data: Vec<Vec<String>> = vec![];
        let mut datetime: Vec<String> = vec![];
        let observedproperties: Vec<& str> = content[0]
            .trim()
            .split(&delimiter)
            .collect();
        for el in content[1..].into_iter() {
            if !el.is_empty() {
                let mut tmp: Vec<String> = vec![];
                let el_trimmed: Vec<& str> = el.trim()
                    .split(&delimiter).collect();
                let mut idx = 0;
                for elem in el_trimmed.iter() {
                    if idx == 0 {
                        let d_tmp: Vec<&str> = elem.to_owned().split(&delimiter).collect();
                        let date_time_string = d_tmp[0];
                        let date_time: String;
                        let from = parse_datetime_string(date_time_string);
                        let from: istsos::BasicResult = match from {
                            Ok(r) => r,
                            Err(v) => return Err(
                                JsValue::from_serde(&istsos::BasicResult{
                                success: false,
                                message: v.message
                            }).unwrap())
                        };
                        date_time = from.message;
                        tmp.insert(0, date_time.to_owned());
                        datetime.push(date_time);
                    } else {
                        tmp.push(
                            elem.to_string()
                        )
                    }
                    idx += 1;
                }
                if tmp.len() > 1 {
                    data.push(tmp);
                }
            }
        }
        let file_info = FileInfo{
            name: &name,
            datetime: datetime,
            observedproperties: observedproperties[1..].to_vec(),
            data: data
        };
        Ok(JsValue::from_serde(&file_info).unwrap())
    } else{
        let name: &str = &content[0].trim().replace(&delimiter, "").to_owned();
        let char_delimiter: Vec<char>  = delimiter.chars().collect();
        let date_time_string: &str = content[1].trim().trim_matches(char_delimiter[0]);
        let datetime: String;
        let from = parse_datetime_string(date_time_string);
        let from: istsos::BasicResult = match from {
            Ok(r) => r,
            Err(_) => return Err(
                JsValue::from_serde(&istsos::BasicResult{
                success: false,
                message: "Can't parse datetime".to_string()
            }).unwrap())
        };
        datetime = from.message;
        let observedproperties: Vec<& str> = content[3]
            .trim()
            .split(&delimiter)
            .collect();
        let mut data: Vec<Vec<String>> = vec![];
        for el in content[5..].into_iter() {
            let mut tmp = el
                .trim()
                .split(&delimiter)
                .map(
                    |x| x
                        .to_string()
                    ).collect::<Vec<String>>();
            if tmp.len() > 1 {
                let d_t = datetime.clone();
                tmp.insert(0, d_t);
                data.push(tmp);
            }
        }
        let file_info = FileInfo{
            name: &name,
            datetime: vec![datetime],
            observedproperties: observedproperties,
            data: data
        };
        
        Ok(JsValue::from_serde(&file_info).unwrap())
    }
}

#[wasm_bindgen]
pub async fn create(
    form: JsValue,
    service: JsValue,
    options: JsValue,
    base_url: JsValue
) -> Result<JsValue, JsValue> {
    let profile = form.into_serde();
    let mut profile: istsos::Profile = match profile {
        Ok(r) => r,
        Err(_) => return Err(
            JsValue::from_serde(
                &istsos::BasicResult{
                    success: false,
                    message: "Can't load JSON of the procedure".to_string()
                }).unwrap()
            )
    };
    let service = service.as_string();
    let service: String = match service {
        Some(r) => r,
        None => return Err(
            JsValue::from_serde(
                &istsos::BasicResult{
                    success: false,
                    message: "Can't parse service name".to_string()
                }).unwrap()
            )
    };
    let base_url = base_url.as_string();
    let base_url: String = match base_url {
        Some(r) => r,
        None => return Err(
            JsValue::from_serde(
                &istsos::BasicResult{
                    success: false,
                    message: "Can't parse url".to_string()
                }).unwrap()
            )
    };
    let options = options.into_serde(); 
    let options: istsos::FetchOptions = match options {
        Ok(r) => r,
        Err(_) => return Err(
            JsValue::from_serde(
                &istsos::BasicResult{
                    success: false,
                    message: "Can't parse options for fetching features".to_string()
                }).unwrap()
            )
    };
    let url = format!("{}/{}/procedures", base_url, service);
    let profile_create = profile.create(&url, &options).await;
    let profile_create = match profile_create {
        Ok(r) => r,
        Err(e) => return Err(JsValue::from_serde(&e).unwrap())
    };
    let url = format!(
        "{}/{}/virtualprocedures/{}/code",
        base_url,
        service,
        profile.system_id
    );
    let procedures_body = profile.procedures.join("\": \":\",\"");
    let code_body = format!(
        "from istsoslib.responders.GOresponse import VirtualProcessProfile\nclass istvp(VirtualProcessProfile):\n\tprocedures = {{\"{}\": \":\"}}\n",
        procedures_body
    ).to_string();
    let add_code = profile.add_code(
        &url,
        code_body,
        &options
    ).await;
    let add_code = match add_code {
        Ok(r) => r,
        Err(e) => return Err(JsValue::from_serde(&e).unwrap())
    };

    log_js(JsValue::from_serde(&profile).unwrap());

    Ok(JsValue::from_serde(
        &istsos::BasicResult{
            success: true,
            message: "Profile correctly registered".to_string()
        }).unwrap()
    )
}

#[wasm_bindgen]
pub async fn upload_v(
    form: JsValue,
    data: JsValue,
    datetime: JsValue,
    service: JsValue,
    overwrite: JsValue,
    options: JsValue,
    base_url: JsValue
) -> Result<JsValue, JsValue> {
    let profile = form.into_serde();
    let mut profile: istsos::Profile = match profile {
        Ok(r) => r,
        Err(_) => return Err(
            JsValue::from_serde(
                &istsos::BasicResult{
                    success: false,
                    message: "Can't load JSON of the procedure".to_string()
                }).unwrap()
            )
    };
    profile.identification[0].value = format!(
        "{}{}",
        profile.identification[0].value,
        profile.system_id
    );
    let service = service.as_string();
    let service: String = match service {
        Some(r) => r,
        None => return Err(
            JsValue::from_serde(
                &istsos::BasicResult{
                    success: false,
                    message: "Can't parse service name".to_string()
                }).unwrap()
            )
    };
    let base_url = base_url.as_string();
    let base_url: String = match base_url {
        Some(r) => r,
        None => return Err(
            JsValue::from_serde(
                &istsos::BasicResult{
                    success: false,
                    message: "Can't parse url".to_string()
                }).unwrap()
            )
    };
    let event_time = datetime.as_string();
    let event_time: String = match event_time {
        Some(r) => r,
        None => return Err(
            JsValue::from_serde(
                &istsos::BasicResult{
                    success: false,
                    message: "Can't parse datetime".to_string()
                }).unwrap()
            )
    };
    let options = options.into_serde(); 
    let options: istsos::FetchOptions = match options {
        Ok(r) => r,
        Err(_) => return Err(
            JsValue::from_serde(
                &istsos::BasicResult{
                    success: false,
                    message: "Can't parse options for fetching features".to_string()
                }).unwrap()
            )
    };
    let data = data.into_serde();
    let data: Vec<Vec<f32>> = match data {
        Ok(r) => r,
        Err(_) => return Err(
            JsValue::from_serde(
                &istsos::BasicResult{
                    success: false,
                    message: "Can't parse data grid".to_string()
                }).unwrap()
            )
    };
    let depth_idx = profile
        .outputs
        .iter()
        .position(
            |x| x.definition.contains("depth")
        );
    let depth_idx = match depth_idx {
        Some(p) => p,
        None => return Err(JsValue::from_str("Error"))
    };
    let mut index = 1;
    let event_time_list: Vec<&str> = event_time.split(",").collect();
    let mut event_time_is_list: bool = false; 
    if event_time_list.len() > 1 {
        event_time_is_list = true;
    }
    for row in data.iter() {
        let suffix = row[depth_idx-1]
            .to_string()
            .replace(".", "_");
        let name: &str = &format!(
            "{}_{}",
            profile.system_id,
            suffix
        );
        profile.procedures.push(name.to_string());
        let mut tmp_sensor = profile.clone();
        tmp_sensor.system_id = name.to_string();
        tmp_sensor.system = name.to_string();
        tmp_sensor.identification[0].value = format!(
            "{}{}",
            tmp_sensor.identification[0].value,
            &name
        );
        tmp_sensor.classification[0].value = "insitu-fixed-point".to_string();
        tmp_sensor.location.properties.name = format!(
            "{}_{}",
            tmp_sensor.location.properties.name,
            suffix
        );
        let altitude = tmp_sensor.location.geometry.coordinates[2] - row[depth_idx-1] as f64;

        tmp_sensor.location.geometry.coordinates[2] = altitude;
        tmp_sensor.outputs = tmp_sensor.outputs
            .into_iter()
            .filter(
                |x| !x.definition.contains("depth")
            ).collect();
        let url = format!("{}/{}/procedures", base_url, service);

        let resp_create = tmp_sensor.create(&url, &options).await;
        let resp_create = match resp_create {
            Ok(r) => r,
            Err(e) => return Err(JsValue::from_serde(&e).unwrap())
        };
        if !resp_create.success && !resp_create.message.contains("already exist") {
            return Err(JsValue::from_serde(&resp_create).unwrap()) 
        }
        let mut options_get = options.clone();
        options_get.method = "GET".to_string();
        let get_assigned_id = tmp_sensor.get_assigned_id(
            &format!(
                "{}/{}",
                &url,
                name.to_string().to_owned()
            ),
            &options_get
        ).await;
        let get_assigned_id = match get_assigned_id {
            Ok(r) => r,
            Err(e) => return Err(JsValue::from_serde(&e).unwrap())
        };
        
        let mut row_tmp: Vec<String> = row
            .clone()
            .iter()
            .map(
                |x| x.to_string()
            )
            .collect();
        row_tmp.remove(depth_idx - 1);
        let event_time_tmp = match event_time_is_list {
            true => event_time_list[index-1].to_string(),
            false => event_time.clone()
        };
        
        let post_data = format!(
            "{};{},{}",
            get_assigned_id.data.assignedSensorId,
            event_time_tmp,
            row_tmp.join(",")
        );
        log_string(post_data.clone());
        let insert_observation = tmp_sensor.insert_observation(
            &format!("{}/{}/operations/insertobservation", base_url, service),
            &post_data,
            &options,
            true
        ).await;
        let insert_observation = match insert_observation {
            Ok(r) => r,
            Err(e) => return Err(JsValue::from_serde(&istsos::BasicResult{
                success: false,
                message: format!("Error in data row index {}\nMessage: {}", index, e.message)
            }).unwrap())
        };
        index += 1;
        // log_js(
        //     JsValue::from_serde(&insert_observation.unwrap()).unwrap()
        // );
        // log_string(post_data);
    }
    let url = format!("{}/{}/procedures", base_url, service);
    profile.system = profile.system_id.clone();
    let profile_create = profile.create(&url, &options).await;
    let profile_create = match profile_create {
        Ok(r) => r,
        Err(e) => return Err(JsValue::from_serde(&e).unwrap())
    };
    let url = format!(
        "{}/{}/virtualprocedures/{}/code",
        base_url,
        service,
        profile.system_id
    );
    let procedures_body = profile.procedures.join("\": \":\",\"");
    let code_body = format!(
        "from istsoslib.responders.GOresponse import VirtualProcessProfile\nclass istvp(VirtualProcessProfile):\n\tprocedures = {{\"{}\": \":\"}}\n",
        procedures_body
    ).to_string();
    let add_code = profile.add_code(
        &url,
        code_body,
        &options
    ).await;
    let add_code = match add_code {
        Ok(r) => r,
        Err(e) => return Err(JsValue::from_serde(&e).unwrap())
    };
    Ok(JsValue::from_serde(
        &istsos::BasicResult{
            success: true,
            message: "Data loaded correctly".to_string()
        }).unwrap()
    )
    // Ok(JsValue::from_str("ciao"))
}

#[wasm_bindgen]
pub async fn upload(
    form: JsValue,
    data: JsValue,
    beginposition: JsValue,
    endposition: JsValue,
    service: JsValue,
    overwrite: JsValue,
    options: JsValue,
    base_url: JsValue
) -> Result<JsValue, JsValue> {
    let profile = form.into_serde();
    let mut profile: istsos::Profile = match profile {
        Ok(r) => r,
        Err(_) => return Err(
            JsValue::from_serde(
                &istsos::BasicResult{
                    success: false,
                    message: "Can't load JSON of the procedure".to_string()
                }).unwrap()
            )
    };
    profile.identification[0].value = format!(
        "{}{}",
        profile.identification[0].value,
        profile.system_id
    );
    let service = service.as_string();
    let service: String = match service {
        Some(r) => r,
        None => return Err(
            JsValue::from_serde(
                &istsos::BasicResult{
                    success: false,
                    message: "Can't parse service name".to_string()
                }).unwrap()
            )
    };
    let base_url = base_url.as_string();
    let base_url: String = match base_url {
        Some(r) => r,
        None => return Err(
            JsValue::from_serde(
                &istsos::BasicResult{
                    success: false,
                    message: "Can't parse url".to_string()
                }).unwrap()
            )
    };
    let begin_position = beginposition.as_string();
    let begin_position: String = match begin_position {
        Some(r) => r,
        None => return Err(
            JsValue::from_serde(
                &istsos::BasicResult{
                    success: false,
                    message: "Can't parse beginposition".to_string()
                }).unwrap()
            )
    };
    let end_position = endposition.as_string();
    let end_position: String = match end_position {
        Some(r) => r,
        None => return Err(
            JsValue::from_serde(
                &istsos::BasicResult{
                    success: false,
                    message: "Can't parse endposition".to_string()
                }).unwrap()
            )
    };
    let options = options.into_serde(); 
    let options: istsos::FetchOptions = match options {
        Ok(r) => r,
        Err(_) => return Err(
            JsValue::from_serde(
                &istsos::BasicResult{
                    success: false,
                    message: "Can't parse options for fetching features".to_string()
                }).unwrap()
            )
    };
    let data = data.into_serde();
    let data: Vec<Vec<String>> = match data {
        Ok(r) => r,
        Err(_) => return Err(
            JsValue::from_serde(
                &istsos::BasicResult{
                    success: false,
                    message: "Can't parse data grid".to_string()
                }).unwrap()
            )
    };
    let mut data_parsed: Vec<Vec<String>> = [].to_vec();

    for d in data {
        let mut d_tmp: Vec<String> = d.clone();
        let outputs_len = profile.outputs.len() as isize;
        let d_len = d_tmp.len() as isize;
        if d_len != outputs_len {
           let diff = outputs_len - d_len;
           if diff > 0 {
                while d_tmp.len() < profile.outputs.len() { 
                    d_tmp.push(
                        "-999.99".to_string()
                    );
                }
           } else if diff < 0 {
                d_tmp = d_tmp[0..profile.outputs.len()].to_vec();
           }
        }
        let mut ok = true;
        let mut idx = 0usize;
        for c in d_tmp.clone() {
            if c.contains("**") {
                ok = false;
            }
            if c.contains("N/A") {
                d_tmp[idx] = "-999.99".to_string();
            }
            if c.contains("N/D") {
                d_tmp[idx] = "-999.99".to_string();
            }
            if c == "" {
                d_tmp[idx] = "-999.99".to_string();
            }
            idx += 1;
        }
        if ok {
            data_parsed.push(d_tmp);
        }
    }
    let overwrite = overwrite.as_bool();
    let overwrite: bool = match overwrite {
        Some(r) => r,
        None => return Err(
            JsValue::from_serde(
                &istsos::BasicResult{
                    success: false,
                    message: "Can't parse overwrite param".to_string()
                }).unwrap()
            )
    };
    let url = format!("{}/{}/procedures", base_url, service);
    profile.system = profile.system_id.clone();
    let profile_create = profile.create(&url, &options).await;
    let profile_create = match profile_create {
        Ok(r) => r,
        Err(e) => return Err(JsValue::from_serde(&e).unwrap())
    };

    if !profile_create.success && !profile_create.message.contains("already exist") {
        return Err(JsValue::from_serde(&profile_create).unwrap()) 
    }
    let mut options_get = options.clone();
    options_get.method = "GET".to_string();
    let get_assigned_id = profile.get_assigned_id(
        &format!(
            "{}/{}",
            &url,
            profile.system_id.to_string().to_owned()
        ),
        &options_get
    ).await;
    let get_assigned_id = match get_assigned_id {
        Ok(r) => r,
        Err(e) => return Err(JsValue::from_serde(&e).unwrap())
    };

    log("--> inserting observations");
    let insert_observation = profile.insert_observation2(
        &format!("{}/{}/operations/insertobservation", base_url, service),
        &get_assigned_id.data.assignedSensorId,
        &begin_position,
        &end_position,
        data_parsed,
        &options,
        overwrite
    ).await;
    let insert_observation = match insert_observation {
        Ok(r) => r,
        Err(e) => return Err(JsValue::from_serde(&istsos::BasicResult{
            success: false,
            message: format!("Error in inserobservation[{}]", e.message)
        }).unwrap())
    };
    if insert_observation.success {
        Ok(JsValue::from_serde(
            &istsos::BasicResult{
                success: true,
                message: "Data loaded correctly".to_string()
            }).unwrap()
        )    
    } else {
        Err(JsValue::from_serde(&istsos::BasicResult{
            success: false,
            message: format!("Error in inserobservation[{}]", insert_observation.message)
        }).unwrap())
    }
    
    // Ok(JsValue::from_str("ciao"))
}

#[wasm_bindgen]
pub async fn upload_data(
    form: JsValue,
    data: JsValue,
    beginposition: JsValue,
    endposition: JsValue,
    service: JsValue,
    overwrite: JsValue,
    options: JsValue,
    base_url: JsValue
) -> Result<JsValue, JsValue> {
    let profile = form.into_serde();
    let mut profile: istsos::Profile = match profile {
        Ok(r) => r,
        Err(_) => return Err(
            JsValue::from_serde(
                &istsos::BasicResult{
                    success: false,
                    message: "Can't load JSON of the procedure".to_string()
                }).unwrap()
            )
    };
    profile.identification[0].value = format!(
        "{}{}",
        profile.identification[0].value,
        profile.system_id
    );
    let service = service.as_string();
    let service: String = match service {
        Some(r) => r,
        None => return Err(
            JsValue::from_serde(
                &istsos::BasicResult{
                    success: false,
                    message: "Can't parse service name".to_string()
                }).unwrap()
            )
    };
    let base_url = base_url.as_string();
    let base_url: String = match base_url {
        Some(r) => r,
        None => return Err(
            JsValue::from_serde(
                &istsos::BasicResult{
                    success: false,
                    message: "Can't parse url".to_string()
                }).unwrap()
            )
    };
    let begin_position = beginposition.as_string();
    let begin_position: String = match begin_position {
        Some(r) => r,
        None => return Err(
            JsValue::from_serde(
                &istsos::BasicResult{
                    success: false,
                    message: "Can't parse beginposition".to_string()
                }).unwrap()
            )
    };
    let end_position = endposition.as_string();
    let end_position: String = match end_position {
        Some(r) => r,
        None => return Err(
            JsValue::from_serde(
                &istsos::BasicResult{
                    success: false,
                    message: "Can't parse endposition".to_string()
                }).unwrap()
            )
    };
    let options = options.into_serde(); 
    let options: istsos::FetchOptions = match options {
        Ok(r) => r,
        Err(_) => return Err(
            JsValue::from_serde(
                &istsos::BasicResult{
                    success: false,
                    message: "Can't parse options for fetching features".to_string()
                }).unwrap()
            )
    };
    let data = data.into_serde();
    let data: Vec<Vec<String>> = match data {
        Ok(r) => r,
        Err(_) => return Err(
            JsValue::from_serde(
                &istsos::BasicResult{
                    success: false,
                    message: "Can't parse data grid".to_string()
                }).unwrap()
            )
    };
    let mut data_parsed: Vec<Vec<String>> = [].to_vec();
    for d in data {
        let mut d_tmp: Vec<String> = d.clone();
        let outputs_len = profile.outputs.len() as isize;
        let d_len = d_tmp.len() as isize;
        if d_len != outputs_len {
           let diff = outputs_len - d_len;
           if diff > 0 {
                while d_tmp.len() < profile.outputs.len() { 
                    d_tmp.push(
                        "-999.99".to_string()
                    );
                }
           } else if diff < 0 {
                d_tmp = d_tmp[0..profile.outputs.len()].to_vec();
           }
        }
        let mut ok = true;
        let mut idx = 0usize;
        for c in d_tmp.clone() {
            if c.contains("**") {
                ok = false;
            }
            if c == "" {
                d_tmp[idx] = "-999.99".to_string();
            }
            if c.contains("N/D") {
                d_tmp[idx] = "-999.99".to_string();
            }
            if c.contains("NA") {
                d_tmp[idx] = "-999.99".to_string();
            }
            idx += 1;
        }
        if ok {
            data_parsed.push(d_tmp);
        }
    }
    let overwrite = overwrite.as_bool();
    let overwrite: bool = match overwrite {
        Some(r) => r,
        None => return Err(
            JsValue::from_serde(
                &istsos::BasicResult{
                    success: false,
                    message: "Can't parse overwrite param".to_string()
                }).unwrap()
            )
    };
    let url = format!("{}/{}/procedures", base_url, service);
    profile.system = profile.system_id.clone();
    let mut options_get = options.clone();
    options_get.method = "GET".to_string();
    let get_assigned_id = profile.get_assigned_id(
        &format!(
            "{}/{}",
            &url,
            profile.system_id.to_string().to_owned()
        ),
        &options_get
    ).await;
    let get_assigned_id = match get_assigned_id {
        Ok(r) => r,
        Err(e) => return Err(JsValue::from_serde(&e).unwrap())
    };
    let insert_observation = profile.insert_observation2(
        &format!("{}/{}/operations/insertobservation", base_url, service),
        &get_assigned_id.data.assignedSensorId,
        &begin_position,
        &end_position,
        data_parsed,
        &options,
        overwrite
    ).await;
    let insert_observation = match insert_observation {
        Ok(r) => r,
        Err(e) => return Err(JsValue::from_serde(&istsos::BasicResult{
            success: false,
            message: format!("Error in inserobservation[{}]", e.message)
        }).unwrap())
    };
    if insert_observation.success {
        Ok(JsValue::from_serde(
            &istsos::BasicResult{
                success: true,
                message: "Data loaded correctly".to_string()
            }).unwrap()
        )    
    } else {
        Err(JsValue::from_serde(&istsos::BasicResult{
            success: false,
            message: format!("Error in inserobservation[{}]", insert_observation.message)
        }).unwrap())
    }
    
    // Ok(JsValue::from_str("ciao"))
}