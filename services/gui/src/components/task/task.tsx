
import styles from './task.module.css'

const TaskSimple: React.FC<{
    subtitle: string,
    title: string,
    selected: boolean,
    onClick: Function
  }> = ({
    subtitle,
    title,
    selected,
    onClick
  }) => {
  return <div
    onClick={(e)=>onClick(e)}
    className={`${styles.task} ${selected ? 'bg-gray-400 hover:bg-gray-400' : ''}`}
  >
    <h2 className={styles.tasktitle}>{title}</h2>
    <div>{subtitle}</div>
  </div>
}


export {
  TaskSimple
}