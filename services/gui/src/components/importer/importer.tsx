import ProcedureProfileContainer from '../../containers/procedure/ProcedureProfileContainer'
import styles from './importer.module.css'

interface Procedure {
  name: string
  assignedid: string
}

interface Imp {
  mode: string
  delimiter: string
  onChangeMode: (e: React.ChangeEvent<HTMLSelectElement>) => any
  onChange: (e: React.ChangeEvent<HTMLSelectElement>) => any
  handleMultiFileChosen: (e: React.ChangeEvent<HTMLInputElement>) => any
  handleOverwrite: (e: React.ChangeEvent<HTMLInputElement>) => any
  handleClickSubmit: Function
  onChangeSelect: (e: React.ChangeEvent<HTMLSelectElement>) => any
  handleUpload: (e: React.MouseEvent<HTMLInputElement>) => any
  procedures: Procedure[]
  errors: any[]
  messages: any[]
  files: any[]
  token: string
  service: string
  overwrite: boolean
  outputs: any[]
}

const ImporterComponent: React.FC<Imp> = ({
  mode, delimiter, onChangeMode, handleMultiFileChosen, onChange, handleOverwrite,
  handleClickSubmit, onChangeSelect, handleUpload,
  procedures, messages, errors, files, token, service, overwrite, outputs
}) => {
  let obs_message =  undefined;
  if (files.length> 0 && mode === 'add-profile') {
    obs_message = files[0].file_info.observedproperties.join(',')
  }
  return <div>
    <form onSubmit={(e)=>e.preventDefault()} >
      <fieldset>
        <legend>Mode</legend>
        <select value={mode ? mode : ''} onChange={onChangeMode}>
        <option value=''>Select a mode...</option>
          <option value='add-profile'>New profile and data</option>
          <option value='import-profile-data'>Import data profile</option>
        </select>
      </fieldset>
      <fieldset>
        <legend>File</legend>
        <label>Delimiter: </label>
        <select defaultValue={delimiter} onChange={onChange}>
          <option value=" ">space</option>
          <option value=",">,</option>
          <option value=";">;</option>
          <option value={`\t`}>tab</option>
        </select>
        <br/><br/>
        <label>Select a file: </label>
        <input
          id="file"
          key={`key-prof-input-${mode}`}
          type="file"
          accept='.csv,.txt'
          onChange={handleMultiFileChosen}
          multiple={mode == "add-profile" ? false : true}
        />
        <br/>
        {
          mode !== 'add-profile' ?
          <div>
            <label>Overwrite: </label>
            <input
              id="file"
              key={`key-prof-overwrite-${mode}`}
              type="checkbox"
              onChange={handleOverwrite}
              checked={overwrite}
            />
          </div> : <></>
        }
        {messages.map((item, index) => <p key={`key-msg-${index}`} className={styles.success}>{item}</p>)}
        {errors.map(
          (item, index) => {
            if (item.includes('Key (id_eti_fk, id_pro_fk, val_depth)')) {
              if (item.includes('already exists')) {
                return <p key={`key-err-${index}`} className={styles.error}><br/>{item}</p>
                // return <p key={`key-err-${index}`} className={styles.error}><br/>{"Error --> The relation between \"Time\" and \"depth\" columns must be unique!"}</p>
              } else {
                return <p key={`key-err-${index}`} className={styles.error}><br/>{item}</p>
              }
              // return <XMLViewer xml={`<?xml${xml_error}`} />
            } else if (item.includes('already exist, consider to change the name')) {
              return <p key={`key-err-${index}`} className={styles.error}><br/>{"Error --> The prcedure name is already used. Consider to change it or to select the importer mode to update data."}</p>
            } else {
              return <p key={`key-err-${index}`} className={styles.error}><br/>{item}</p>
            }
          }
        )}
        {
          obs_message ?
          <div><br/>Columns name: {obs_message}</div> :
          <></>
        }
      </fieldset>
      {
        mode === 'add-profile' && files.length >0 ?
        <div>
          <ProcedureProfileContainer service={service} token={token} submit={handleClickSubmit} outputs={outputs}/>
        </div> : 
        mode === 'import-profile-data' && files.length >0 ? 
        <>
          <fieldset>
              <legend>Profile</legend>
              <label>Procedure name: </label>
              <select onChange={onChangeSelect}>
                <option value={undefined} >Select a procedure...</option>
                {procedures.map(
                  (item, index) => (
                    <option key={`key-proc-name-${index}`} value={`${item.assignedid}-${item.name}`}>{item.name}</option>
                  )
                )}
              </select>
              <br/>
              <br/>
              {/* <label>Depth column:</label>
              <input min="-1" type="number" value={state.depth_column} onChange={(el)=> setData({...state, depth_column: parseFloat(el.target.value)})} /> */}
          </fieldset>
          <button onClick={handleUpload}>Upload data</button>
        </> :
        <></>
      }
    </form>
  </div>
}

export default ImporterComponent