import {useState} from 'react'
import styles from './sidebar.module.css'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Link from 'next/link'
import { useRouter } from 'next/router';
import {
  useWindowSize
} from '@react-hook/window-size'

import {
  faArrowRight, faArrowDown, faAngleDoubleLeft
} from '@fortawesome/free-solid-svg-icons'

interface NavItem{
  label: string;
  url: string;
}


const SideBar: React.FC<{data: any; title?: string, home?: string, navlist?: any[]}> = ({data, title, home, navlist}) => {
  const [state, setData] = useState({
    selected: 0,
    open: false
  })
  const router = useRouter()
  const [width, height] = useWindowSize()

  function openSelect() {
    setData({
      ...state,
      open: !state.open
    })
  }

  function onChange(e) {
    let selected = e.target.innerText.split('\n')[e.target.value].toLowerCase();
    
    if (e.target.value==='0') {
      router.push({
        pathname: '/',
        query: {},
      })
    } else {
      let element = navlist.filter(item => item.value.toString()===e.target.value)[0]
      router.push({
        pathname: element.pathname,
        query: element.query
      })
    }
  }
  var selected: number;
  if (router.query) {
    if (router.query.id && navlist) {
      let navlist_filtered = navlist.filter(item => router.query.id === item.label)
      if (navlist_filtered.length > 0 ) {
        selected = navlist_filtered[0].value
      } else {
        selected = 0
      }
    } else {
      selected = 0  
    }
  } else {
    selected = 0
  }
  
  return(
    <div>
      {
        title ? <div className={styles.title}>
          {
            home ? <div onClick={()=>(
              router.push({
                pathname: '/',
                query: '',
              }))
            }>
              <FontAwesomeIcon className={styles.icon} icon={faAngleDoubleLeft} />
            </div> : <></>
          }
          <div>
            {title}
          </div>
        </div> : <></>
      }
      <div className={`${styles.sidebar}`}>
        <ul className={`${styles.sidebarlist}`}>
          { navlist ?
            <li className={state.open ? styles.sidebarli : ""}>
              <a
                onClick={openSelect}
                className={`${styles.sidebarlistitem}`}
              >              
                <div className={styles.sidebarlistitemel}>
                  <FontAwesomeIcon className={styles.icon} icon={state.open ? faArrowDown : faArrowRight} />
                </div>
                <div className={styles.sidebarlistitemtitle}>Services</div>
              </a>
            </li> : <></>
          }
          {
            state.open  ? <ul className={styles.navitem}>
              {navlist.map(
                item => <li
                  onClick={
                    ()=>{
                      openSelect()
                      router.push({
                        pathname: item.pathname,
                        query: item.query,
                      })
                  }}
                  className={styles.sidebarlittlelistitem}
                  value={item.value}
                  key={`key-side-${item.value}`}
                  id={(item.value).toString()}
                >{item.label}</li>
              )}
            </ul>: <></>
          }
          {
            router.pathname === '/' ? 
            <li className={styles.divider}>{`${title} - `}<em>Settings</em></li> :
            <li className={styles.divider}>{`${title} - `}<em>Data</em></li>
          }
          {data.slice(0,5).map(
            (item, index) => (
              <li key={`list-btn-nav-${index}`} className={item.selected ? styles.sidebarli : ""}>
                  <a onClick={
                    ()=>(
                      router.push({
                        pathname: item.pathname,
                        query: item.query,
                      })
                    )}
                    className={`${styles.sidebarlistitem}`}
                  >
                    <div className={styles.sidebarlistitemel}>
                      <FontAwesomeIcon className={styles.icon} icon={item.icon} />
                    </div>
                    <div className={styles.sidebarlistitemtitle}>{item.title}</div>
                  </a>
              </li>
            )
          )}
          {
            router.pathname === '/' ? 
            <></> :
            <li className={styles.divider}>{`${title} - `}<em>Settings</em></li>
          }
          {data.slice(5,data.length).map(
            (item, index) => (
              <li key={`list-btn-nav-${index}`} className={item.selected ? styles.sidebarli : ""}>
                  <a onClick={
                    ()=>(
                      router.push({
                        pathname: item.pathname,
                        query: item.query,
                      })
                    )}
                    className={`${styles.sidebarlistitem}`}
                  >
                    <div className={styles.sidebarlistitemel}>
                      <FontAwesomeIcon className={styles.icon} icon={item.icon} />
                    </div>
                    <div className={styles.sidebarlistitemtitle}>{item.title}</div>
                  </a>
              </li>
            )
          )}
        </ul>
      </div>
    </div>
  )
}

export default SideBar;