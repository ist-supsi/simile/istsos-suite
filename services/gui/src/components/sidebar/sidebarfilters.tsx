import {useState} from 'react'
import styles from './sidebar.module.css'
import {
  useWindowSize
} from '@react-hook/window-size'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


const SideBarFilters: React.FC<{title: string, children: React.ReactNode}> = ({title, children}) => {
  const [width, height] = useWindowSize()
  return(
    <div>
      <h1>{title}</h1>
      {height ? 
        <div style={{minHeight: `${height-64}px`}}>
          <div className={`${styles.sidebar}`}>
          <ul className={`${styles.sidebar_list}`}>
          {children}
          </ul> 
          </div>
        </div> :

			// <ul className={`${styles.sidebar_list}`}>
      //   <li className="my-px">
			// 		<span className={styles.sidebar_el}>{title}</span>
			// 	</li>
      //   {children}
      // </ul> 
        <></>
      }
    </div>
  )
}

export default SideBarFilters;