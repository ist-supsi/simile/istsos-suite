
import {useState} from 'react'
import { useRouter } from 'next/router'
import styles from './navbar.module.css'
import Image from 'next/image'

const NavBar: React.FC<{authenticated: boolean, user: any, logout: any, role?: string}> = ({children, authenticated, user, logout, role}) => {
  const [state, setState] = useState({
    open: false
  })
  const router = useRouter()
  
  function onClick() {
    setState({open: !state.open})
  }
  let content = <div 
    key='key-tool-1'
    className={styles.navbaritem}
  >
    Hello guest!
  </div>
  if (role) {
    if (role.indexOf('editor') > -1) {
      content = <div
        key='key-tool-1'
        className={styles.navbaritem}
      >
        {`Hello ${user.preferred_username}!`}
      </div>
    } else if (role.indexOf('admin') > -1) {
      content = <div className={styles.modalprofilecontainer}>
        <div
          key='key-tool-1'
          className={styles.navbaritem}
        >
          {`Hello ${user.preferred_username}!`}
        </div>
        <hr/>
        <div className={styles.istsosgrid}>
          <a href={`/grafana`} target='_blank'>
            <div className={styles.istsoscell}>
              <img
                className={styles.imgcentered}
                src={'/static/grafana_logo.png'} alt="Go to Grafana"
              />
              <div className="text-center">Grafana</div>
            </div>
          </a>
          <a href={`/auth`} target='_blank'>
            <div className={styles.istsoscell}>
              <img
                className={styles.imgcentered}
                src={'/static/keycloak_logo.png'} alt="Go to Keycloak"
              />
              <div className="text-center">Keycloak</div>
            </div>
          </a>
        </div>
        <hr/>
      </div>
    } else if (role.indexOf('viewer') > -1) {
      console.log(user);
      
      content = <div>
        <div
          key='key-tool-1'
          className={styles.navbaritem}
        >
          {`Hello ${user.preferred_username}!`}
        </div>
      </div>
    }
  }
  return(
    <div className={styles.navbar}>
      {
        authenticated?
        <div className={styles.alignleft}>
          {/* left aligned items */}
          <div className={styles.alignleft}>
            <div >
              <img
                className={styles.logo}
                src="/static/progetto-simile.png"
                alt="logo"
                height="20"
                width="120"
              />
            </div>
          </div>
          {/* right aligned items */}
          <div className={styles.alignright}>
            <div
              className={`${styles.navbaritem} ${styles.profile}`}
              onClick={onClick}
            >
              <span className={styles.content}>👤</span>
            </div>
          </div>
        </div> : <div>ciao</div>
      }
      {
        state.open ? <div className={styles.modalprofile}>
          <div>
            {content}
          </div>
          <div className={styles.centered}>
            <button onClick={logout}>Logout</button>
          </div>
        </div> : undefined}
    </div>
  )
}

export default NavBar