import {useEffect, useState} from 'react';
import "leaflet/dist/leaflet.css"
import { withSize } from 'react-sizeme'

// css
import styles from './mapcomponent.module.css'

var bbox_set=false;

// class MapComponent extends Component<{
//         onChangeBBOX: Function,
//         bbox: any,
//         data: any,
//         size: any
//     }> {

//     componentDidUpdate() {
//         if (this.props.bbox && !bbox_set && this.props.data.length > 0) {
//             // map.fitBounds(this.props.bbox.geometry.coordinates)
//             bbox_set = true;
//         //     this.props.data.map(
//         //         (item) => (
//         //             L.circleMarker(
//         //                     [item.coords.geometry.coordinates[1], item.coords.geometry.coordinates[0]],
//         //                     {
//         //                         radius: 1
//         //                     }
//         //                 ).addTo(map)
//         //                 .bindPopup(item.name)
//         //         )
//         //     )
//         }
//     }
    // componentDidMount() {
    //     L = require('leaflet');
    //     map = L.map('map')
    //     L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    //         attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    //     }).addTo(map);
    //     map.on('moveend', ()=>this.props.onChangeBBOX(map.getBounds().toBBoxString(), bbox_set));

    //     if (this.props.data.length > 0) {
    //         var group = L.featureGroup(
    //             this.props.data.map(
    //                 (item) => (
    //                     L.circleMarker(
    //                             [item.coords.geometry.coordinates[1], item.coords.geometry.coordinates[0]],
    //                             {
    //                                 radius: 1
    //                             }
    //                         ).bindPopup(item.name)
    //                 )
    //             )
    //         ).addTo(map);
    //         map.fitBounds(group.getBounds())
    //     }
        
    // }
//     render() {
//         let { data } = this.props
        
//         return(
//             <ContainerDimensions>
//                 { ({ width, height }) => 
//                     <MapContainer style={{width: width, height: height}} center={[51.505, -0.09]} zoom={13} scrollWheelZoom={false}>
//                         <TileLayer
//                             attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
//                             url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
//                         />
//                         <Marker position={[51.505, -0.09]}>
//                             <Popup>
//                             A pretty CSS3 popup. <br /> Easily customizable.
//                             </Popup>
//                         </Marker>
//                     </MapContainer>
//                 }
//             </ContainerDimensions>
//         )
//     }
// }


const MapComponent: React.FC<{size, onChangeBBOX?, data, bbox?}> = ({size, onChangeBBOX, data, bbox}) => {
  const [state, setData] = useState({
    mounted: false,
    loading: true,
    map: undefined
  })
  useEffect(
    () => {
      if (!state.mounted) {
        setData({...state, mounted: true})
      }
      if (state.mounted && state.loading) {
        var L = require('leaflet');
        var map = L.map('map')
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
          attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);
        if (onChangeBBOX) {
          map.on('moveend', ()=>onChangeBBOX(
            map.getBounds().toBBoxString(), true
          ));          
        }

        if (data.length > 0) {
          var group = L.featureGroup(
            data.map(
              (item) => (
                L.circleMarker(
                  [item.coords.geometry.coordinates[1], item.coords.geometry.coordinates[0]],
                  {
                    radius: 1
                  }
                ).bindPopup(item.name)
              )
            )
          ).addTo(map);
          map.fitBounds(group.getBounds())
        } else {
          map.setView([0, 0], 0);
        }
        setData({
          ...state,
          map: map,
          loading: false
        })
      }
      if (!state.loading) {
        var L = require('leaflet');
        if (bbox && !bbox_set  && data.length > 0) {
          state.map.fitBounds(bbox.geometry.coordinates)
          bbox_set = true;
          data.map(
            (item) => (
              L.circleMarker(
                    [item.coords.geometry.coordinates[1], item.coords.geometry.coordinates[0]],
                  {
                    radius: 1
                  }
                ).addTo(state.map)
                .bindPopup(item.name)
            )
          )
        } else if (data.length > 0 ) {
          // state.map.fitBounds(bbox.geometry.coordinates)
          // bbox_set = true;
          var group = L.featureGroup(
            data.map(
              (item) => (
                L.circleMarker(
                  [item.coords.geometry.coordinates[1], item.coords.geometry.coordinates[0]],
                  {
                    radius: 1
                  }
                ).bindPopup(item.name)
              )
            )
          ).addTo(state.map);
          state.map.fitBounds(group.getBounds())
        }
      }
      if (state.mounted && state.map!=undefined) {
        state.map.invalidateSize()
      }
          
    }, [state.mounted, data]
  )
  
  if (state.mounted) {
    return <div id='map' className={styles.map} style={{width: '100%', height: 231}}/>
  } else {
    return <div>Loading...</div>
  }
}

export default withSize({monitorHeight: true})(MapComponent)