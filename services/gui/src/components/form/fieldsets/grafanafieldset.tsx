import styles from './grafanafieldset.module.css'
import {withSize} from 'react-sizeme'


const GrafanaFieldset: React.FC<{
    src: string,
    legend?: string
    size: any
  }> = ({src, legend, size}) => {
  
  return <div 
    key={src.split('?')[0]}
    style={{height: '100vh'}}
  >
    <object  type="text/html"
      width={'100%'}
      height={size.height - 110}
      data={src}
    />
  </div>
}

export default withSize({monitorHeight: true})(GrafanaFieldset)