

import styles from './contact.module.css'

interface Contact {
  administrativeArea: string,
  city: string,
  country: string,
  deliveryPoint: string,
  email: string,
  fax: string,
  individualName: string,
  organizationName: string,
  postalcode: string,
  role: string,
  voice: string,
  web: string
}

const Contacts: React.FC<{
  data: Contact[],
  onChange: Function,
}> = ({data, onChange}) => {
  function capitalize (text: any) {
    return text.charAt(0).toUpperCase() + text.slice(1);
  }
  
  if (data.length > 0 ) {
    return <div className={styles.flex}>
      {
        data.map((item, index) => <div key={`key-contacts-${index}`}>
          <label className={styles.label}>{capitalize(item.role.split(':').pop())}</label>
          <input
            id={`contacts-${index}-organizationName`}
            type="text"
            className={styles.inputinline}
            placeholder="Organization name..."
            onChange={(e)=>onChange(e)}
            value={item.organizationName}
          />
          <fieldset className={styles.fieldset}>
            <legend>Details</legend>
            <label className={styles.label}>Person</label>
            <input
              id={`contacts-${index}-individualName`}
              type="text"
              className={styles.inputinline}
              onChange={(e)=>onChange(e)}
              value={item.individualName}
            />
            <label className={styles.label}>Telephone</label>
            <input
              id={`contacts-${index}-voice`}
              type="text"
              className={styles.inputinline}
              onChange={(e)=>onChange(e)}
              value={item.voice}
            />
            <label className={styles.label}>Fax</label>
            <input
              id={`contacts-${index}-fax`}
              type="text"
              className={styles.inputinline}
              onChange={(e)=>onChange(e)}
              value={item.fax}
            />
            <label className={styles.label}>E-mail</label>
            <input
              id={`contacts-${index}-email`}
              type="text"
              className={styles.inputinline}
              onChange={(e)=>onChange(e)}
              value={item.email}
            />
            <label className={styles.label}>Web</label>
            <input
              id={`contacts-${index}-web`}
              type="text"
              className={styles.inputinline}
              onChange={(e)=>onChange(e)}
              value={item.web}
            />
            <label className={styles.label}>Address</label>
            <input
              id={`contacts-${index}-deliveryPoint`}
              type="text"
              className={styles.inputinline}
              onChange={(e)=>onChange(e)}
              value={item.deliveryPoint}
            />
            <label className={styles.label}>City</label>
            <input
              id={`contacts-${index}-city`}
              type="text"
              className={styles.inputinline}
              onChange={(e)=>onChange(e)}
              value={item.city}
            />
            <label className={styles.label}>Country</label>
            <input
              id={`contacts-${index}-country`}
              type="text"
              className={styles.inputinline}
              onChange={(e)=>onChange(e)}
              value={item.country}
            />
            <label className={styles.label}>Admin area</label>
            <input
              id={`contacts-${index}-administrativeArea`}
              type="text"
              className={styles.inputinline}
              onChange={(e)=>onChange(e)}
              value={item.administrativeArea}
            />
            <label className={styles.label}>Postal code</label>
            <input
              id={`contacts-${index}-postalcode`}
              type="text"
              className={styles.inputinline}
              onChange={(e)=>onChange(e)}
              value={item.postalcode}
            />            
          </fieldset>
        </div>)
      }
    </div>
  } else {
    return <></>
  }
}

export type { Contact }
export default Contacts
/*
administrativeArea: string,
  country: string,
  postalcode: string,

*/