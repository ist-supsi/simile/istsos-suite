import styles from './fieldsets.module.css'

interface Option {
  value: string
  text: string
}

interface InputItem {
  id?: string,
  label: string
  type: string
  value: string | number
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => any
  onChangeSelect?: (e: React.ChangeEvent<HTMLSelectElement>) => any
  options?: Option[]
  hidden?: Boolean
}

interface Field {
  title: string
  inputs: InputItem[]
}

const FieldSets: React.FC<{
    fields: Field[],
  }> = ({fields}) => {
  return <>{
    fields.map(
      (item, index) => <fieldset className={styles.fieldset} key={`key-fieldset-${index}`}>
        <legend className={styles.legend}>{item.title}</legend>
        {item.inputs.map(
          (item2, index2) => <div style={item2.hidden ? {display: 'none'} : {marginBottom: '5px'}} key={`key-fom-inputs-${index}-${index2}`}>
            <label className={styles.label} key={`key-fieldset-${index}-label-${index2}`}>{item2.label}</label>
            {item2.type==='select' ? <select
              value={item2.value}
              onChange={item2.onChangeSelect}>
                {item2.options?.map(
                  (item3, index3) => <option
                    key={`key-fieldset-${index}-${index2}-options-${index3}`}
                    value={item3.value}
                  >
                    {item3.text}
                  </option>
                )}
              </select> : <input
              id={item2.id}
              key={`key-fieldset-input-${index}`}
              type={item2.type}
              value={item2.value}
              onChange={item2.onChange}
            />}
          </div>
        )}
      </fieldset>
    )
  }</>
}

export default FieldSets