import styles from './procedurefieldset.module.css'

// interface Option {
//   value: string
//   text: string
// }

// interface InputItem {
//   label: string
//   type: string
//   value: string | number
//   onChange?: (e: React.ChangeEvent<HTMLInputElement>) => any
//   onChangeSelect?: (e: React.ChangeEvent<HTMLSelectElement>) => any
//   options?: Option[]
// }

// interface Metadata {
//   name: string
//   inputs: InputItem[]
// }

const ProcedureFieldset: React.FC<{
    name: string,
    metadata: any,
    beginposition?: string,
    endposition?: string,
    onClose?: any
  }> = ({name, metadata, beginposition, endposition, onClose}) => {
  
  return <fieldset className={styles.fieldset}>
    <legend className={styles.legend}>
      {name}
      {onClose ? <span className={styles.close_btn} onClick={onClose}> &times;</span> : ''}
    </legend>
    { beginposition && endposition ? <div className={styles.divtime}>From: {beginposition} <br/> 
    To: {endposition}<hr className={styles.hr} /></div> : ''}
    {metadata.map(
      (item: any, index: number) => {
        return <div key={`label-input-${index}`}><label className={styles.label}>
          <input
            className={styles.input}
            type='checkbox'
            checked={item.checked}
            onChange={item.onCheck}
            value={item.name}
            id={item.name}
            name={item.name}
            disabled={item.disabled}
          />
            {item.name} {item.uom ? `(${item.uom})` : ''}
        </label>
        </div>
      }
    )}
  </fieldset>
}

export default ProcedureFieldset