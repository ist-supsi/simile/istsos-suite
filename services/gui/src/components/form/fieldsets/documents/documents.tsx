
import { useState } from 'react'
import styles from './documents.module.css'

interface Document {
  date?: string,
  // "date": "2021-07-16",
  definition?: string,
  // "definition": "",
  description?: string,
  // "description": "asd",
  extension?: string,
  // "extension": ".a",
  format?: string,
  // "format": "application/octet-stream",
  link?: string,
  // "link": "asd",
  name?: string,
  // "name": "",
  value?: string
  // "value": ""
}

const Documents: React.FC<{
  data: Document[],
  onChange: Function,
}> = ({data, onChange}) => {
  const [state, setData] = useState({
    date: '',
    definition: '',
    description: '',
    extension: '',
    format: '',
    link: '',
    name: '',
    value: ''
  })
  function capitalize (text: any) {
    return text.charAt(0).toUpperCase() + text.slice(1);
  }

  function _onChange(e: any) {
    setData({
      ...state,
      [e.target.id]: e.target.value
    })
  }
  
  return <div className={styles.flex}>
    <fieldset>
      <label className={styles.label}>Description</label>
      <input
        id={`description`}
        type="text"
        className={styles.inputinline}
        onChange={(e)=>_onChange(e)}
        value={state.description}
      />
      <label className={styles.label}>Format</label>
      <input
        id={`format`}
        type="text"
        className={styles.inputinline}
        onChange={(e)=>_onChange(e)}
        value={state.format}
      />
      <label className={styles.label}>Link</label>
      <input
        id={`link`}
        type="text"
        className={styles.inputinline}
        onChange={(e)=>_onChange(e)}
        value={state.link}
      />
      <label className={styles.label}>Date</label>
      <input
        id={`date`}
        type="date"
        className={styles.inputinline}
        onChange={(e)=>_onChange(e)}
        value={state.date}
      />
    </fieldset>
    <fieldset className={styles.fieldsetspan2col}>
      <div>ciao</div>
      {
      data.map((item, index) => <div>
          <label className={styles.label}>Date</label>
          <input
            id={`documentation-${index}-date`}
            type="date"
            className={styles.inputinline}
            onChange={(e)=>onChange(e)}
            value={item.date ? Date.parse(item.date) : undefined}
          />
      </div>)
      }
    </fieldset>
  </div>
}

export type { Document }
export default Documents