// components
// import SmartTable from '../table/smarttable'
import styles from './formanalyzer.module.css'

const FormAnalyzer: React.FC<{
    onSubmit: Function,
    onChange: Function,
    form: any,
    procedures: any[],
    services: any[]
  }> = ({onSubmit, onChange, form, procedures, services}) => {
  
  return <form onSubmit={(e) => e.preventDefault()}  className={styles.form}>
    <fieldset className={styles.fieldset}>
    {Object.keys(form).map(
      (item, index) => <div key={`key-${item}-${index}`}>
        <label className={styles.label}>{form[item].label}</label>
        <p className={styles.description}>{form[item].description}</p>
        {
          form[item].type === 'checkbox' ?
          <input
            type={form[item].type}
            id={item}
            checked={form[item].value}
            onChange={(e) => onChange(e)}
          /> :
          form[item].type === 'profile-ob' ?
          <div>
            <select onChange={(e) => onChange(e)} id={item}>
              <option value=''>Select a procedure...</option>
              {
                procedures
                  .filter(item => item.sensortype==='profile')
                  .map(
                    (item2, index2) => <option
                      key={`key-${item}-${item2}-${index2}`}
                      value={item2.name}
                    >
                      {item2.name}
                    </option>
                  )
              }
            </select>
            {
              form[item].value ?
              <div>
                <label className={styles.label}>Observed properties for {form[item].value}</label>
                <p></p>
                <select id={`${item}-observedproperty`} onChange={(e) => onChange(e)}>
                  <option value=''>Select observed property...</option>
                  {
                    procedures
                      .filter(item2 => item2.name===form[item].value)
                      .map(item2 => 
                        item2.observedproperties.map(
                          (item3: any, index3: number) => <option
                            key={`key-${item}-ob-${index3}`}
                          >
                            {item3.name}
                          </option>
                        )
                      )
                  }
                </select>
              </div> :
              <></>
            }
          </div> :
          form[item].type === 'text' ? <input
            type={form[item].type}
            id={item}
            value={form[item].value}
            onChange={(e) => onChange(e)}
          /> :
          form[item].type === 'number' ? <input
            type={form[item].type}
            id={item}
            value={form[item].value}
            onChange={(e) => onChange(e)}
          /> :
          form[item].type === 'procedure-ob' ?
          <div>
            <select onChange={(e) => onChange(e)} id={item}>
              <option value=''>Select a procedure...</option>
              {
                procedures
                  .filter(item => item.sensortype==='insitu-fixed-point')
                  .map(
                    (item2, index2) => <option
                      key={`key-${item}-${item2}-${index2}`}
                      value={item2.name}
                    >
                      {item2.name}
                    </option>
                  )
              }
            </select>
            {
              form[item].value ?
              <div>
                <label className={styles.label}>Observed properties for {form[item].value}</label>
                <p></p>
                <select id={`${item}-observedproperty`} onChange={(e) => onChange(e)}>
                  <option value=''>Select observed property...</option>
                  {
                    procedures
                      .filter(item2 => item2.name===form[item].value)
                      .map(item2 => 
                        item2.observedproperties.map(
                          (item3: any, index3: number) => <option
                            key={`key-${item}-ob-${index3}`}
                          >
                            {item3.name}
                          </option>
                        )
                      )
                  }
                </select>
              </div> :
              <></>
            }
          </div> :
          form[item].type === 'text' ? <input
            type={form[item].type}
            id={item}
            value={form[item].value}
            onChange={(e) => onChange(e)}
          /> :
          form[item].type === 'number' ? <input
            type={form[item].type}
            id={item}
            value={form[item].value}
            onChange={(e) => onChange(e)}
          /> :
          form[item].type === 'textarea' ? <textarea
            rows={5}
            id={item}
            value={form[item].value}
            onChange={(e) => onChange(e)}
          /> :
          form[item].type === 'service' ? <div>
            <select onChange={(e) => onChange(e)} id={item}>
              <option value=''>Select a service...</option>
              {
                services
                  .map(
                    (item2, index2) => <option
                      key={`key-${item}-${item2.service}-${index2}`}
                      value={item2.service}
                    >
                      {item2.service}
                    </option>
                  )
              }
            </select>
          </div> :
          <></>
        }
      </div>
    )}
    </fieldset>
    <button onClick={(e) => onSubmit(e)}>Submit</button>
  </form>
}

export default FormAnalyzer