// react
import {useState, useEffect} from 'react'

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons'

// components
import TableComponent from  '../table/table'

// external components
import {
  useWindowSize
} from '@react-hook/window-size'

// style
import styles from'./formmain.module.css'

const FormMain: React.FC<{
    type: string,
    data: any,
    observedProperties: any,
    uoms: any,
    epsg: any,
    procedures: any,
    onChange: any,
    onChangeCheck: any,
    onChangeCheckProfile: any,
    onClick: any,
    method: any
  }> = ({
    type,
    data,
    observedProperties,
    uoms,
    epsg,
    procedures,
    onChange,
    onChangeCheck,
    onChangeCheckProfile,
    onClick,
    method
  }) => {
  const [ state, setData ] = useState({wasmLoading: true, wasm: Object()})
  const [width, height] = useWindowSize()
  useEffect(() => {
    let promises = [import('../../wasm/profile/pkg/profile')]
    Promise.all(promises)
      .then(res => {
        setData({
          ...state,
          wasmLoading: false,
          wasm: res[0]
        })  
      })
  },[])

  function omit (obj: any, arr: any) {
    let tmp_obj = Object.keys(obj)
      .filter(k => !arr.includes(k))
      .reduce((acc: any, key) => ((acc[key] = obj[key]), acc), {})        
    return tmp_obj
  }

  let test_data = {
    ...omit(data, ['assignedid', 'message', 'error']),
    procedures: procedures.checked
  }
  
  if (state.wasmLoading) {
    return <div>Loading...</div>
  } else {
    if (type == 'insitu-fixed-point' || type == 'insitu-fixed-specimen') {
      let table_alias = {
        select: "Select",
        name: "Name",
        description: "Description",
        uom: <a onClick={()=>onClick('open', 'uom')} >Uom <FontAwesomeIcon icon={faPlusCircle}/></a>
      }

      let table_data = observedProperties.data.map(
        (item: any, index: number) => {
          let outputs_filtered = data.outputs.filter((el: any)=> el.name == item.name)
          if (outputs_filtered.length > 0) {
            return {
              select: <input id={`outputs-checkbox-${index}`} checked={true} onChange={(e)=>onChangeCheck(e, item)} type="checkbox"></input>,
              name: outputs_filtered[0].name,
              description: outputs_filtered[0].description,
              uom: <select defaultValue={outputs_filtered[0].uom} id={`outputs-select-${index}`} onChange={(e)=>onChangeCheck(e, item)}>
                {
                  uoms.data.map(
                    (item2: any, index2: number) => (
                      <option key={`key-uom-row-${index2}`} value={item2.name}>{`${item2.name} ${item2.description}`}</option>
                    )
                  )
                }
              </select>
            }
          } else {
            return {
              select: <input id={`outputs-checkbox-${index}`} checked={false} onChange={(e)=>onChangeCheck(e, item)} type="checkbox"></input>,
              name: item.name,
              description: item.description,
              uom: <select defaultValue="null" id={`outputs-select-${index}`} onChange={(e)=>onChangeCheck(e, item)}>
                {
                  uoms.data.map(
                    (item2: any, index2: number) => (
                      <option key={`key-uom-row-${index2}`} value={item2.name}>{`${item2.name} ${item2.description}`}</option>
                    )
                  )
                }
              </select>
            }
          }
        }
      )  
      return (
        <div className={styles['div-form']}>
          <form>
            <div className={styles["form-main"]}>
              <div className={styles["form-left"]}>
                <fieldset>
                  <legend>General info</legend>
                  <label>Name:</label>
                  <input id="system_id" type="text" placeholder="Name" onChange={(e)=>onChange(e)} value={data.system_id} />
                  <label>Description:</label>
                  <input id="description" type="text" placeholder="Description" onChange={(e)=>onChange(e)} value={data.description}></input>
                  <label>Keywords:</label>
                  <input id="keywords" type="text" placeholder="key1,key2,key3" onChange={(e)=>onChange(e)} value={data.keywords}></input>
                </fieldset>
                <fieldset>
                  <legend>Classification</legend>
                  <label>System type:</label>
                  <input id="systemtype" type="text" value={type} disabled></input>
                  <label>Sensor type:</label>
                  <input id="classification-1-value" type="text" placeholder="tipping-bucket gauge, probe, etc." onChange={(e)=>onChange(e)} value={data.classification[1].value} />
                </fieldset>
                <fieldset>
                  <legend>Location</legend>
                  <label>Feature of Interest (FOI) name:</label>
                  <input id="location-properties-name" type="text" placeholder="FOI name" onChange={(e)=>onChange(e)} value={data.location.properties.name}></input>
                  <label>EPSG:</label>
                  <select defaultValue={epsg.data[0]} id="location-crs-properties-name" onChange={(e)=>onChange(e)}>
                    {epsg.data.map(
                        (item: any, index: number) => <option key={`key-epsg-option-${index}`}>{item.name}</option>
                    )}
                  </select>
                  <fieldset className="flex flex-row">
                      <label>X: </label>
                    <input id="location-geometry-coordinates-0" type="number" step="any" placeholder="X coord" onChange={(e)=>onChange(e)} value={data.location.geometry.coordinates[0]} />
                    <label>Y: </label>
                    <input id="location-geometry-coordinates-1" type="number" step="any" placeholder="Y coord" onChange={(e)=>onChange(e)} value={data.location.geometry.coordinates[1]} />
                    <label>Z: </label>
                    <input id="location-geometry-coordinates-2" type="number" step="any" placeholder="Z coord" onChange={(e)=>onChange(e)} value={data.location.geometry.coordinates[2]} />
                  </fieldset>
                </fieldset>
              </div>
              <div className={styles["form-rigth"]} style={{height: '570px'}}>
                <fieldset style={{height: '566px'}}>
                  <legend>Observed properties <a onClick={()=>onClick('open', 'op')} ><FontAwesomeIcon icon={faPlusCircle}/></a></legend>
                  <TableComponent key={"t-proc"} init_data={{alias: table_alias, data: table_data}} height={566-50} pagination={false} filter={true} />
                </fieldset>
                <fieldset>
                  <legend>Optional parameters</legend>
                </fieldset>
              </div>
            </div>
          </form>
          <div>{data.error}</div>
          <div>{data.message}</div>
          {method == "POST" ? <input type="button" onClick={() => onClick("add", "proc")} value={"Submit"} /> : <input type="button" onClick={() => onClick("put", "proc")} value={"Submit"} />}
        </div>
        ) 
      } else if (type == 'profile') {
          let table_alias = {
            select: "Select",
            name: "Name",
            observedproperties: "Observed properties",
            offering: "Offering"
          }
          
          let table_data = procedures.data.features.map(
            (item: any, index: number) => ({
              select: <input type="checkbox" onChange={(e)=>onChangeCheckProfile(e, item)} checked={procedures.checked.indexOf(item.properties.name) > -1}/>,
              name: item.properties.name,
              observedproperties: item.properties.observedproperties.map((item: any) => item.name).join(', '),
              offering: item.properties.offerings.map((item: any) => item).join(', ')
            })
          )
          return(
            <div >
              <form>
                <div className={styles["form-main"]}>
                  <div className={styles["form-left"]}>
                    <fieldset>
                      <legend>General info</legend>
                      <label>Name:</label>
                      <input id="system_id" type="text" placeholder="Name" onChange={(e)=>onChange(e)} value={data.system_id} />
                      <label>Description:</label>
                      <input id="description" type="text" placeholder="Description" onChange={(e)=>onChange(e)} value={data.description}></input>
                      <label>Keywords:</label>
                      <input id="keywords" type="text" placeholder="key1,key2,key3" onChange={(e)=>onChange(e)} value={data.keywords}></input>
                    </fieldset>
                    <fieldset>
                      <legend>Classification</legend>
                      <label>System type:</label>
                      <input id="systemtype" type="text" value={type} disabled></input>
                      <label>Sensor type:</label>
                      <input id="classification-1-value" type="text" placeholder="tipping-bucket gauge, probe, etc." onChange={(e)=>onChange(e)} value={data.classification[1].value} />
                    </fieldset>
                    <fieldset>
                      <legend>Location</legend>
                      <label>Feature of Interest (FOI) name:</label>
                      <input id="location-properties-name" type="text" placeholder="FOI name" onChange={(e)=>onChange(e)} value={data.location.properties.name}></input>
                      <label>EPSG:</label>
                      <select defaultValue={epsg.data[0]} id="location-crs-properties-name" onChange={(e)=>onChange(e)}>
                        {epsg.data.map(
                          (item: any, index: number) => <option key={`key-epsg-option-${index}`}>{item.name}</option>
                        )}
                      </select>
                      <fieldset className="flex flex-row">
                        <label>X: </label>
                        <input id="location-geometry-coordinates-0" type="number" step="any" placeholder="X coord" onChange={(e)=>onChange(e)} value={data.location.geometry.coordinates[0]} />
                        <label>Y: </label>
                        <input id="location-geometry-coordinates-1" type="number" step="any" placeholder="Y coord" onChange={(e)=>onChange(e)} value={data.location.geometry.coordinates[1]} />
                        <label>Z: </label>
                        <input id="location-geometry-coordinates-2" type="number" step="any" placeholder="Z coord" onChange={(e)=>onChange(e)} value={data.location.geometry.coordinates[2]} />
                      </fieldset>
                    </fieldset>
                  </div>
                  <div className={styles["form-rigth"]}>
                    <fieldset className="flex flex-row">
                      <legend>Procedure</legend>
                      <TableComponent key="t-profile" init_data={{alias: table_alias, data: table_data}} height={570} pagination={false} filter={true} />
                    </fieldset>
                  </div>
                </div>
              </form>
              <input type="button" onClick={() => onClick(test_data)} value={"Submit"} />
            </div>
          )
      }else {
          return(
              <div>ciao</div>
          )
      }
  }
}

export default FormMain