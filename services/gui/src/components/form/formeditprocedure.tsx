// components
import SmartTable from '../table/smarttable'
import Contacts, {Contact} from './fieldsets/contact/contact'
import Constraint from './constraint/constraint'
import Documents, {Document} from './fieldsets/documents/documents'
import styles from './formprocedure.module.css'
import {useState, useEffect, useRef} from 'react'

interface item {
  name: string,
  definition: string,
  value: string,
  description?: string,
}

interface output {
  name: string,
  definition: string,
  uom: string,
  description: string,
  constraint: {}
}

interface location {
  type: string,
  geometry: {
    type: string,
    coordinates: number[]
  },
  crs: {
    type: string,
    properties: {
        name: string
    }
  },
  properties: {
    name: string
  }
}

interface contact {
  administrativeArea: string,
  city: string,
  country: string,
  deliveryPoint: string,
  email: string,
  fax: string,
  individualName: string,
  organizationName: string,
  postalcode: string,
  role: string,
  voice: string,
  web: string
}

interface form {
  assignedSensorId: string,
  system_id: string,
  system: string,
  description: string,
  keywords: string,
  identification: item[],
  classification: item[],
  characteristics: string,
  contacts: contact[],
  documentation: any[],
  capabilities: any[],
  location: location,
  interfaces: string,
  inputs: any[],
  outputs: output[],
  mqtt: string,
  history?: any[],
  procedures?: any[]
}

const FormEditProcedure: React.FC<{
    onSubmitFunc: Function,
    allowedepsg: string,
    defaultepsg: string,
    systemtype: string,
    form: form
  }> = ({
    onSubmitFunc, allowedepsg,
    defaultepsg, systemtype, form
  }) => {
  const [state, setData] = useState({
    contacts: false,
    documents: false,
    interfaces: false,
    identification: false,
    characteristics: false,
    capabilities: false,
    mqtt: false,
    table_data: [],
    table_alias: {},
    form: {
      "system_id": "",
      "assignedSensorId": "",
      "system": "",
      "description": "",
      "keywords": "",
      "identification": [
          {
              "name":"uniqueID",
              "definition":"urn:ogc:def:identifier:OGC:uniqueID",
              "value":"urn:ogc:def:procedure:x-istsos:1.0:"
          }
      ],
      "classification": [
          {
              "name": "System Type",
              "definition": "urn:ogc:def:classifier:x-istsos:1.0:systemType",
              "value": systemtype
          },
          {
              "name": "Sensor Type",
              "definition": "urn:ogc:def:classifier:x-istsos:1.0:sensorType",
              "value": ""
          }
      ],
      "characteristics": "",
      "contacts": [
        {
          "administrativeArea": '',
          "city": '',
          "country": '',
          "deliveryPoint": '',
          "email": '',
          "fax": '',
          "individualName": '',
          "organizationName": '',
          "postalcode": '',
          "role": "urn:x-ogc:def:classifiers:x-istsos:1.0:contactType:owner",
          "voice": '',
          "web": ''
        }, {
          "administrativeArea": '',
          "city": '',
          "country": '',
          "deliveryPoint": '',
          "email": '',
          "fax": '',
          "individualName": '',
          "organizationName": '',
          "postalcode": '',
          "role": "urn:x-ogc:def:classifiers:x-istsos:1.0:contactType:manufacturer",
          "voice": '',
          "web": ''
        }, {
          "administrativeArea": '',
          "city": '',
          "country": '',
          "deliveryPoint": '',
          "email": '',
          "fax": '',
          "individualName": '',
          "organizationName": '',
          "postalcode": '',
          "role": "urn:x-ogc:def:classifiers:x-istsos:1.0:contactType:operator",
          "voice": '',
          "web": ''
        }
      ],
      "documentation": Array(),
      "capabilities": Array(),
      "location": {
          "type": "Feature",
          "geometry": {
              "type": "Point",
              "coordinates": [
                  0, 0, 0
              ]
          },
          "crs": {
              "type": "name",
              "properties": {
                  "name": ""
              }
          },
          "properties": {
              "name": ""
          }
      },
      "interfaces": "",
      "inputs": Array(),
      "outputs": [
          {
              "name": "Time",
              "definition": "urn:ogc:def:parameter:x-istsos:1.0:time:iso8601",
              "uom": "iso8601",
              "description": "",
              "constraint": {}
          }
      ],
      "mqtt": "",
      "procedures": Array()
    },
    isLoading: true
  })
  useEffect(
    () => {
      let table_alias;
      switch (systemtype) {
        case 'virtual-profile':
          table_alias = {
            name: "Name",
            description: "Description",
            observedproperties: 'Observed properties'
          }
          setData({
            ...state,
            table_alias: table_alias,
            form: {
              ...state.form,
              ...form,
              contacts: state.form.contacts.map(
                (item) => {
                  let filtered_contacts = form.contacts.filter(item2 => item2.role==item.role)
                  if (filtered_contacts.length) {
                    return {...filtered_contacts[0]}
                  } else {
                    return item
                  }                    
                }
              ),
            },
            isLoading: false
          })
          break;
        case 'insitu-mobile-point':
        case 'virtual':
        case 'insitu-fixed-specimen':
        case 'profile':
        case 'insitu-fixed-point':
          table_alias = {
            name: "Name",
            description: "Description",
            definition: "Definition",
            uom: "Uom",
            constraint: "Constraint",
            // from: 'From',
            // to: 'To',
            // list: "List (comma separeted values)"
          }
          setData({
            ...state,
            table_alias: table_alias,
            form: {
              ...state.form,
              ...form,
              contacts: state.form.contacts.map(
                (item) => {
                  let filtered_contacts = form.contacts.filter(item2 => item2.role==item.role)
                  if (filtered_contacts.length) {
                    return {...filtered_contacts[0]}
                  } else {
                    return item
                  }                    
                }
              ),
            },
            isLoading: false
          })
          break;
      
        default:
          break;
      }
    }
  , [form])

  function _onChange(el: any) {
    if (el.target.id.includes("contacts")) {
      let target_id_splitted = el.target.id.split('-')
      let id_target: number = target_id_splitted[1]

      let newArray = [...state.form.contacts]
      newArray[id_target] = {
        ...state.form.contacts[id_target],
        [target_id_splitted.pop()]: el.target.value.toString()
      }
      setData({
        ...state,
        form: {
          ...state.form,
          contacts: newArray
        }
      })
        
    } else if (el.target.id.includes("classification")) {
      let newArray = [...state.form.classification]
      newArray[1] = {
        ...state.form.classification[1],
        value: el.target.value.toString()
      }
      setData({
        ...state,
        form: {
          ...state.form,
          classification: newArray
        }
      })
        
    } else if (el.target.id.includes("location-properties")) {
      setData({
        ...state,
        form: {
          ...state.form,
          location: {
            ...state.form.location,
            properties: {
              ...state.form.location.properties,
              name: el.target.value
            }
          }
        }
      })
        
    } else if (el.target.id.includes("location-crs")) {
        
      setData({
        ...state,
        form: {
          ...state.form,
          location: {
            ...state.form.location,
            crs: {
              ...state.form.location.crs,
              properties: {
                ...state.form.location.crs.properties,
                name: el.target.value.toString()
              }
            }
          }
        }
      })
        
    } else if (el.target.id.includes("location-geometry")) {
      let id_splitted = el.target.id.split("-")
      let newArray = [...state.form.location.geometry.coordinates]
      newArray[parseInt(id_splitted[3])] = parseFloat(el.target.value)
      setData({
        ...state,
        form: {
          ...state.form,
          location: {
            ...state.form.location,
            geometry: {
              ...state.form.location.geometry,
              coordinates: newArray
            }
          }
        }
      })
        
    } else if (el.target.id == "system_id") {
      let newArray = [...state.form.identification]
      newArray[0] = {
        ...newArray[0],
        value: `${el.target.value}`
      }
      setData({
        ...state,
        form: {
          ...state.form,
          system_id: el.target.value,
          system: el.target.value,
          identification: newArray
        }
      })
    } else {
      setData({
        ...state,
        form: {
          ...state.form,
          [el.target.id]: el.target.value
        }
      })
    }
  }

  let table_data = []
  let table_alias = {}

  if (systemtype=='virtual-profile') {
    table_data = state.form.procedures
    table_alias = {

    }
  } else {
    table_data = state.form.outputs.map(
      (item: any) => {
        return {
          name: item.name,
          description: item.description,
          definition: item.definition,
          uom: item.uom,
          from: item.constraint ? item.constraint['interval'] ? item.constraint['interval'][0] : item.constraint['min'] ? item.constraint['min'] : '' : '',
          to: item.constraint ? item.constraint['interval'] ? item.constraint['interval'][1] : item.constraint['max'] ? item.constraint['max'] : '' : '',
          list: item.constraint ? item.constraint['valueList'] ? item.constraint['valueList'] : '' : ''
        }
      }
    )
    table_alias = {
      name: "Name",
      description: "Description",
      definition: "Definition",
      uom: "Uom"
    }
  }
  let data_contacts: Contact[] = [...state.form.contacts]
  let data_documents: Document[] = [...state.form.documentation]
  // defaultepsg
  let selected_epsg = defaultepsg
  if (state.form.location.crs.properties.name) {
    let epsg = state.form.location.crs.properties.name.split(':')
    if (epsg.length==2) {
      selected_epsg = epsg[1]
    }
  }
  console.log(selected_epsg);
  
  return <form className={styles.form}>
      <div className={styles.grid}>
        <fieldset className={styles.fieldset_33}>
          <legend className={styles.legend}>General info</legend>
          <label className={styles.label}>Name:</label>
          <input
            id="system_id"
            type="text"
            placeholder="Name"
            onChange={(e)=>_onChange(e)}
            value={state.form.system_id}
          />
          <label className={styles.label}>Description:</label>
          <input
            id="description"
            type="text"
            placeholder="Description"
            onChange={(e)=>_onChange(e)}
            value={state.form.description}
          />
          <label className={styles.label}>Keywords:</label>
          <input
            id="keywords"
            type="text"
            placeholder="key1,key2,key3"
            onChange={(e)=>_onChange(e)}
            value={state.form.keywords} 
          />
        </fieldset>
        <fieldset className={styles.fieldset_33}>
          <legend className={styles.legend}>Classification</legend>
          <label className={styles.label}>System type:</label>
          <input
            id="classification-0-value"
            type="text"
            value={state.form.classification[0].value}
            disabled
          />
          <label className={styles.label}>Sensor type:</label>
          <input
            id="classification-1-value"
            type="text"
            placeholder="tipping-bucket gauge, probe, etc."
            onChange={(e)=>_onChange(e)}
            value={state.form.classification[1].value}
          />
        </fieldset>
        <fieldset className={styles.fieldset_33}>
          <legend className={styles.legend}>Location</legend>
          <label className={styles.label}>Feature of Interest (FOI) name:</label>
          <input
            id="location-properties-name"
            type="text"
            placeholder="FOI name"
            onChange={(e)=>_onChange(e)}
            value={state.form.location.properties.name}
          />
          <div className={`${styles.fieldset}`} style={{marginTop: 10}} >
            <label className={styles.label}>EPSG:</label>
            <select defaultValue={selected_epsg} id="location-crs-properties-name" onChange={(e)=>_onChange(e)}>
              <option key={`key-epsg-option-00`}>{defaultepsg}</option>
                {allowedepsg?.split(',').map(
                  (item, index) => <option key={`key-epsg-option-${index}`}>{item}</option>
                )}
            </select>
            <div className={styles.flex}>
              <label className={styles.label}>X: </label>
              <input
                id="location-geometry-coordinates-0"
                type="number"
                step="any"
                placeholder="X coord"
                onChange={(e)=>_onChange(e)}
                value={state.form.location.geometry.coordinates[0]}
              />
              <label className={styles.label}>Y: </label>
              <input
                id="location-geometry-coordinates-1"
                type="number"
                step="any"
                placeholder="Y coord"
                onChange={(e)=>_onChange(e)}
                value={state.form.location.geometry.coordinates[1]}
              />
              <label className={styles.label}>Z: </label>
              <input
                id="location-geometry-coordinates-2"
                type="number"
                step="any"
                placeholder="Z coord"
                onChange={(e)=>_onChange(e)}
                value={state.form.location.geometry.coordinates[2]}
              />
            </div>
          </div>
        </fieldset>
      </div>
      <div>
        <div>
          {
            systemtype == 'virtual-profile' ?
            <fieldset className={styles.fieldset}>
              <legend className={styles.legend}>Procedures</legend>
              <SmartTable
                id='profile'
                height={100}
                key="key-t-profile-proc"
                init_data={
                  {
                    alias: table_alias,
                    data: table_data
                  }
                }
                pagination={false}
                filter={true}
              />
            </fieldset> : 
            <fieldset className={styles.fieldset}>
              <legend className={styles.legend}>Observed properties</legend>
              <SmartTable
                id='observedproperty'
                key="key-t-insitu-obs"
                init_data={
                  {
                    alias: table_alias,
                    data: table_data
                  }
                }
                pagination={false}
                filter={true}
              />
            </fieldset>
          }
        </div>
    </div>
    <fieldset>
      <legend>
        <a onClick={()=>setData({...state, contacts: !state.contacts})} >
          Contacts {state.contacts ? '➖' : '➕'}
        </a>
      </legend>
      {
        state.contacts ? <Contacts
          data={data_contacts} 
          onChange={(e: any)=>_onChange(e)}
        /> : <></>
      }
    </fieldset>
    <fieldset>
      <legend>
        <a onClick={()=>setData({...state, documents: !state.documents})} >
          Documents {state.documents ? '➖' : '➕'}
        </a>
      </legend>
      {
        state.documents ? <Documents
          data={data_documents} 
          onChange={(e: any)=>_onChange(e)}
        /> : <></>
      }
    </fieldset>
    <fieldset>
      <legend>
        <a onClick={()=>setData({...state, interfaces: !state.interfaces})} >
          Interfaces {state.interfaces ? '➖' : '➕'}
        </a>
      </legend>
      {
        state.interfaces ? 
        <div>TBD</div> : <></>
      }
    </fieldset>
    <fieldset>
      <legend>
        <a onClick={()=>setData({...state, identification: !state.identification})} >
          Identification {state.identification ? '➖' : '➕'}
        </a>
      </legend>
      {
        state.identification ? 
        <div>TBD</div> : <></>
      }
    </fieldset>
    <fieldset>
      <legend>
        <a onClick={()=>setData({...state, characteristics: !state.characteristics})} >
        Characteristics {state.characteristics ? '➖' : '➕'}
        </a>
      </legend>
      {
        state.characteristics ? 
        <div>TBD</div> : <></>
      }
    </fieldset>
    <fieldset>
      <legend>
        <a onClick={()=>setData({...state, capabilities: !state.capabilities})} >
        Capabilities {state.capabilities ? '➖' : '➕'}
        </a>
      </legend>
      {
        state.capabilities ? 
        <div>TBD</div> : <></>
      }
    </fieldset>
    <div
      className={styles.footer}
      onClick={
        systemtype == 'virtual-profile' ?
        ()=>onSubmitFunc(state.form) :
        ()=>onSubmitFunc(state.form)
      }
    >
      SUBMIT
    </div>
  </form>
}

export default FormEditProcedure