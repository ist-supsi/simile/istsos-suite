// components
import SmartTable from '../table/smarttable'
import Contacts, {Contact} from './fieldsets/contact/contact'
import Constraint from './constraint/constraint'
import Documents, {Document} from './fieldsets/documents/documents'
import styles from './formprocedure.module.css'
import {useState, useEffect, useRef} from 'react'
import difference from '@turf/difference'


interface item {
  name: string,
  definition: string,
  value: string,
  description?: string,
}

interface output {
  name: string,
  definition: string,
  uom: string,
  description: string,
  constraint: {}
}

interface location {
  type: string,
  geometry: {
    type: string,
    coordinates: [
      number, number, number
    ]
  },
  crs: {
    type: string,
    properties: {
        name: string
    }
  },
  properties: {
    name: string
  }
}

interface form {
  system_id: string,
  system: string,
  description: string,
  keywords: string,
  identification: [
      item
  ],
  classification: [
      item,
      item
  ],
  characteristics: string,
  contacts: [],
  documentation: [],
  capabilities: [],
  location: location,
  interfaces: string,
  inputs: [],
  outputs: [
    output
  ],
  mqtt: string,
  history?: [],
  procedures?: []
}

const FormProcedure: React.FC<{
    onSubmitFunc: Function,
    allowedepsg: string,
    defaultepsg: string,
    systemtype: string,
    table_data: any[],
    uoms?: item[],
    edit: boolean,
    form?: form
  }> = ({onSubmitFunc, allowedepsg, defaultepsg, systemtype, table_data, uoms, edit, form}) => {
  const [state_uoms, setUoms] = useState({
    "name": "Time",
    "definition": "urn:ogc:def:parameter:x-istsos:1.0:time:iso8601",
    "uom": "iso8601",
    "description": "",
    "constraint": {}
  })
  const [state, setData] = useState({
    contacts: false,
    documents: false,
    interfaces: false,
    identification: false,
    characteristics: false,
    capabilities: false,
    mqtt: false,
    table_data: Array(),
    table_alias: Object(),
    form: {
      "system_id": "",
      "assignedSensorId": "",
      "system": "",
      "description": "",
      "keywords": "",
      "identification": [
          {
              "name":"uniqueID",
              "definition":"urn:ogc:def:identifier:OGC:uniqueID",
              "value":"urn:ogc:def:procedure:x-istsos:1.0:"
          }
      ],
      "classification": [
          {
              "name": "System Type",
              "definition": "urn:ogc:def:classifier:x-istsos:1.0:systemType",
              "value": systemtype
          },
          {
              "name": "Sensor Type",
              "definition": "urn:ogc:def:classifier:x-istsos:1.0:sensorType",
              "value": ""
          }
      ],
      "characteristics": "",
      "contacts": [{
        "administrativeArea": '',
        "city": '',
        "country": '',
        "deliveryPoint": '',
        "email": '',
        "fax": '',
        "individualName": '',
        "organizationName": '',
        "postalcode": '',
        "role": "urn:x-ogc:def:classifiers:x-istsos:1.0:contactType:owner",
        "voice": '',
        "web": ''
      }, {
        "administrativeArea": '',
        "city": '',
        "country": '',
        "deliveryPoint": '',
        "email": '',
        "fax": '',
        "individualName": '',
        "organizationName": '',
        "postalcode": '',
        "role": "urn:x-ogc:def:classifiers:x-istsos:1.0:contactType:manufacturer",
        "voice": '',
        "web": ''
      }, {
        "administrativeArea": '',
        "city": '',
        "country": '',
        "deliveryPoint": '',
        "email": '',
        "fax": '',
        "individualName": '',
        "organizationName": '',
        "postalcode": '',
        "role": "urn:x-ogc:def:classifiers:x-istsos:1.0:contactType:operator",
        "voice": '',
        "web": ''
      }],
      "documentation": [],
      "capabilities": [],
      "location": {
          "type": "Feature",
          "geometry": {
              "type": "Point",
              "coordinates": [
                  0, 0, 0
              ]
          },
          "crs": {
              "type": "name",
              "properties": {
                  "name": ""
              }
          },
          "properties": {
              "name": ""
          }
      },
      "interfaces": "",
      "inputs": [],
      "outputs": [
          {
              "name": "Time",
              "definition": "urn:ogc:def:parameter:x-istsos:1.0:time:iso8601",
              "uom": "iso8601",
              "description": "",
              "constraint": {}
          }
      ],
      "mqtt": "",
      "history": [],
      "error": "",
      "message": "",
      "procedures": Array()
  },
  default: {
      "system_id": "",
      "assignedSensorId": "",
      "system": "",
      "description": "",
      "keywords": "",
      "identification": [
          {
              "name":"uniqueID",
              "definition":"urn:ogc:def:identifier:OGC:uniqueID",
              "value":"urn:ogc:def:procedure:x-istsos:1.0:"
          }
      ],
      "classification": [
          {
              "name": "System Type",
              "definition": "urn:ogc:def:classifier:x-istsos:1.0:systemType",
              "value": systemtype
          },
          {
              "name": "Sensor Type",
              "definition": "urn:ogc:def:classifier:x-istsos:1.0:sensorType",
              "value": ""
          }
      ],
      "characteristics": "",
      "contacts": [
        {
          "administrativeArea": '',
          "city": '',
          "country": '',
          "deliveryPoint": '',
          "email": '',
          "fax": '',
          "individualName": '',
          "organizationName": '',
          "postalcode": '',
          "role": "urn:x-ogc:def:classifiers:x-istsos:1.0:contactType:owner",
          "voice": '',
          "web": ''
        }, {
          "administrativeArea": '',
          "city": '',
          "country": '',
          "deliveryPoint": '',
          "email": '',
          "fax": '',
          "individualName": '',
          "organizationName": '',
          "postalcode": '',
          "role": "urn:x-ogc:def:classifiers:x-istsos:1.0:contactType:manufacturer",
          "voice": '',
          "web": ''
        }, {
          "administrativeArea": '',
          "city": '',
          "country": '',
          "deliveryPoint": '',
          "email": '',
          "fax": '',
          "individualName": '',
          "organizationName": '',
          "postalcode": '',
          "role": "urn:x-ogc:def:classifiers:x-istsos:1.0:contactType:operator",
          "voice": '',
          "web": ''
        }
      ],
      "documentation": [],
      "capabilities": [],
      "location": {
          "type": "Feature",
          "geometry": {
              "type": "Point",
              "coordinates": [
                  0, 0, 0
              ]
          },
          "crs": {
              "type": "name",
              "properties": {
                  "name": ""
              }
          },
          "properties": {
              "name": ""
          }
      },
      "interfaces": "",
      "inputs": [],
      "outputs": [
          {
              "name": "Time",
              "definition": "urn:ogc:def:parameter:x-istsos:1.0:time:iso8601",
              "uom": "iso8601",
              "description": "",
              "constraint": {}
          }
      ],
      "mqtt": "",
      "history": [],
      "error": "",
      "message": "",
      "procedures": []
    },
    isLoading: true
  })
  const outputs = table_data.map(item => useRef<HTMLSelectElement>(null));
  const froms = table_data.map(item => useRef<HTMLInputElement>(null));
  const tos = table_data.map(item => useRef<HTMLInputElement>(null));
  const lists = table_data.map(item => useRef<HTMLInputElement>(null));
  useEffect(
    () => {
      if (state.isLoading) {
        let table_alias;
        switch (systemtype) {
          case 'virtual-profile':
            table_alias = {
              name: "Name",
              description: "Description",
              observedproperties: 'Observed properties'
            }
            table_data = table_data.map(
              (item, index) => {
                return {
                  name: item.name,
                  description: item.description,
                  observedproperties: item.observedproperties.map((item: any)=>item.name).join()
                }
              }
            )
            setData({
              ...state,
              table_data: table_data,
              table_alias: table_alias,
              form: {...state.default},
              isLoading: false
            })
            break;
          case 'insitu-mobile-point':
          case 'virtual':
          case 'insitu-fixed-specimen':
          case 'profile':
          case 'insitu-fixed-point':
            table_alias = {
              name: "Name",
              description: "Description",
              definition: "Definition",
              uom: "Uom",
              constraint: "Constraint",
              // from: 'From',
              // to: 'To',
              // list: "List (comma separeted values)"
            }
        
            table_data = table_data.map(
              (item, index) => {
                let op_selected = state.form.outputs.filter(item2=>item2.name==item.name);
                return {
                  name: item.name,
                  description: item.description,
                  definition: item.definition,
                  uom: <select ref={outputs[index]} defaultValue={
                    op_selected.length > 0 ? op_selected[0].uom : "null"} id={`${item.name}$outputs-select-${index}`} onChange={(e)=>_onChangeOption(e, item)}>
                    {
                      uoms?.map(
                        (item2, index2) => (
                          <option key={`key-uom-row-${index2}`} value={item2.name}>{`${item2.name} ${item2.description}`}</option>
                        )
                      )
                    }
                  </select>,
                  constraint: <Constraint onChange={(e: any)=>_onChangeConstraint(e, item)}>
                    <input
                      onChange={(e)=>_onChangeConstraintValue(e, item)}
                      id={`${item.name}$constraints-from-${index}`}
                      type='number'
                      style={{width:'100%'}}
                      ref={froms[index]}
                    />
                    <input
                      onChange={(e)=>_onChangeConstraintValue(e, item)}
                      id={`${item.name}$constraints-to-${index}`}
                      type='number'
                      style={{width:'100%'}}
                      ref={tos[index]}
                    />
                    <input
                      onChange={(e)=>_onChangeConstraintValue(e, item)}
                      id={`${item.name}$constraints-list-${index}`}
                      type='text'
                      style={{width:'100%'}}
                      ref={lists[index]}
                    />
                    <input
                      onChange={(e)=>_onChangeConstraintValue(e, item)}
                      id={`${item.name}$constraints-bt-from-${index}`}
                      type='number'
                      style={{width:'100%'}}
                      ref={froms[index]}
                    />
                    <input
                      onChange={(e)=>_onChangeConstraintValue(e, item)}
                      id={`${item.name}$constraints-bt-to-${index}`}
                      type='number'
                      style={{width:'100%'}}
                      ref={tos[index]}
                    />
                  </Constraint>,
                }
              }
            )
            setData({
              ...state,
              table_data: table_data,
              table_alias: table_alias,
              form: {...state.default},
              isLoading: false
            })
            break;
        
          default:
            break;
        }
      } else {
        let newOutputs = [...state.form.outputs.map(item => {
          if (item.name===state_uoms.name) {
            return {
              ...item,
              ...state_uoms
            }
          } else {
            return item
          }
        })]
        setData({
          ...state,
          form: {
            ...state.form,
            outputs: [...newOutputs],
            classification: [
              {
                  "name": "System Type",
                  "definition": "urn:ogc:def:classifier:x-istsos:1.0:systemType",
                  "value": systemtype
              },
              state.form.classification[1]
            ]
          }
        })
      }
    }
  , [systemtype, state_uoms])

  function _onChangeConstraintValue(el: any, item: any) {
    const [name, constraint] = el.target.id.split('$')

    let ob_names = table_data.map(
      item2 => item2.name
    )
    let idx_name = ob_names.indexOf(name)
    console.log(constraint);
    
    if (constraint.includes('constraints-from')) {
      setUoms({
        ...state_uoms,
        ...item,
        constraint: {
          ...item.constraint,
          role: "urn:ogc:def:classifiers:x-istsos:1.0:qualityIndex:check:reasonable",
          min: `${el.target.value}`
        }
      })
    } else if (constraint.includes('constraints-to')) {
      setUoms({
        ...state_uoms,
        ...item,
        constraint: {
          ...item.constraint,
          role: "urn:ogc:def:classifiers:x-istsos:1.0:qualityIndex:check:reasonable",
          max: `${el.target.value}`
        }
      })
    } else if (constraint.includes('bt')) {
      if (constraint.includes('constraints-bt-from')) {
        setUoms({
          ...state_uoms,
          ...item,
          constraint: {
            ...item.constraint,
            role: "urn:ogc:def:classifiers:x-istsos:1.0:qualityIndex:check:reasonable",
            interval: [...[`${el.target.value}`], `${tos[idx_name]?.current?.value}`]
          }
        })
      } else {
        setUoms({
          ...state_uoms,
          ...item,
          constraint: {
            ...item.constraint,
            role: "urn:ogc:def:classifiers:x-istsos:1.0:qualityIndex:check:reasonable",
            interval: [...[`${froms[idx_name]?.current?.value}`, `${el.target.value}`]]
          }
        })
      }
      
    } else if (constraint.includes('valueList')) {
      try {
        setUoms({
          ...state_uoms,
          ...item,
          constraint: {
            ...item.constraint,
            role: "urn:ogc:def:classifiers:x-istsos:1.0:qualityIndex:check:reasonable",
            valueList: el.target.value.split(',')
          }
        })  
      } catch (error) {
        alert("Please insert comma separeted values.")
      }
      
    }
    
  }

  function _onChangeConstraint(el: any, item: any) {    
    if (el==='') {
      setUoms({
        ...item,
        constraint: {}
      })
    } else if (el==='gt') {
      if (Object.keys(item).includes('name')) {
        let o;
        try {
          o = froms?.filter(
            item2 => item2?.current?.id.split("$")[0] === item.name
          )[0]?.current?.value
        } catch (error) {
          o = ''
        }        
        setUoms({
          ...item,
          constraint: {
            max: `${o}`,
            role: "urn:ogc:def:classifiers:x-istsos:1.0:qualityIndex:check:reasonable"
          }
        })
      }
    } else if (el==='lt') {
      if (Object.keys(item).includes('name')) {
        let o;
        try {
          o = tos.filter(
            item2 => item2?.current?.id.split("$")[0] === item.name
          )[0]?.current?.value
        } catch (error) {
          o = ''
        }
        setUoms({
          ...item,
          constraint: {
            min: `${o}`,
            role: "urn:ogc:def:classifiers:x-istsos:1.0:qualityIndex:check:reasonable"
          }
        })
      }
    } else if (el==='ls') {
      if (Object.keys(item).includes('name')) {
        let o;
        try {
          o = lists.filter(
            item2 => item2?.current?.id.split("$")[0] === item.name
          )[0]?.current?.value
        } catch (error) {
          o = ''
        }
        try {
          let o_splitted = o?.split(',')
          setUoms({
            ...item,
            constraint: {
              valueList: o_splitted,
              role: "urn:ogc:def:classifiers:x-istsos:1.0:qualityIndex:check:reasonable"
            }
          })
        } catch (error) {
          alert("Please insert comma separeted values.")
        }
      }
    } else if (el==='bt') {
      if (Object.keys(item).includes('name')) {
        let o;
        let i;
        try {
          o = froms.filter(
            item2 => item2?.current?.id.split("$")[0] === item.name
          )[0]?.current?.value
          i = tos.filter(
            item2 => item2?.current?.id.split("$")[0] === item.name
          )[0]?.current?.value  
        } catch (error) {
          o = ''
          i = ''
        }
        
        setUoms({
          ...item,
          constraint: {
            interval: [`${o}`, `${i}`],
            role: "urn:ogc:def:classifiers:x-istsos:1.0:qualityIndex:check:reasonable"
          }
        })
      }
    }
  }
  
  function _onChangeOption(el: any, item: any) {
    setUoms({
      ...item,
      uom: el.target.value
    })
  }

  function _onChange(el: any) {
    if (el.target.id.includes("contacts")) {
      let target_id_splitted = el.target.id.split('-')
      let id_target: number = target_id_splitted[1]

      let newArray = [...state.form.contacts]
      newArray[id_target] = {
        ...state.form.contacts[id_target],
        [target_id_splitted.pop()]: el.target.value.toString()
      }
      setData({
        ...state,
        form: {
          ...state.form,
          contacts: newArray
        }
      })
        
    } else if (el.target.id.includes("classification")) {
      let newArray = [...state.form.classification]
      newArray[1] = {
        ...state.form.classification[1],
        value: el.target.value.toString()
      }
      setData({
        ...state,
        form: {
          ...state.form,
          classification: newArray
        }
      })
        
    } else if (el.target.id.includes("location-properties")) {
      setData({
        ...state,
        form: {
          ...state.form,
          location: {
            ...state.form.location,
            properties: {
              ...state.form.location.properties,
              name: el.target.value
            }
          }
        }
      })
        
    } else if (el.target.id.includes("location-crs")) {
        
      setData({
        ...state,
        form: {
          ...state.form,
          location: {
            ...state.form.location,
            crs: {
              ...state.form.location.crs,
              properties: {
                ...state.form.location.crs.properties,
                name: el.target.value.toString()
              }
            }
          }
        }
      })
        
    } else if (el.target.id.includes("location-geometry")) {
      let id_splitted = el.target.id.split("-")
      let newArray = [...state.form.location.geometry.coordinates]
      newArray[parseInt(id_splitted[3])] = parseFloat(el.target.value)
      setData({
        ...state,
        form: {
          ...state.form,
          location: {
            ...state.form.location,
            geometry: {
              ...state.form.location.geometry,
              coordinates: newArray
            }
          }
        }
      })
        
    } else if (el.target.id == "system_id") {
      let newArray = [...state.form.identification]
      newArray[0] = {
        ...newArray[0],
        value: `${el.target.value}`
      }
      setData({
        ...state,
        form: {
          ...state.form,
          system_id: el.target.value,
          system: el.target.value,
          identification: newArray
        }
      })
    } else {
      setData({
        ...state,
        form: {
          ...state.form,
          [el.target.id]: el.target.value
        }
      })
    }
  }

  function _onRowSelectProfile(el: any) {
    if (el.length>0) {
      let el_obs = el.map(
        (item: any) => item.original.observedproperties.split(',')
      )
      let obs_tmp = []
      let checked = true;
      let diff=[];
      for (let index = 0; index < el_obs.length; index++) {
        const obs_tmp_tmp = el_obs[index];
        if (obs_tmp.length===0) {
          obs_tmp = obs_tmp_tmp
        } else {
          diff = obs_tmp.filter(
            (item: any, index: number) => obs_tmp_tmp.indexOf(item) === index
          )
          if (diff.length!==obs_tmp.length) {
            checked = false
          }
        }
      }
      if (!checked) {
        alert('Can\'t add the procedure because they measure different type of observedproperties.')
      } else {
        let outputs_tmp = table_data.filter(
          item => item.name===el[0].original.name
        )[0].observedproperties
        let names = el.map((item: any) => item.original.name)
        setData({
          ...state,
          form: {
            ...state.form,
            outputs: [
              ...state.default.outputs,
              ...outputs_tmp
            ],
            procedures: [...names]
          }
        })
      }
    }
  }

  function _onRowSelect(e: any) {
    let rows_name = state.form.outputs.map(item => item.name)
    let newRows = [...e.filter(
      (item: any, index: number) => rows_name.indexOf(item.original.name)<0 )
    ].map(
      item=>{
        let f = froms.filter(
          item2 => item2.current ? item2.current.id.split("$")[0] === item.original.uom.props.id.split("$")[0] : false
        )
        let t = tos.filter(
          item2 => item2.current ? item2.current.id.split("$")[0] === item.original.uom.props.id.split("$")[0] : false
        )
        let l = lists.filter(
          item2 => item2.current ?item2.current.id.split("$")[0] === item.original.uom.props.id.split("$")[0] : false
        )
        
        let o = outputs.filter(
          item2 => item2?.current?.id === item.original.uom.props.id
        )
        if (l.length>0) {
          try {
            let o_splitted = l[0]?.current?.value.split(',')
            return {
              ...item.original,
              uom: o[0]?.current?.value,
              constraint: {
                valueList: o_splitted,
                role: "urn:ogc:def:classifiers:x-istsos:1.0:qualityIndex:check:reasonable"
              }
            }
          } catch (error) {
            alert("Please insert comma separeted values.")
          }
        } else if (t.length>0 && f.length>0) {
          t = t[0]?.current?.value ? t[0]?.current?.value : Object()
          f = f[0]?.current?.value ? f[0]?.current?.value : Object()
          return {
            ...item.original,
            uom: o[0].current?.value,
            constraint: {
              interval: [`${f}`, `${t}`],
              role: "urn:ogc:def:classifiers:x-istsos:1.0:qualityIndex:check:reasonable"
            }
          }
        } else if (t.length>0) {
          t = t[0].current?.value ? t[0]?.current?.value : Object()
          return {
            ...item.original,
            uom: o[0].current?.value,
            constraint: {
              min: `${t}`,
              role: "urn:ogc:def:classifiers:x-istsos:1.0:qualityIndex:check:reasonable"
            }
          }
        } else if (f.length>0) {
          f = f[0].current?.value ? f[0]?.current?.value : Object()
          return {
            ...item.original,
            uom: o[0].current?.value,
            constraint: {
              max: `${f}`,
              role: "urn:ogc:def:classifiers:x-istsos:1.0:qualityIndex:check:reasonable"
            }
          }
        } else {
          return {
            ...item.original,
            uom: o[0].current?.value,
            constraint: {}
          }
        }
      }
    )
    if (newRows.length===0) {
      rows_name = e.map((item: any) => item.original.name)
      let deleteRows = [...state.form.outputs.filter(item => rows_name.indexOf(item.name)>=0 || item.name==='Time' )]
      setData({
        ...state,
        form: {
          ...state.form,
          outputs: deleteRows
        }
      })
    } else {
      let newOutputs = [...state.form.outputs.concat(newRows)]

      setData({
        ...state,
        form: {
          ...state.form,
          outputs: newOutputs
        }
      })
    }
  }
  let data_contacts: Contact[] = [...state.form.contacts]
  let data_documents: Document[] = [...state.form.documentation]
  return <form className={styles.form}>
      <div className={styles.grid}>
        <fieldset className={styles.fieldset_33}>
          <legend className={styles.legend}>General info</legend>
          <label className={styles.label}>Name:</label>
          <input
            id="system_id"
            type="text"
            placeholder="Name"
            onChange={(e)=>_onChange(e)}
            value={state.form.system_id}
          />
          <label className={styles.label}>Description:</label>
          <input
            id="description"
            type="text"
            placeholder="Description"
            onChange={(e)=>_onChange(e)}
            value={state.form.description}
          />
          <label className={styles.label}>Keywords:</label>
          <input
            id="keywords"
            type="text"
            placeholder="key1,key2,key3"
            onChange={(e)=>_onChange(e)}
            value={state.form.keywords} 
          />
        </fieldset>
        <fieldset className={styles.fieldset_33}>
          <legend className={styles.legend}>Classification</legend>
          <label className={styles.label}>System type:</label>
          <input
            id="classification-0-value"
            type="text"
            value={state.form.classification[0].value}
            disabled
          />
          <label className={styles.label}>Sensor type:</label>
          <input
            id="classification-1-value"
            type="text"
            placeholder="tipping-bucket gauge, probe, etc."
            onChange={(e)=>_onChange(e)}
            value={state.form.classification[1].value}
          />
        </fieldset>
        <fieldset className={styles.fieldset_33}>
          <legend className={styles.legend}>Location</legend>
          <label className={styles.label}>Feature of Interest (FOI) name:</label>
          <input
            id="location-properties-name"
            type="text"
            placeholder="FOI name"
            onChange={(e)=>_onChange(e)}
            value={state.form.location.properties.name}
          />
          <div className={`${styles.fieldset}`} style={{marginTop: 10}} >
            <label className={styles.label}>EPSG:</label>
            <select defaultValue={defaultepsg} id="location-crs-properties-name" onChange={(e)=>_onChange(e)}>
              <option key={`key-epsg-option-00`}>{defaultepsg}</option>
                {allowedepsg?.split(',').map(
                  (item, index) => <option key={`key-epsg-option-${index}`}>{item}</option>
                )}
            </select>
            <div className={styles.flex}>
              <label className={styles.label}>X: </label>
              <input
                id="location-geometry-coordinates-0"
                type="number"
                step="any"
                placeholder="X coord"
                onChange={(e)=>_onChange(e)}
                value={state.form.location.geometry.coordinates[0]}
              />
              <label className={styles.label}>Y: </label>
              <input
                id="location-geometry-coordinates-1"
                type="number"
                step="any"
                placeholder="Y coord"
                onChange={(e)=>_onChange(e)}
                value={state.form.location.geometry.coordinates[1]}
              />
              <label className={styles.label}>Z: </label>
              <input
                id="location-geometry-coordinates-2"
                type="number"
                step="any"
                placeholder="Z coord"
                onChange={(e)=>_onChange(e)}
                value={state.form.location.geometry.coordinates[2]}
              />
            </div>
          </div>
        </fieldset>
      </div>
      <div>
        <div>
          { edit ? 
          systemtype == 'virtual-profile' ?
          <></> :
          <fieldset className={styles.fieldset}>
            <legend className={styles.legend}>
              Observed properties
            </legend>
            <SmartTable
              id='observedproperty'
              height={100}
              key="key-t-insitu-obs"
              init_data={
                {
                  alias: state.table_alias,
                  data: state.table_data
                }
              }
              onRowSelect={_onRowSelect}
              pagination={false}
              filter={true}
            />
          </fieldset> :
          systemtype == 'virtual-profile' ?
            <fieldset className={styles.fieldset}>
              <legend className={styles.legend}>Procedures</legend>
              <SmartTable
                id='profile'
                height={100}
                key="key-t-profile-proc"
                init_data={
                  {
                    alias: state.table_alias,
                    data: state.table_data
                  }
                }
                onRowSelect={(e: any) => _onRowSelectProfile(e)}
                pagination={false}
                filter={true}
              />
            </fieldset> : 
            <fieldset className={styles.fieldset}>
              <legend className={styles.legend}>Observed properties</legend>
              <SmartTable
                id='observedproperty'
                key="key-t-insitu-obs"
                init_data={
                  {
                    alias: state.table_alias,
                    data: state.table_data
                  }
                }
                onRowSelect={(e: any) => _onRowSelect(e)}
                pagination={false}
                filter={true}
              />
            </fieldset>
          }
        </div>
    </div>
    <fieldset>
      <legend>
        <a onClick={()=>setData({...state, contacts: !state.contacts})} >
          Contacts {state.contacts ? '➖' : '➕'}
        </a>
      </legend>
      {
        state.contacts ? <Contacts
          data={data_contacts} 
          onChange={(e: any)=>_onChange(e)}
        /> : <></>
      }
    </fieldset>
    <fieldset>
      <legend>
        <a onClick={()=>setData({...state, documents: !state.documents})} >
          Documents {state.documents ? '➖' : '➕'}
        </a>
      </legend>
      {
        state.documents ? <Documents
          data={data_documents} 
          onChange={(e: any)=>_onChange(e)}
        /> : <></>
      }
    </fieldset>
    <fieldset>
      <legend>
        <a onClick={()=>setData({...state, interfaces: !state.interfaces})} >
          Interfaces {state.interfaces ? '➖' : '➕'}
        </a>
      </legend>
      {
        state.interfaces ? 
        <div>TBD</div> : <></>
      }
    </fieldset>
    <fieldset>
      <legend>
        <a onClick={()=>setData({...state, identification: !state.identification})} >
          Identification {state.identification ? '➖' : '➕'}
        </a>
      </legend>
      {
        state.identification ? 
        <div>TBD</div> : <></>
      }
    </fieldset>
    <fieldset>
      <legend>
        <a onClick={()=>setData({...state, characteristics: !state.characteristics})} >
        Characteristics {state.characteristics ? '➖' : '➕'}
        </a>
      </legend>
      {
        state.characteristics ? 
        <div>TBD</div> : <></>
      }
    </fieldset>
    <fieldset>
      <legend>
        <a onClick={()=>setData({...state, capabilities: !state.capabilities})} >
        Capabilities {state.capabilities ? '➖' : '➕'}
        </a>
      </legend>
      {
        state.capabilities ? 
        <div>TBD</div> : <></>
      }
    </fieldset>
    {/* <fieldset>
      <legend>
        <a onClick={()=>setData({...state, mqtt: !state.mqtt})} >
        MQTT Broker {state.mqtt ? '➖' : '➕'}
        </a>
      </legend>
      <div>TBD</div>
    </fieldset> */}
    <div
      className={styles.footer}
      onClick={
        systemtype == 'virtual-profile' ?
        ()=>onSubmitFunc(state.form) :
        ()=>onSubmitFunc(state.form)
      }
    >
      SUBMIT
    </div>
  </form>
}

export default FormProcedure