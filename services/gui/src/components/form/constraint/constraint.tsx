
import { useState } from 'react'

const Constraint: React.FC<{children: any, onChange?: Function }> = ({children, onChange}) => {  
  const [state, setData] = useState({
    constraint: ''
  })
  function _onChange(e: any) {
    setData({
      ...state,
      constraint: e.target.value
    })
    if (onChange) {
      onChange(e.target.value)
    }
  }
  return <div>
    <select onChange={_onChange}>
      <option value=''></option>
      <option value='gt'>Greater then</option>
      <option value='lt'>Lower then</option>
      <option value='bt'>Between</option>
      <option value='ls'>Value list</option>
    </select>
    {
      state.constraint==='gt' ?
      <div>
        {children[0]}
      </div> :
      state.constraint==='lt' ?
      <div>
        {children[1]}
      </div> :
      state.constraint==='bt' ?
      <div>
        <label>From</label>
        {children[3]}
        <label>To</label>
        {children[4]}
      </div> :
      state.constraint==='ls' ?
      <div>
        {children[2]}
      </div> :
      <></>
    }
  </div>
}

export default Constraint;