// css
import styles from './form.module.css'

// components
import FieldSets from '../../components/form/fieldsets/fieldsets'

interface Option {
  value: string
  text: string
}

interface InputItem {
  label: string
  type: string
  value: string | number
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => any
  onChangeSelect?: (e: React.ChangeEvent<HTMLSelectElement>) => any
  options?: Option[]
}

interface Field {
  title: string
  inputs: InputItem[]
}

const Form: React.FC<{
    onSubmit: (e: React.FormEvent<HTMLButtonElement>) => any,
    fields: Field[],
    edit: boolean
  }> = ({onSubmit, fields, edit}) => {  
  return <form className={styles.form}>
      <FieldSets fields={fields} />
      {
        edit ? <button type='button' className={styles.submit} onClick={onSubmit}>Submit</button> : <></>
      }
  </form>
}

export default Form;