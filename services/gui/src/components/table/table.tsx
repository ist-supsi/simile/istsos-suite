
import {useEffect, useMemo} from 'react'
import React from 'react'
import {
	useTable,
  useSortBy,
  usePagination,
  useResizeColumns,
	useBlockLayout,
	Column,
	useFilters, useGlobalFilter, useRowSelect, useAsyncDebounce
} from 'react-table'
import {matchSorter} from 'match-sorter'
import {withSize} from 'react-sizeme'

import AddRemoveBtns from '../buttons/addremove'

// styles
import styles from './table.module.css'

interface Props {
  indeterminate?: boolean;
  rest?: any
}

const useCombinedRefs = (...refs): React.MutableRefObject<any> => {
  const targetRef = React.useRef();

  React.useEffect(() => {
    refs.forEach(ref => {
      if (!ref) return;

      if (typeof ref === 'function') {
        ref(targetRef.current);
      } else {
        ref.current = targetRef.current;
      }
    });
  }, [refs]);

  return targetRef;
};


const IndeterminateCheckbox = React.forwardRef<HTMLInputElement, Props>(
  ({ indeterminate, ...rest }, ref: React.Ref<HTMLInputElement>) => {
    const defaultRef = React.useRef(null);
    const combinedRef = useCombinedRefs(ref, defaultRef);

    useEffect(() => {
      if (combinedRef?.current) {
        combinedRef.current.indeterminate = indeterminate ?? false;
      }
    }, [combinedRef, indeterminate]);

    return (
      <React.Fragment>
        <input type="checkbox" ref={combinedRef} {...rest} />
      </React.Fragment>
    );
  }
);

// Define a default UI for filtering
// Define a default UI for filtering
function GlobalFilter({
  preGlobalFilteredRows,
  globalFilter,
  setGlobalFilter,
}) {
  const count = preGlobalFilteredRows.length
  const [value, setValue] = React.useState(globalFilter)
  const onChange = useAsyncDebounce(value => {
    setGlobalFilter(value || undefined)
  }, 200)

  return (
    <div className={styles.search}>
      Search:{' '}
      <input
        value={value || ""}
        onChange={e => {
          setValue(e.target.value);
          onChange(e.target.value);
        }}
				type='text'
        placeholder={`${count} records...`}
        style={{
          // fontSize: "0.75rem",
          // lineHeight: "1rem",
          border: '0',
          paddingTop: 0,
          paddingBottom: 0
        }}
      />
    </div>
  )
}
  
function fuzzyTextFilterFn(rows, id, filterValue) {
	return matchSorter(rows, filterValue, { keys: [row => row] })
}
  
// Let the table remove the filter if the string is empty
fuzzyTextFilterFn.autoRemove = val => !val
  

function Table({ columns, data, sort, onRowDbClick, pagination, editable, height, onAdd, onRemove, onRowSelect }) {
	const defaultColumn = React.useMemo(
    () => ({
      width: 150,
    }),
    []
  )

  function _onAdd() {
    onAdd()
  }
  function _onRemove() {
    let to_remove = Object.keys(state.selectedRowIds).map(
      i => data[i]
    )
    onRemove(to_remove)
  }
  
  // Use the state and functions returned from useTable to build your UI
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    rows,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
		preGlobalFilteredRows,
    selectedFlatRows,
    setGlobalFilter,
    visibleColumns,
    state: { pageIndex, pageSize},
		state
  } = useTable(
    {
      columns,
      data,
			defaultColumn,
      initialState: { pageIndex: 0  },
      // autoResetPage: false
    },
    useBlockLayout,
		useResizeColumns,
		useGlobalFilter,
		useSortBy,
		usePagination,
		useRowSelect,
    hooks => {
      if (editable) {
        return hooks.visibleColumns.push(columns => [
          // Let's make a column for selection
          {
            id: 'selection',
            // The header can use the table's getToggleAllRowsSelectedProps method
            // to render a checkbox
            Header: ({ getToggleAllRowsSelectedProps }) => (
              <div>
                <IndeterminateCheckbox {...getToggleAllRowsSelectedProps()} />
              </div>
            ),
            // The cell can use the individual row's getToggleRowSelectedProps method
            // to the render a checkbox
            Cell: ({ row }) => (
              <div>
                <IndeterminateCheckbox {...row.getToggleRowSelectedProps()} />
              </div>
            ),
            width: 30
          },
          ...columns,
        ])
      } else { return {...columns}}
    }
  )
  
  useEffect(() => {
    if (onRowSelect) {
      onRowSelect(selectedFlatRows);
    }
  }, [ selectedFlatRows ]);

  // Render the UI for your table
  return (
    <div className={styles.div}>
      <div {...getTableProps()} className={styles.table}>
        <div className={styles.thead}>
					<div {...headerGroups[0].getHeaderGroupProps()} className={styles.search}>
            <div
              className={styles.search}
            >
              <GlobalFilter
                preGlobalFilteredRows={preGlobalFilteredRows}
                globalFilter={state.globalFilter}
                setGlobalFilter={setGlobalFilter}
              />

              <div style={{float: "right"}}>
              {
                editable ? <AddRemoveBtns
                  onAdd={onAdd ? _onAdd : undefined}
                  onRemove={onRemove ? _onRemove : undefined}
                /> : <></>
              }
              </div>
            </div>
          </div>
          {headerGroups.map(headerGroup => (
            <div {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map(column => (
                <div {...column.getHeaderProps()} className={styles.th}>
									{column.render('Header')}
									{sort ? <span {...column.getSortByToggleProps()}>
										{column.isSorted
										? column.isSortedDesc
											? ' ⬇️'
											: ' ⬆️'
										: ' ▪️'}
									</span>: null}
									<div
										{...column.getResizerProps()}
										className={`${styles.resizer} ${
											column.isResizing ? styles.resizer : ''
										}`}
									/>
								</div>
              ))}
            </div>
          ))}
        </div>
        <div {...getTableBodyProps()} className={styles.tbody}>
          {pagination ? page.map((row, i) => {
            prepareRow(row)
            return (
              <div
								{...row.getRowProps()}
								className={styles.tr}
							>
                {row.cells.map((cell, index) => {
									
									if (cell.value === "false" || cell.value === false) {
										let id = row.cells[index].value;
										return (
											<div {...cell.getCellProps()} className={styles.td}>
												🔴
											</div>
										)  
									} else if (cell.value === "true" || cell.value === true ) {
										let id = row.cells[index].value;
										return (
											<div {...cell.getCellProps()} className={styles.td}>
												🟢
											</div>
										)  
									} {
										return (
											<div onDoubleClick={onRowDbClick ? () => onRowDbClick(row) : () =>("")} {...cell.getCellProps()} className={styles.td}>
												{cell.render('Cell')}
											</div>
										)
									}
								})}
              </div>
            )
          }) : rows.map((row, i) => {
            prepareRow(row)
            return (
              <div
								{...row.getRowProps()}
								className={styles.tr}
							>
                {row.cells.map((cell, index) => {
									
									if (cell.value === "false" || cell.value === false) {
										let id = row.cells[index].value;
										return (
											<div {...cell.getCellProps()} className={styles.td}>
												🔴
											</div>
										)  
									} else if (cell.value === "true" || cell.value === true ) {
										let id = row.cells[index].value;
										return (
											<div {...cell.getCellProps()} className={styles.td}>
												🟢
											</div>
										)  
									} {
										return (
											<div onDoubleClick={onRowDbClick ? () => onRowDbClick(row) : () =>("")} {...cell.getCellProps()} className={styles.td}>
												{cell.render('Cell')}
											</div>
										)
									}
								})}
              </div>
            )
          })}
        </div>
      </div>
      {
        data.length > 10 ? pagination ?
        <div className={styles.pagination}>
          <button onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
            {'<<'}
          </button>{' '}
          <button onClick={() => previousPage()} disabled={!canPreviousPage}>
            {'<'}
          </button>{' '}
          <button onClick={() => nextPage()} disabled={!canNextPage}>
            {'>'}
          </button>{' '}
          <button onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
            {'>>'}
          </button>{' '}
          <span>
            Page{' '}
            <strong>
              {pageIndex + 1} of {pageOptions.length}
            </strong>{' '}
          </span>
          <span>
            | Go to page:{' '}
            <input
              type="number"
              defaultValue={pageIndex + 1}
              onChange={e => {
                const page = e.target.value ? Number(e.target.value) - 1 : 0
                gotoPage(page)
              }}
              style={{ width: '100px' }}
            />
          </span>{' '}
          <select
            value={pageSize}
            onChange={e => {
              setPageSize(Number(e.target.value))
            }}
          >
            {[10, 20, 30, 40, 50].map(pageSize => (
              <option key={pageSize} value={pageSize}>
                Show {pageSize}
              </option>
            ))}
          </select>
        </div> : <></> : <></>
      }
    </div>
  )
}


interface TableComponentProps {
	key?: string,
	init_data: any,
  pagination: boolean,
  height: number,
	filter: boolean,
	sort?: boolean,
	status_table?: any,
  onAdd?: Function,
  onRowDbClick?: Function,
  onRowSelect?: Function,
  onRemove?: Function,
  pageIndex?: number,
  size: any
};

const TableComponent: React.FC<TableComponentProps> = ({
		key, size, init_data,
		pagination, onRowDbClick,
		height, filter, sort,
		onRowSelect, status_table,
		onRemove, pageIndex,
    onAdd
	}) => {
	if (init_data.data.length > 0) {		
		const data = useMemo(() => [...init_data.data], [init_data.data]);
		// const data = React.useMemo(() => init_data.data, []);
		var col_width = (size.width-30) / Object.keys(init_data.alias).length
    col_width = onAdd ? col_width : onRowSelect ? col_width : col_width
		const columns = useMemo(
			() => Object.keys(init_data.alias).map(
				(item, index)=>({
					Header: init_data.alias[item],
					accessor: item,
					// width: init_data.alias[item] == 'Select' || init_data.alias[item] == 'Edit' ? 50 : init_data.alias[item] == 'URN' ? col_width+(col_width-100) : col_width,
          width: col_width,
					minWidth: 50, // minWidth is only used as a limit for resizing
					// maxWidth: 200, // maxWidth is only used as a limit for resizing
					// width: item=="select"? 63 : col_width
				})
			),
			[],
		)
		return (
      <Table
        key={key}
        columns={columns}
        onAdd={onAdd ? () => onAdd() : undefined}
        onRemove={onRemove ? (to_remove)=>onRemove(to_remove) : undefined}
        onRowSelect={onRowSelect ? (e)=>onRowSelect(e) : undefined}
        sort={sort}
        data={data}
        height={size.height}
        pagination={pagination}
        onRowDbClick={onRowDbClick}
        editable={onAdd || onRemove ? true : onRowSelect ? true : false}
      />
		)
	} else {
		return <div>No data available</div>
	}
}

export default withSize({monitorHeight: true})(TableComponent)