
// components
import TableComponent from './table'
import AddRemoveBtns from '../buttons/addremove'
import {
  useWindowSize
} from '@react-hook/window-size'

// styles
import styles from './smarttable.module.css'

const SmartTable:React.FC<{
      id: string,
      init_data: object,
      height?: number,
      pagination: boolean,
      filter: boolean,
      onSelect?: Function,
      onAdd?: Function,
      onRemove?: Function,
      onRowSelect?: Function,
      onCheck?: Function
}> = ({
  id,
  init_data,
  height,
  pagination,
  filter,
  onRowSelect,
  onAdd,
  onRemove,
  onCheck
}) => {
  const [width, win_height] = useWindowSize()
  return <div className={styles.container}>
    <TableComponent
      key={id}
      init_data={init_data}
      height={height ? height : 400}
      pagination={pagination}
      filter={filter}
      onRowSelect={onRowSelect ? (e) => onRowSelect(e) : undefined}
      onAdd={onAdd ? () => onAdd() : undefined}
      onRemove={onRemove ? (to_remove) => onRemove(to_remove) : undefined}
      // onCheck={onCheck ? (selected) => onCheck(selected) : undefined}
    />
  </div>
}

export default SmartTable