
import React, { useState, useEffect } from 'react';

import styles from './toast.module.css';

interface ToastItem {
  id: number,
  icon?: string,
  description: string,
  title: string,
  backgroundColor: string
}

const Toast: React.FC<{
    toastList: ToastItem[],
    position?: string,
    autoDelete?: boolean,
    dismissTime?: number
  }> = ({
    toastList,
    position,
    autoDelete,
    dismissTime
  }) => {
    const [list, setList] = useState(toastList);

    useEffect(() => {
      setList([...toastList]);

      // eslint-disable-next-line
    }, [toastList]);

    useEffect(() => {
      const interval = setInterval(() => {
        if (autoDelete && toastList.length && list.length) {
          deleteToast(toastList[0].id);
        }
      }, dismissTime);
      
      return () => {
        clearInterval(interval);
      }

      // eslint-disable-next-line
    }, [toastList, autoDelete, dismissTime, list]);

    const deleteToast = id => {
      const listItemIndex = list.findIndex(e => e.id === id);
      const toastListItem = toastList.findIndex(e => e.id === id);
      list.splice(listItemIndex, 1);
      toastList.splice(toastListItem, 1);
      setList([...list]);
    }

    return (
      <>
        <div className={`${styles.notificationcontainer} ${styles.bottomright}`}>
          {
            list.map((toast, i) =>     
              <div 
                key={i}
                className={`${styles.notification} ${styles.toast}`}
                style={{ backgroundColor: toast.backgroundColor }}
              >
                <button onClick={() => deleteToast(toast.id)}>
                  X
                </button>
                {toast.icon ? <div className={styles.notificationimage}>
                  <img src={toast.icon} alt="" />
                </div> : <></>}
                <div>
                  <div className={styles.notificationtitle}>{toast.title}</div>
                  <div className={styles.notificationmessage}>
                    {toast.description}
                  </div>
                </div>
              </div>
            )
          }
        </div>
      </>
    );
}

export { Toast };