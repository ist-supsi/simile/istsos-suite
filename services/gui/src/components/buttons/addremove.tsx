
import styles from './addremove.module.css'

const AddRemoveBtns: React.FC<{onAdd?: Function, onRemove?: Function}> = ({onAdd, onRemove}) => {
  return <div className={styles.element}>

    {onAdd ? <a className={styles.add} onClick={() => onAdd()}>➕ Add</a> : <></> }
    {onRemove ? 
    <a
      className={styles.remove}
      onClick={
        () => onRemove()
      }
    >
      🗑️ Remove
    </a> : <></> }
  </div>
}

export default AddRemoveBtns