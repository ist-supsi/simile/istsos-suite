
import styles from './modal.module.css'

const Modal:React.FC<{onModalClose, title}> = ({onModalClose, title, children}) => {

  return <div className={`${styles.modal}`}>
    <div className={styles.modal_content}>
      <div className={styles.toolbar}>
        <div className={styles.title}>
          {title}
        </div>
        <div className={styles.overlay}>
          <span className={styles.close_btn} onClick={onModalClose}>&times;</span>
        </div>
      </div>
      <div className={styles.modalchildren}>
      {children}
      </div>
    </div>
</div>
}

export default Modal