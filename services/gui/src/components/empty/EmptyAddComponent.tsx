import styles from './empty.module.css'

const EmptyAddComponent: React.FC<{onClick: Function}> = ({children, onClick}) => {
  return <div className={styles.div}>
    <p className="inline-block align-middle" >
      {children}
    </p>
    <br/>
    <button onClick={() => onClick()}>Add</button>
  </div>
} 

export default EmptyAddComponent