
import styles from './cardlist.module.css'

interface SystemType {
  name: string,
  description: string,
  selected?: boolean
  icon?: string,
}


const icon = [
  {
    name: 'insitu-fixed-point',
    icon: '/static/icons/baseline_room_black_24dp.png'
  },
  {
    name: 'virtual',
    icon: '/static/icons/baseline_insights_black_24dp.png'
  },
  {
    name: 'virtual-profile',
    icon: '/static/icons/baseline_auto_fix_high_black_24dp.png'
  },
  {
    name: 'insitu-fixed-specimen',
    icon: '/static/icons/baseline_biotech_black_24dp.png'
  },
  {
    name: 'profile',
    icon: '/static/icons/baseline_directions_boat_black_24dp.png'
  },
  {
    name: 'insitu-mobile-point',
    icon: '/static/icons/baseline_directions_bike_black_24dp.png'
  }
]

const CardListSystemType: React.FC<{systemtypes: Array<SystemType>, onClick: Function, width?: number}> = ({systemtypes, onClick, width}) => {
  return <div className={styles.card} style={{width: width ? `${width}%` : ''}}>
    {systemtypes.map(
      item => {
        return (
          <div
            onClick={() => onClick(item.name)} key={`key-system-${item.name}`}
            className={`${styles.cardtype} ${item.selected ? styles.cardtypeselected : '' }`}
          >
            <div className={styles.cardcontent}>
              <img
                className={styles.cardimg}
                src={icon.filter(item2 => item2.name === item.name)[0].icon}
                alt={item.name}
              />
              <div className={styles.carddiv}>
                <p className={`${styles.cardp} ${item.selected ? styles.cardpselected : '' }`}>{item.name}</p>
                <p className={`${styles.cardp2} ${item.selected ? styles.cardp2selected : '' }`}>{item.description}</p>
              </div>
            </div>
          </div>
        )
      }
    )}
  </div> 
}



interface ItemGeneric {
  title: string,
  description: string,
  icon: string,
  selected?: boolean
}

const CardList: React.FC<{data: Array<ItemGeneric>, onClick: Function}> = ({data, onClick}) => {
  return <div className={styles.card}>
    {data.map(
      (item, index) => {
        return (
          <div
            onClick={() => onClick(index)} key={`key-card-item-${item.title}`}
            className={`${styles.carditem}  ${item.selected ? styles.selected : '' }`}
          >
            <div className={styles.cardcontent}>
              {item.icon ? <img
                className={styles.cardimg}
                src={item.icon}
                alt={item.title}
              /> : <></>}
              <div>
                <p className={`${styles.cardtitle} ${item.selected ? styles.cardtitleselected : '' }`}>{item.title}</p>
                <p className={`${styles.cardtext} ${item.selected ? styles.cardtextselected : '' }`}>{item.description}</p>
              </div>
            </div>
          </div>
        )
          
      }
    )}
  </div> 
}


export {
  CardListSystemType,
  CardList
}