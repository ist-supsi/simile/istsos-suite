import React from 'react';
import {useState} from 'react'
import PropTypes from 'prop-types';
import {
	useTable, useFilters, useGlobalFilter, useAsyncDebounce
} from 'react-table'
import {matchSorter} from 'match-sorter'
import styles from './options.module.css'


function fuzzyTextFilterFn(rows, id, filterValue) {
    return matchSorter(rows, filterValue, { keys: [row => row] })
  }
// Let the table remove the filter if the string is empty
fuzzyTextFilterFn.autoRemove = val => !val

// Define a default UI for filtering
function GlobalFilter({
	preGlobalFilteredRows,
	globalFilter,
    setGlobalFilter,
    title
  }) {
	const count = preGlobalFilteredRows.length
	const [value, setValue] = React.useState(globalFilter)
	const onChange = useAsyncDebounce(value => {
	  setGlobalFilter(value || undefined)
	}, 200)
  
	return (
    <div className={styles.div_legend}>
      <label style={{display: "inline"}}>{title}  </label>
      {
        count > 2 ? <input
          value={value || ""}
          onChange={e => {
          setValue(e.target.value);
          onChange(e.target.value);
          }}
          style={{
              fontSize: "70%",
              // width: "90px",
              maxWidth: "90px"
          }}
          placeholder={` search ${count} records...`}
        /> : <></>
      }
        
    </div>
	)
}

function Inputs({columns, data, title, width, height}) {
    const filterTypes = React.useMemo(
        () => ({
          // Add a new fuzzyTextFilterFn filter type.
          fuzzyText: fuzzyTextFilterFn,
          // Or, override the default text filter to use
          // "startWith"
          text: (rows, id, filterValue) => {
            return rows.filter(row => {
              const rowValue = row.values[id]
              return rowValue !== undefined
                ? String(rowValue)
                    .toLowerCase()
                    .startsWith(String(filterValue).toLowerCase())
                : true
            })
          },
        }),
        []
    )
    
    const {
        rows,
        prepareRow,
        state,
        preGlobalFilteredRows,
        setGlobalFilter,
        globalFilter
    } = useTable(
        {
            columns,
            data,
            filterTypes,
        },
        useFilters, // useFilters!
        useGlobalFilter // useGlobalFilter!
    )
    return(
        <fieldset className={styles.fieldset} style={{height: height}}>
            <legend className={styles.legend}>
                <GlobalFilter
                    preGlobalFilteredRows={preGlobalFilteredRows}
                    globalFilter={globalFilter}
                    setGlobalFilter={setGlobalFilter}
                    title={title}
                />
            </legend>
            <div className={styles.div_options}>
            {rows.map((row, i) => {
                prepareRow(row)
                return <div key={`key-options-${i}`}>
                    <input
                        type={row.cells[2].value}
                        value={row.cells[0].value}
                        checked={row.cells[1].value}
                        onChange={row.cells[4].value}
                        disabled={row.cells[3].value}
                    />
                    {
                        row.cells[2].value==='text' ?
                        <></> :
                        <label
                            style={{
                                display: 'inline-block',
                                textAlign: 'right',
                                marginLeft: '5px',
                                marginRight: '15px'
                            }}
                        >
                            {row.cells[0].value}
                        </label>
                    }
                </div>
            })}
            </div>
        </fieldset>
    )
}


export default function Options({ data, title, width, height }) {

    const columns = React.useMemo(
        () => [{
            Header: 'value',
            accessor: 'value',
        },{
            Header: 'checked',
            accessor: 'checked',
        },{
            Header: 'type',
            accessor: 'type',
        },{
            Header: 'disabled',
            accessor: 'disabled',
        },{
            Header: 'onChange',
            accessor: 'onChange',
        }], []
    )
    const init_data = React.useMemo(() => [...data], [data]);
    
    return (
        data.length > 0 ? 
            <Inputs
                columns={columns}
                data={init_data}
                title={title}
                width={width}
                height={height}
            />
        : <></>
    )
}

Options.propTypes = {
    data: PropTypes.array.isRequired,
    title: PropTypes.string,
    width: PropTypes.number,
    height: PropTypes.number
};