
// external

import styles from './toplayout.module.css'


const TopLayout: React.FC<{}> = ({children}) => {
  return <div className={`${styles.top_layout}`}>
    <div className={`${styles.top_layout_top}`}>
      {children[0]}
    </div>
    <div
      className={`${styles.top_layout_content}`}
      style={{
        paddingBottom: 0
      }}
    >
      {children[2]?children[2]: <></>}
      {children[1]}
    </div>
  </div>
}

export default TopLayout;