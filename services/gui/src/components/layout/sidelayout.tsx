
// external

import styles from './sidelayout.module.css'


const SideLayout: React.FC<{}> = ({children}) => {
  return <div className={styles.sidemain}>
    <aside className={styles.sideleft}>
      {children[0]}
    </aside>
    <div className={styles.sideright}>
      {children[1]}
    </div>
  </div>
}

export default SideLayout;