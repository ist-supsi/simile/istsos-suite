import Head from 'next/head'
import styles from './layout.module.css'
import { useState } from 'react'
import { NextPage } from 'next'
import NavBar from '../navbar/navbar'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faBars, faWindowClose
} from '@fortawesome/free-solid-svg-icons'

const name = 'SUPSI -IST Daniele Strigaro'
export const siteTitle = 'SIMILE - Sistema Informativo per il Monitoraggio Integrato dei Laghi insubrici e dei loro Ecosistemi'

const Layout: NextPage<{
  auth?: boolean
  authenticated?: boolean
  home?: boolean
  user?: any
  logout: any
  role?: string
}> = ({
  children,
  auth,
  authenticated,
  user,
  logout,
  role
}) => {
  const [state, setData] = useState({menu: false})
  function onClick() {
    console.log('asd');
    
    setData({
      ...state,
      menu: !state.menu
    })
  }
  if (auth) {
    return <div>{children}</div>
  } else {
    return (
      <div className={styles.main}>
        <Head>
          <link rel="icon" href="/favicon.png" />
          <meta
            name="description"
            content="SIMILE - Sistema Informativo per il Monitoraggio Integrato dei Laghi insubrici e dei loro Ecosistemi"
          />
        </Head>
        <div className={`${styles.sideleft} ${state.menu ? styles.open : ''}`}>
          {children[1]}
        </div>
        <div className={`${styles.sideright} ${state.menu ? styles.leftshift : ''}`}>
          <div className={styles.header}>
            <div className={styles.menu}>
              <div className={styles.verticalcenter}>
                <a onClick={onClick}>
                  <FontAwesomeIcon className={styles.iconmenu} icon={state.menu ? faWindowClose : faBars} />
                </a>
              </div>
            </div>
            {
              auth ?
                undefined
              : <NavBar authenticated={authenticated} user={user} logout={logout} role={role}/>
            }
          </div>
          <div className={styles.contentbox}>
            {children[2]}
          </div>
        </div>
      </div>
    )
  }
}

export default Layout