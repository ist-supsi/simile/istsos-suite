
// ext lib
import { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import Form from '../../components/form/form'

const init_data = {
  tag: 'provider',
  title: 'Provider',
  isLoading: true,
  form: {
    providername: "",
    providersite: "",
    contactname: "",
    contactposition: "",
    contactdeliverypoint: "",
    contactpostalcode: "",
    contactcity: "",
    contactadminarea: "",
    contactcountry: "",
    contactvoice: "",
    contactfax: "",
    contactemail: ""
  },
  base_url: `/istsos/wa/istsos/services`,
  api_url: 'configsections/provider',
  data: {},
  message: "",
  error: "",
}


const ProviderContainer: React.FC<{service: string, token: string, user?: string}> = ({service, token, user}) => {
  const [state, setData] = useState(init_data)
  const router = useRouter()

  let service_url = service ? `/${service}/` : '/' 

  useEffect(
    () => {
      fetch(
        `${state.base_url}${service_url}${state.api_url}`,
        {
          headers: {
            'Authorization': `Bearer ${token}`
          },
          method: 'GET',
        }
      ).then(
        (response) => {
          if (response.ok && response.json) {                                    
            return response.json()
          } else{
            return {success: false}
          }
        }
      ).then(
        (res) => {
          if (res.success) {
            return setData({
              ...state,
              form: {
                ...state.form,
                ...res.data
              },
              data: {
                ...state.data,
                ...res.data
              },
              isLoading: false
            })
          } else {
            return alert(res.message)
          }
        }
      )
    }
  , [service])

  // ********
  // FUNCs
  // *******
  function onChange(key, e) {
    setData({
      ...state,
      form: {
        ...state.form,
        [key]: e.target.value
      }
    })
  }

  function onSubmit() {
    fetch(
      `${state.base_url}${service_url}${state.api_url}`,
      {
        headers: {
          'Authorization': `Bearer ${token}`
        },
        method: 'PUT',
        body: JSON.stringify(state.form)
      }
    ).then(response => {
      if (response.ok && response.json) {                                    
        return response.json()
      } else{
        return {success: false}
      }
    })
    .then(res => {
      if (res.success) {
        router.push({
          ...router,
          query: {
            ...router.query,
            reload: Math.floor(Math.random() * (50 - 0 + 1)) + 0
          }
        })
        return alert(res.message)
      } else {
        return alert(res.message)
      }
    }).catch(
      (e) => {
        alert('Request failed!')
        console.log(e);
      }
    )
  }
  const form_data = [
    {
      title: state.title,
      inputs: [
        {
          label: 'Name',
          type: 'text',
          value: state.form.providername,
          onChange: user==='viewer' ? (e) => (e) : (e) => onChange('providername', e)
        },
        {
          label: 'Web site',
          type: 'text',
          value: state.form.providersite,
          onChange: user==='viewer' ? (e) => (e) : (e) => onChange('providersite', e)
        },
        {
          label: 'Contact name',
          type: 'text',
          value: state.form.contactname,
          onChange: user==='viewer' ? (e) => (e) : (e) => onChange('contactname', e)
        },
        {
          label: 'Contact position',
          type: 'text',
          value: state.form.contactposition,
          onChange: user==='viewer' ? (e) => (e) : (e) => onChange('contactposition', e)
        },
        {
          label: 'Voice',
          type: 'text',
          value: state.form.contactvoice,
          onChange: user==='viewer' ? (e) => (e) : (e) => onChange('contactvoice', e)
        },
        {
          label: 'Fax',
          type: 'text',
          value: state.form.contactfax,
          onChange: user==='viewer' ? (e) => (e) : (e) => onChange('contactfax', e)
        },
        {
          label: 'Email',
          type: 'text',
          value: state.form.contactemail,
          onChange: user==='viewer' ? (e) => (e) : (e) => onChange('contactemail', e)
        },
        {
          label: 'Address',
          type: 'text',
          value: state.form.contactdeliverypoint,
          onChange: user==='viewer' ? (e) => (e) : (e) => onChange('contactdeliverypoint', e)
        },
        {
          label: 'Postal code',
          type: 'text',
          value: state.form.contactpostalcode,
          onChange: user==='viewer' ? (e) => (e) : (e) => onChange('contactpostalcode', e)
        },
        {
          label: 'City',
          type: 'text',
          value: state.form.contactcity,
          onChange: user==='viewer' ? (e) => (e) : (e) => onChange('contactcity', e)
        },
        {
          label: 'State',
          type: 'text',
          value: state.form.contactadminarea,
          onChange: user==='viewer' ? (e) => (e) : (e) => onChange('contactadminarea', e)
        },
        {
          label: 'Country',
          type: 'text',
          value: state.form.contactcountry,
          onChange: user==='viewer' ? (e) => (e) : (e) => onChange('contactcountry', e)
        }
      ]
    }
  ]
  return <div className='container'>
    <div className="containertitle">
      <div className='title'><div>{state.title}</div></div>
    </div>
    <div className="containercontent">
      <Form
        onSubmit={onSubmit}
        fields={form_data}
        edit={true}
      />
    </div>
  </div>
}

export default ProviderContainer