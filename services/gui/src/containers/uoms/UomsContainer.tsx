
// ext lib
import { useRouter } from 'next/router';
import {useEffect, useState} from 'react'

// components
import SmartTable from '../../components/table/smarttable'
import Modal from '../../components/modal/modal';
import Form from '../../components/form/form'
import AddRemoveBtns from '../../components/buttons/addremove'

const init_data = {
  title: 'Unit of measures',
  base_url:`/istsos/wa/istsos/services`,
  api_url: 'uoms',
  form: {
    name: "",
    description: "",
    // constraint: {
    //     role: "un:x-ogc:def:classifiers:x-istsos:1.0:qualityIndexCheck:level0",
    //     interval: ["", ""]
    // },
  },
  default: {
    name: "",
      description: "",
  },
  data: [],
  isLoading: true,
  selected: [],

  to_edit: "",
  modal: false,
  edit: false
}


const UomsContainer: React.FC<{service: string, token: string}> = ({service, token}) => {
  const [ state, setData] = useState(init_data)

  const router = useRouter();

  let service_url = service ? `/${service}/` : '/' 

  useEffect(
    ()=>{
      if (state.isLoading) {
        fetch(
          `${state.base_url}${service_url}${state.api_url}`,
          {
            headers: {
              'Authorization': `Bearer ${token}`
            },
            method: 'GET',
          }
        ).then(
          (response) => {
            if (response.ok && response.json) {                                    
              return response.json()
            } else{
              return {success: false}
            }
          }
        ).then(
          (res) => {
            if (res.success) {
              return setData({
                ...state,
                form: {
                  ...state.form,
                  ...state.default
                },
                data: [
                  ...res.data
                ],
                isLoading: false,
                selected: [],
                modal: false
              })
            } else {
              return alert(res.message)
            }
          }
        )
      }
    }
  , [service, state.isLoading])

  function onModalClose() {
    setData({
      ...state,
      modal: !state.modal
    })
  }
  function onAdd() {
    setData({
      ...state,
      form: {
        ...state.form,
        name: ''
      },
      modal: !state.modal,
      edit: false
    }) 
  }
  function onEdit(item) {
    setData({
      ...state,
      form: {
        ...state.form,
        ...item
      },
      modal: !state.modal,
      to_edit: item.name,
      edit: true
    })
  }
  function onRemove(uoms) {
    if (uoms.length === 0) {
      alert('No unit of measure(s) selected.')
    } else {
      let uom_names = uoms.map(i=>i['name'])
      if (
        confirm(
          `This operation cannot be undone!\n\nAre you sure you want to erase the following unit of measure(s)?\n${uom_names}`)
        ) {
          let promises = [];
          for (let index = 0; index < uoms.length; index++) {
            const uom_name = uoms[index]['name'];
            let options = {
              method: 'DELETE',
              headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + token 
              }
            } 
            promises.push(fetch(
              `${state.base_url}${service_url}${state.api_url}/${uom_name}`,
              options
            ).then(
              res => {
                if (res.status === 200) {
                  return res.json()
                } else {
                  throw 'Delete request failed.'
                }
            }).then(
              res => {
                if (res.success) {
                  return {success: true}
                } else {
                  return {success: false, error: res.message}
                }
              }
            ).catch(
              err => ({success: false, error: err})
            ))
              
          }
          Promise.all(promises).then(
            res => {
              let index_deleted = [];
              let msg_errors = []
              for (let index = 0; index < res.length; index++) {
                const element = res[index];
                if (element.success) {
                  index_deleted.push(index)
                } else {
                  alert(element.error)
                }
              }
              if (index_deleted.length>0) {
                setData({
                  ...state,
                  isLoading: true
                })
              }
              
            }
          )
      }
    }
  }
  function onChangeCheck(e) {
    if (e.target.checked) {
      setData({
        ...state,
        selected: [
          ...state.selected,
          e.target.value
        ]
      })
    } else {
      let new_array = state.selected.filter(item=>item!==e.target.value)
      setData({
        ...state,
        selected: new_array
      })
    }
    
  }
  function onChange(key, e) {
    setData({
      ...state,
      form: {
        ...state.form,
        [key]: e.target.value
      }
    })
  }
  function onSubmit(e, mode) {
    e.preventDefault();
    let url = `${state.base_url}${service_url}${state.api_url}`
    if (mode=='PUT') {
      url = `${url}/${state.to_edit}`
    }
    fetch(
      url,
      {
        headers: {
          'Authorization': `Bearer ${token}`
        },
        method: mode,
        body: JSON.stringify(state.form)
      }
    ).then(response => {
      if (response.ok && response.json) {                                    
        return response.json()
      } else{
        return {success: false}
      }
    })
    .then(res => {
      if (res.success) {
        alert(res.message)
        setData({
          ...state,
          isLoading: true
        })
      } else {
        return alert(res.message)
      }
    }).catch(
      (e) => {
        alert('Request failed!')
        console.log(e);
      }
    )
  }

  // ****************************
  // PREPARING DATA FOR RENDERING
  // ****************************

  let data = state.data.map(
    item => ({
      // select: <input
      //   type="checkbox"
      //   value={item.name}
      //   checked={state.selected.indexOf(item.name) > -1}
      //   onChange={(e)=>onChangeCheck(e)}
      // />,
      // id: item.id,
      name: item.name,
      description: item.description,
      procedures: item.procedures.join(','),
      edit: <a onClick={()=>onEdit(item)}>🔧</a>
    })
  )

  const form_data = [
    {
      title: state.title,
      inputs: [
        {
          label: 'Name',
          type: 'text',
          value: state.form.name,
          onChange:(e) => onChange('name', e)
        },
        {
          label: 'Description',
          type: 'text',
          value: state.form.description,
          onChange:(e) => onChange('description', e)
        }
      ]
    }
  ]

  return <div className='container'>
    <div className="containertitle">
      <div className='title'><div>{state.title}</div></div>
    </div>
    <div className="containercontent">
      <SmartTable
        id={`t-uoms`}
        key={`t-uoms`}
        init_data={
          {
            alias: {
              // select: 'Select',
              name: 'Name',
              description: 'Description',
              procedures: 'Procedures',
              edit: "Edit"
            },
            data: data
          }
        }
        pagination={false}
        filter={true}
        onAdd={() => onAdd()}
        onRemove={(uoms) => onRemove(uoms)}
      />
    </div>
    {
      state.modal ? 
      <Modal onModalClose={onModalClose} title={state.edit ? `Modify \'${state.form.name}\' unit of measure` : 'Add a new unit of measure'}>
        <Form
          onSubmit= {state.edit ? (e)=>onSubmit(e, 'PUT') : (e)=>onSubmit(e, 'POST')}
          fields={form_data}
          edit={true}
        />
      </Modal> :
      <></>
    }
  </div>
}

export default UomsContainer