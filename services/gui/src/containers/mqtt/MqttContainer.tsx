
// ext lib
import { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import Form from '../../components/form/form'

const init_data = {
  tag: 'mqtt',
  title: 'MQTT broker',
  isLoading: true,
  form: {
    broker_url: "",
    broker_port: 1883,
    broker_topic: "/istsos/",
    broker_user: "",
    broker_password: ""
  },
  base_url: `/istsos/wa/istsos/services`,
  api_url: 'configsections/mqtt',
  data: {},
  message: "",
  error: "",
}


const MqttContainer: React.FC<{service: string, token: string, user?: string}> = ({service, token, user}) => {
  const [state, setData] = useState(init_data)
  const router = useRouter()

  let service_url = service ? `/${service}/` : '/' 

  useEffect(
    () => {
      fetch(
        `${state.base_url}${service_url}${state.api_url}`,
        {
          headers: {
            'Authorization': `Bearer ${token}`
          },
          method: 'GET',
        }
      ).then(
        (response) => {
          if (response.ok && response.json) {                                    
            return response.json()
          } else{
            return {success: false}
          }
        }
      ).then(
        (res) => {
          if (res.success) {
            return setData({
              ...state,
              form: {
                ...state.form,
                ...res.data
              },
              data: {
                ...state.data,
                ...res.data
              },
              isLoading: false
            })
          } else {
            return alert(res.message)
          }
        }
      )
    }
  , [service])

  // ********
  // FUNCs
  // *******
  function onChange(key, e) {
    setData({
      ...state,
      form: {
        ...state.form,
        [key]: e.target.value
      }
    })
  }
  function onSubmit(e) {
    e.preventDefault();
    fetch(
      `${state.base_url}${service_url}${state.api_url}`,
      {
        headers: {
          'Authorization': `Bearer ${token}`
        },
        method: 'PUT',
        body: JSON.stringify(state.form)
      }
    ).then(response => {
      if (response.ok && response.json) {                                    
        return response.json()
      } else{
        return {success: false}
      }
    })
    .then(res => {
      if (res.success) {
        router.push({
          ...router,
          query: {
            ...router.query,
            reload: Math.floor(Math.random() * (50 - 0 + 1)) + 0
          }
        })
        return alert(res.message)
      } else {
        return alert(res.message)
      }
    }).catch(
      (e) => {
        alert('Request failed!')
        console.log(e);
      }
    )
  }
  const form_data = [
    {
      title: state.title,
      inputs: [
        {
          label: 'URL',
          type: 'text',
          value: state.form.broker_url,
          onChange: user==='viewer' ? (e) => (e) : (e) => onChange('broker_url', e)
        },
        {
          label: 'Port',
          type: 'text',
          value: state.form.broker_port,
          onChange: user==='viewer' ? (e) => (e) : (e) => onChange('broker_port', e)
        },
        {
          label: 'Topic base',
          type: 'text',
          value: state.form.broker_topic,
          onChange: user==='viewer' ? (e) => (e) : (e) => onChange('broker_topic', e)
        },
        {
          label: 'User',
          type: 'text',
          value: state.form.broker_user,
          onChange: user==='viewer' ? (e) => (e) : (e) => onChange('broker_user', e)
        },
        {
          label: 'Password',
          type: 'password',
          value: state.form.broker_password,
          onChange: user==='viewer' ? (e) => (e) : (e) => onChange('broker_password', e)
        }
      ]
    }
  ]
  return <div className='container'>
    <Form
      onSubmit={onSubmit}
      fields={form_data}
      edit={user==='viewer' ? false : true}
    />
  </div>
}

export default MqttContainer