import {useReducer, useEffect, useState} from 'react'

// external components
import {useRouter} from 'next/router'
import Loader from 'react-loader-spinner'
import withSize from 'react-sizeme'

// components
import Layout from '../../components/layout/layout'
import SideLayout from '../../components/layout/sidelayout'
import SideBar from '../../components/sidebar/sidebar'

// reducers
import { initialServer, actionsReducer } from "../../reducers/server/reducer";

// containers
import DatabaseContainer from '../database/DatabaseContainer'
import ProviderContainer from '../provider/ProviderContainer'
import ObservedPropertyContainer from '../observedproperty/ObservedPropertyContainer';
import ProcedureContainer from '../procedure/ProcedureContainer';
import IdentificationContainer from '../identification/IdentificationContainer';
import CoordinatesContainer from '../coordinates/CoordinatesContainer';
import MqttContainer from '../mqtt/MqttContainer';
import GetObservationConfContainer from '../getobservation/GetobservationConfContainer';
import ProxyContainer from '../proxy/ProxyContainer';
import StatusContainer from '../status/StatusContainer';

const IndexContainer: React.FC<{
  authenticated:boolean, token: string, logout: Function, user: any, size: any, role: string
  }> = ({authenticated, token, logout, user, size, role}) => {
  const [state, setData] = useState({reload: 0})
  const [reducer, dispatch] = useReducer(actionsReducer, initialServer)
  const router = useRouter()

  // ************
  //  CONTENT
  // ************

  useEffect(
    () => {
      var promises = initialServer.map((item, index) => {
        if (item.isLoading && item.url) {
          return fetch(
            item.url,
            {
              method: 'GET',
              headers: {
                  'Accept': 'application/json',
                  'Authorization': 'Bearer ' + token
              }
            }
          ).then(
            res => {
              if (res.status == 200) {
                return res.json()
              } else {
                return {
                  success: false,
                  error: 'Can\'t retrieve data' 
                }
              }
            }
          )  
        } else {
          return item
        }
        
      })
      Promise.all(promises).then(
        res => {
          return reducer.map(
            (item, index) => {
              return {
                ...item,
                data: res[index].data,
                isLoading: false
              }
            }
          )
        }
      ).then(
        res => dispatch({
          data: res,
          type: 'LOAD'
        })
      )
      
    }
  , [state.reload])

  // ***********
  // remove func
  // ***********

  function reload() {
    setData({
      ...state,
      reload: state.reload+=1
    })
  }

  // ************
  //  CONTENT
  // ************

  function createContent(e) {
    switch (e.type) {
      case 'observedproperty':
        return <ObservedPropertyContainer service={"default"} token={token} />
      case 'database':
        return <DatabaseContainer service={"default"} token={token} />
      case 'provider':
        return <ProviderContainer service={"default"} token={token} />
      case 'identification':
        return <IdentificationContainer service={"default"} token={token} />
      case 'coordinates':
        return <CoordinatesContainer service={"default"} token={token} />
      case 'mqtt':
        return <MqttContainer service={"default"} token={token} />
      case 'getobservation':
        return <GetObservationConfContainer service={"default"} token={token} />
      case 'serviceurl':
        return <ProxyContainer service={"default"} token={token} />
      case 'status':
        return <StatusContainer service={""} token={token} reload={reload}/>
      default:
        return <div>Sorry content N/A. Something wrong.</div>
    }
  }
  
  // ************
  //  RENDERING
  // ************
  let isLoading = reducer.filter(item => item.isLoading);
  let content = <div>Sorry N/A. Something wrong</div>;
  let type_selected = 'status'
  if (router.query) {
    if (router.query.id) {
      let reducer_filtered = reducer.filter(
        item => (item.type === router.query.id)
      )[0]
      content = createContent(reducer_filtered)
      if (typeof(router.query.id) === 'string') {
        type_selected = router.query.id
      }  
    } else {
      content = createContent({
        type: 'status'
      })
    }
  } else {
    content = createContent({
      type: 'status'
    })
  }

  return <Layout authenticated={authenticated} user={user} logout={logout} role={role}>
    {
      isLoading.length > 0 ? <div className="loader">
        <div className='loading-gif'>
          <Loader
              type="Bars"
              color="#000"
              height={100}
              width={100}
          />
        </div>
      </div> : <></>
    }
    <SideBar
      data={reducer.map(
        item => ({
          ...item,
          pathname: '/',
          query: {
            id: item.tag
          },
          selected: type_selected === item.tag
        })
      )}
      navlist={reducer.length > 0 ? reducer[0].data.map(
        (item, index)=>({
          label: item.service,
          value: index+1,
          pathname: `/service`,
          query: {
            id: item.service
          }
        })    
      ) : []}
      title="istSOS"
    />
    {content}
  </Layout>
}

export default withSize({monitorHeight: true})(IndexContainer);