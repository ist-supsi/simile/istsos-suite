
// ext lib
import { useRouter } from 'next/router';
import {useEffect, useState} from 'react'

// components
import SmartTable from '../../components/table/smarttable'
import Modal from '../../components/modal/modal';
import Form from '../../components/form/form'
import AddRemoveBtns from '../../components/buttons/addremove'

const init_data = {
  title: 'Observed properties',
  base_url: `/istsos/wa/istsos/services`,
  api_url: 'observedproperties',
  form: {
    name: "",
    definition: "",
    description: "",
    constraint: {},
    // constraint: {
    //     role: "un:x-ogc:def:classifiers:x-istsos:1.0:qualityIndexCheck:level0",
    //     interval: ["", ""]
    // },
  },
  default: {
      name: "",
      definition: "urn:ogc:def:parameter:x-istsos:1.0:",
      description: "",
      constraint: {},
      // constraint: {
      //     role: "un:x-ogc:def:classifiers:x-istsos:1.0:qualityIndexCheck:level0",
      //     interval: ["", ""]
      // },
  },
  constraint_edit: {},
  data: [],
  isLoading: true,
  selected: [],
  to_edit: '',
  modal: false,
  edit: false
}


const ObservedPropertyContainer: React.FC<{service: string, token: string}> = ({service, token}) => {
  const [ state, setData] = useState(init_data)

  const router = useRouter();

  let service_url = service ? `/${service}/` : '/' 

  useEffect(
    ()=>{
      if (state.isLoading) {
        fetch(
          `${state.base_url}${service_url}${state.api_url}`,
          {
            headers: {
              'Authorization': `Bearer ${token}`
            },
            method: 'GET',
          }
        ).then(
          (response) => {
            if (response.ok && response.json) {                                    
              return response.json()
            } else{
              return {success: false}
            }
          }
        ).then(
          (res) => {
            if (res.success) {
              return setData({
                ...state,
                form: {
                  ...state.form,
                  ...state.default
                },
                data: [
                  ...res.data
                ],
                isLoading: false,
                selected: [],
                modal: false
              })
            } else {
              return alert(res.message)
            }
          }
        )
      }
    }
  , [service, state.isLoading])

  function onModalClose() {
    setData({
      ...state,
      modal: !state.modal
    })
  }
  function onAdd() {
    setData({
      ...state,
      form: {
        ...state.default,
        name: ''
      },
      modal: !state.modal,
      edit: false
    }) 
  }
  function onEdit(item) {
    setData({
      ...state,
      form: {
        ...state.form,
        ...item
      },
      constraint_edit: {...item.constraint},
      modal: !state.modal,
      to_edit: item.definition,
      edit: true
    })
  }
  function onRemove(obs) {
    if (obs.length === 0) {
      alert('No observedprotperty(s) selected.')
    } else {
      let ob_names = obs.map(i => i['name'])
      if (
        confirm(
          `This operation cannot be undone!\n\nAre you sure you want to erase the following observedproperty(s)?\n${ob_names}`)
        ) {
          let promises = [];
          for (let index = 0; index < obs.length; index++) {
            const ob_name = obs[index]['definition'];
            let options = {
              method: 'DELETE',
              headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + token 
              },
            } 
            promises.push(fetch(
              `${state.base_url}${service_url}${state.api_url}/${ob_name}`,
              options
            ).then(
              res => {
                if (res.status === 200) {
                  return res.json()
                } else {
                  throw 'Delete request failed.'
                }
            }).then(
              res => {
                if (res.success) {
                  return {success: true}
                } else {
                  return {success: false, error: res.message}
                }
              }
            ).catch(
              err => ({success: false, error: err})
            ))
              
          }
          Promise.all(promises).then(
            res => {
              let index_deleted = [];
              let msg_errors = []
              for (let index = 0; index < res.length; index++) {
                const element = res[index];
                if (element.success) {
                  index_deleted.push(index)
                } else {
                  alert(element.error)
                }
              }
              if (index_deleted.length>0) {
                setData({
                  ...state,
                  isLoading: true
                })
              }
              
            }
          )
      }
    }
  }
  function onChangeCheck(e) {
    if (e.target.checked) {
      setData({
        ...state,
        selected: [
          ...state.selected,
          e.target.value
        ]
      })
    } else {
      let new_array = state.selected.filter(item=>item!==e.target.value)
      setData({
        ...state,
        selected: new_array
      })
    }
    
  }
  function onChange(key, e) {
    if (key==='constraint' && e.target.value) {
      let v = {
        role: "urn:x-ogc:def:classifiers:x-istsos:1.0:qualityIndexCheck:level0"
      }
      if (!state.edit) {
        if (e.target.value === 'min') {
          v['min'] = ''
        } else if (e.target.value === 'max') {
          v['max'] = ''
        } else if (e.target.value === 'between') {
          v['interval'] = ['', '']
        } else if (e.target.value === 'list') {
          v['valueList'] = ''
        }
      } else {
        if (Object.keys(state.constraint_edit).includes(e.target.value)) {
          v = {...v, ...state.constraint_edit}
        } else if (e.target.value === 'min') {
          v['min'] = ''
        } else if (e.target.value === 'max') {
          v['max'] = ''
        } else if (e.target.value === 'between') {
          if (Object.keys(state.constraint_edit).includes('between')) {
            v = {...v, ...state.constraint_edit}
          } else {
            v['interval'] = ['', ''] 
          }
        } else if (e.target.value === 'list') {
          if (Object.keys(state.constraint_edit).includes('valueList')) {
            v = {...v, ...state.constraint_edit}
          } else {
            v['valueList'] = '' 
          }
        }
      }
      setData({
        ...state,
        form: {
          ...state.form,
          [key]: {...v}
        }
      })
    } else if (key==='constraint-options') {
      if (Object.keys(state.form.constraint).includes('interval')) {
        let newArray = [...state.form.constraint['interval']]
        if (e.target.id==='max') {
          newArray[1] = e.target.value
        } else {
          newArray[0] = e.target.value
        }
        setData({
          ...state,
          form: {
            ...state.form,
            constraint: {
              ...state.form.constraint,
              interval: [...newArray]
            }
          }
        })
      } else {
        setData({
          ...state,
          form: {
            ...state.form,
            constraint: {
              ...state.form.constraint,
              [e.target.id]: `${e.target.value}`
            }
          }
        })
      }
    } else {
      setData({
        ...state,
        form: {
          ...state.form,
          [key]: e.target.value
        }
      })
    }
  }
  function onSubmit(e, mode) {
    e.preventDefault();
    let url = `${state.base_url}${service_url}${state.api_url}`
    if (mode=='PUT') {
      url = `${url}/${state.to_edit}`
    }
    fetch(
      url,
      {
        headers: {
          'Authorization': `Bearer ${token}`
        },
        method: mode,
        body: JSON.stringify(state.form)
      }
    ).then(response => {
      if (response.ok && response.json) {                                    
        return response.json()
      } else{
        return {success: false}
      }
    })
    .then(res => {
      if (res.success) {
        alert(res.message)
        setData({
          ...state,
          isLoading: true
        })
      } else {
        return alert(res.message)
      }
    }).catch(
      (e) => {
        alert('Request failed!')
        console.log(e);
      }
    )
  }

  // ****************************
  // PREPARING DATA FOR RENDERING
  // ****************************

  let data = state.data.map(
    item => ({
      // select: <input
      //   type="checkbox"
      //   value={item.definition}
      //   checked={state.selected.indexOf(item.definition) > -1}
      //   onChange={(e)=>onChangeCheck(e)}
      // />,
      // id: item.id,
      name: item.name,
      definition: item.definition,
      description: item.description,
      procedures: item.procedures.join(','),
      edit: <a onClick={()=>onEdit(item)}>🔧</a>
    })
  )

  const form_data = [
    {
      title: state.title,
      inputs: [
        {
          label: 'Name',
          type: 'text',
          value: state.form.name,
          onChange:(e) => onChange('name', e)
        },
        {
          label: 'Definition URN',
          type: 'text',
          value: state.form.definition,
          onChange:(e) => onChange('definition', e)
        },
        {
          label: 'Description',
          type: 'text',
          value: state.form.description,
          onChange:(e) => onChange('description', e)
        },
        {
          label: 'Constraint',
          type: 'select',
          value: state.form.constraint ? Object.keys(state.form.constraint).includes('min') ? 'min' : Object.keys(state.form.constraint).includes('max') ? 'max' : Object.keys(state.form.constraint).includes('interval') ? 'between' : '' : '',
          options: [{
            value: "",
            text: "None",
          },{
            value: "max",
            text: "Greater then",
          }, {
            value: "min",
            text: "Less then",
            selected: false
          }, {
            value: "between",
            text: "Between",
          }, {
            value: "list",
            text: "Value list (comma separated)",
          }],
          onChangeSelect:(e) => onChange('constraint', e)
        },
        {
          id: 'min',
          label: 'From',
          type: 'number',
          hidden: state.form.constraint ? Object.keys(state.form.constraint).includes('min') ? false : Object.keys(state.form.constraint).includes('interval') ? false : true : true,
          value: state.form.constraint ? Object.keys(state.form.constraint).includes('min') ? state.form.constraint['min'] : Object.keys(state.form.constraint).includes('interval') ? state.form.constraint['interval'][0] : '' : '',
          onChange:(e) => onChange('constraint-options', e)
        },
        {
          id: 'max',
          label: 'To',
          type: 'number',
          hidden: state.form.constraint ? Object.keys(state.form.constraint).includes('max') ? false : Object.keys(state.form.constraint).includes('interval') ? false : true : true,
          value: state.form.constraint ? Object.keys(state.form.constraint).includes('max') ? state.form.constraint['max'] : Object.keys(state.form.constraint).includes('interval') ? state.form.constraint['interval'][1] : '' : '',
          onChange:(e) => onChange('constraint-options', e)
        },
        {
          id: 'valueList',
          label: 'List',
          type: 'text',
          hidden: state.form.constraint ? !Object.keys(state.form.constraint).includes('valueList') : true,
          value: state.form.constraint ? state.form.constraint['valueList'] : '',
          onChange:(e) => onChange('constraint-options', e)
        }
      ]
    }
  ]

  return <div className='container'>
    <div className="containertitle">
      <div className='title'><div>{state.title}</div></div>
      {/* <AddRemoveBtns
        onAdd={() => onAdd()}
        onRemove={(obs) => onRemove(obs)}
      /> */}
    </div>
    <div className="containercontent">
      <SmartTable
        id={`t-observed-properties`}
        key={`t-observed-properties`}
        init_data={
          {
            alias: {
              // select: 'Select',
              name: 'Name',
              definition: 'URN',
              description: 'Description',
              procedures: 'Procedures',
              edit: "Edit"
            },
            data: data
          }
        }
        pagination={false}
        filter={true}
        onAdd={() => onAdd()}
        onRemove={(obs) => onRemove(obs)}
      />
    </div>
    {
      state.modal ? 
      <Modal onModalClose={onModalClose} title={state.edit ? `Modify \'${state.form.name}\' observed property` : 'Add a new observed property'}>
        <Form
          onSubmit= {state.edit ? (e)=>onSubmit(e, 'PUT') : (e)=>onSubmit(e, 'POST')}
          fields={form_data}
          edit={true}
        />
      </Modal> :
      <></>
    }
  </div>
}

export default ObservedPropertyContainer