
// ext lib
import { useRouter } from 'next/router';
import {useEffect, useState} from 'react'

// components
import SmartTable from '../../components/table/smarttable'
import Modal from '../../components/modal/modal';
import Form from '../../components/form/form'
import AddRemoveBtns from '../../components/buttons/addremove'

const init_data = {
  title: 'Observed properties',
  base_url: `/istsos/wa/istsos/services`,
  api_url: 'observedproperties',
  form: {
    name: "",
    definition: "",
    description: "",
    constraint: {},
    // constraint: {
    //     role: "un:x-ogc:def:classifiers:x-istsos:1.0:qualityIndexCheck:level0",
    //     interval: ["", ""]
    // },
  },
  default: {
      name: "",
      definition: "urn:ogc:def:parameter:x-istsos:1.0:",
      description: "",
      constraint: {},
      // constraint: {
      //     role: "un:x-ogc:def:classifiers:x-istsos:1.0:qualityIndexCheck:level0",
      //     interval: ["", ""]
      // },
  },
  data: [],
  isLoading: true,
  selected: [],
  to_edit: '',
  modal: false,
  edit: false
}


const ObservedPropertyViewerContainer: React.FC<{service: string, token: string}> = ({service, token}) => {
  const [ state, setData] = useState(init_data)

  const router = useRouter();

  let service_url = service ? `/${service}/` : '/' 

  useEffect(
    ()=>{
      if (state.isLoading) {
        fetch(
          `${state.base_url}${service_url}${state.api_url}`,
          {
            headers: {
              'Authorization': `Bearer ${token}`
            },
            method: 'GET',
          }
        ).then(
          (response) => {
            if (response.ok && response.json) {                                    
              return response.json()
            } else{
              return {success: false}
            }
          }
        ).then(
          (res) => {
            if (res.success) {
              return setData({
                ...state,
                form: {
                  ...state.form,
                  ...state.default
                },
                data: [
                  ...res.data
                ],
                isLoading: false,
                selected: [],
                modal: false
              })
            } else {
              return alert(res.message)
            }
          }
        )
      }
    }
  , [service, state.isLoading])

  // ****************************
  // PREPARING DATA FOR RENDERING
  // ****************************

  let data = state.data.map(
    item => ({
      name: item.name,
      definition: item.definition,
      description: item.description,
      procedures: item.procedures.join(','),
    })
  )

  const form_data = [
    {
      title: state.title,
      inputs: [
        {
          label: 'Name',
          type: 'text',
          value: state.form.name,
        },
        {
          label: 'Definition URN',
          type: 'text',
          value: state.form.definition,
        },
        {
          label: 'Description',
          type: 'text',
          value: state.form.description,
        }
      ]
    }
  ]

  return <div className='container'>
    <div className="containertitle">
      <div className='title'><div>{state.title}</div></div>
    </div>
    <div className="containercontent">
      <SmartTable
        id={`t-observed-properties`}
        key={`t-observed-properties`}
        init_data={
          {
            alias: {
              name: 'Name',
              definition: 'URN',
              description: 'Description',
              procedures: 'Procedures',
            },
            data: data
          }
        }
        pagination={false}
        filter={true}
      />
    </div>
  </div>
}

export default ObservedPropertyViewerContainer