
// ext lib
import { useRouter } from 'next/router';
import {useEffect, useState} from 'react'

// components
import SmartTable from '../../components/table/smarttable'
import Modal from '../../components/modal/modal';
import EmptyAddComponent from '../../components/empty/EmptyAddComponent'
import Form from '../../components/form/form'
import AddRemoveBtns from '../../components/buttons/addremove'

const init_data = {
  title: 'Services status',
  base_url:`/istsos/wa/istsos`,
  api_post_url: 'services',
  api_url: 'operations/status',
  api_db_url: 'services/default/configsections/connection',
  api_db_test_url: `/istsos/wa/istsos/operations/validatedb`,
  form: {
    service: "",
    customdb: "on",
    user: "",
    password: "",
    host: "",
    port: 0,
    dbname: "",
    epsg: "4326"
  },
  default: {
      name: "",
      definition: "urn:ogc:def:parameter:x-istsos:1.0:",
      description: "",
      constraint: {},
      // constraint: {
      //     role: "un:x-ogc:def:classifiers:x-istsos:1.0:qualityIndexCheck:level0",
      //     interval: ["", ""]
      // },
  },
  data: [],
  data_database: {},
  isLoading: true,
  selected: [],
  to_edit: '',
  modal: false,
  edit: false
}


const StatusContainer: React.FC<{service: string, token: string, reload?: Function}> = ({service, token, reload}) => {
  const [ state, setData] = useState(init_data)

  const router = useRouter();

  let service_url = service ? `/${service}/` : '/' 

  useEffect(
    ()=>{
      if (state.isLoading) {
        let promise1 = fetch(
          `${state.base_url}${service_url}${state.api_url}`,
          {
            headers: {
              'Authorization': `Bearer ${token}`
            },
            method: 'GET',
          }
        ).then(
          (response) => {
            if (response.ok && response.json) {                                    
              return response.json()
            } else{
              return {success: false}
            }
          }
        )

        let promise2 = fetch(
          `${state.base_url}${service_url}${state.api_db_url}`,
          {
            headers: {
              'Authorization': `Bearer ${token}`
            },
            method: 'GET',
          }
        ).then(
          (response) => {
            if (response.ok && response.json) {                                    
              return response.json()
            } else{
              return {success: false}
            }
          }
        )

        Promise.all([promise1, promise2])
          .then(
            res => {
              return {
                data: res[0].data,
                data_db: res[1].data
              }
            }
          ).then(
            res => setData({
              ...state,
              form: {
                ...init_data.form,
                ...res.data_db
              },
              data: [...res.data],
              isLoading: false
            })
          )
        }
  }, [service, state.isLoading])

  function onModalClose() {
    setData({
      ...state,
      modal: !state.modal,
      edit: false
    })
  }
  function onAdd() {
    setData({
      ...state,
      form: {
        ...state.form,
        service: ''
      },
      modal: !state.modal,
      edit: false
    })
    
  }
  function onEdit(item) {
    setData({
      ...state,
      form: {
        ...state.form,
        service: item.service
      },
      modal: !state.modal,
      to_edit: item.service,
      edit: true
    })
  }
  function onRemove(services) {
    if (services.length === 0) {
      alert('No service(s) selected.')
    } else {
      if (
        confirm(
          `This operation cannot be undone!\n\nAre you sure you want to erase the service(s) selected?\n`)
        ) {
          let promises = [];
          for (let index = 0; index < services.length; index++) {
            const service_name = services[index]['service'];
            let options = {
              method: 'DELETE',
              headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + token 
              },
              body: JSON.stringify({
                service: service_name
              })
            } 
            promises.push(fetch(
              `${state.base_url}${service_url}${state.api_post_url}/${service_name}`,
              options
            ).then(
              res => {
                if (res.status === 200) {
                  return res.json()
                } else {
                  throw 'Delete request failed.'
                }
            }).then(
              res => {
                if (res.success) {
                  return {success: true}
                } else {
                  return {success: false, error: res.message}
                }
              }
            ).catch(
              err => ({success: false, error: err})
            ))
              
          }
          Promise.all(promises).then(
            res => {
              let index_deleted = [];
              let msg_errors = []
              for (let index = 0; index < res.length; index++) {
                const element = res[index];
                if (element.success) {
                  index_deleted.push(index)
                } else {
                  alert(element.error)
                }
              }
              if (index_deleted.length>0) {
                setData({
                  ...state,
                  selected: [
                    ...state.selected.filter((e,i)=> index_deleted.indexOf(i)==-1)
                  ],
                  isLoading: true
                })
                if (reload) {
                  reload()
                }
                
              }
              
            }
          )
      }
    }
  }
  function onValidate(e) {
    e.preventDefault();
    fetch(
      `${state.api_db_test_url}`,
      {
        headers: {
          'Authorization': `Bearer ${token}`
        },
        method: 'POST',
        body: JSON.stringify(state.form)
      }
    ).then(response => {
      if (response.ok && response.json) {                                    
        return response.json()
      } else{
        return {success: false}
      }
    })
    .then(res => {
      if (res.success) {
        router.push({
          ...router,
          query: {
            ...router.query,
            reload: Math.floor(Math.random() * (50 - 0 + 1)) + 0
          }
        })
        return alert(
          `${res.message}\n\nThe database is ${res.data.database}`
        )
      } else {
        return alert(res.message)
      }
    }).catch(
      (e) => {
        alert('Request failed!')
        console.log(e);
      }
    )
  }
  function onChangeCheck(e) {
    if (e.target.checked) {
      setData({
        ...state,
        selected: [
          ...state.selected,
          e.target.value
        ]
      })
    } else {
      let new_array = state.selected.filter(item=>item!==e.target.value)
      setData({
        ...state,
        selected: new_array
      })
    }
    
  }
  function onChange(key, e) {
    setData({
      ...state,
      form: {
        ...state.form,
        [key]: e.target.value
      }
    })
  }
  function onSubmit(e, mode) {
    e.preventDefault();
    // fetch(`/grafana/api/datasources/name/${state.form.service}`)
    //   .then(
    //     res => {
    //       if (res.status == 404) {
            
    //       } else {
    //         console.log('Data source already exists in grafana');
    //       }
    //     }
    //   )
    let url = `${state.base_url}${service_url}${state.api_post_url}`
    if (mode=='PUT') {
      url = `${url}/${state.to_edit}`
    }    
    
    fetch(
      url,
      {
        headers: {
          'Authorization': `Bearer ${token}`
        },
        method: mode,
        body: JSON.stringify(state.form)
      }
    ).then(response => {
      if (response.ok && response.json) {                                    
        return response.json()
      } else{
        return {success: false}
      }
    })
    .then(res => {
      if (res.success) {
        alert(res.message)
        setData({
          ...state,
          isLoading: true,
          modal: !state.modal,
          edit: false
        })
        if (reload) {
          reload()
        }
      } else {
        return alert(res.message)
      }
    }).catch(
      (e) => {
        alert('Request failed!')
        console.log(e);
      }
    )
  }

  // ****************************
  // PREPARING DATA FOR RENDERING
  // ****************************

  let data = state.data.map(
    item => ({
      // select: <input
      //   type="checkbox"
      //   value={item.service}
      //   checked={state.selected.indexOf(item.service) > -1}
      //   onChange={(e)=>onChangeCheck(e)}
      // />,
      // id: item.id,
      service: item.service,
      availability: item.availability,
      offerings: item.offerings,
      procedures: item.procedures,
      observedproperties: item['observedProperties'],
      featuresofinterest: item['featuresOfInterest'],
      getcapabilities: item.getcapabilities,
      describesensor: item.describesensor,
      getobservation: item.getobservation,
      getfeatureofinterest: item.getfeatureofinterest,
      insertobservation: item.insertobservation,
      registersensor: item.registersensor,
      edit: <a onClick={()=>onEdit(item)}>🔧</a>
    })
  )

  const form_data = [
    {
      title: state.title,
      inputs: [
        {
          label: 'Service name',
          type: 'text',
          value: state.form.service,
          onChange:(e) => onChange('service', e)
        },
        {
          label: 'EPSG',
          type: 'number',
          value: state.form.epsg,
          onChange:(e) => onChange('epsg', e)
        }
      ]
    }
  ]

  return <div className='container'>
    <div className="containertitle">
      <div className='title'><div>{state.title}</div></div>
    </div>
    {
      state.data.length == 0 ?
        <div className="containercontent">
          <EmptyAddComponent onClick={() => onAdd()}>
            No services found
          </EmptyAddComponent>
        </div>
      :
      <div className="containercontent">
        <SmartTable
          key={`t-status`}
          id={`t-status`}
          init_data={
            {
              alias: {
                // select: 'Select',
                service: "Service",
                availability: "Availability",
                offerings: "Offerings",
                procedures: "Procedures",
                observedproperties: 'ObservedProperties',
                featuresofinterest: 'FeaturesOfInterest',
                getcapabilities: "GetCapabilities",
                describesensor: "DescribeSensor",
                getobservation: "GetObservation",
                getfeatureofinterest: "GetFeatureOfInterest",
                insertobservation: "InsertObservation",
                registersensor: "RegisterSensor",
                edit: "Edit"
              },
              data: data
            }
          }
          height={350}
          pagination={false}
          filter={true}
          onAdd={() => onAdd()}
          onRemove={(services) => onRemove(services)}
        />
      </div>    
    }
    {
      state.modal ? 
      <Modal onModalClose={onModalClose} title={state.edit ? `Modify \'${state.form.service}\' service` : 'Add a new service'}>
        <Form
          onSubmit= {state.edit ? (e)=>onSubmit(e, 'PUT') : (e)=>onSubmit(e, 'POST')}
          fields={form_data}
          edit={true}
        />
      </Modal> :
      <></>
    }
  </div>
}

export default StatusContainer