
// ext lib
import { useRouter } from 'next/router';
import {useEffect, useState} from 'react'

// components
import SmartTable from '../../components/table/smarttable'
import Modal from '../../components/modal/modal';
import EmptyAddComponent from '../../components/empty/EmptyAddComponent'
import Form from '../../components/form/form'
import AddRemoveBtns from '../../components/buttons/addremove'

const init_data = {
  title: 'Services status',
  base_url:`/istsos/wa/istsos`,
  api_post_url: 'services',
  api_url: 'operations/status',
  data: [],
  data_database: {},
  isLoading: true,
  selected: [],
  to_edit: '',
  modal: false,
  edit: false
}


const StatusContainer: React.FC<{service: string, token: string, reload?: Function}> = ({service, token, reload}) => {
  const [ state, setData] = useState(init_data)

  const router = useRouter();

  let service_url = service ? `/${service}/` : '/' 

  useEffect(
    ()=>{
      let promise1 = fetch(
        `${state.base_url}${service_url}${state.api_url}`,
        {
          headers: {
            'Authorization': `Bearer ${token}`
          },
          method: 'GET',
        }
      ).then(
        (response) => {
          if (response.ok && response.json) {                                    
            return response.json()
          } else{
            return {success: false}
          }
        }
      )

      Promise.all([promise1])
        .then(
          res => {
            return {
              data: res[0].data
            }
          }
        ).then(
          res => setData({
            ...state,
            data: [...res.data],
            isLoading: false
          })
        )
  }, [service, state.isLoading])

  // ****************************
  // PREPARING DATA FOR RENDERING
  // ****************************

  let data = state.data.map(
    item => ({
      service: item.service,
      availability: item.availability,
      offerings: item.offerings,
      procedures: item.procedures,
      observedproperties: item['observedProperties'],
      featuresofinterest: item['featuresOfInterest'],
      getcapabilities: item.getcapabilities,
      describesensor: item.describesensor,
      getobservation: item.getobservation,
      getfeatureofinterest: item.getfeatureofinterest,
      insertobservation: item.insertobservation,
      registersensor: item.registersensor,
    })
  )

  return <div className='container'>
    <div className="containertitle">
      <div className='title'><div>{state.title}</div></div>
    </div>
    {
      state.data.length == 0 ?
        <div className="containercontent">
          No services found
        </div>
      :
      <div className="containercontent">
        <SmartTable
          key={`t-status`}
          id={`t-status`}
          init_data={
            {
              alias: {
                // select: 'Select',
                service: "Service",
                availability: "Availability",
                offerings: "Offerings",
                procedures: "Procedures",
                observedproperties: 'ObservedProperties',
                featuresofinterest: 'FeaturesOfInterest',
                getcapabilities: "GetCapabilities",
                describesensor: "DescribeSensor",
                getobservation: "GetObservation",
                getfeatureofinterest: "GetFeatureOfInterest",
                insertobservation: "InsertObservation",
                registersensor: "RegisterSensor"
              },
              data: data
            }
          }
          height={350}
          pagination={false}
          filter={true}
        />
      </div>
    }
  </div>
}

export default StatusContainer