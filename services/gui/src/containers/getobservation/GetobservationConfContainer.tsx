
// ext lib
import { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import Form from '../../components/form/form'

const init_data = {
  tag: 'getobservation',
  title: 'GetObservation configuration',
  isLoading: true,
  form: {
    aggregatenodata: "",
    maxgoperiod: "",
    aggregatenodataqi: "",
    defaultqi: "",
    correct_qi: "",
    stat_qi: "",
    transactional_log: "true"
  },
  base_url: `/istsos/wa/istsos/services`,
  api_url: 'configsections/getobservation',
  api_dq_url: 'dataqualities',
  data: {},
  data_dq: [],
  message: "",
  error: "",
}


const GetObservationConfContainer: React.FC<{service: string, token: string}> = ({service, token}) => {
  const [state, setData] = useState(init_data)
  const router = useRouter()

  let service_url = service ? `/${service}/` : '/' 

  useEffect(
    () => {
      let promise1 = fetch(
        `${state.base_url}${service_url}${state.api_url}`,
        {
          headers: {
            'Authorization': `Bearer ${token}`
          },
          method: 'GET',
        }
      ).then(
        (response) => {
          if (response.ok && response.json) {                                    
            return response.json()
          } else{
            return {success: false}
          }
        }
      )

      let promise2 = fetch(
        `${state.base_url}${service_url}${state.api_dq_url}`,
        {
          headers: {
            'Authorization': `Bearer ${token}`
          },
          method: 'GET',
        }
      ).then(
        (response) => {
          if (response.ok && response.json) {                                    
            return response.json()
          } else{
            return {success: false}
          }
        }
      )

      Promise.all([promise1, promise2])
        .then(
          res => {
            return {
              data: res[0].data,
              data_dq: res[1].data
            }
          }
        ).then(
          res => setData({
            ...state,
            form: {
              ...state.form,
              ...res.data
            },
            data: {
              ...res.data
            },
            data_dq: [...res.data_dq],
            isLoading: false
          })
        )

      // setData({
      //   ...state,
      //   form: {
      //     ...state.form,
      //     ...res.data
      //   },
      //   data: {
      //     ...res.data
      //   }
      // })

      // setData({
      //   ...state,
      //   data_dq: [...res.data],
      //   isLoading: false
      // })
    }
  , [service])

  // ********
  // FUNCs
  // *******
  function onChange(key, e) {
    setData({
      ...state,
      form: {
        ...state.form,
        [key]: e.target.value
      }
    })
  }

  function onCheck(key) {
    setData({
      ...state,
      form: {
        ...state.form,
        [key]: !state.form[key]
      }
    })
  }

  function onSubmit(e) {
    e.preventDefault();
    fetch(
      `${state.base_url}${service_url}${state.api_url}`,
      {
        headers: {
          'Authorization': `Bearer ${token}`
        },
        method: 'PUT',
        body: JSON.stringify(state.form)
      }
    ).then(response => {
      if (response.ok && response.json) {                                    
        return response.json()
      } else{
        return {success: false}
      }
    })
    .then(res => {
      if (res.success) {
        router.push({
          ...router,
          query: {
            ...router.query,
            reload: Math.floor(Math.random() * (50 - 0 + 1)) + 0
          }
        })
        return alert(res.message)
      } else {
        return alert(res.message)
      }
    }).catch(
      (e) => {
        alert('Request failed!')
        console.log(e);
      }
    )
  }

  const form_data = [
    {
      title: state.title,
      inputs: [
        {
          label: 'Max request interval (hours)',
          type: 'text',
          value: state.form.maxgoperiod,
          onChange:(e) => onChange('maxgoperiod', e)
        },
        {
          label: 'Transactional request logger',
          type: 'checkbox',
          value: state.form.transactional_log,
          onChange:(e) => onChange('transactional_log', e)
        },
        {
          label: 'Default quality index:',
          type: 'select',
          value: state.form.defaultqi,
          onChangeSelect:(e) => onChange('defaultqi', e),
          options: state.data_dq.map(
            item => ({
              value: item.code,
              text: `${item.code} - ${item.name}`
            })
          )
        },
      ]
    }
  ]
  
  return <div className='container'>
    <div className="containertitle">
      <div className='title'><div>{state.title}</div></div>
    </div>
    <div className="containercontent">
      <Form
        onSubmit={onSubmit}
        fields={form_data}
        edit={true}
      />
    </div>
  </div>
}

export default GetObservationConfContainer