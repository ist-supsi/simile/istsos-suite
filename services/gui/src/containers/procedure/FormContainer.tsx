
// ext lib
import { SettingsInputAntennaTwoTone } from '@material-ui/icons';
import { useState, useEffect } from 'react'
import FormProcedure from '../../components/form/formprocedure';
import FormEditProcedure from '../../components/form/formeditprocedure';
import { Toast } from '../../components/toast/toast'
import INIT_DATA from '../../states/init_form_procedures'

interface output {
  name: string,
  definition: string,
  uom: string,
  description: string,
  constraint: {}
}

interface item {
  name: string,
  definition: string,
  value: string
}

const FormContainer: React.FC<{
    systemtype: string, token: string,
    observedproperties: output[],
    uoms: item[], service_url: string,
    procedures: any[], config_data: any,
    service: string, reload: Function, edit: string
  }> = ({
    systemtype, token,
    observedproperties, uoms,
    service_url, procedures,
    config_data, service, reload, edit
  }) => {
  
  const [state, setData] = useState(INIT_DATA.INIT_DATA)

  useEffect(
    () => {
      if (edit) {
        let url = `${state.base_url}${service_url}${state.api_url}/${edit}`
        
        fetch(
          url,
          {
            headers: {
              'Authorization': `Bearer ${token}`
            },
            method: 'GET',
          }
        ).then(response => {
          if (response.ok && response.json) {                                    
            return response.json()
          } else{
            return {success: false}
          }
        }).then(
          res => {
            if (res['success']) {
              setData({
                ...state,
                form: {
                  ...state.form,
                  ...res['data']
                }
              })
            }
          }
        )
      }
    }
  , [])

  function checkParams(form) {
    if (form.system_id==="") {
      setData({
        ...state,
        toasts: [...[{
          id: 0,
          description: <p>Please insert the procedure <strong>name</strong>.</p>,
          title: 'Missing information',
          backgroundColor: '#CE0E2D'
        }]]
      })
      return false
    }
    if (form.description==="") {
      setData({
        ...state,
        toasts: [...[{
          id: 0,
          description: <p>Please insert the procedure <strong>description</strong>.</p>,
          title: 'Missing information',
          backgroundColor: '#CE0E2D'
        }]]
      })
      return false
    }
    if (form.keywords==="") {
      setData({
        ...state,
        toasts: [...[{
          id: 0,
          description: <p>Please insert the procedure <strong>keywords</strong>.</p>,
          title: 'Missing information',
          backgroundColor: '#CE0E2D'
        }]]
      })
      return false
    }
    if (form.classification[1].value==="") {
      setData({
        ...state,
        toasts: [...[{
          id: 0,
          description: <p>Please insert the procedure <strong>sensor type</strong>.</p>,
          title: 'Missing information',
          backgroundColor: '#CE0E2D'
        }]]
      })
      return false
    }
    if (form.location.properties.name==="") {
      setData({
        ...state,
        toasts: [...[{
          id: 0,
          description: <p>Please insert the procedure <strong>feature of interest name</strong>.</p>,
          title: 'Missing information',
          backgroundColor: '#CE0E2D'
        }]]
      })
      return false
    }
    if (form.classification[0].value==='virtual-profile') {
      if (form.procedures.length===0) {
        setData({
          ...state,
          toasts: [...[{
            id: 0,
            description: <p>Please select at leat one <strong>insitu-fixed-point procedure</strong>.</p>,
            title: 'Missing information',
            backgroundColor: '#CE0E2D'
          }]]
        })
        return false
      }
    } else {
      if (form.outputs.length<=1) {
        setData({
          ...state,
          toasts: [...[{
            id: 0,
            description: <p>Please select at leat one <strong>observed properties</strong>.</p>,
            title: 'Missing information',
            backgroundColor: '#CE0E2D'
          }]]
        })
        return false
      }
    }
    let contacts = prepareContacts(form.contacts)
    let missing = contacts.filter(
      item => item.missing.length > 0
    )
    if (missing.length>0) {
      let role = missing[0].role.split(':')
      setData({
        ...state,
        toasts: [...[{
          id: 0,
          description: <p>Fill the {role[role.length-1]} contact field {missing[0].missing[0]}.</p>,
          title: 'Missing information',
          backgroundColor: '#CE0E2D'
        }]]
      })
      return false
    }

    return {
      ...form,
      contacts: [
        ...contacts.map(
          i => ({
            ...omit(i, ['missing'])
          })
        )
      ]
    }
  }

  function omit (obj, arr) {
    let tmp_obj = Object.keys(obj)
      .filter(k => !arr.includes(k))
      .reduce((acc, key) => ((acc[key] = obj[key]), acc), {})
    return tmp_obj
  }

  function prepareContacts(contacts) {
    let missing_info = []
    let contacts_filtered = contacts.filter(
      contact => {
        let lengthContactKeys = Object.keys(contact).length-1
        let tmp_obj = Object.keys(contact)
          .filter(i => contact[i]==="" && !contact[i].includes('role'))
        if (tmp_obj.length===lengthContactKeys) {
          return false
        } else {
          missing_info.push(tmp_obj)
          return true
        }
      }
    )

    if (contacts_filtered.length>0) {
      return contacts_filtered.map(
        (con, index) => ({
          ...con,
          missing: missing_info[index]
        }
      ))
    } else {
      return []
    }
  }

  function onSubmit(method, form) {
    let checked = checkParams(form)
    console.log(checked);
    
    if (checked) {
      if (systemtype==='virtual-profile') {
        checked.outputs = checked.outputs.map(
          item => ({
            constraint: {},
            ...item
          })
        )
        if (checked.outputs.filter(item=> item.name == "water-depth").length == 0) {
          
          checked.outputs.push({
            "name": "water-depth",
            "definition": "urn:ogc:def:parameter:x-istsos:1.0:water:depth",
            "uom": "m",
            "description": "",
            "constraint": {}
          })
          var form_obj = {
            ...omit(
              checked, [
                'assignedid', 'message', 'error'
              ]
            )
          }
          import('../../wasm/profile/pkg/profile')
            .then(res => res.create(
              form_obj,
              service,
              {
                method: "POST",
                auth:  `Bearer ${token}`
              },
              `/istsos/wa/istsos/services`
            ).then(
              res=> {
                alert(res.message)
                setData({
                  ...state,
                  isLoading: true,
                })
                return res
            })
            .catch(
              err => alert(err.message)
            )
          ).catch(
            err => alert(err.message)
          )
        }
      } else {
        let url = `${state.base_url}${service_url}${state.api_url}`
        if (method=='PUT') {
          url = `${url}/${state.to_edit}`
        }    
        fetch(
          url,
          {
            headers: {
              'Authorization': `Bearer ${token}`
            },
            method: method,
            body: JSON.stringify(checked)
          }
        ).then(response => {
          if (response.ok && response.json) {                                    
            return response.json()
          } else{
            return {
              success: false,
              message: `Request failed - ${response.status}`
            }
          }
        })
        .then(res => {
          if (res.success) {
            // alert(res.message)
            var text_message = res.message;
            if (text_message.includes('AssignedSensorId')) {
              var toast = [{
                id: 3,
                description: 'Sensor registered!',
                title: 'Success',
                backgroundColor: '#5cb85c'
              }]
              setData({
                ...state,
                // to_edit: '',
                // edit: false,
                // isLoading: true,
                toasts: [...toast]
              })
              reload()
            } else {
              var toast = [{
                id: 2,
                description: 'Something wrong',
                title: 'Warning',
                backgroundColor: '#f0ad4e'
              }]
              setData({
                ...state,
                // to_edit: '',
                // edit: false,
                // isLoading: true,
                toasts: [...toast]
              })
            }
          } else {
            var err_msg = typeof(res.message) === 'string' ? `${res.message}` : '' 
            var toast = [{
              id: 1,
              description: err_msg,
              title: 'Error',
              backgroundColor: '#d9534f'
            }]
            setData({
              ...state,
              // to_edit: '',
              // edit: false,
              toasts: [...toast]
            })
          }
        }).catch(
          (e) => {
            var toast = [{
              id: 1,
              description: `Request failed`,
              title: 'Error',
              backgroundColor: '#d9534f'
            }]
            setData({
              ...state,
              // to_edit: '',
              // edit: false,
              toasts: [...toast]
            })
          }
        )
      }
    }
  }  

  return <div className='container' key={systemtype}>
    {
      edit ?
      <FormEditProcedure
        onSubmitFunc={
          (form) => onSubmit('PUT', form)
        }
        allowedepsg={config_data.geo.allowedepsg}
        defaultepsg={config_data.geo.istsosepsg}
        systemtype={systemtype}
        form={state.form}
      /> :
      <FormProcedure
        // onSubmitFunc={
        //   state.form.classification[0].value == 'virtual-profile' ?
        //   state.edit ? () => onSubmit('PUT') : () => onSubmitPro(test_data) :
        //   () => onSubmit(state.edit ? 'PUT' : 'POST')
        // }
        // onSubmitFunc={
        //   (form) => onSubmit('POST', form)
        // }
        onSubmitFunc={
          edit ?
          (form) => onSubmit('PUT', form) :
          (form) => onSubmit('POST', form)
        }
        allowedepsg={config_data.geo.allowedepsg}
        defaultepsg={config_data.geo.istsosepsg}
        systemtype={systemtype}
        table_data={
          systemtype === 'virtual-profile' ?
          procedures.filter(item => item.sensortype==='insitu-fixed-point') :
          observedproperties
        }
        uoms={uoms}
        edit={false}
        // onAdd={() => onAdd()}
        // onRemove={(procedures) => onRemove(procedures)}
      />
    }
    {
      state.toasts.length > 0 ? 
      <Toast
        toastList={state.toasts}
        autoDelete={true}
        dismissTime={5000}
      /> : <></>
    }
    
  </div>
}

export default FormContainer