
// ext lib
import { useRouter } from 'next/router';
import {useEffect, useState} from 'react'

// components
import SmartTable from '../../components/table/smarttable'

// css
import styles from './procedureprofile.module.css'

const init_data = {
  title: 'Procedures',
  base_url:`/istsos/wa/istsos/services`,
  api_get_list_url: 'procedures/operations/getlist',
  api_url: 'procedures',
  api_config_url: 'configsections',
  api_uom_url: 'uoms',
  api_observedproperty_url: 'observedproperties',
  api_systemtypes_url: `systemtypes`,
  form: {
    "system_id": "",
    "assignedSensorId": "",
    "system": "",
    "description": "",
    "keywords": "",
    "identification": [
        {
            "name":"uniqueID",
            "definition":"urn:ogc:def:identifier:OGC:uniqueID",
            "value":"urn:ogc:def:procedure:x-istsos:1.0:"
        }
    ],
    "classification": [
        {
            "name": "System Type",
            "definition": "urn:ogc:def:classifier:x-istsos:1.0:systemType",
            "value": "profile"
        },
        {
            "name": "Sensor Type",
            "definition": "urn:ogc:def:classifier:x-istsos:1.0:sensorType",
            "value": ""
        }
    ],
    "characteristics": "",
    "contacts": [],
    "documentation": [],
    "capabilities": [],
    "location": {
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": [
                0, 0, 0
            ]
        },
        "crs": {
            "type": "name",
            "properties": {
                "name": ""
            }
        },
        "properties": {
            "name": ""
        }
    },
    "interfaces": "",
    "inputs": [],
    "outputs": [
        {
            "name": "Time",
            "definition": "urn:ogc:def:parameter:x-istsos:1.0:time:iso8601",
            "uom": "iso8601",
            "description": "",
            "constraint": {}
        }
    ],
    "mqtt": "",
    "history": [],
    "error": "",
    "message": "",
    "procedures": []
  },
  default: {
    "system_id": "",
    "assignedSensorId": "",
    "system": "",
    "description": "",
    "keywords": "",
    "identification": [
        {
            "name":"uniqueID",
            "definition":"urn:ogc:def:identifier:OGC:uniqueID",
            "value":"urn:ogc:def:procedure:x-istsos:1.0:"
        }
    ],
    "classification": [
        {
            "name": "System Type",
            "definition": "urn:ogc:def:classifier:x-istsos:1.0:systemType",
            "value": "profile"
        },
        {
            "name": "Sensor Type",
            "definition": "urn:ogc:def:classifier:x-istsos:1.0:sensorType",
            "value": ""
        }
    ],
    "characteristics": "",
    "contacts": [],
    "documentation": [],
    "capabilities": [],
    "location": {
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": [
                "", "", ""
            ]
        },
        "crs": {
            "type": "name",
            "properties": {
                "name": ""
            }
        },
        "properties": {
            "name": ""
        }
    },
    "interfaces": "",
    "inputs": [],
    "outputs": [
        {
            "name": "Time",
            "definition": "urn:ogc:def:parameter:x-istsos:1.0:time:iso8601",
            "uom": "iso8601",
            "description": "",
            "constraint": {}
        }
    ],
    "mqtt": "",
    "history": []
  },
  data: [],
  data_uoms : [],
  data_observedproperties: [],
  data_systemtypes: [],
  config_data: {
    urn: {
      procedure: ''
    },
    geo: {
      istsosepsg: "4326",
      allowedepsg: ""
    }
  },
  uom_tmp: [],
  checked: [],
  isLoading: true,
  selected: [],
  modal: false
}


const ProcedureProfileContainer: React.FC<{service: string, token: string, submit?: Function, outputs: object[]}> = ({service, token, submit, outputs}) => {
  const [ state, setData] = useState({
    ...init_data,
    outputs: [...outputs],
    form: {
      ...init_data.form,
      outputs: [
        ...outputs
      ]
    }
  })

  const router = useRouter();

  let service_url = service ? `/${service}/` : '/' 

  useEffect(
    ()=>{
      let promise1 = fetch(
        `${state.base_url}${service_url}${state.api_get_list_url}`,
        {
          headers: {
            'Authorization': `Bearer ${token}`
          },
          method: 'GET',
        }
      ).then(
        (response) => {
          if (response.ok && response.json) {                                    
            return response.json()
          } else{
            return {success: false}
          }
        }
      )

      let promise2 = fetch(
        `${state.base_url}${service_url}${state.api_config_url}`,
        {
          headers: {
            'Authorization': `Bearer ${token}`
          },
          method: 'GET',
        }
      ).then(
        (response) => {
          if (response.ok && response.json) {                                    
            return response.json()
          } else{
            return {success: false}
          }
        }
      )

      let promise3 = fetch(
        `${state.base_url}${service_url}${state.api_uom_url}`,
        {
          headers: {
            'Authorization': `Bearer ${token}`
          },
          method: 'GET',
        }
      ).then(
        (response) => {
          if (response.ok && response.json) {                                    
            return response.json()
          } else{
            return {success: false}
          }
        }
      )

      let promise4 = fetch(
        `${state.base_url}${service_url}${state.api_observedproperty_url}`,
        {
          headers: {
            'Authorization': `Bearer ${token}`
          },
          method: 'GET',
        }
      ).then(
        (response) => {
          if (response.ok && response.json) {                                    
            return response.json()
          } else{
            return {success: false}
          }
        }
      )

      let promise5 = fetch(
        `${state.base_url}${service_url}${state.api_systemtypes_url}`,
        {
          headers: {
            'Authorization': `Bearer ${token}`
          },
          method: 'GET',
        }
      ).then(
        (response) => {
          if (response.ok && response.json) {                                    
            return response.json()
          } else{
            return {success: false}
          }
        }
      )

      Promise.all([promise1, promise2, promise3, promise4, promise5])
        .then(
          res => {
            return {
              data: res[0].data,
              config_data: {...res[1].data},
              data_uoms: [...res[2].data],
              data_observedproperties: [...res[3].data],
              data_systemtypes: [...res[4].data]
            }
          }
        ).then(
          res => setData({
            ...state,
            // form: {
            //   ...state.form,
            // },
            outputs: [...outputs],
            form: {
              ...state.form,
              outputs: [...outputs],
              location: {
                ...state.form.location,
                crs: {
                  ...state.form.location.crs,
                  properties: {
                    ...state.form.location.crs.properties,
                    name: res.config_data.geo.istsosepsg
                  }
                }
              }
            },
            data: [
              ...res.data
            ],
            config_data: {...res.config_data},
            data_uoms: res.data_uoms,
            data_observedproperties: res.data_observedproperties,
            data_systemtypes: res.data_systemtypes,
            isLoading: false
          })
        )
    }
  , [service, state.isLoading, outputs])

  function onChangeCheckProc(e) {
    if (e.target.checked) {
      setData({
        ...state,
        selected: [
          ...state.selected,
          e.target.value
        ]
      })
    } else {
      let new_array = state.selected.filter(item=>item!==e.target.value)
      setData({
        ...state,
        selected: new_array
      })
    }
  
  }
  function onChange(el) {
    if (el.target.id.includes("classification")) {
      let newArray = [...state.form.classification]
      newArray[1] = {
        ...state.form.classification[1],
        value: el.target.value.toString()
      }
      setData({
        ...state,
        form: {
          ...state.form,
          classification: newArray
        }
      })
        
    } else if (el.target.id.includes("location-properties")) {
      setData({
        ...state,
        form: {
          ...state.form,
          location: {
            ...state.form.location,
            properties: {
              ...state.form.location.properties,
              name: el.target.value
            }
          }
        }
      })
        
    } else if (el.target.id.includes("location-crs")) {
        
      setData({
        ...state,
        form: {
          ...state.form,
          location: {
            ...state.form.location,
            crs: {
              ...state.form.location.crs,
              properties: {
                ...state.form.location.crs.properties,
                name: el.target.value.toString()
              }
            }
          }
        }
      })
        
    } else if (el.target.id.includes("location-geometry")) {
      let id_splitted = el.target.id.split("-")
      let newArray = [...state.form.location.geometry.coordinates]
      newArray[parseInt(id_splitted[3])] = parseFloat(el.target.value)
      setData({
        ...state,
        form: {
          ...state.form,
          location: {
            ...state.form.location,
            geometry: {
              ...state.form.location.geometry,
              coordinates: newArray
            }
          }
        }
      })
        
    } else if (el.target.id == "system_id") {
      let newArray = [...state.form.identification]
      newArray[0] = {
        ...newArray[0],
        value: `${state.config_data.urn.procedure}${el.target.value}`
      }
      setData({
        ...state,
        form: {
          ...state.form,
          system_id: el.target.value,
          system: el.target.value,
          identification: newArray
        }
      })
    } else {
      setData({
        ...state,
        form: {
          ...state.form,
          [el.target.id]: el.target.value
        }
      })
    }
  }
  function onChangeOp(idx, e) {
    if (e.target.value != -1) {
      let newArray = state.form.outputs.map(
        (item, index) => {
          if (index===idx) {
            return {
              ...item,
              name: state.data_observedproperties[e.target.value].name,
              definition: state.data_observedproperties[e.target.value].definition
            }
          } else {
            return {...item}
          }
        }
      )
      setData({
        ...state,
        form: {
          ...state.form,
          outputs: [...newArray]
        }
      })
    }
  }
  function onChangeUom(idx, e) {
    if (e.target.value != -1) {
      let newArray = [...state.form.outputs]
      newArray[idx] = {
        ...newArray[idx],
        uom: state.data_uoms[e.target.value].name
      }
      setData({
        ...state,
        form: {
          ...state.form,
          outputs: newArray
        }
      })
    }
    
  }
  function onSubmitPro(data) {

    if (data.classification[0].value == "profile") {
      if (data.outputs.filter(item=> item.name == "water-depth").length == 0) {

        data.outputs.push({
          "name": "water-depth",
          "definition": "urn:ogc:def:parameter:x-istsos:1.0:water:depth",
          "uom": "m",
          "description": "",
          "constraint": {}
        })
      }
    }
    import('../../wasm/profile/pkg/profile')
      .then(res => res.create(
          data,
          service,
          {
            method: "POST",
            auth:  `Bearer ${token}`
          },
          `/istsos/wa/istsos/services`
        ).then(
          res=> {
            alert(res.message)
            setData({
              ...state,
              isLoading: true,
            })
            return res
        }
      )
      .catch(
        err => alert(err.message)
      ))
  }

  // ****************************
  // PREPARING DATA FOR RENDERING
  // ****************************

  function omit (obj, arr) {
    let tmp_obj = Object.keys(obj)
      .filter(k => !arr.includes(k))
      .reduce((acc, key) => ((acc[key] = obj[key]), acc), {})        
    return tmp_obj
  }

  let test_data = {
    ...omit(state.form, ['assignedid', 'message', 'error']),
    procedures: state.checked
  }

  let data = state.data.map(
    item => ({
      select: <input
        type="checkbox"
        value={item.name}
        checked={state.selected.indexOf(item.name) > -1}
        onChange={(e)=>onChangeCheckProc(e)}
      />,
      // id: item.id,
      name: item.name,
      description: item.description,
      assignedid: item.assignedid,
      sensortype: item.sensortype,
      beginposition: item.samplingTime.beginposition,
      endposition: item.samplingTime.endposition,
      observedproperties: item.observedproperties.map(item =>item.name).join(',')
    })
  )
  let table_data = [];
  let table_alias = {};
  table_alias = {
    id: "Column",
    name: "Observed property",
    // uom: <a onClick={()=>onClick('open', 'uom')} >Uom <FontAwesomeIcon icon={faPlusCircle}/></a>
    uom: "Uom"
  }

  table_data = outputs.map(
    (item, index) => {
      return {
        id: `Col #${index}`,
        name: <select onChange={(e)=>onChangeOp(index, e)}>
        <option value={-1}>---</option>
            {state.data_observedproperties.map(
                (item2, index2) => <option value={index2} key={`key-profile-input-op-th-${index2}`}>{item2.name}</option>
            )}
        </select>,
        uom: <select onChange={(e)=>onChangeUom(index, e)} >
          <option value={-1}>---</option>
          {state.data_uoms.map(
            (item2, index2) => <option value={index2} key={`key-profile-input-uom-th-${index2}`}>{item2.name}</option>
          )}
        </select>
      }
    }
  ).filter(item=> item.id!="Col #0")

  return <div>
    {/* <h1>{state.title}</h1> */}
      <form className={styles.form}>
        <div className={styles.div}>
          <div className={styles.divleft}>
            <fieldset className={styles.fieldset}>
              <legend className={styles.legend}>General info</legend>
              <label className={styles.label}>Name:</label>
              <input id="system_id" type="text" placeholder="Name" onChange={(e)=>onChange(e)} value={state.form.system_id} />
              <label className={styles.label}>Description:</label>
              <input id="description" type="text" placeholder="Description" onChange={(e)=>onChange(e)} value={state.form.description}></input>
              <label className={styles.label}>Keywords:</label>
              <input id="keywords" type="text" placeholder="key1,key2,key3" onChange={(e)=>onChange(e)} value={state.form.keywords}></input>
            </fieldset>
            <fieldset className={styles.fieldset}>
              <legend className={styles.legend}>Classification</legend>
              <label className={styles.label}>System type:</label>
              <input id="classification-0-value" type="text" value={state.form.classification[0].value} disabled/>
              <label className={styles.label}>Sensor type:</label>
              <input id="classification-1-value" type="text" placeholder="tipping-bucket gauge, probe, etc." onChange={(e)=>onChange(e)} value={state.form.classification[1].value} />
            </fieldset>
            <fieldset className={styles.fieldset}>
              <legend className={styles.legend}>Location</legend>
              <label>Feature of Interest (FOI) name:</label>
              <input id="location-properties-name" type="text" placeholder="FOI name" onChange={(e)=>onChange(e)} value={state.form.location.properties.name}></input>
              <label className={styles.label}>EPSG:</label>
              <select defaultValue={state.config_data.geo.istsosepsg} id="location-crs-properties-name" onChange={(e)=>onChange(e)}>
                <option key={`key-epsg-option-00`}>{state.config_data.geo.istsosepsg}</option>
                  {state.config_data.geo.allowedepsg?.split(',').map(
                    (item, index) => <option key={`key-epsg-option-${index}`}>{item}</option>
                  )}
              </select>
              <fieldset className={styles.fieldsetcoords}>
                <label className={styles.label}>X: </label>
                <input id="location-geometry-coordinates-0" type="number" step="any" placeholder="X coord" onChange={(e)=>onChange(e)} value={state.form.location.geometry.coordinates[0]} />
                <label className={styles.label}>Y: </label>
                <input id="location-geometry-coordinates-1" type="number" step="any" placeholder="Y coord" onChange={(e)=>onChange(e)} value={state.form.location.geometry.coordinates[1]} />
                <label className={styles.label}>Z: </label>
                <input id="location-geometry-coordinates-2" type="number" step="any" placeholder="Z coord" onChange={(e)=>onChange(e)} value={state.form.location.geometry.coordinates[2]} />
              </fieldset>

            </fieldset>
          </div>
          <div className={styles.divright}>
            <fieldset className={styles.fieldset}>
              <legend className={styles.legend}>Observed properties</legend>
              <SmartTable
                id="key-t-insitu-obs"
                height={435}
                init_data={{alias: table_alias, data: table_data}}
                pagination={false}
                filter={true}
              />
            </fieldset>
          </div>
          <button type="button" onClick={submit ? ()=>submit(test_data) : ()=>onSubmitPro(test_data)}>Submit</button>
        </div>
      </form>
  </div>
}

export default ProcedureProfileContainer