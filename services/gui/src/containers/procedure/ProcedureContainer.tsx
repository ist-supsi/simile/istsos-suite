
// ext lib
import { useRouter } from 'next/router';
import {useEffect, useState} from 'react'
// components
import SmartTable from '../../components/table/smarttable'
import EmptyAddComponent from '../../components/empty/EmptyAddComponent'
import Modal from '../../components/modal/modal';
import { CardListSystemType } from '../../components/cardlist/cardlist';
import FormProcedure from '../../components/form/formprocedure';
import FormContainer from './FormContainer'
import Loader from 'react-loader-spinner'

const init_data = {
  title: 'Procedures',
  service: '',
  base_url:`/istsos/wa/istsos/services`,
  api_get_list_url: 'procedures/operations/getlist',
  api_url: 'procedures',
  api_config_url: 'configsections',
  api_uom_url: 'uoms',
  api_observedproperty_url: 'observedproperties',
  api_systemtypes_url: `systemtypes`,
  config_data: {},
  data: [],
  edit: '',
  data_uoms : [],
  data_observedproperties: [],
  data_systemtypes: [],
  isLoading: true,
  modal: false,
  loader: false
}

const ProcedureContainer: React.FC<{service: string, token: string}> = ({service, token}) => {
  const [ state, setData] = useState(init_data)
  const router = useRouter();
  let service_url = service ? `/${service}/` : '/'
  useEffect(
    ()=>{
      if (state.isLoading || state.service != service) {
        let promise1 = fetch(
          `${state.base_url}${service_url}${state.api_get_list_url}`,
          {
            headers: {
              'Authorization': `Bearer ${token}`
            },
            method: 'GET',
          }
        ).then(
          (response) => {
            if (response.ok && response.json) {                                    
              return response.json()
            } else{
              return {success: false}
            }
          }
        )

        let promise2 = fetch(
          `${state.base_url}${service_url}${state.api_config_url}`,
          {
            headers: {
              'Authorization': `Bearer ${token}`
            },
            method: 'GET',
          }
        ).then(
          (response) => {
            if (response.ok && response.json) {                                    
              return response.json()
            } else{
              return {success: false}
            }
          }
        )

        let promise3 = fetch(
          `${state.base_url}${service_url}${state.api_uom_url}`,
          {
            headers: {
              'Authorization': `Bearer ${token}`
            },
            method: 'GET',
          }
        ).then(
          (response) => {
            if (response.ok && response.json) {                                    
              return response.json()
            } else{
              return {success: false}
            }
          }
        )

        let promise4 = fetch(
          `${state.base_url}${service_url}${state.api_observedproperty_url}`,
          {
            headers: {
              'Authorization': `Bearer ${token}`
            },
            method: 'GET',
          }
        ).then(
          (response) => {
            if (response.ok && response.json) {                                    
              return response.json()
            } else{
              return {success: false}
            }
          }
        )

        let promise5 = fetch(
          `${state.base_url}${service_url}${state.api_systemtypes_url}`,
          {
            headers: {
              'Authorization': `Bearer ${token}`
            },
            method: 'GET',
          }
        ).then(
          (response) => {
            if (response.ok && response.json) {                                    
              return response.json()
            } else{
              return {success: false}
            }
          }
        )

        Promise.all([promise1, promise2, promise3, promise4, promise5])
          .then(
            res => {
              return {
                data: res[0].data,
                config_data: {...res[1].data},
                data_uoms: [...res[2].data],
                data_observedproperties: [...res[3].data],
                data_systemtypes: [...res[4].data]
              }
            }
          ).then(
            res => setData({
              ...state,
              // form: {
              //   ...state.form,
              // },
              service: service,
              data: [
                ...res.data
              ],
              config_data: {...res.config_data},
              data_uoms: res.data_uoms,
              data_observedproperties: res.data_observedproperties,
              data_systemtypes: res.data_systemtypes.map(
                item => ({
                  ...item,
                  selected: item.name==='insitu-fixed-point',
                })
              ),
              isLoading: false
            })
          )
      }
    }
  , [service, state.isLoading])
  //
  // FUNCTIONS
  //
  function onAdd() {
    setData({
      ...state,
      modal: !state.modal
    })
  }
  function onRemove(procedures) {
    if (procedures.length === 0) {
      alert('No procedure(s) selected.')
    } else {
      let proc_names = procedures.map(i=>i['name'])
      if (
        confirm(
          `This operation cannot be undone!\n\nAre you sure you want to erase the following procedure(s)?\n${proc_names}`)
        ) {
          setData({
            ...state,
            loader:true
          })
          let promises = [];
          for (let index = 0; index < procedures.length; index++) {
            const proc_name = procedures[index]['name'];
            let options = {
              method: 'DELETE',
              headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + token 
              }
            } 
            promises.push(fetch(
              `${state.base_url}${service_url}${state.api_url}/${proc_name}`,
              options
            ).then(
              res => {
                if (res.status === 200) {
                  return res.json()
                } else {
                  throw 'Delete request failed.'
                }
            }).then(
              res => {
                if (res.success) {
                  return {success: true}
                } else {
                  return {success: false, error: res.message}
                }
              }
            ).catch(
              err => ({success: false, error: err})
            ))
              
          }
          Promise.all(promises).then(
            res => {
              let index_deleted = [];
              for (let index = 0; index < res.length; index++) {
                const element = res[index];
                if (element.success) {
                  index_deleted.push(index)
                } else {
                  alert(element.error)
                }
              }
              if (index_deleted.length>0) {
                setData({
                  ...state,
                  isLoading: true,
                  loader: false
                })
              } else {
                setData({
                  ...state,
                  loader: false
                })
              }
              
            }
          )
      }
    }
  }
  function reload() {
    setData({
      ...state,
      isLoading: true
    })
  }
  function onModalClose() {
    setData({
      ...state,
      modal: !state.modal,
      edit: ''
    })
  }
  function onEdit(name) {
    setData({
      ...state,
      edit: name,
      modal: true
    })
  }
  function onSystemTypeClick(name) {
        
    let newArray = state.data_systemtypes.map(
      item => {
        if (item.name === name) {
          return {
            ...item,
            selected: true,
          }
        } else {
          return {
            ...item,
            selected: false,
          }
        }
      }
    )
    setData({
      ...state,
      data_systemtypes: newArray,
    })
  }
  //
  // DATA PREPARING
  //
  let data = state.data.map(
    item => ({
      name: item.name,
      description: item.description,
      assignedid: item.assignedid,
      sensortype: item.sensortype,
      beginposition: item.samplingTime.beginposition,
      endposition: item.samplingTime.endposition,
      observedproperties: item.observedproperties.map(item =>item.name).join(','),
      edit: <a onClick={()=>onEdit(item.name)}>🔧</a>
    })
  )

  return  <div className='container'>
    <div className="containertitle">
      <div className='title'>{state.title}</div>
    </div>
    {
      state.loader ? <div className="loader">
        <div className='loading-gif'>
          <Loader
            type="Bars"
            color="#000"
            height={100}
            width={100}
          />
        </div>
      </div> : <></>
    }
    {
      data.length > 0 ?
      <div className="containercontent">
        <SmartTable
          id={`t-procedures`}
          init_data={
            {
              alias: {
                name: 'Name',
                description: 'Description',
                assignedid: 'Assignedid',
                sensortype: 'Sensortype',
                beginposition: 'Begin',
                endposition: 'End',
                observedproperties: 'Properties',
                edit: 'Edit'
              },
              data: data
            }
          }
          pagination={false}
          filter={true}
          onAdd={() => onAdd()}
          onRemove={(procedures) => onRemove(procedures)}
        /> 
      </div>:
      <div className="containercontent">
        <EmptyAddComponent onClick={() => onAdd()}>
          No procedures found in the selected <i>'{service}'</i> service
        </EmptyAddComponent>
      </div>
    }
    {
      state.modal ? 
      <Modal
        onModalClose={onModalClose}
        title={
          state.edit ?
          `Modify '${state.edit}' procedure` :
          'Add a new procedure'
        }
      >
        {
          state.edit ?
          <></> :
          <CardListSystemType
            systemtypes={state.data_systemtypes}
            onClick={onSystemTypeClick}
            width={100}
          />
        }
        <FormContainer
          systemtype={
            state.data_systemtypes.filter(
              item=>item.selected
            )[0].name
          }
          config_data={state.config_data}
          service_url={service_url}
          observedproperties={state.data_observedproperties}
          uoms={state.data_uoms}
          token={token}
          procedures={state.data}
          service={service}
          reload={reload}
          edit={state.edit}
        />
      </Modal> : <></>
    }
  </div>
}

export default ProcedureContainer