
// ext lib
import { useRouter } from 'next/router';
import {useEffect, useState} from 'react'
import dynamic from 'next/dynamic'

// components
import SmartTable from '../../components/table/smarttable'
import EmptyAddComponent from '../../components/empty/EmptyAddComponent'
import Modal from '../../components/modal/modal';
import { CardListSystemType } from '../../components/cardlist/cardlist';
import FormProcedure from '../../components/form/formprocedure';
import AddRemoveBtns from '../../components/buttons/addremove'

const init_data = {
  title: 'Procedures',
  base_url:`/istsos/wa/istsos/services`,
  api_get_list_url: 'procedures/operations/getlist',
  api_url: 'procedures',
  api_config_url: 'configsections',
  api_uom_url: 'uoms',
  api_observedproperty_url: 'observedproperties',
  api_systemtypes_url: `systemtypes`,
  form: {
    "system_id": "",
    "assignedSensorId": "",
    "system": "",
    "description": "",
    "keywords": "",
    "identification": [
        {
            "name":"uniqueID",
            "definition":"urn:ogc:def:identifier:OGC:uniqueID",
            "value":"urn:ogc:def:procedure:x-istsos:1.0:"
        }
    ],
    "classification": [
        {
            "name": "System Type",
            "definition": "urn:ogc:def:classifier:x-istsos:1.0:systemType",
            "value": "insitu-fixed-point"
        },
        {
            "name": "Sensor Type",
            "definition": "urn:ogc:def:classifier:x-istsos:1.0:sensorType",
            "value": ""
        }
    ],
    "characteristics": "",
    "contacts": [],
    "documentation": [],
    "capabilities": [],
    "location": {
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": [
                0, 0, 0
            ]
        },
        "crs": {
            "type": "name",
            "properties": {
                "name": ""
            }
        },
        "properties": {
            "name": ""
        }
    },
    "interfaces": "",
    "inputs": [],
    "outputs": [
        {
            "name": "Time",
            "definition": "urn:ogc:def:parameter:x-istsos:1.0:time:iso8601",
            "uom": "iso8601",
            "description": "",
            "constraint": {}
        }
    ],
    "mqtt": "",
    "history": [],
    "error": "",
    "message": "",
    "procedures": []
  },
  default: {
    "system_id": "",
    "assignedSensorId": "",
    "system": "",
    "description": "",
    "keywords": "",
    "identification": [
        {
            "name":"uniqueID",
            "definition":"urn:ogc:def:identifier:OGC:uniqueID",
            "value":"urn:ogc:def:procedure:x-istsos:1.0:"
        }
    ],
    "classification": [
        {
            "name": "System Type",
            "definition": "urn:ogc:def:classifier:x-istsos:1.0:systemType",
            "value": "insitu-fixed-point"
        },
        {
            "name": "Sensor Type",
            "definition": "urn:ogc:def:classifier:x-istsos:1.0:sensorType",
            "value": ""
        }
    ],
    "characteristics": "",
    "contacts": [],
    "documentation": [],
    "capabilities": [],
    "location": {
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": [
                "", "", ""
            ]
        },
        "crs": {
            "type": "name",
            "properties": {
                "name": ""
            }
        },
        "properties": {
            "name": ""
        }
    },
    "interfaces": "",
    "inputs": [],
    "outputs": [
        {
            "name": "Time",
            "definition": "urn:ogc:def:parameter:x-istsos:1.0:time:iso8601",
            "uom": "iso8601",
            "description": "",
            "constraint": {}
        }
    ],
    "mqtt": "",
    "history": [],
    "error": "",
    "message": "",
    "procedures": []
  },
  data: [],
  data_uoms : [],
  data_observedproperties: [],
  data_systemtypes: [],
  config_data: {
    urn: {
      procedure: ''
    },
    geo: {
      istsosepsg: "4326",
      allowedepsg: ""
    }
  },
  uom_tmp: [],
  checked: [],
  isLoading: true,
  to_edit: '',
  selected: [],
  modal: false,
  edit: false
}


const ProcedureViewerContainer: React.FC<{service: string, token: string}> = ({service, token}) => {
  const [ state, setData] = useState(init_data)

  const router = useRouter();

  let service_url = service ? `/${service}/` : '/' 

  useEffect(
    ()=>{
      let promise1 = fetch(
        `${state.base_url}${service_url}${state.api_get_list_url}`,
        {
          headers: {
            'Authorization': `Bearer ${token}`
          },
          method: 'GET',
        }
      ).then(
        (response) => {
          if (response.ok && response.json) {                                    
            return response.json()
          } else{
            return {success: false}
          }
        }
      )

      Promise.all([promise1])
        .then(
          res => {
            return {
              data: res[0].data,
            }
          }
        ).then(
          res => setData({
            ...state,
            data: [
              ...res.data
            ],
            isLoading: false
          })
        )
    }
  , [service, state.isLoading])

  function onModalClose() {
    setData({
      ...state,
      form: {
        ...state.form,
        ...state.default,
        location: {
          ...state.form.location,
          crs: {
            ...state.form.location.crs,
            properties: {
              ...state.form.location.crs.properties,
              name: state.config_data.geo.istsosepsg
            }
          }
        }
      },
      modal: !state.modal,
      to_edit: '',
      edit: false
    })
  }
  function onChange(el) {
    if (el.target.id.includes("classification")) {
      let newArray = [...state.form.classification]
      newArray[1] = {
        ...state.form.classification[1],
        value: el.target.value.toString()
      }
      setData({
        ...state,
        form: {
          ...state.form,
          classification: newArray
        }
      })
        
    } else if (el.target.id.includes("location-properties")) {
      setData({
        ...state,
        form: {
          ...state.form,
          location: {
            ...state.form.location,
            properties: {
              ...state.form.location.properties,
              name: el.target.value
            }
          }
        }
      })
        
    } else if (el.target.id.includes("location-crs")) {
        
      setData({
        ...state,
        form: {
          ...state.form,
          location: {
            ...state.form.location,
            crs: {
              ...state.form.location.crs,
              properties: {
                ...state.form.location.crs.properties,
                name: el.target.value.toString()
              }
            }
          }
        }
      })
        
    } else if (el.target.id.includes("location-geometry")) {
      let id_splitted = el.target.id.split("-")
      let newArray = [...state.form.location.geometry.coordinates]
      newArray[parseInt(id_splitted[3])] = parseFloat(el.target.value)
      setData({
        ...state,
        form: {
          ...state.form,
          location: {
            ...state.form.location,
            geometry: {
              ...state.form.location.geometry,
              coordinates: newArray
            }
          }
        }
      })
        
    } else if (el.target.id == "system_id") {
      let newArray = [...state.form.identification]
      newArray[0] = {
        ...newArray[0],
        value: `${state.config_data.urn.procedure}${el.target.value}`
      }
      setData({
        ...state,
        form: {
          ...state.form,
          system_id: el.target.value,
          system: el.target.value,
          identification: newArray
        }
      })
    } else {
      setData({
        ...state,
        form: {
          ...state.form,
          [el.target.id]: el.target.value
        }
      })
    }
  }
  function onSubmitPro(data) {
    console.log(data);

    if (data.classification[0].value == "virtual-profile") {
      if (data.outputs.filter(item=> item.name == "water-depth").length == 0) {

        data.outputs.push({
          "name": "water-depth",
          "definition": "urn:ogc:def:parameter:x-istsos:1.0:water:depth",
          "uom": "m",
          "description": "",
          "constraint": {}
        })
      }
    }
    import('../../wasm/profile/pkg/profile')
      .then(res => res.create(
        data,
        service,
        {
          method: "POST",
          auth:  `Bearer ${token}`
        },
        `/istsos/wa/istsos/services`
      ).then(
        res=> {
          alert(res.message)
          setData({
            ...state,
            isLoading: true,
          })
          return res
      })
      .catch(
        err => alert(err.message)
      )
    ).catch(
      err => alert(err.message)
    )
  }

  function onSubmit(mode) {
    let url = `${state.base_url}${service_url}${state.api_url}`
    if (mode=='PUT') {
      url = `${url}/${state.to_edit}`
    }    
    fetch(
      url,
      {
        headers: {
          'Authorization': `Bearer ${token}`
        },
        method: mode,
        body: JSON.stringify(state.form)
      }
    ).then(response => {
      if (response.ok && response.json) {                                    
        return response.json()
      } else{
        return {success: false}
      }
    })
    .then(res => {
      if (res.success) {
        alert(res.message)
        setData({
          ...state,
          to_edit: '',
          edit: false,
          isLoading: true
        })
      } else {
        alert(res.message)
        setData({
          ...state,
          to_edit: '',
          edit: false
        })
      }
    }).catch(
      (e) => {
        alert('Request failed!')
        setData({
          ...state,
          to_edit: '',
          edit: false
        })
      }
    )
  }

  function onEdit(name) {
    // e.preventDefault();
    let url = `${state.base_url}${service_url}${state.api_url}/${name}`
    fetch(
      url,
      {
        headers: {
          'Authorization': `Bearer ${token}`
        },
        method: "GET",
      }
    ).then(response => {
      if (response.ok && response.json) {                                    
        return response.json()
      } else{
        return {success: false}
      }
    })
    .then(res => {
      if (res.success) {
        setData({
          ...state,
          form: {
            ...state.form,
            ...res['data'],
            location: {
              ...state.form.location,
              crs: {
                ...state.form.location.crs,
                properties: {
                  ...state.form.location.crs.properties,
                  name: res['data'].location.crs.properties.name.replace("EPSG:", "")
                }
              }
            },
          },
          modal: !state.modal,
          to_edit: name,
          edit: true
        })
      } else {
        return alert(res.message)
      }
    }).catch(
      (e) => {
        alert('Request failed!')
        console.log(e);
      }
    )
    // setData({
    //   ...state,
    //   form: {
    //     ...state.form,
    //     system_id: item.name,
    //     system: item.name,
    //     description: item.description,
    //     ...item
    //   },
    //   modal: !state.modal,
    //   to_edit: item.name,
    //   edit: true
    // })
  }

  function onBtnClick(name) {
        
    let newArray = state.data_systemtypes.map(
      item => {
        if (item.name === name) {
          return {
            ...item,
            selected: true,
          }
        } else {
          return {
            ...item,
            selected: false,
          }
        }
      }
    )
    let defaultForm = {...state.default}
    let newArray2 = [...defaultForm.classification]
    newArray2[0] = {
      ...defaultForm.classification[0],
      value: name
    }
    setData({
      ...state,
      data_systemtypes: newArray,
      form: {
        ...defaultForm,
        classification: newArray2,
        location: {
          ...state.form.location
        }
      }
    })
  }

  // ****************************
  // PREPARING DATA FOR RENDERING
  // ****************************

  let data = state.data.map(
    item => ({
      name: item.name,
      description: item.description,
      sensortype: item.sensortype,
      beginposition: item.samplingTime.beginposition,
      endposition: item.samplingTime.endposition,
      observedproperties: item.observedproperties.map(item =>item.name).join(','),
    })
  )

  return <div className='container'>
    <div className="containertitle">
      <div className='title'><div>{state.title}</div></div>
    </div>
    {
      data.length > 0 ?
      <div className="containercontent">
        <SmartTable
          id={`t-procedures`}
          init_data={
            {
              alias: {
                // select: "Select",
                name: 'Name',
                description: 'Description',
                sensortype: 'Sensortype',
                beginposition: 'Begin',
                endposition: 'End',
                observedproperties: 'Properties',
              },
              data: data
            }
          }
          height={450}
          pagination={true}
          filter={true}
        />
      </div>:
      <div className="containercontent">
          No procedures found in the selected <i>'{service}'</i> service
      </div>
    }
    {/* {
      state.modal ? 
      <Modal onModalClose={onModalClose} title={'Add a new procedure'}>
        {state.edit ? <></> : <CardListSystemType
          systemtypes={state.data_systemtypes.map(
            item => ({
              ...item,
              selected: state.form.classification[0].value===item.name
            })
          )}
          onClick={onBtnClick}
        />}
        <FormProcedure
          onSubmitFunc={
            state.form.classification[0].value == 'virtual-profile' ?
            state.edit ? () => onSubmit('PUT') : () => onSubmitPro(test_data) :
            () => onSubmit(state.edit ? 'PUT' : 'POST')
          }
          onChange={onChange}
          allowedepsg={state.config_data.geo.allowedepsg}
          form={{...omit(state.form, ['assignedid', 'message', 'error'])}}
          defaultepsg={state.config_data.geo.istsosepsg}
          systemtype={state.form.classification[0].value}
          table_data={table_data}
          table_alias={table_alias}
          edit={state.edit}
        />
      </Modal> :
      <></>
    } */}
  </div>
}

export default ProcedureViewerContainer