
// ext lib
import { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import Form from '../../components/form/form'

// components
import SmartTable from '../../components/table/smarttable'


const init_data = {
  tag: 'process',
  title: 'Processes',
  isLoading: true,
  url: '/istsos/process',
  data: [],
  message: "",
  error: "",
  selected: []
}


const ProcessContainer: React.FC<{service: string, token: string, user?: string}> = ({service, token, user}) => {
  const [state, setData] = useState(init_data)
  const router = useRouter()

  let service_url = service ? `/${service}/` : '/' 

  useEffect(
    () => {
      if (state.isLoading) {
        fetch(
          `${state.url}/${service}`,
          {
            headers: {
              'Authorization': `Bearer ${token}`
            },
            method: 'GET',
          }
        ).then(
          (response) => {
            if (response.ok && response.json) {                                    
              return response.json()
            } else{
              return {success: false}
            }
          }
        ).then(
          (res) => {
            if (res.success) {
              return setData({
                ...state,
                data: [...res.data],
                isLoading: false
              })
            } else {
              return alert(res.message)
            }
          }
        )
      }
    }
  , [service, state.isLoading])

  function onChange(el) {
    let newArray = []
    if (state.selected.indexOf(el.target.value) > -1) {
      newArray = state.selected.filter(item=> item!=el.target.value)
    } else {
      newArray = [...state.selected]
      newArray.push(el.target.value)
      console.log(newArray);
      
    }
    setData({
      ...state,
      selected: newArray
    })
  }

  function onRemove(items) {
    if (items.length === 0) {
      alert('No process(es) selected.')
    } else {
      let items_names = items.map(i => i['name'])
      if (
        confirm(
          `This operation cannot be undone!\n\nAre you sure you want to erase the following process(es)?\n${items_names}`)
        ) {
          let promises = [];
          for (let index = 0; index < items.length; index++) {
            const item_id = items[index]['id'];
            
            let options = {
              method: 'DELETE',
              headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + token 
              },
            } 
            promises.push(fetch(
              `${state.url}/${item_id}`,
              options
            ).then(
              res => {
                if (res.status === 200) {
                  return res.json()
                } else {
                  throw 'Delete request failed.'
                }
            }).then(
              res => {
                if (res.success) {
                  return {success: true}
                } else {
                  return {success: false, error: res.message}
                }
              }
            ).catch(
              err => ({success: false, error: err})
            ))
              
          }
          Promise.all(promises).then(
            res => {
              let index_deleted = [];
              let msg_errors = []
              for (let index = 0; index < res.length; index++) {
                const element = res[index];
                if (element.success) {
                  index_deleted.push(index)
                } else {
                  alert(element.error)
                }
              }
              if (index_deleted.length>0) {
                setData({
                  ...state,
                  isLoading: true
                })
              }
            }
          )
      }
    }
  }
  let data = state.data.map(
    item => ({
      // select: <input
      //   type="checkbox"
      //   value={item.processid}
      //   checked={state.selected.indexOf(item.processid) > -1}
      //   onChange={(e)=>onChange(e)}
      // />,
      id: item.processid,
      name: item.indicatorname,
      service: item.indicatorservice,
      target: item.indicatorform?.service?.value,
      module: item.indicatormodule,
      indicator: item.indicatormethod,
      user: item.processusername,
      status: item.processstatus,
      message: item.processmessage,
      progress: item.processprogress,
    })
  )
  return <div className='container'>
    <div className="containertitle">
      <div className='title'>
        <div onClick={()=>setData({...state, isLoading:true})}>{state.title} 🔃</div>
      </div>
      {/* <AddRemoveBtns
        onAdd={() => onAdd()}
        onRemove={(obs) => onRemove(obs)}
      /> */}
    </div>
    <div className="containercontent">
      <SmartTable
        id={`t-observed-properties`}
        key={`t-observed-properties`}
        init_data={
          {
            alias: {
              name: 'Proc. name',
              user: 'User',
              service: 'Service',
              target: 'Target service',
              status: 'Status',
              module: 'Module',
              indicator: 'Indicator',
              message: 'Message',
              progress: 'Progress'
            },
            data: data
          }
        }
        pagination={false}
        filter={true}
        onRemove={(items) => onRemove(items)}
        // onAdd={() => onAdd()}
      />
    </div>
  </div>
}

export default ProcessContainer