
import { useState, useEffect } from 'react'

// ext lib
// external component
import moment from 'moment'
import "react-datetime/css/react-datetime.css";
import Loader from 'react-loader-spinner'

// containers
import ProcedureProfileContainer from '../../containers/procedure/ProcedureProfileContainer'

// components
import ImporterComponent from '../../components/importer/importer'

const ImporterContainer: React.FC<{service: string, token: string}> = ({service, token}) => {
  const [ state, setData ] = useState({
    isLoading: true,
    files: [],
    message: [],
    form: {
      "system_id": "",
      "assignedSensorId": "",
      "system": "",
      "description": "",
      "keywords": "",
      "identification": [
          {
              "name":"uniqueID",
              "definition":"urn:ogc:def:identifier:OGC:uniqueID",
              "value":"urn:ogc:def:procedure:x-istsos:1.0:"
          }
      ],
      "classification": [
          {
              "name": "System Type",
              "definition": "urn:ogc:def:classifier:x-istsos:1.0:systemType",
              "value": "profile"
          },
          {
              "name": "Sensor Type",
              "definition": "urn:ogc:def:classifier:x-istsos:1.0:sensorType",
              "value": ""
          }
      ],
      "characteristics": "",
      "contacts": [],
      "documentation": [],
      "capabilities": [],
      "location": {
          "type": "Feature",
          "geometry": {
              "type": "Point",
              "coordinates": [
                  0, 0, 0
              ]
          },
          "crs": {
              "type": "name",
              "properties": {
                  "name": ""
              }
          },
          "properties": {
              "name": ""
          }
      },
      "interfaces": "",
      "inputs": [],
      "outputs": [],
      "mqtt": "",
      "history": [],
      "error": "",
      "message": "",
      "procedures": []
    },
    default: {
      form: {
        "system_id": "",
        "assignedSensorId": "",
        "system": "",
        "description": "",
        "keywords": "",
        "identification": [
            {
                "name":"uniqueID",
                "definition":"urn:ogc:def:identifier:OGC:uniqueID",
                "value":"urn:ogc:def:procedure:x-istsos:1.0:"
            }
        ],
        "classification": [
            {
                "name": "System Type",
                "definition": "urn:ogc:def:classifier:x-istsos:1.0:systemType",
                "value": "profile"
            },
            {
                "name": "Sensor Type",
                "definition": "urn:ogc:def:classifier:x-istsos:1.0:sensorType",
                "value": ""
            }
        ],
        "characteristics": "",
        "contacts": [],
        "documentation": [],
        "capabilities": [],
        "location": {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [
                    0, 0, 0
                ]
            },
            "crs": {
                "type": "name",
                "properties": {
                    "name": ""
                }
            },
            "properties": {
                "name": ""
            }
        },
        "interfaces": "",
        "inputs": [],
        "outputs": [
            {
                "name": "Time",
                "definition": "urn:ogc:def:parameter:x-istsos:1.0:time:iso8601",
                "uom": "iso8601",
                "description": "",
                "constraint": {}
            }
        ],
        "mqtt": "",
        "history": [],
        "error": "",
        "message": "",
        "procedures": []
      }
    },
    mode: undefined,
    reload: false,
    delimiter: " ",
    errors: [],
    procedure: "",
    procedures: [],
    overwrite: false,
    depth_column: -1,
    header: true,
  })

  // ********************
  //  useEffect
  // ********************
  useEffect(() => {
    if (state.isLoading && !state.reload) {
      let promise1 = fetch(
        `/istsos/wa/istsos/services/${service}/procedures/operations/getlist`,
        {
          headers: {
            'Authorization': `Bearer ${token}`
          },
          method: 'GET',
        }
      ).then(
        (response) => {
          if (response.ok && response.json) {                                    
            return response.json()
          } else{
            return {success: false}
          }
        }
      ).then(res => {
        if (res.success) {
          setData({
            ...state,
            procedures: [...res.data.filter(item=>item.sensortype==='profile')],
            isLoading: false,
          })
        } else {
          setData({
            ...state,
            isLoading: false,
          })
        }
      })
    }
    if (state.isLoading && state.reload) {
      let promise1 = fetch(
        `/istsos/wa/istsos/services/${service}/procedures/operations/getlist`,
        {
          headers: {
            'Authorization': `Bearer ${token}`
          },
          method: 'GET',
        }
      ).then(
        (response) => {
          if (response.ok && response.json) {                                    
            return response.json()
          } else{
            return {success: false}
          }
        }
      ).then(res => {
        if (res.success) {
          setData({
            ...state,
            procedures: [...res.data.filter(item=>item.sensortype==='profile')],
            isLoading: false,
            reload: false
          })
        } else {
          setData({
            ...state,
            isLoading: false,
            reload: false
          })
        }
      })
    }
  },[state.reload])

  // ********************
  //  FUNCS
  // ********************

  function handleOverwrite() {
    setData({
      ...state,
      overwrite: !state.overwrite 
    })
  }

  function handleRead(content, name) {
    return import('../../wasm/profile/pkg/profile')
      .then(
        res => {
          try {
            let file_info = res.load(
              content,
              state.delimiter
            );
            if (file_info.data.length > 0) {
              let ops = file_info.data[0].slice(1,file_info.data[0].length).map(
                item => ({
                  "name": "",
                  "definition": "",
                  "uom": "",
                  "description": "",
                  "constraint": {}
                })
              )
              let newArray = [
                ...state.default.form.outputs,
                ...ops
              ]
              let dates = file_info.datetime.map(
                item => moment(item, moment.ISO_8601)
              )
              let check = dates.filter(item => !item.isValid())
              if (check.length>0) {
                dates = file_info.datetime.map(
                  item => moment(file_info.datetime[0], 'DD/MM/YYYY HH:mm:ss')
                )
                check = dates.filter(item => !item.isValid())
              }
              if (check.lenght>0) {
                throw "Can't parse datetime"
              }
              return {
                success: true,
                file: content,
                file_name: name,
                file_info: file_info,
                data: file_info.data,
                datetime: [...dates.map(item => item.toDate())],
                outputs: newArray
              }    
            } else {
              return {
                success: false,
                error: `${name} --> Can't parse file.`
              }
            }
          } catch (err) {
            return {
              success: false,
              error: err.message == undefined ? `${name} --> ${err}` : `${name} --> ${err.message}`
            }            
          }
        }
      )
  }

  function handleMultiFileChosen(e) {
        
    setData({
      ...state, isLoading: true,
      files: [],
      message: [],
      errors: []
    })
    let promises = [];
    let file_names = []
    
    for (let file of e.target.files) {
      file_names.push(file.name)
      let filePromise = new Promise(resolve => {
        let reader = new FileReader();
        reader.readAsText(file);
        reader.onload = () => resolve(reader.result)
      });
      promises.push(filePromise);
    }
    Promise.all(promises).then(fileContents => {
      return Promise.all(fileContents.map(
        (item, index) => handleRead(item, file_names[index])
      )).then(res => res)
    }).then(
      new_files => {
        let ok_files = new_files.filter(item => item.success)
        let err_messages = new_files.filter(item => !item.success).map(
          item => {
            return item.error
          }
        )
        if (ok_files.length > 0) {
            return setData({
              ...state,
              files: ok_files,
              isLoading: false,
              form: {
                ...state.form,
                system_id: "",
                outputs: ok_files[0].outputs
              },
              message: [],
              errors: err_messages
          })    
        } else {
          return setData({
            ...state,
            files: [],
            isLoading: false,
            form: {
              ...state.default.form,
              system_id: "",
              outputs: []
            },
            message: [],
            errors: [...err_messages]
          })
        }
    });
    
  }
  function onChangeMode(e) {
    setData({
      ...state,
      reload: true,
      isLoading: true,
      mode: e.target.value,
      files: [],
      message: [],
      errors:[]
    })
  }
  function onChangeDate(el, id) {
    let newArray = [...state.files]
    
    newArray[id] = {
      ...newArray[id],
      datetime: [new el.toDate()]
    }
    setData({
      ...state,
      files: newArray
    })  
  }
  function omit (obj, arr) {
    let tmp_obj = Object.keys(obj)
      .filter(k => !arr.includes(k))
      .reduce((acc, key) => ((acc[key] = obj[key]), acc), {})        
    return tmp_obj
  }
  function onChangeSelect(e) {
    let splitted_id = e.target.value.split("-")
    setData({...state, isLoading: true})
    fetch(
      `/istsos/wa/istsos/services/${service}/procedures/${splitted_id[1]}`,
      {
        method: "GET",
        headers: {
          "Authorization": `Bearer ${token}`
        }
      })
    .then(response => {
      if (response.ok && response.json) {
        return response.json()
      } else{
        return {success: false}
      }
    })
    .then(res => {
      setData({
        ...state,
        form: {
          ...state.form,
          ...res.data
        }
      })
    })
    .catch(err => err)
  }
  function handleUpload() {
    let idx = state.form.outputs.filter(item => item.name == 'water-depth').length
    if (idx == 1) {
      setData({
        ...state,
        isLoading: true,
        message: [],
        errors: []
      })
      let test_data = {
        ...omit(state.form, ['message', 'error'])
      }
      let promises = state.files.map(
        (item, index) => {
            var begin_time = "";
            var end_time = "";
            if (item.datetime.length==1) {
              begin_time = item.datetime.map(
                item2 => item2.toISOString()
              ).join(",");
              end_time = item.datetime.map(
                item2 => item2.toISOString()
              ).join(",");
            } else {
              begin_time = item.datetime[0].toISOString()
              end_time = item.datetime[item.datetime.length-1].toISOString()
            }
            return import('../../wasm/profile/pkg/profile')
              .then(res => res.upload_data(
                test_data,
                item.data,
                begin_time,
                end_time,
                service,
                state.overwrite,
                {
                  method: "POST",
                  auth: `Bearer ${token}`
                },
                `/istsos/wa/istsos/services`
              ).then(res => {
                let newArray = [...state.files]
                newArray[index] = {...newArray[index], isLoading: false} 
                setData({
                  ...state,
                  files: newArray
                })
                return res
              }).catch(err => {
                if (Object.keys(err).indexOf("message") > -1) {
                  return {
                    success: false,
                    message: `Data uploading FAILED: ${err.message}`
                  }
                } else {
                  return {
                    success: false,
                    message: `Data uploading FAILED`
                  }
                }  
              })
            )
        }
      )
      Promise.all(promises)
          .then(res => {
              let ok_message = []
              let err_message = []
              res.map(
                  (item, index) => {
                      if (item.success) {
                          ok_message.push(`${state.files[index].file_name} --> ${item.message}`)
                      } else {
                          err_message.push(`${state.files[index].file_name} --> ${item.message.includes('already exist') ? 'Already loaded. Check overwrite button to overwrite data.' : item.message }`)
                      }
                  }
              )
              setData({
                  ...state,
                  isLoading: false,
                  message: ok_message,
                  errors: err_message
              })
          })
          .catch(err => console.log(err))
    } else if (idx == 0) {
      alert("Please select one column with the observed property: water-depth")
    } else {
      alert("Plaese select only ONE column with the observed property: water-depth")
    }  
  }
  function handleClickSubmit(form) {

    let idx = form.outputs.filter(item => item.name == 'water-depth').length
    let system_id = form.system_id.length
    let idx_empty = form.outputs.filter(item => item.name == '').length
    if (idx == 1 && system_id > 0 && idx_empty==0) {
      setData({...state, isLoading: true})
      let test_data = {
        ...omit(form, ['message', 'error'])
      }
      let promises = state.files.map(
        (item, index) => {
          var begin_time = "";
          var end_time = "";
          if (item.datetime.length==1) {
            begin_time = item.datetime.map(
              item2 => item2.toISOString()
            ).join(",");
            end_time = item.datetime.map(
              item2 => item2.toISOString()
            ).join(",");
          } else {
            begin_time = item.datetime[0].toISOString()
            end_time = item.datetime[item.datetime.length-1].toISOString()
          }
          if (begin_time.length > 0 && end_time.length > 0) {
            try {
              return import('../../wasm/profile/pkg/profile')
                  .then(res => res.upload(
                    test_data,
                    item.data,
                    begin_time,
                    end_time,
                    service,
                    state.overwrite,
                    {
                      method: "POST",
                      auth: `Bearer ${token}`
                    },
                    `/istsos/wa/istsos/services`
                  ).then(res => {
                    let newArray = [...state.files]
                    newArray[index] = {...newArray[index], isLoading: false} 
                    setData({
                      ...state,
                      files: newArray
                    })
                    return res
                  }).catch(err => {
                    if (Object.keys(err).indexOf("message") > -1) {
                      return {
                        success: false,
                        message: `Data uploading FAILED: ${err.message}`
                      }
                    } else {
                      return {
                        success: false,
                        message: `Data uploading FAILED`
                      }
                    }  
                  })
                )
            } catch (error) {
              if (Object.keys(error).indexOf("message") > -1) {
                return {
                  success: false,
                  message: `Data uploading FAILED: ${error.message}`
                }
              } else {
                return {
                  success: false,
                  message: `Data uploading FAILED`
                }
              }
            }
          } else {
            return {
              success: false,
              message: "Datetime is empty."
            }
          }
        }
      )
      Promise.all(promises)
        .then(res => {
          let ok_message = []
          let err_message = []
          res.map(
            (item, index) => {
              if (item.success) {
                ok_message.push(`${state.files[index].file_name} --> ${item.message}`)
              } else {
                err_message.push(`${state.files[index].file_name} --> ${item.message}`)
              }
            }
          )
          setData({
            ...state,
            isLoading: false,
            message: ok_message,
            errors: err_message
          })
        })
        .catch(err => {
          setData({
            ...state,
            isLoading: false,
            errors: [err.message]
          })
        })
    } else if (system_id == 0) {
      alert("Please write the procedure name")
    } else if (idx_empty>0) {
      alert("Please select all the observed properties")
    } else if (idx == 0) {
      alert("Please select one column with the observed property: water-depth")
    } else {
      alert("Plaese select only ONE column with the observed property: water-depth")
    }
  }
  let obs_message =  undefined;
  if (state.files.length> 0 && state.mode === 'add-profile') {
    obs_message = state.files[0].file_info.observedproperties.join(',')
  }
  
  return <div className='container'>
    <div className="containertitle">
      <div className='title'><div>Data importer</div></div>
    </div>
    {
      state.isLoading ? <div className="loader">
        <div className='loading-gif'>
          <Loader
            type="Bars"
            color="#000"
            height={100}
            width={100}
          />
        </div>
      </div> : <></>
    }
    <div className="containercontent">
      <ImporterComponent
        mode={state.mode}
        delimiter={state.delimiter}
        onChangeMode={onChangeMode}
        onChange={(e)=>setData({...state, delimiter: e.target.value})}
        handleMultiFileChosen={handleMultiFileChosen}
        handleOverwrite={handleOverwrite}
        handleClickSubmit={handleClickSubmit}
        onChangeSelect={onChangeSelect}
        procedures={state.procedures}
        handleUpload={handleUpload}
        errors={state.errors}
        messages={state.message}
        files={state.files}
        token={token}
        service={service}
        overwrite={state.overwrite}
        outputs={state.form.outputs}
      /> 
    </div>
  </div>
}

export default ImporterContainer