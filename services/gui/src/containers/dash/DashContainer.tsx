import {useEffect, useState} from 'react'

// external components
import Loader from 'react-loader-spinner'
import crossfilter from 'crossfilter2';
import * as turf from '@turf/turf'
import Iframe from 'react-iframe'

// components
import SideLayout from '../../components/layout/sidelayout'
import Options from '../../components/options/options'
import MapComponent from '../../components/map/mapcomponent'
import ProcedureFieldset from '../../components/form/fieldsets/procedurefieldset'
import GrafanaFieldset from '../../components/form/fieldsets/grafanafieldset'
import DateTimeRangePicker from '@wojtekmaj/react-datetimerange-picker/dist/entry.nostyle';


const DashContainer: React.FC<{
  service: string,
  token: string,
  size?: any
}> = ({service, token, size}) => {
  const [dates, onChangeDate] = useState([undefined, undefined]);
  const [viewer_mode, onChangeViewerMode] = useState([{
    label: 'single stats',
    viewer_mode: 'single',
    checked: true
  },
  {
    label: 'compare',
    viewer_mode: 'multi',
    checked: false
  }]);
  const [state, setData] = useState({
    fetch_data: false,
    base_url: `/istsos/wa/istsos/services/`,
    api_geojson_api: "/procedures/operations/geojson",
    data: [],
    min_time: undefined,
    max_time: undefined,
    crossfilter: undefined,
    bbox: undefined,
    openFilter: true,
    isLoading: true,
    no_data: '-9999,-999.99,-999',
    quality_index: '>=100'
  })

  // *****************
  //  useEffect
  // *****************
  useEffect(
    ()=>{

      if (state.isLoading) {
        fetch(
            `${state.base_url}${service}${state.api_geojson_api}`,
            {
              headers: {
                'Authorization': `Bearer ${token}`
              },
              method: 'GET',
            }
          ).then(
            (response) => {
              if (response.ok && response.json) {                                    
                return {
                  success: true,
                  data: response.json()
                }
              } else{
                return {success: false}
              }
            }
          ).then(
            res => {
              if (res.success) {
                return res.data
              } else {
                throw Error(`Can't load ${service} data`)
              }
            }
          ).then(
            res => {
              if (res.features) {
                return res.features.map(
                  item2 => {
                    let point = turf.point(item2.geometry.coordinates)                        
                    let beginposition = Date.parse(item2.properties.samplingTime.beginposition)
                    let endposition = Date.parse(item2.properties.samplingTime.endposition)
                    return {
                      service: service,
                      name: item2.properties.name,
                      op:item2.properties.observedproperties,
                      offerings: item2.properties.offerings,
                      sensortype: item2.properties.sensortype,
                      coords: point,
                      begin_time: beginposition,
                      end_time: endposition,
                      checked: false
                    }
                  }
                )
              } else {
                return []
              }
            }
          ).then(
            res => {
              if (res.length > 0) {
                let points = res.map(item=>item.coords)
                var begin_times = res.map( item => item.begin_time);
                var end_times = res.map( item => item.end_time);
                var min_time  = Math.min.apply(Math, begin_times);
                var max_time  = Math.max.apply(Math, end_times);
                return setData({
                  ...state,
                  isLoading: false,
                  data: [...res],
                  min_time: min_time,
                  max_time: max_time,
                  bbox: turf.bbox(turf.featureCollection(points))
                })
              } else {
                return setData({
                  ...state,
                  isLoading: false,
                })
              }
            }
          ).catch(
            err => alert(err)
          )
      }
    }, [state.isLoading]
  )

  // *****************
  //  FUNCS
  // *****************
  function onOpCheck(proc, op) {
    let data = [...state.data.map(
      item => {
        if (viewer_mode.filter(item=>item.checked)[0].viewer_mode=='multi') {
          return {
            ...item,
            op: [
              ...item.op.map(
                item2 => ({
                  ...item2,
                  checked: item2.name === op ? !item2.checked : item2.checked
                })
              )
            ]
          }
        } else {
          if (item.name === proc) {
            return {
              ...item,
              op: [
                ...item.op.map(
                  item2 => ({
                    ...item2,
                    checked: item2.name === op ? !item2.checked : item2.checked
                  })
                )
              ]
            }
          } else {
            return item
          }
        }
      }
    )]
    setData({
      ...state,
      data: data,
    })
  }
  function onClose(name) {
    let item_begin_time = 0
    let item_end_time = 0 
    let data = [...state.data.map(
      item => {
        if (item.name === name) {
          return {
            ...item,
            checked: !item.checked
          }
        } else {
          if (item.checked && item.begin_time && item.end_time) {
            if (item.begin_time < item_begin_time || item_begin_time===0) {
              item_begin_time = item.begin_time
            }

            if (item.end_time > item_end_time || item_end_time===0) {
              item_end_time = item.end_time
            }
          }
          
          return item
        }
      }
    )]

    let checked = data.filter(item =>item.checked)
    // console.log(new Date(item_begin_time));
    // console.log(new Date(item_end_time));
    let dates_state = [...dates]
    if (checked.length > 0) {
      if (dates_state[0] < new Date(item_begin_time)) {
        dates_state[0] = new Date(item_begin_time)
      }
      if (dates_state[1] > new Date(item_end_time)) {
        dates_state[1] = new Date(item_end_time)
      }
      onChangeDate([...dates_state])
    } else {
      onChangeDate([undefined, undefined])
    }

    setData({
      ...state,
      data: data,
      max_time: item_end_time !== 0 ? item_end_time : state.max_time,
      min_time: item_begin_time !== 0 ? item_begin_time : state.min_time,
    })
  }
  function onChangeMode(e) {
    let new_viewer_mode = viewer_mode.map(
      item => item.label===e.target.value ? {...item, checked: true} : {...item, checked: false}
    )
    onChangeViewerMode([...new_viewer_mode])
    setData({
      ...state,
      data: [...state.data.map(
        item => ({
          ...item,
          checked: false,
          op: [...item.op.map(item2=>({...item2, checked: false}))]
        })
      )]
    })
  }
  function onChangeNoData(e) {
    setData({
      ...state,
      no_data: e.target.value
    })
    
  }
  function onChangeQualityIndex(e) {
    setData({
      ...state,
      quality_index: e.target.value
    })
    
  }
  function onChangeCheck(key, index, el) {
    let item_begin_time = 0
    let item_end_time = 0
    let sensortype = undefined;
    let already_checked = state.data.filter(item =>item.checked)
    var multi = false;
    let data = [...state.data.map(
      item => {
        if (item.name === el.target.value) {
          if (viewer_mode.filter(item=>item.checked)[0].viewer_mode=='multi') {
            if (!item.checked) {
              multi = !item.checked
              if (item.begin_time < item_begin_time || item_begin_time===0) {
                item_begin_time = item.begin_time
              }
  
              if (item.end_time > item_end_time || item_end_time===0) {
                item_end_time = item.end_time
              }
            }
            if (already_checked.length>0) {
              if (item.sensortype===already_checked[0].sensortype) {
                return {
                  ...item,
                  checked: !item.checked
                }
              } else {
                alert("You can't compare different sensortype")
                return item
              }
            } else {
              return {
                ...item,
                checked: !item.checked
              }
            }
          } else {
            if (!item.checked) {
              if (item_begin_time || item_begin_time===0) {
                item_begin_time = item.begin_time
              }
  
              if (item_end_time || item_end_time===0) {
                item_end_time = item.end_time
              }
            }
            return {
              ...item,
              checked: !item.checked,
              op: [...item.op.map(item2 => ({...item2, checked: true}))]
            }
          }
          
        } else {
          if (viewer_mode.filter(item=>item.checked)[0].viewer_mode=='multi') {
            if (item.checked && item.begin_time && item.end_time) {
              if (item.begin_time < item_begin_time || item_begin_time===0) {
                item_begin_time = item.begin_time
              }

              if (item.end_time > item_end_time || item_end_time===0) {
                item_end_time = item.end_time
              }
            }
            return item
          } else {
            return {...item, checked: false}
          }
        }
      }
    )]
    let checked = data.filter(item =>item.checked)
    // console.log(new Date(item_begin_time));
    // console.log(new Date(item_end_time));
    let dates_state = [...dates]
    if(multi) {
      if (item_begin_time!==0) {
        dates_state[0] = new Date(item_begin_time)
      }
      if (item_end_time!==0) {
        dates_state[1] = new Date(item_end_time)
      }
      if (state.max_time) {
        dates_state[1] = new Date(state.max_time)
        var previousweek = new Date(
          dates_state[1].getFullYear(),
          dates_state[1].getMonth(),
          dates_state[1].getDate()-7
        );
        dates_state[0] = previousweek  
      } else {
        var previousweek = new Date(
          dates_state[1].getFullYear(),
          dates_state[1].getMonth(),
          dates_state[1].getDate()-7
        );
        dates_state[0] = previousweek  
      }
      
      onChangeDate(dates_state)
    } else {
      if (item_begin_time!==0) {
        dates_state[0] = new Date(item_begin_time)
      }
      if (item_end_time!==0) {
        dates_state[1] = new Date(item_end_time)
      }
      if (checked.length===0) {
        onChangeDate([undefined, undefined])
      } else {
        var previousweek = new Date(
          dates_state[1].getFullYear(),
          dates_state[1].getMonth(),
          dates_state[1].getDate()-7
        );
        if (previousweek>dates_state[0]) {
          dates_state = [previousweek, dates_state[1]]
        }
        onChangeDate(dates_state)
      }
    }    

    setData({
      ...state,
      data: data,
      max_time: item_end_time !== 0 ? item_end_time : state.max_time,
      min_time: item_begin_time !== 0 ? item_begin_time : state.min_time,
    })
  }

  // *****************
  //  PREPARING DATA
  // *****************
  var procedures = [];
  var checked_procedures = [];
  var activated_procedures = [];
  var grafana_iframes = [];
  if (!state.isLoading && state.data.length > 0) {
    var data_cf = crossfilter([...state.data])
    // preparing dimensions
    let procedures_dim = data_cf.dimension((d) =>(d.name));
    let offerings_dim = data_cf.dimension((d) =>(d.offerings), true);
    let ops_dim = data_cf.dimension((d) =>(d.op), true);
    let begin_time_dim = data_cf.dimension((d) =>(d.begin_time));
    let end_time_dim = data_cf.dimension((d) =>(d.end_time));
    let coords_dim = data_cf.dimension((d)=>(d.coords));
    let checked_dim = data_cf.dimension((d)=>(d.checked));
    let points = state.data.map(item=>item.coords)

    end_time_dim.filterFunction(
      (d)=> d ? true : false
    )
    begin_time_dim.filterFunction(
      (d)=> d ? true : false
    )
    var procedures = data_cf.allFiltered()
    checked_dim.filterFunction(
      (d)=> d
    )
    checked_procedures = data_cf.allFiltered()
    end_time_dim.filterFunction(
      (d)=>(new Date(d)>=dates[0])
    )
    begin_time_dim.filterFunction(
      (d)=>(new Date(d)<=dates[1])
    )
    var activated_procedures = data_cf.allFiltered().map(item => item.name)
    // var base_domain = `${process.env.NEXT_PUBLIC_GRAFANA_PROTOCOL}://${process.env.NEXT_PUBLIC_GN_DOMAIN}:${process.env.NEXT_PUBLIC_GRAFANA_PORT}`
    var base_domain = '';
    if (viewer_mode.filter(item=>item.checked)[0].viewer_mode==='multi') {
      let data_filtered = data_cf.allFiltered()
      // let insitu_ops = [];
      // let profile_ops = [];
      // let virtual_profile_ops = [];
      let insitu_fixed_points = [];
      let virtual_profiles = [];
      let profiles = [];
      data_filtered.map(
          (item) => {
            if (item.sensortype==='insitu-fixed-point') {
              insitu_fixed_points.push(item.name)
              // item.op.map(
              //   item2 => {
              //     if (ops.indexOf(item2.name)>= 0 && insitu_ops.indexOf(item2.name)<0) {
              //       insitu_ops.push(item2.name)
              //     }
              //   }
              // )
            } else if (item.sensortype==='virtual-profile') {
              virtual_profiles.push(item.name)
              // item.op.map(
              //   item2 => {
              //     if (ops.indexOf(item2.name)>= 0 && virtual_profile_ops.indexOf(item2.name)<0) {
              //       virtual_profile_ops.push(item2.name)
              //     }
              //   }
              // )
            } else if (item.sensortype==='profile') {
              profiles.push(item.name)
              // item.op.map(
              //   item2 => {
              //     if (ops.indexOf(item2.name)>= 0 && profile_ops.indexOf(item2.name)<0) {
              //       profile_ops.push(item2.name)
              //     }
              //   }
              // )
            }                  
          }
      )
      if (data_filtered.length > 0) {
        if (insitu_fixed_points.length>0) {
          var base_url = `${base_domain}/grafana/d/compare_insitu-fixed-points/compare_insitu-fixed-points`
          var grafana_vars = `from=${dates[0].getTime()}&to=${dates[1].getTime()}`
          grafana_vars = `${grafana_vars}&orgId=1&var-service=${service}&theme=light&kiosk=1`
          var ops = []
          var procs = data_cf.allFiltered().map(
            item => {
              ops = item.op.filter(
                item2 => item2.checked
              ).map(item2 => `var-observedproperties=${item2.name}`).join('&')
              if (ops.length > 0) {
                return `var-insitufixedpoints=${item.name}`
              } else {
                return ``
              }
            }
          ).filter(item => item)
          if (procs.length>0) {
            grafana_vars = `${grafana_vars}&${procs.join('&')}&${ops}`
            grafana_vars = `${grafana_vars}&reload=${Math.random().toString(16).substr(2, 14)}`
            grafana_iframes = [`${base_url}?${grafana_vars}`]
          } else {
            grafana_iframes = []
          }
        } else if (profiles.length > 0) {
          var base_url = `${base_domain}/grafana/d/profile/profile`
          var grafana_vars = `from=${dates[0].getTime()}&to=${dates[1].getTime()}`
          grafana_vars = `${grafana_vars}&orgId=1&var-service=${service}&theme=light&kiosk=1`
          var ops = []
          var procs = data_cf.allFiltered().map(
            item => {
              ops = item.op.filter(
                item2 => item2.checked
              ).map(item2 => `var-profile_observedproperties=${item2.name}`).join('&')
              if (ops.length > 0) {
                return `var-profiles=${item.name}`
              } else {
                return ``
              }
            }
          ).filter(item => item)
          if (procs.length>0) {
            grafana_vars = `${grafana_vars}&${procs.join('&')}&${ops}`
            grafana_vars = `${grafana_vars}&reload=${Math.random().toString(16).substr(2, 14)}`
            grafana_iframes = [`${base_url}?${grafana_vars}`]
          } else {
            grafana_iframes = []
          }
        } else if (virtual_profiles.length > 0) {
          grafana_iframes = []
        } else {
          grafana_iframes = []
        }
      } else {
        grafana_iframes = []
      }
    } else {
      grafana_iframes = data_cf.allFiltered().map(
        (item) => {
          var grafana_vars = ''
          var obs = ''
          if (item.sensortype==='insitu-fixed-point') {
            var base_url = `${base_domain}/grafana/d/insitu-fixed-point/insitu-fixed-point`            
            obs = item.op.filter(
              item2 => item2.checked
            ).map(item2 => `var-observedproperties=${item2.name}`).join('&')
            
            grafana_vars = `from=${dates[0].getTime()}&to=${dates[1].getTime()}`
            grafana_vars = `${grafana_vars}&orgId=1&var-service=${item.service}&theme=light&kiosk=1`
            grafana_vars = `${grafana_vars}&var-insitufixedpoints=${item.name}`
            grafana_vars = `${grafana_vars}&${obs}`
            grafana_vars = `${grafana_vars}&reload=${Math.random().toString(16).substr(2, 14)}`
          } else if (item.sensortype==='virtual-profile') {
            var base_url = `${base_domain}/grafana/d/virtual-profile/virtual-profile`
            obs = item.op.filter(
              item2 => {
                if (item2.checked) {
                  return `var-observedproperties=${item}`
                }
              }
            ).join('&')
            grafana_vars = `from=${dates[0].getTime()}&to=${dates[1].getTime()}`
            grafana_vars = `${grafana_vars}&orgId=1&var-service=${item.service}&theme=light&kiosk=1`
            grafana_vars = `${grafana_vars}&var-virtual-profiles=${item.name}`
            grafana_vars = `${grafana_vars}&${obs}`
            grafana_vars = `${grafana_vars}&reload=${Math.random().toString(16).substr(2, 14)}`
          } else if (item.sensortype==='profile') {
            var base_url = `${base_domain}/grafana/d/profile/profile`
            obs = item.op.filter(
              item2 => item2.checked
            ).map(item2 => `var-profile_observedproperties=${item2.name}`).join('&')
            grafana_vars = `from=${dates[0].getTime()}&to=${dates[1].getTime()}`
            grafana_vars = `${grafana_vars}&orgId=1&var-service=${item.service}&theme=light&kiosk=1`
            grafana_vars = `${grafana_vars}&var-profiles=${item.name}`
            grafana_vars = `${grafana_vars}&${obs}`
            grafana_vars = `${grafana_vars}&reload=${Math.random().toString(16).substr(2, 14)}`
          }
          if (obs.length > 0) {
            return `${base_url}?${grafana_vars}&var-null=${state.no_data}&var-qi=${state.quality_index}`
          } else {
            return undefined
          }
        }
      )
    }
  }
  grafana_iframes = grafana_iframes.filter(item => item != undefined)
  // *****************
  //  RENDERING
  // *****************

  return <div className='container' style={{padding: 0}}>
    <div className='title'><div>Data viewer</div></div>
    {
      state.isLoading ? <div className="loader">
        <div className='loading-gif'>
          <Loader
            type="Bars"
            color="#000"
            height={100}
            width={100}
          />
        </div>
      </div>: <></>
    }
    <SideLayout>
      {/* <div>Title</div> */}

      <div>
        <div style={{
          fontSize: "0.75rem",
          lineHeight: "1rem"
        }}>From</div>
        <DateTimeRangePicker
          key={`${dates[0]}-${dates[1]}-${state.max_time}-${state.min_time}`}
          value={dates}
          disableCalendar={false}
          locale={navigator.language}
          onChange={onChangeDate}
          rangeDivider={'to'}
          disableClock={true}
          maxDate={new Date(state.max_time)}
          minDate={new Date(state.min_time)}
          clearIcon={null}
          calendarIcon={null}
        />
        <Options
          data={viewer_mode.map(
            (item, index) => ({
              type: "radio",
              value: item.label,
              name: item.viewer_mode,
              checked: item.checked,
              onChange: (e) => onChangeMode(e)
            })
          )}
          title="Viewer mode"
        />
        <Options
          data={[{
              type: "text",
              value: state.quality_index,
              onChange: (e) => onChangeQualityIndex(e)
          }]}
          title="Quality Index"
        />
        <Options
          data={[{
              type: "text",
              value: state.no_data,
              onChange: (e) => onChangeNoData(e)
          }]}
          title="No data"
        />
        <Options
          data={procedures.map(
            (item, index) => ({
              type: viewer_mode.filter(item => item.checked)[0].viewer_mode ==='multi' ? "checkbox" : "radio",
              value: item.name,
              checked: item.checked,
              onChange: (e) => onChangeCheck('procedures', index, e),
              disabled: activated_procedures && item.checked ? activated_procedures.indexOf(item.name) == -1 : false
            })
          )}
          title="Procedures"
        />
        {checked_procedures.map(
          (item, index) => <ProcedureFieldset
            key={`key-proc-fieldset-${index}`}
            name={item.name}
            metadata={item.op.map(item2 => ({
              ...item2,
              disabled: activated_procedures.indexOf(item.name) == -1,
              onCheck: () => onOpCheck(item.name, item2.name)
            }))}
            beginposition={new Date(item.begin_time).toISOString()}
            endposition={new Date(item.end_time).toISOString()}
            onClose={() => onClose(item.name)}
          />
        )}
        <fieldset>
        <MapComponent
          data={checked_procedures.length > 0 ? checked_procedures : procedures}
          // bbox={turf.bboxPolygon(state.form.bbox)}
          // onChangeBBOX={onChangeBBOX}
        />
        </fieldset>
      </div>
      <div style={{height: '100%'}}>
        {
          grafana_iframes.map(item =>
            <GrafanaFieldset
              src={item}
              legend={'ciao'}
            />
          )
        }
      </div>
    </SideLayout>
  </div>
}

export default DashContainer