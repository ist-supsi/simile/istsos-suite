
// ext lib
import { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import Form from '../../components/form/form'
import { title } from 'process'
import { Description } from '@material-ui/icons'

import { CardList } from '../../components/cardlist/cardlist'
import FormAnalyzer from '../../components/form/formanalyzer'
import SideLayout from '../../components/layout/sidelayout'
import { TaskSimple } from '../../components/task/task'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faArrowAltCircleUp
} from '@fortawesome/free-solid-svg-icons'

const init_data = {
  tag: 'analysis',
  title: 'Analysis',
  isLoading: true,
  base_url: `/istsos/`,
  api_indicators: 'indicators',
  api_procedures: 'configsections/geo',
  form: {},
  data: [],
  procedures: [],
  services: [],
  plugin_selected: undefined,
  selected_indicator: undefined,
  message: "",
  error: "",
}

const AnalysisContainer: React.FC<{service: string, token: string}> = ({service, token}) => {
  const [state, setData] = useState(init_data)
  const router = useRouter()

  let service_url = service ? `/${service}/` : '/' 
  useEffect(
    () => {
      let promise1 = fetch(
        `${state.base_url}${state.api_indicators}`,
        {
          headers: {
            'Authorization': `Bearer ${token}`
          },
          method: 'GET',
        }
      ).then(
        res => res.json()
      ).catch(
        err => alert(err)
      )

      let promise2 = fetch(
        `${state.base_url}wa/istsos/services/${service}/procedures/operations/getlist`,
        {
          headers: {
            'Authorization': `Bearer ${token}`
          },
          method: 'GET',
        }
      ).then(
        res => res.json()
      ).catch(
        err => alert(err)
      );

      let promise3 = fetch(
        `${state.base_url}wa/istsos/operations/status`,
        {
          headers: {
            'Authorization': `Bearer ${token}`
          },
          method: 'GET',
        }
      ).then(
        res => res.json()
      ).catch(
        err => alert(err)
      );
      let promises = [ promise1, promise2, promise3 ];
      Promise.all(promises).then(
        res => {
          setData({
            ...state,
            services: res[2]['data'] ? res[2]['data'] : [],
            data: res[0]['data'] ? res[0]['data'] : [],
            procedures: res[1]['data'] ? res[1]['data'] : []
          })
        }
      )
    }
  , [])

  function onClick(i) {
    setData({
      ...state,
      selected_indicator: undefined,
      plugin_selected: i+1,
      form: {}
    })
  }
  function onClickItem(ind, i) {
    setData({
      ...state,
      selected_indicator: i,
      form: {...ind.form},
    })    
  }
  function onChangeFormItem(e) {
    if (e.target.type === 'checkbox') {
      var v = e.target.checked
    } else {
      v = e.target.value
    }
    console.log(e.target.id);
    
    setData({
      ...state,
      form: {
        ...state.form,
        [e.target.id]: {
          ...state.form[e.target.id],
          value: v
        }
      }
    })
    
  }
  function onSubmit() {
    console.log(state.form);
    fetch(
      `${state.base_url}${state.api_indicators}/${service}/${state.data[state.plugin_selected-1].name}/${state.selected_indicator}`,
      {
        headers: {
          'Authorization': `Bearer ${token}`
        },
        method: 'POST',
        body: JSON.stringify(state.form)
      }
    ).then(response => {
      if (response.ok && response.json) {                                    
        return response.json()
      } else{
        return {success: false}
      }
    }).then(res => {
      if (res.success) {
        alert(res.message)
      } else {
        alert(res.message)
      }
    }).catch(
      (e) => {
        alert('Request failed!')
      }
    )
  }
  return <div className='container' style={{padding: 0}}>
    <CardList
      data={state.data.map(
        (item, index) => ({
          title: item.title,
          description: item.description,
          icon: item.icon,
          selected: index === state.plugin_selected-1
        })
      )}
      onClick={onClick}
    />
    {
      state.plugin_selected ?
      <div>
        <SideLayout>
          <div>
          {
            state.plugin_selected ?
              Object.keys(state.data[state.plugin_selected-1].indicators).map(
                (item, index) => <TaskSimple
                  key={`key-indicaror-${index}`}
                  onClick={() => onClickItem(state.data[state.plugin_selected-1].indicators[item], item)}
                  selected={item === state.selected_indicator}
                  title={state.data[state.plugin_selected-1].indicators[item].title}
                  subtitle={state.data[state.plugin_selected-1].indicators[item].subtitle}
                />
              ) :
              <></>
          }
          </div>
          <div>
            <div style={{display: "flex", flexDirection: 'row', width: "100%"}}>
              {
                Object.keys(state.form).length > 0 ?
                <div style={{width: "inherit"}}>
                  <div className="title">{state.data[state.plugin_selected-1].indicators[state.selected_indicator].title}</div>
                  <div className="description">{state.data[state.plugin_selected-1].indicators[state.selected_indicator].description}</div>
                  <FormAnalyzer
                    onSubmit={onSubmit}
                    onChange={onChangeFormItem}
                    services={state.services}
                    procedures={state.procedures}
                    form={state.form}
                  />
                </div> :
                <></>
              }
            </div>
          </div>
        </SideLayout>
      </div>:
      <div className='div-centered'>
        <div className="suggestion">
          <FontAwesomeIcon style={{width: '50px'}} icon={faArrowAltCircleUp} />
          <h6>Select one module</h6>
        </div>
      </div>
    }
  </div>
}

export default AnalysisContainer