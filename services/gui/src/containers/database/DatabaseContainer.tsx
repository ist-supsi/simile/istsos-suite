
// ext lib
import { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import Form from '../../components/form/form'

const init_data = {
  tag: 'database',
  title: 'Database',
  isLoading: true,
  form: {
    user: "",
    password: "",
    host: "",
    port: 0,
    dbname: ""
  },
  default: {
    user: "postgres",
    password: "postgres",
    host: "localhost",
    port: 5432,
    dbname: "istsos"
  },
  base_url: `/istsos/wa/istsos/services`,
  api_url: 'configsections/connection',
  api_test_url: `/istsos/wa/istsos/operations/validatedb`,
  data: {},
  message: "",
  error: "",
}


const DatabaseContainer: React.FC<{service: string, token: string}> = ({service, token}) => {
  const [state, setData] = useState(init_data)
  const router = useRouter()

  let service_url = service ? `/${service}/` : '/' 

  useEffect(
    () => {
      fetch(
        `${state.base_url}${service_url}${state.api_url}`,
        {
          headers: {
            'Authorization': `Bearer ${token}`
          },
          method: 'GET',
        }
      ).then(
        (response) => {
          if (response.ok && response.json) {                                    
            return response.json()
          } else{
            return {success: false}
          }
        }
      ).then(
        (res) => {
          if (res.success) {
            return setData({
              ...state,
              form: {
                ...state.form,
                ...res.data
              },
              data: {
                ...state.data,
                ...res.data
              },
              isLoading: false
            })
          } else {
            return alert(res.message)
          }
        }
      )
    }
  , [service])

  // ********
  // FUNCs
  // *******
  function onChange(key, e) {
    setData({
      ...state,
      form: {
        ...state.form,
        [key]: e.target.value
      }
    })
  }

  function onValidate(e) {
    e.preventDefault();
    fetch(
      `${state.api_test_url}`,
      {
        headers: {
          'Authorization': `Bearer ${token}`
        },
        method: 'POST',
        body: JSON.stringify(state.form)
      }
    ).then(response => {
      if (response.ok && response.json) {                                    
        return response.json()
      } else{
        return {success: false}
      }
    })
    .then(res => {
      if (res.success) {
        router.push({
          ...router,
          query: {
            ...router.query,
            reload: Math.floor(Math.random() * (50 - 0 + 1)) + 0
          }
        })
        return alert(
          `${res.message}\n\nThe database is ${res.data.database}`
        )
      } else {
        return alert(res.message)
      }
    }).catch(
      (e) => {
        alert('Request failed!')
        console.log(e);
      }
    )
  }

  function onSubmit(e) {
    e.preventDefault();
    fetch(
      `${state.base_url}${service_url}${state.api_url}`,
      {
        headers: {
          'Authorization': `Bearer ${token}`
        },
        method: 'PUT',
        body: JSON.stringify(state.form)
      }
    ).then(response => {
      if (response.ok && response.json) {                                    
        return response.json()
      } else{
        return {success: false}
      }
    })
    .then(res => {
      if (res.success) {
        router.push({
          ...router,
          query: {
            ...router.query,
            reload: Math.floor(Math.random() * (50 - 0 + 1)) + 0
          }
        })
        return alert(res.message)
      } else {
        return alert(res.message)
      }
    }).catch(
      (e) => {
        alert('Request failed!')
        console.log(e);
      }
    )
  }
  const form_data = [
    {
      title: state.title,
      inputs: [
        {
          label: 'User',
          type: 'text',
          value: state.form.user,
          onChange:(e) => onChange('user', e)
        },
        {
          label: 'Password',
          type: 'text',
          value: state.form.password,
          onChange:(e) => onChange('password', e)
        },
        {
          label: 'Host',
          type: 'text',
          value: state.form.host,
          onChange:(e) => onChange('host', e)
        },
        {
          label: 'Port',
          type: 'number',
          value: state.form.port,
          onChange:(e) => onChange('port', e)
        },
        {
          label: 'Database',
          type: 'text',
          value: state.form.dbname,
          onChange:(e) => onChange('dbname', e)
        },
      ]
    }
  ]
  return <div className='container'>
    <div className="containertitle">
      <div className='title'><div>{state.title}</div></div>
    </div>
    <div className="containercontent">
      <Form
        onSubmit={onSubmit}
        fields={form_data}
        edit={true}
      />
    </div>
  </div>
}

export default DatabaseContainer