
// external lib
import {useRouter} from 'next/router'
import { useReducer, useEffect, useState } from 'react'
import Loader from 'react-loader-spinner'
import Layout from '../../components/layout/layout'

// reducers
import { initialService, actionsReducer } from "../../reducers/service/reducer_viewer";

// components
import SideLayout from '../../components/layout/sidelayout'
import SideBar from '../../components/sidebar/sidebar'
import { Router } from 'next/router';
import Modal from '../../components/modal/modal';

// containers
import ProviderContainer from '../provider/ProviderContainer'
import ProcedureViewerContainer from '../procedure/ProcedureViewerContainer';
import IdentificationContainer from '../identification/IdentificationContainer';
import CoordinatesContainer from '../coordinates/CoordinatesContainer';
import MqttContainer from '../mqtt/MqttContainer';
import DashContainer from '../dash/DashContainer'

const IndexViewerContainer: React.FC<{
  service: string, authenticated:boolean, token: string, logout: Function, user: any, role: string
  }> = ({service, token, authenticated, logout, user, role}) => {
  const [state, setData] = useState({reload: 0, modal: false, loading: true})
  const [reducer, dispatch] = useReducer(actionsReducer, initialService(service))
  const router = useRouter()
  // ************
  //  CONTENT
  // ************

  function createContent(e) {
    switch (e.type) {
      case 'procedure':
        return <ProcedureViewerContainer service={service} token={token}/>
      case 'provider':
        return <ProviderContainer service={service} token={token} user={user}/>
      case 'identification':
        return <IdentificationContainer service={service} token={token} user={user}/>
      case 'coordinates':
        return <CoordinatesContainer service={service} token={token} user={user}/>
      case 'mqtt':
        return <MqttContainer service={service} token={token} user={user}/>
      case 'dashboard':
        return <DashContainer service={service} token={token} />
      default:
        return <div>Sorry content N/A. Something wrong.</div>
    }
  }

  // ************
  //  ON ADD OPEN MODAL
  // ************

  function onBtnAdd() {
    setData({
      ...state,
      modal: !state.modal
    })
  }


  // ************
  //  useEeffect
  // ************
  useEffect(
    () => {
      if (reducer.length > 0) {
        let isLoading = reducer.filter(item => item.isLoading);
        if (isLoading.length === 0) {
          if (typeof(router.query.id) === 'string') {
            var promises = initialService(router.query.id).map((item, index) => {
              if (item.isLoading && item.url) {
                return fetch(
                  item.url,
                  {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ' + token
                    }
                  }
                ).then(
                  res => {
                    if (res.status == 200) {
                      return res.json()
                    } else {
                      return {
                        success: false,
                        error: 'Can\'t retrieve data' 
                      }
                    }
                  }
                )  
              } else {
                return item
              }
              
            })
            Promise.all(promises).then(
              res => {
                if (typeof(router.query.id) === 'string') {
                  return initialService(router.query.id).map(
                    (item, index) => {
                      return {
                        ...item,
                        data: res[index]['data'],
                        isLoading: false
                      }
                    }
                  )
                } else {
                  return state
                }
              }
            ).then(
              res => dispatch({
                data: res,
                type: 'CHANGE_SERVICE'
              })
            )
            // dispatch({
            //   data: router.query.id,
            //   type: 'CHANGE_SERVICE'
            // })
            setData({
              ...state,
              loading: false,
              reload: state.reload+1
            })
          }
        }
      }
      var promises2 = reducer.map((item, index) => {
        if (item.isLoading && item.url) {
          console.log('reloading');
          return fetch(
            item.url,
            {
              method: 'GET',
              headers: {
                  'Accept': 'application/json',
                  'Authorization': 'Bearer ' + token
              }
            }
          ).then(
            res => {
              if (res.status == 200) {
                return res.json()
              } else {
                return {
                  success: false,
                  error: 'Can\'t retrieve data' 
                }
              }
            }
          )  
        } else {
          return item
        }
        
      })
      Promise.all(promises2).then(
        res => {
          return reducer.map(
            (item, index) => {
              return {
                ...item,
                data: res[index]['data'],
                isLoading: false
              }
            }
          )
        }
      ).then(
        res => dispatch({
          data: res,
          type: 'LOAD'
        })
      ).then(
        r => setData({
          ...state,
          loading: false,
        })
      )
      setData({
        ...state,
        modal: false
      })
    }
  , [state.reload, router.query.id, router.query.reload])

  // ************
  //  RENDERING
  // ************
  let isLoading = reducer.filter(item => item.isLoading);
  let content = <div>Sorry N/A. Something wrong</div>;
  let type_selected = 'provider'
  if (router.query) {
    if (router.query.tag) {
      let reducer_filtered = reducer.filter(
        item => (item.tag === router.query.tag)
      )[0]
      content = createContent(reducer_filtered)
      if (typeof(router.query.tag) === 'string') {
        type_selected = router.query.tag
      }      
    } else {
      content = createContent({
        type: 'provider'
      })
    }
  } else {
    content = createContent({
      type: 'provider'
    })
  } 


  if (state.loading) {
    return <div className="loader">
      <div className='loading-gif'>
        <Loader
          type="Bars"
          color="#000"
          height={100}
          width={100}
        />
      </div>
    </div>
  } else {
    return <Layout authenticated={authenticated} user={user} logout={logout} role={role}>
      {isLoading.length > 0 ? <div className="loader">
        <div className='loading-gif'>
          <Loader
            type="Bars"
            color="#000"
            height={100}
            width={100}
          />
        </div>
      </div> : <></>
      }
      <SideBar
        title={service}
        home={'/'}
        data={reducer.filter(item => item.type != 'hidden').map(
          item => ({
            ...item,
            pathname: '/service',
            query: {
              ...router.query,
              tag: item.tag
            },
            selected: item.tag === type_selected
          })
        )}
        navlist={reducer.length > 0 ? reducer[0].data.map(
          (item, index)=>({
            label: item.service,
            value: index+1,
            pathname: `/service`,
            query: {
              id: item.service
            }
          })    
        ) : []}
      />
      {content}
      {
        state.modal ? 
        <Modal onModalClose={onBtnAdd} title={'ciao'}>
          {content}
        </Modal> : <></>
      }
    </Layout>
  }
}

export default IndexViewerContainer