/** @type {import('next').NextConfig} */
module.exports = {
  webpack5: false,
  experiments: {
    asyncWebAssembly: true,
    syncWebAssembly: true,
    importAsync: true
  }
}

// module.rules = {
//   type: "webassembly/async"
// }