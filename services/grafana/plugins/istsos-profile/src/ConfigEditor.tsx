import React, { ChangeEvent, PureComponent, SyntheticEvent } from 'react';
import { LegacyForms } from '@grafana/ui';
import { DataSourcePluginOptionsEditorProps } from '@grafana/data';
import { MyDataSourceOptions } from './types';

const { SecretFormField, FormField, Switch } = LegacyForms;

interface Props extends DataSourcePluginOptionsEditorProps<MyDataSourceOptions> {}

interface State {}

export class ConfigEditor extends PureComponent<Props, State> {
  onPathChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { onOptionsChange, options } = this.props;
    const jsonData = {
      ...options.jsonData,
      path: event.target.value,
    };
    onOptionsChange({ ...options, jsonData });
  };

  // Secure field (only sent to the backend)
  onUserChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { onOptionsChange, options } = this.props;
    onOptionsChange({
      ...options,
      jsonData: {
        ...options.jsonData,
        user: event.target.value,
      },
    });
  };

  onResetUser = () => {
    const { onOptionsChange, options } = this.props;
    onOptionsChange({
      ...options,
      secureJsonFields: {
        ...options.secureJsonFields,
        user: false,
      },
      jsonData: {
        ...options.jsonData,
        user: '',
      },
    });
  };

  // Password funcs
  onPasswordChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { onOptionsChange, options } = this.props;
    onOptionsChange({
      ...options,
      jsonData: {
        ...options.jsonData,
        password: event.target.value,
      },
    });
  };

  onResetPassword = () => {
    const { onOptionsChange, options } = this.props;
    onOptionsChange({
      ...options,
      secureJsonFields: {
        ...options.secureJsonFields,
        password: false,
      },
      jsonData: {
        ...options.jsonData,
        password: '',
      },
    });
  };

  // client id funcs
  onClientIdChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { onOptionsChange, options } = this.props;
    onOptionsChange({
      ...options,
      jsonData: {
        ...options.jsonData,
        oauthclientid: event.target.value,
      },
    });
  };

  onResetClientId = () => {
    const { onOptionsChange, options } = this.props;
    onOptionsChange({
      ...options,
      secureJsonFields: {
        ...options.secureJsonFields,
        oauthclientid: false,
      },
      jsonData: {
        ...options.jsonData,
        oauthclientid: '',
      },
    });
  };

  // token url funcs
  onTokenUrlChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { onOptionsChange, options } = this.props;
    onOptionsChange({
      ...options,
      jsonData: {
        ...options.jsonData,
        oauthtokenurl: event.target.value,
      },
    });
  };

  onResetTokenUrl = () => {
    const { onOptionsChange, options } = this.props;
    onOptionsChange({
      ...options,
      secureJsonFields: {
        ...options.secureJsonFields,
        oauthtokenurl: false,
      },
      jsonData: {
        ...options.jsonData,
        oauthtokenurl: '',
      },
    });
  };

  // client secret funcs
  onClientSecretChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { onOptionsChange, options } = this.props;
    onOptionsChange({
      ...options,
      jsonData: {
        ...options.jsonData,
        oauthclientsecret: event.target.value,
      },
    });
  };

  onResetClientSecret = () => {
    const { onOptionsChange, options } = this.props;
    onOptionsChange({
      ...options,
      secureJsonFields: {
        ...options.secureJsonFields,
        oauthclientsecret: false,
      },
      jsonData: {
        ...options.jsonData,
        oauthclientsecret: '',
      },
    });
  };

  // oauth
  onOauthChange = (event: SyntheticEvent<HTMLInputElement>) => {
    const { onOptionsChange, options } = this.props;
    const jsonData = {
      ...options.jsonData,
      oauth: !options.jsonData.oauth,
    };
    onOptionsChange({ ...options, jsonData });
  };

  render() {
    const { options } = this.props;
    const { jsonData, secureJsonFields } = options;
    // const secureJsonData = (options.secureJsonData || {}) as MySecureJsonData;

    return (
      <div className="gf-form-group">
        <div className="gf-form">
          <FormField
            label="istSOS url path"
            labelWidth={6}
            inputWidth={20}
            onChange={this.onPathChange}
            value={jsonData.path || ''}
            placeholder="istSOS url path"
          />
          <Switch checked={jsonData.oauth || false} label="OAUTH" onChange={this.onOauthChange} />
        </div>
        <div className="gf-form-inline">
          <div className="gf-form">
            <FormField
              value={jsonData.user || ''}
              label="User"
              placeholder="user"
              labelWidth={6}
              inputWidth={20}
              onReset={this.onResetUser}
              onChange={this.onUserChange}
            />
            <SecretFormField
              isConfigured={(secureJsonFields && secureJsonFields.password) as boolean}
              value={jsonData.password || ''}
              label="Password"
              placeholder="password"
              labelWidth={6}
              inputWidth={20}
              onReset={this.onResetPassword}
              onChange={this.onPasswordChange}
            />
          </div>
        </div>
        {jsonData.oauth ? (
          <div className="gf-form">
            <div>
              <FormField
                value={jsonData.oauthclientid || ''}
                label="Oauth client id"
                placeholder="client id"
                labelWidth={6}
                inputWidth={20}
                onReset={this.onResetClientId}
                onChange={this.onClientIdChange}
              />
              <FormField
                value={jsonData.oauthtokenurl || ''}
                label="Oauth url"
                placeholder="token url"
                labelWidth={6}
                inputWidth={20}
                onReset={this.onResetTokenUrl}
                onChange={this.onTokenUrlChange}
              />
            </div>
            <div>
              <SecretFormField
                isConfigured={(secureJsonFields && secureJsonFields.oauthclientsecret) as boolean}
                value={jsonData.oauthclientsecret || ''}
                label="Oauth secret"
                placeholder="client secret"
                labelWidth={6}
                inputWidth={20}
                onReset={this.onResetClientSecret}
                onChange={this.onClientSecretChange}
              />
            </div>
          </div>
        ) : (
          <></>
        )}
      </div>
    );
  }
}
