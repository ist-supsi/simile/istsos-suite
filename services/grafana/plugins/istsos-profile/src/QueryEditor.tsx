import defaults from 'lodash/defaults';

import React, { ChangeEvent, PureComponent } from 'react';
import { LegacyForms } from '@grafana/ui';
import { QueryEditorProps } from '@grafana/data';
import { DataSource } from './datasource';
import { defaultQuery, MyDataSourceOptions, MyQuery } from './types';

const { FormField } = LegacyForms;

type Props = QueryEditorProps<DataSource, MyQuery, MyDataSourceOptions>;

export class QueryEditor extends PureComponent<Props> {
  onProcedureChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { onChange, query, onRunQuery } = this.props;
    onChange({ ...query, procedure: event.target.value });
    // executes the query
    onRunQuery();
  };

  onOpChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { onChange, query, onRunQuery } = this.props;
    onChange({ ...query, observedproperty: event.target.value });
    // executes the query
    onRunQuery();
  };

  onServiceChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { onChange, query, onRunQuery } = this.props;
    onChange({ ...query, service: event.target.value });
    // executes the query
    onRunQuery();
  };

  render() {
    const query = defaults(this.props.query, defaultQuery);
    const { procedure, observedproperty, service } = query;

    return (
      <div className="gf-form">
        <FormField width={4} value={service || ''} onChange={this.onServiceChange} label="Service" />
        <FormField
          labelWidth={8}
          value={procedure || ''}
          onChange={this.onProcedureChange}
          label="Procedure"
          tooltip="procedure name"
        />
        <FormField
          labelWidth={8}
          value={observedproperty || ''}
          onChange={this.onOpChange}
          label="Observed properties"
          tooltip="List of observed properties (comma separeted)"
        />
      </div>
    );
  }
}
