import { DataQuery, DataSourceJsonData } from '@grafana/data';

export interface MyQuery extends DataQuery {
  procedure: string;
  observedproperty: string;
  service: string;
}

export const defaultQuery: Partial<MyQuery> = {
  service: 'demo',
  procedure: 'LUGANO',
  observedproperty: 'air:temperature',
};

/**
 * These are options configured for each DataSource instance
 */
export interface MyDataSourceOptions extends DataSourceJsonData {
  path?: string;
  user?: string;
  oauth?: boolean;
  oauthclientid?: string;
  oauthtokenurl?: string;
  password?: string;
  oauthclientsecret?: string;
  token?: string;
}

/**
 * Value that is used in the backend, but never sent over HTTP to the frontend
 */
export interface MySecureJsonData {
  password?: string;
  oauthclientsecret?: string;
}

// url = 'https://simile.ddns.net/istsos'
// service_name = 'ceresioremote'
// user = 'simile-admin'
// password = 'simile'
// oauth_client_id = 'simile-station'
// oauth_token_url = 'https://simile.ddns.net/auth/realms/simile/protocol/openid-connect/token'
// oauth_client_secret = 'e8d7e641-4372-4881-a766-f6d51f893e08'
