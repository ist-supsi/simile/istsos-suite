import defaults from 'lodash/defaults';
import { getBackendSrv, getTemplateSrv } from '@grafana/runtime';

import {
  DataQueryRequest,
  DataQueryResponse,
  DataSourceApi,
  DataSourceInstanceSettings,
  MutableDataFrame,
  FieldType,
} from '@grafana/data';

import { MyQuery, MyDataSourceOptions, defaultQuery } from './types';

interface OpenId {
  client_secret: string;
  username: string;
  password: string;
  grant_type: string;
  client_id: string;
}

export class DataSource extends DataSourceApi<MyQuery, MyDataSourceOptions> {
  jsonData?: MyDataSourceOptions;
  token?: string;
  auth_type?: string;
  path?: string;
  constructor(instanceSettings: DataSourceInstanceSettings<MyDataSourceOptions>) {
    super(instanceSettings);
    this.jsonData = instanceSettings.jsonData;
    this.path = this.jsonData.path;
  }
  async query(options: DataQueryRequest<MyQuery>): Promise<DataQueryResponse> {
    const { range } = options;
    // console.log('targets');
    // console.log(targets);
    // console.log('scopedVars vars');
    // console.log(scopedVars);
    // console.log('template vars');
    // console.log(vars);
    const from = range!.from.valueOf();
    const to = range!.to.valueOf();
    const from_date = new Date(from);
    const to_date = new Date(to);
    //////////////
    // get token
    //////////////
    if (this.jsonData?.oauth) {
      let body: OpenId = {
        client_secret: this.jsonData?.oauthclientsecret || '',
        username: this.jsonData?.user || '',
        password: this.jsonData?.password || '',
        grant_type: 'password',
        client_id: this.jsonData?.oauthclientid || '',
      };
      if (this.jsonData.oauthtokenurl) {
        let token_req = await this.getToken(body, this.jsonData.oauthtokenurl);
        this.token = token_req.access_token;
        this.auth_type = 'Bearer ';
      }
    } else {
      this.auth_type = 'Basic ';
    }
    // Return a constant for each query.
    const get_requests = options.targets.map((target) => {
      const query = defaults(target, defaultQuery);
      // console.log('PRIMA');
      // console.log(query.service);
      // console.log(query.procedure);
      // console.log(query.observedproperty);
      query.observedproperty = getTemplateSrv().replace(query.observedproperty, options.scopedVars);
      query.service = getTemplateSrv().replace(query.service, options.scopedVars);
      query.procedure = getTemplateSrv().replace(query.procedure, options.scopedVars);
      // console.log('DOPO');
      // console.log(query.service);
      // console.log(query.procedure);
      // console.log(query.observedproperty);
      let url = `${this.path}/${query.service}?service=SOS&request=GetObservation&procedure=${
        query.procedure
      }&offering=temporary&eventTime=${from_date.toISOString()}/${to_date.toISOString()}&observedProperty=depth,${query.observedproperty.replace(
        /[-]/g,
        ':'
      )}&responseFormat=application/json&version=1.0.0&qualityIndex=False`;

      const res = this.doRequest(url, 'GET', this.auth_type, this.token).then((res) => {
        if (typeof res === 'string' || typeof res === undefined) {
          return new MutableDataFrame({
            refId: query.refId,
            fields: [
              { name: 'Time', values: [from, to], type: FieldType.time },
              { name: 'Value', values: [], type: FieldType.number },
              { name: 'Depth', values: [], type: FieldType.number },
            ],
          });
        } else if (res) {
          let depth_idx = res['ObservationCollection']['member'][0]['observedProperty'][
            'component'
          ].findIndex((item: any[]) => item.includes('depth'));
          var date_group: any[] = [];
          var values: any[] = [];
          var depth: any[] = [];
          depth = depth.concat([
            ...res['ObservationCollection']['member'][0]['result']['DataArray']['values'].map(
              (item: any[]) => item[depth_idx]
            ),
          ]);
          values = values.concat([
            ...res['ObservationCollection']['member'][0]['result']['DataArray']['values'].map(
              (item: any[]) => item[depth_idx - 1 < 0 ? depth_idx + 1 : depth_idx - 1]
            ),
          ]);
          date_group = date_group.concat([
            ...res['ObservationCollection']['member'][0]['result']['DataArray']['values'].map((item: any[]) => item[0]),
          ]);
          return new MutableDataFrame({
            refId: query.refId,
            fields: [
              { name: 'Time', values: date_group, type: FieldType.time },
              { name: 'Value', values: values, type: FieldType.number },
              { name: 'Depth', values: depth, type: FieldType.number },
            ],
          });
        } else {
          return new MutableDataFrame({
            refId: query.refId,
            fields: [
              { name: 'Time', values: [from, to], type: FieldType.time },
              { name: 'Value', values: [], type: FieldType.number },
              { name: 'Depth', values: [], type: FieldType.number },
            ],
          });
        }
      });
      return res;
    });
    const promised = Promise.all(get_requests);
    const data = await promised;
    return { data };
  }

  // GET REQUEST
  async doRequest(url: string, method: string, auth_type?: string, token?: string, data?: any) {
    const result = await getBackendSrv()
      .request({
        method: 'GET',
        url: url,
        headers: {
          Authorization: `${auth_type}${token}`,
          'Access-Control-Allow-Origin': '*',
        },
      })
      .then((res) => res)
      .catch((err) => console.log(err));
    return result;
  }

  // POST REQUEST
  async doPostRequest(body: any, url: string) {
    const result = await getBackendSrv().fetch({
      method: 'POST',
      url: url,
      data: body,
      hideFromInspector: true,
    });
    return result;
  }
  // GET TOKEN
  async getToken(body: OpenId, url: string) {
    const result = await getBackendSrv()
      .request({
        method: 'POST',
        url: url,
        headers: {
          'content-type': 'application/x-www-form-urlencoded',
        },
        data: body,
        hideFromInspector: true,
      })
      .then((res) => res)
      .catch((err) => console.log(err));
    return result;
  }
  // TEST
  async testDatasource() {
    // Implement a health check for your data source.
    return {
      status: 'success',
      message: 'Success',
    };
  }
}
