import React from 'react';
import { useEffect, useState } from 'react';
import { PanelProps } from '@grafana/data';
import { ProfileOptions } from 'types';
import { useTheme } from '@grafana/ui';
import * as echarts from 'echarts';
import { EChartsType } from 'echarts';

interface Props extends PanelProps<ProfileOptions> {}

interface State {
  mounted: boolean;
  echarts?: EChartsType;
  div_id: string;
}

interface FormatterParams {
  componentType: 'series';
  seriesType: string;
  marker?: string;
  seriesIndex: number;
  seriesName: string;
  name: string;
  dataIndex: number;
  data: Object;
  value: number | number[] | Object;
  encode: Object;
  dimensionNames: String[];
  dimensionIndex: number;
  color: string;
  percent: number;
}

export const ProfilePanel: React.FC<Props> = ({ options, data, width, height }) => {
  const theme = useTheme();
  const [state, setData] = useState({
    mounted: false,
    div_id: Math.random().toString().substr(2, 8),
  } as State);
  useEffect(() => {
    if (state.echarts) {
      var pflChart: EChartsType;
      state.echarts.dispose();
      while (!state.echarts.isDisposed()) {}
      const div_echart = document.getElementById(state.div_id);
      if (div_echart instanceof HTMLElement) {
        pflChart = echarts.init(div_echart, {
          width: 'auto',
          height: 'auto',
        });
        setData({
          ...state,
          mounted: true,
          echarts: pflChart,
        });
      }
    } else {
      const div_echart = document.getElementById(state.div_id);
      if (div_echart instanceof HTMLElement) {
        pflChart = echarts.init(div_echart, {
          width: 'auto',
          height: 'auto',
        });
        setData({
          ...state,
          mounted: true,
          echarts: pflChart,
        });
      }
    }
  }, [width, height, state.mounted]);
  let series = [];
  let x_axis_data: string[] = [];
  let y_axis_data: number[] = [];
  let z_axis_data: number[] = [];
  let v_name = '';
  let x_name = '';
  let y_name = '';
  for (let index = 0; index < data.series.length; index++) {
    const serie = data.series[index];
    let p_serie = serie.fields.filter((item) => item.type === 'string')[0];
    let p_data = p_serie.values.toArray();
    v_name = p_data[0];
    let v_serie = serie.fields.filter((item) => item.type === 'number')[0];
    // v_name = serie.name ? serie.name : '';
    let v_data = v_serie.values.toArray();
    let x_serie = serie.fields.filter((item) => item.type === 'time')[0];
    x_name = '';
    let x_data = x_serie.values.toArray().map((item) => new Date(item).toLocaleString());
    let y_serie = serie.fields.filter((item) => item.type === 'number')[1];
    y_name = 'Depth (m)';
    let y_data = y_serie.values.toArray();
    z_axis_data = z_axis_data.concat(v_data);
    x_axis_data = x_axis_data.concat(x_data);
    y_axis_data = y_axis_data.concat(y_data);
    series.push({
      x_data: x_data,
      x_name: x_name,
      y_data: y_data,
      y_name: y_name,
      v_data: [x_data, y_data, v_data],
      v_name: v_name,
    });
  }
  x_axis_data = Array.from(new Set(x_axis_data));
  y_axis_data = Array.from(new Set(y_axis_data))
    .sort((a, b) => a - b)
    .reverse();
  if (state.mounted && state.echarts && series.length > 0) {
    const option = {
      tooltip: {
        bordeRadius: theme.border.radius.lg,
        borderWidth: theme.border.width.sm,
        borderColor: theme.colors.panelBg,
        backgroundColor: theme.colors.bodyBg,
        textStyle: {
          fontSize: theme.typography.size.xs,
          color: theme.colors.text,
        },
        formatter: (e: FormatterParams) => {
          let marker = e.marker;
          if (typeof e.value === 'number') {
            var v = e.value;
          } else if (e.value instanceof Array) {
            v = e.value[2];
          } else {
            v = -999;
          }
          return '<div><div>' + e.name + '</div><div>' + marker + ' ' + v + ' ' + v_name + '</div></div>';
        },
      },
      xAxis: {
        type: 'category',
        data: x_axis_data,
        name: x_name,
        nameTextStyle: {
          color: theme.colors.text,
        },
        axisLabel: {
          color: theme.colors.text,
        },
      },
      yAxis: {
        type: 'category',
        data: y_axis_data,
        name: y_name,
        nameTextStyle: {
          color: theme.colors.text,
        },
        axisLabel: {
          color: theme.colors.text,
        },
      },
      visualMap: {
        min: Math.min(...z_axis_data.filter((v) => v > -900)),
        max: Math.max(...z_axis_data),
        orient: 'horizontal',
        textStyle: {
          color: theme.colors.text,
        },
        calculable: true,
        realtime: false,
        inRange: {
          color: [
            '#313695',
            '#4575b4',
            '#74add1',
            '#abd9e9',
            '#e0f3f8',
            '#ffffbf',
            '#fee090',
            '#fdae61',
            '#f46d43',
            '#d73027',
            '#a50026',
          ],
        },
      },
      series: series.map((item) => ({
        name: 'Gaussian',
        type: 'heatmap',
        data: item['v_data'][0].map((item2: any, index: number) => [
          x_axis_data.indexOf(item2),
          y_axis_data.indexOf(item['v_data'][1][index]),
          item['v_data'][2][index],
        ]),
        pointSize: 5,
        blurSize: 6,
        emphasis: {
          itemStyle: {
            borderColor: '#333',
            borderWidth: 1,
          },
        },
        progressive: 1000,
        animation: false,
      })),
    };
    state.echarts.setOption(option, {
      notMerge: true,
      lazyUpdate: true,
    });
  } else if (state.mounted && state.echarts && x_axis_data.length === 0) {
    state.echarts.dispose();
  }
  return (
    <div id={state.div_id} className="panel-empty">
      <p>No data found in response</p>
    </div>
  );
};
