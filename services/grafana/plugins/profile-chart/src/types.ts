type SeriesSize = 'sm' | 'md' | 'lg';

export interface ProfileOptions {
  text: string;
  showSeriesCount: boolean;
  seriesCountSize: SeriesSize;
}
