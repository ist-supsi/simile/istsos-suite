#!/bin/bash

# COPY SCRIPT INTO DOCKER CONTAINER
docker cp docker-exec-cmd-backup.sh istsos-suite_keycloak_1:/tmp/docker-exec-cmd-backup.sh
# RUN SCRIPT INSIDE THE CONTAINER
# docker exec -it istsos-suite_keycloak_1 /app/envsubst.sh
docker exec -it istsos-suite_keycloak_1 /tmp/docker-exec-cmd-backup.sh
# COPY THE BACKUP IN JSON LOCALLY
docker cp istsos-suite_keycloak_1:/tmp/istsos.json .
