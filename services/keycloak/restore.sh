#!/bin/bash

# COPY SCRIPT INTO DOCKER CONTAINER
# docker cp docker-exec-cmd-restore.sh istsos-suite_keycloak_1:/tmp/docker-exec-cmd-restore.sh
# docker cp istsos.json istsos-suite_keycloak_1:/tmp/istsos.json
# docker cp envsubst.sh istsos-suite_keycloak_1:/tmp/envsubst.sh
# RUN SCRIPT INSIDE THE CONTAINER
# docker exec -it istsos-suite_keycloak_1 /tmp/envsubst.sh
docker exec -it istsos-suite_keycloak_1 /tmp/docker-exec-cmd-restore.sh
# COPY THE BACKUP IN JSON LOCALLY
# docker cp simile-suite_simile-auth_1:/tmp/simile.json .
