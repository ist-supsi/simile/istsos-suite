import tornado.ioloop
import tornado.web
import sys
from tornado.options import define, options
from tornado import concurrent
from tornado.platform.asyncio import AsyncIOMainLoop
import base64
from tornado.httpserver import HTTPServer
import asyncpg as pg
import requests
import asyncio
import json
import binascii
from datetime import datetime, date
import time
import os
import logging
import importlib
import timeit
import yaml
import threading
from paho.mqtt import client as mqtt_client
import jwt
from jwt import PyJWKClient
from v1.utils import get_userinfo, check_permission


executor = concurrent.futures.ThreadPoolExecutor()

sys.path.append('.')

from v1.listeners import EventListener
from v1.db import Db
from v1.handlers.ProcessHandler import ProcessHandler

__abspath__ = os.path.abspath(os.getcwd())
__plugins__ = os.path.join(
    __abspath__,
    'plugins'
)

# POSTGRESQL PARAMETERS
define("port", default=8888, help="Tornado Web port", type=int)
define("pg_user", default="postgres", help="PostgrSQL database user")
define("pg_password", default="postgres", help="PostgrSQL user password")
define("pg_host", default="postgresql", help="PostgrSQL database host")
define("pg_port", default="5432", help="PostgrSQL database port")
define("pg_database", default="istsos", help="PostgrSQL database name")
define("pg_auto_upgrade", default=False, help="Upgrade PostgrSQL schema", type=bool)

# MQTT
broker = 'vernemq'
port = 1883
topic = "istsos/#"
# generate client ID with pub prefix randomly
client_id = f'python-mqtt-istsos'
username = 'simile'
password = 'simile'


def connect_mqtt() -> mqtt_client:
    def on_connect(client, userdata, flags, rc):
        app_log = logging.getLogger('tornado.application')
        if rc == 0:
            app_log.info("Connected to MQTT Broker!")
        else:
            app_log.info("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(client_id)
    client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client

def subscribe(client: mqtt_client):
    app_log = logging.getLogger('tornado.application')
    def on_message(client, userdata, msg):
        app_log.info(f"Received `{msg.payload.decode()}` from `{msg.topic}` topic")
        app_log.info(f"Data --> `{msg.payload.decode()}`")
        payload = msg.payload.decode()
        msg_splitted = msg.topic.split('/')
        if len(msg_splitted)==3:
            basic, service, assignedid = msg_splitted
            req = requests.post(
                'http://istsos/wa/istsos/services/{}/operations/fastinsert'.format(
                    service
                ),
                headers={'Content-Type': 'text/plain'},
                data="{};{}".format(
                    assignedid,
                    payload
                )
            )
            app_log.info(req.text)



    client.subscribe(topic)
    client.on_message = on_message

def mqtt_run():
    client = connect_mqtt()
    subscribe(client)
    client.loop_forever()

# MESSAGES COLORED
def red(message):
    print(f"\033[91m{message}\033[0m")

def green(message):
    print(f"\033[92m{message}\033[0m")

def blue(message):
    print(f"\033[94m{message}\033[0m")

# PG DB CONNECTION
async def get_conn():
    try:
        return await pg.create_pool(
            user=options.pg_user,
            password=options.pg_password,
            database=options.pg_database,
            host=options.pg_host,
            port=options.pg_port
        )
    except Exception as x:
        red("Connection to db failed.")
        raise x

#ThinkPark Handler
class ThinkParkHandler(tornado.web.RequestHandler):
    def post(self):
        app_log = logging.getLogger('tornado.application')
        try:
            app_log.info(self.request.body)
            params = list(self.request.arguments.keys())
            service_with_params = self.request.path.split('/')[-1]
            service_name = service_with_params.split('?')[0]
            if not 'LrnDevEui' in params and 'LrnFPort' not in params and 'LrnInfos' not in params:
                raise Exception('error')
            body_json = json.loads(self.request.body)
            sensor_str = binascii.unhexlify(
                body_json['DevEUI_uplink']['payload_hex']
            ).decode('utf-8')[1:]
            sensor_str_splitted = sensor_str.split(',')
            sensor_name = sensor_str_splitted[0]
            sensor_dt = sensor_str_splitted[1]
            req = requests.get(
                'http://istsos/wa/istsos/services/{}/procedures/{}'.format(
                    service_name,
                    sensor_name
                )
            )
            if req.json()['success']:
                res_json = req.json()
                assigned_id = res_json['data']['assignedSensorId']
                if len(res_json['data']['outputs']) == len(sensor_str_splitted[2:]):
                    v_batt = sensor_str_splitted[2:][-1]
                    sensor_data_str = ','.join(sensor_str_splitted[2:-1])
                else:
                    sensor_data_str = ','.join(sensor_str_splitted[2:])
                try:
                    dt = datetime.fromtimestamp(int(sensor_dt))
                    sensor_data = '{};{}+0200,{}'.format(
                        assigned_id,
                        dt.isoformat(),
                        sensor_data_str
                    )
                except:
                    sensor_data = '{};{}-{}+0200,{}'.format(
                        assigned_id,
                        datetime.now().year,
                        sensor_dt,
                        sensor_data_str
                    )
                req = requests.post(
                    f'http://istsos/wa/istsos/services/{service_name}/operations/fastinsert',
                    headers={'Content-Type': 'text/plain'},
                    data=sensor_data
                )
                print(sensor_data)
                print(req.text)
            else:
                print('error')
            # with open('log.txt', 'a') as f:
            #     f.write(f"{self.request.body}\n")
            self.write(self.request.body)
        except:
            self.set_status(500)
            self.write('E01')


# MAIN ISTSOS HANDLER
class IstsosHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "*")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
    def options(self):
        self.set_status(204)
        self.finish()
    async def get(self):
        app_log = logging.getLogger('tornado.application')
        status = 200
        message=''
        token = ''
        if self.request.headers.get('Authorization'):
            token = self.request.headers.get('Authorization').split(" ")[1]
        userinfo = get_userinfo(self.request.remote_ip, token)
        if userinfo['success']:
            allowed = check_permission(userinfo['data'], self.request.uri, 'GET')
            if allowed:
                req = requests.get(
                    'http://istsos{}'.format(self.request.uri[7:])
                )
                try:
                    if self.request.path=='/istsos/wa/istsos/services' or self.request.path=='/istsos/wa/istsos/operations/status':
                        
                        req_json = req.json()
                        services = req_json['data']
                        roles = userinfo['data']['realm_access']['roles']
                        if '*' in userinfo['data']['groups'] or 'admin' in roles:
                            self.write(req_json)
                        else:
                            groups = list(
                                map(
                                    lambda x: x.strip('/'), userinfo['data']['groups']
                                )
                            )
                            services_filtered = list(
                                filter(lambda x: x['service'] in groups, services)
                            )
                            self.write({
                                "success": True,
                                "data": services_filtered
                            })
                    else:
                        try:
                            self.write(req.json())
                        except:
                            self.write(req.text)
                except:
                    self.clear()
                    self.set_status(402)
                    self.write({
                        "success": False,
                        "message": "An error occured during the operation"
                    })
            else:
                self.clear()
                self.set_status(403)
                self.write({
                    "success": False,
                    "message": "User not allowed for this operation"
                })
            
        else:
            self.clear()
            self.set_status(401)
            self.write({
                "success": False,
                "message": userinfo['message']
            })
    async def put(self):
        app_log = logging.getLogger('tornado.application')
        status = 200
        message=''
        token = ''
        if self.request.headers.get('Authorization'):
            token = self.request.headers.get('Authorization').split(" ")[1]
        userinfo = get_userinfo(self.request.remote_ip, token)
        if userinfo['success']:
            allowed = check_permission(userinfo['data'], self.request.uri, 'PUT')
            if allowed:
                req = requests.put(
                    'http://istsos{}'.format(self.request.uri[7:]),
                    data=self.request.body
                )
                try:
                    res_json = req.json()
                    if res_json['success']:
                        service = self.request.path.replace('/istsos/wa/istsos/services/', "")
                        if '/' not in service:
                            body = json.loads(self.request.body)
                            if self.request.protocol=="https":
                                url = "{}://{}/auth/admin/realms/istsos/groups".format(
                                    os.environ['GN_PROTOCOL'],
                                    os.environ['GN_DOMAIN']
                                )
                            else:
                                url = "{}://{}:{}/auth/admin/realms/istsos/groups".format(
                                    os.environ['GN_PROTOCOL'],
                                    os.environ['GN_DOMAIN'],
                                    os.environ['KEYCLOAK_PORT'],
                                )
                            headers = {'Authorization': self.request.headers.get('Authorization')}
                            req_keycloak = requests.get(
                                url,
                                headers=headers,
                                verify=False
                            )
                            if req_keycloak.status_code==200:
                                groups = req_keycloak.json()
                                group = list(
                                    filter(lambda x: x['name'] == service, groups)
                                )
                                if group:
                                    req_keycloak = requests.put(
                                        "{}/{}".format(
                                            url,
                                            group[0]['id']
                                        ),
                                        json={
                                            "name": body['service']
                                        },
                                        headers=headers,
                                        verify=False
                                    )
                                    if req_keycloak.status_code!=201:
                                        app_log.warning(req_keycloak.text)
                            self.write(res_json)
                        else:
                            self.write(res_json)
                    else:
                        self.write(res_json)
                except:
                    self.write(req.text)
            else:
                self.clear()
                self.set_status(403)
                self.write({
                    "success": False,
                    "message": "User not allowed for this operation"
                })
        else:
            self.clear()
            self.set_status(401)
            self.write({
                "success": False,
                "message": userinfo['message']
            })
    async def post(self):
        app_log = logging.getLogger('tornado.application')
        status = 200
        message=''
        token = ''
        if self.request.headers.get('Authorization'):
            auth_type = self.request.headers.get('Authorization').split(" ")[0]
            token = self.request.headers.get('Authorization').split(" ")[1]
            if auth_type=='Basic':
                if 'fastinsert' in self.request.uri:
                    t = base64.decodebytes(token.encode())
                    user, password = t.decode().split(':')
                    try:
                        req_token = requests.post(
                            "https://istsos.ddns.net/auth/realms/istsos/protocol/openid-connect/token",
                            headers={
                                "Content-Type": "application/x-www-form-urlencoded"
                            },
                            data={
                                "grant_type": "password",
                                "username": user,
                                "password": password,
                                "client_id": "istsos-istsos"
                            }
                        )
                        token = req_token.json()
                        token = token['access_token']
                    except Exception as e:
                        self.clear()
                        self.set_status(401)
                        return self.write({
                            "success": False,
                            "message": str(e)
                        })
                else:
                    self.clear()
                    self.set_status(401)
                    return self.write({
                        "success": False,
                        "message": 'Not authorized'
                    })
        userinfo = get_userinfo(self.request.remote_ip, token)
        if userinfo['success']:
            allowed = check_permission(userinfo['data'], self.request.uri, 'POST')
            if allowed:
                req = requests.post(
                    'http://istsos{}'.format(self.request.uri[7:]),
                    data=self.request.body
                )
                try:
                    res_json = req.json()
                    if res_json['success']:
                        if self.request.path=='/istsos/wa/istsos/services':
                            body = json.loads(self.request.body)
                            if self.request.protocol=="https":
                                url = "{}://{}/auth/admin/realms/istsos/groups".format(
                                    os.environ['GN_PROTOCOL'],
                                    os.environ['GN_DOMAIN']
                                )
                            else:
                                url = "{}://{}:{}/auth/admin/realms/istsos/groups".format(
                                    os.environ['GN_PROTOCOL'],
                                    os.environ['GN_DOMAIN'],
                                    os.environ['GN_PORT'],
                                )
                            headers = {'Authorization': self.request.headers.get('Authorization')}
                            req_keycloak = requests.post(
                                url,
                                json={
                                    "name": body['service']
                                },
                                headers=headers,
                                verify=False
                            )
                            if req_keycloak.status_code!=201:
                                app_log.warning(req_keycloak.text)
                            return self.write(req.json())
                        else:
                            return self.write(req.json())
                    else:
                        return self.write(req.json())
                except Exception as e:
                    app_log.error(str(e))
                    return self.write(req.text)
            else:
                self.clear()
                self.set_status(403)
                return self.write({
                    "success": False,
                    "message": "User not allowed for this operation"
                })
        else:
            self.clear()
            self.set_status(401)
            return self.write({
                "success": False,
                "message": userinfo['message']
            })
    async def delete(self):
        app_log = logging.getLogger('tornado.application')
        status = 200
        message=''
        token = ''
        if self.request.headers.get('Authorization'):
            token = self.request.headers.get('Authorization').split(" ")[1]
        userinfo = get_userinfo(self.request.remote_ip, token)
        if userinfo['success']:
            allowed = check_permission(userinfo['data'], self.request.uri, "DELETE")
            if allowed:
                req = requests.delete(
                    'http://istsos{}'.format(self.request.uri[7:]),
                    data=self.request.body
                )
                try:
                    res_json = req.json()
                    if res_json['success']:
                        service = self.request.path.replace('/istsos/wa/istsos/services/', "")
                        if '/' not in service:
                            body = json.loads(self.request.body)
                            if self.request.protocol=="https":
                                url = "{}://{}/auth/admin/realms/istsos/groups".format(
                                    os.environ['GN_PROTOCOL'],
                                    os.environ['GN_DOMAIN']
                                )
                            else:
                                url = "{}://{}:{}/auth/admin/realms/istsos/groups".format(
                                    os.environ['GN_PROTOCOL'],
                                    os.environ['GN_DOMAIN'],
                                    os.environ['GN_PORT'],
                                )
                            headers = {'Authorization': self.request.headers.get('Authorization')}
                            req_keycloak = requests.get(
                                url,
                                headers=headers,
                                verify=False
                            )
                            if req_keycloak.status_code==200:
                                groups = req_keycloak.json()
                                group = list(
                                    filter(lambda x: x['name'] == service, groups)
                                )
                                if group:
                                    req_keycloak = requests.delete(
                                        "{}/{}".format(
                                            url,
                                            group[0]['id']
                                        ),
                                        headers=headers,
                                        verify=False
                                    )
                                    if req_keycloak.status_code!=201:
                                        app_log.warning(req_keycloak.text)
                            self.write(res_json)
                        else:
                            self.write(res_json)
                    else:
                        self.write(res_json)
                except:
                    self.write(req.text)
            else:
                self.clear()
                self.set_status(403)
                self.write({
                    "success": False,
                    "message": "User not allowed for this operation"
                })
        else:
            self.clear()
            self.set_status(401)
            self.write({
                "success": False,
                "message": userinfo['message']
            })


class IndicatorsHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

    def options(self):
        self.set_status(204)
        self.finish()

    async def get(self):
        try:
            app_log = logging.getLogger('tornado.application')
            app_log.info(
                os.listdir(__plugins__)
            )
            token = ''
            if self.request.headers.get('Authorization'):
                token = self.request.headers.get('Authorization').split(" ")[1]
            userinfo = get_userinfo(self.request.remote_ip, token)
            username = userinfo['data']['preferred_username']
            plugins = []
            uri_splitted = self.request.uri.split('/')
            op = uri_splitted[-1]
            if op=='indicators':
                for plugin in os.listdir(__plugins__):
                    with open(os.path.join(
                            __plugins__,
                            plugin,
                            'metadata.yml')) as f:
                        try:
                            data = yaml.load(f, Loader=yaml.FullLoader)
                            plugins.append(data)
                        except Exception as e:
                            app_log.error(str(e))
                            continue

                self.write({
                    "success": True,
                    "data": plugins
                })
            elif op=='list':
                db = Db(self.application.pool)
                indicators = await db.get_indicators(
                    user=username
                )
                self.write({
                    "success": True,
                    "data": indicators
                })
            else:
                self.write({
                    "success": False,
                })
        except Exception as e:
            self.set_status = 500
            
    async def post(self):
        app_log = logging.getLogger('tornado.application')
        plugins = []
        uri_splitted = self.request.uri.split('/')
        service = uri_splitted[3]
        status = 200
        message=''
        token = ''
        if self.request.headers.get('Authorization'):
            token = self.request.headers.get('Authorization').split(" ")[1]
        userinfo = get_userinfo(self.request.remote_ip, token)
        username = userinfo['data']['preferred_username']
        if userinfo['success']:
            allowed = check_permission(userinfo['data'], self.request.uri, 'POST')
            if allowed:
                for plugin in os.listdir(__plugins__):
                    if plugin in self.request.uri:
                        try:
                            lib = importlib.import_module(f'plugins.{plugin}.{plugin}') 
                            indicator = lib.Indicator()
                            body_json = json.loads(self.request.body)
                            # indicator.create(service, uri_splitted[-1], body_json)
                            procs = []
                            for k in body_json:
                                if 'type' in body_json[k]:
                                    if body_json[k]['type']=='profile-ob' or body_json[k]['type']=='procedure-ob':
                                        procs.append(body_json[k]['value'])
                            db = Db(self.application.pool)
                            process_id, indicator_id = await db.new_process(
                                body_json['name']['value'],
                                service,
                                uri_splitted[-2],
                                uri_splitted[-1],
                                "'{}'".format('\',\''.join(procs)),
                                self.request.body.decode(),
                                username
                            )
                            indicator.check_params(
                                uri_splitted[-1],
                                body_json
                            )
                            executor.submit(
                                indicator.create,
                                service,
                                uri_splitted[-1],
                                body_json,
                                process_id,
                                indicator_id
                            )
                            return self.write({
                                "success": True,
                                "message": 'Process created. It is running in backgroun. Check the processes page to check the status.'
                            })
                            # async with self.application.pool.acquire() as conn:
                            #     try:
                            #         procs = []
                            #         for k in body_json:
                            #             if 'type' in body_json[k]:
                            #                 if body_json[k]['type']=='profile-ob':
                            #                     procs.append(body_json[k]['value'])
                            #         indicator_id = await conn.fetchval("""
                            #             INSERT INTO orchestrator.indicator(
                            #             indicatorname, indicatorservice, indicatormodule, indicatormethod, indicatorprocs, indicatorform)
                            #             VALUES ('%s', '%s', '%s', '%s', ARRAY[%s], '%s') RETURNING indicatorid;
                            #         """ % ())
                            #         process_id = await conn.fetchval("""
                            #             INSERT INTO orchestrator.process(
                            #                 processusername, indicatorid, processprogress, processstatus, processmessage)
                            #                 VALUES ('%s', %s, '%s', '%s', 'Just started') RETURNING processid;
                            #         """ % (username,indicator_id,'0','created'))
                            #         # await conn.execute("NOTIFY \"BACKGROUND_PROCESS\", \'%s,%s\';" % (process_id, res_id))
                            #         db = Db(self.application.pool)
                                    
                            #         await conn.execute("COMMIT;")                                    
                            #         return self.write({
                            #             "success": True,
                            #             "message": 'Process created. It is running in backgroun. Check the processes page to check the status.'
                            #         })
                            #     except Exception as e:
                            #         await conn.execute("ROLLBACK;")
                            #         return self.write({
                            #             "success": False,
                            #             "message": str(e)
                            #         })
                        except Exception as e:
                            return self.write({
                                "success": False,
                                "message": str(e)
                            })
            else:
                self.clear()
                self.set_status(403)
                self.write({
                    "success": False,
                    "message": "User not allowed for this operation"
                })
        else:
            self.clear()
            self.set_status(401)
            self.write({
                "success": False,
                "message": userinfo['message']
            })

    async def update(self):
        app_log = logging.getLogger('tornado.application')
        plugins = []
        uri_splitted = self.request.uri.split('/')
        service = uri_splitted[3]
        token = ''
        if self.request.headers.get('Authorization'):
            token = self.request.headers.get('Authorization').split(" ")[1]
        userinfo = get_userinfo(self.request.remote_ip, token)
        username = userinfo['data']['preferred_username']
        if userinfo['success']:
            allowed = check_permission(userinfo['data'], self.request.uri, 'PUT')
            if allowed:
                for plugin in os.listdir(__plugins__):
                    if plugin in self.request.uri:
                        try:
                            lib = importlib.import_module(f'plugins.{plugin}.{plugin}') 
                            indicator = lib.Indicator()
                            body_json = json.loads(self.request.body)
                            indicator.check_params(
                                uri_splitted[-1],
                                body_json
                            )
                            indicator.update(
                                service,
                                uri_splitted[-1],
                                body_json
                            )
                        except Exception as e:
                            return self.write({
                                "success": False,
                                "message": str(e)
                            })
            else:
                self.clear()
                self.set_status(403)
                self.write({
                    "success": False,
                    "message": "User not allowed for this operation"
                })
        else:
            self.clear()
            self.set_status(401)
            self.write({
                "success": False,
                "message": userinfo['message']
            })

async def close(application):
    # Remove all listeners
    blue("Removing listeners..")
    await application.listener.stop()
    green(" > done.")

    blue("Closing connection pool..")
    # Closing connections pool
    await application.pool.close()
    green(" > done.")

    print("")

if __name__ == "__main__":
    # parse command line vars
    options.parse_command_line()

    # THREAD
    pub = threading.Thread(target=mqtt_run)
    pub.start()

    # SETTINGS INIT
    settings = dict(
        debug=True
    )
    # LOG init
    app_log = logging.getLogger('tornado.application')
    acc_log = logging.getLogger('tornado.access')
    gen_log = logging.getLogger('tornado.general')
    if settings['debug']:
        app_log.setLevel(logging.DEBUG)
        acc_log.setLevel(logging.DEBUG)
        gen_log.setLevel(logging.DEBUG)
    else:
        app_log.setLevel(logging.INFO)
        acc_log.setLevel(logging.INFO)
        gen_log.setLevel(logging.INFO)
        
    gen_log.info('### START ###')
    AsyncIOMainLoop().install()
    ioloop = asyncio.get_event_loop()
    ioloop.set_debug(enabled=True)

    app = tornado.web.Application([
        (r"/istsos/thinkpark", ThinkParkHandler ),
        (r"/istsos/thinkpark/.*", ThinkParkHandler ),
        (r"/istsos/indicators", IndicatorsHandler ),
        (r"/istsos/indicators/.*", IndicatorsHandler ),
        (r"/istsos/process", ProcessHandler ),
        (r"/istsos/process/.*", ProcessHandler ),
        (r"/istsos/.*", IstsosHandler)
    ], **settings)

    # Init database postgresql connection pool
    app.pool = ioloop.run_until_complete(
        get_conn()
    )

    # Create events listeners
    app.listener = EventListener()
    # pub2 = threading.Thread(target=app.listener.start)
    # pub2.start()
    # app.listener.start()
    ioloop.run_until_complete(app.listener.start())
    
    try:
        http_server = HTTPServer(app)
        http_server.listen(options.port)
        ioloop.run_forever()

    except Exception as ex:
        gen_log.error(f"Exception:\n{ex}")

    except KeyboardInterrupt:
        gen_log.warning("\nKeyboard interruption, probably: CTR+C\n")

    finally:
        gen_log.info("\Closing app\n")
        ioloop.run_until_complete(close(app))
