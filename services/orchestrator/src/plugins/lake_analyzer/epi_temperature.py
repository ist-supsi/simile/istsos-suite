import requests
from datetime import timedelta
import sys
import pandas as pd
from dateutil import parser
import plugins.lake_analyzer.methods as lake
import logging
if sys.version_info[0] < 3: 
    from StringIO import StringIO
else:
    from io import StringIO


def execute(form, service, new_proc, sensor_registered, process_id, indicator_id, et=None):
    log = logging.getLogger('tornado.application')
    proc = form['procedure']['value']
    chunk_min = int(form['chunk']['value'])
    if chunk_min <= 0:
        chunk_min = 5000
    service_target = form['service']['value']
    ob = form['procedure-observedproperty']['value']
    (
        ob_urn, data, time_interval, begin_position,
        end_position, proc_interval, proc_interval_min
    ) = lake.get_sensor_data(service, proc, ob, form)
    if et:
        b_et, e_et = et.split('/')
        begin_position = parser.parse(b_et)
        end_position = parser.parse(e_et)
    if begin_position==end_position:
        begin_position = begin_position - timedelta(seconds=1)
    bathymetry = form['bathymetry']['value']
    bathymetry_rows = bathymetry.split('\n')
    if len(bathymetry_rows) < 3:
        raise Exception(f'The bathymetry has less than 3 rows.')
    bth_a = []
    bth_d = []
    for b in bathymetry_rows:
        b_splitted = b.split(',')
        if len(b_splitted) < 2:
            raise Exception(f'The bathymetry has NOT a column for sections and one for corresponding depths.')
        if len(b_splitted) < 2:
            raise Exception(f'The bathymetry has more than two columns.')
        try:
            if b_splitted:
                bth_a.append(float(b_splitted[0]))
                bth_d.append(float(b_splitted[1]))
        except Exception as e:
            raise Exception('Values of bathymetry can\'t be casted to float')
    if proc_interval_min > chunk_min:
        end_position_tmp = begin_position + timedelta(minutes=chunk_min)
        begin_position_tmp = begin_position
        while begin_position_tmp < end_position:
            proc_interval_tmp = end_position - begin_position_tmp
            proc_interval_min_tmp = proc_interval_tmp.total_seconds() / 60
            progress = ((proc_interval_min - proc_interval_min_tmp)/proc_interval_min)*100
            try:
                req = requests.put(
                    'http://orchestrator:8888/istsos/process',
                    json={
                        "progress": round(progress, 2),
                        "status": "running",
                        "message": "The process is running.",
                        "id": process_id
                    }
                )
            except Exception as e:
                log.info(str(e))
            url = f'http://istsos/{service}?service=SOS&request=GetObservation&procedure={proc}&offering=temporary&eventTime={begin_position_tmp.isoformat()}/{end_position_tmp.isoformat()}&observedProperty=depth,{ob.replace("-", ":")}&responseFormat=text/plain&version=1.0.0&qualityIndex=False'
            req = requests.get(
                url,
            )
            df = pd.read_csv(StringIO(req.text), sep=",")
            unique_dates = df['urn:ogc:def:parameter:x-istsos:1.0:time:iso8601'].unique()
            values = []
            for d in unique_dates:
                df_filtered = df.loc[df['urn:ogc:def:parameter:x-istsos:1.0:time:iso8601']==d]
                df_filtered_sorted = df_filtered.sort_values(by='urn:ogc:def:parameter:x-istsos:1.0:water:depth', ascending=True)
                depths = df_filtered_sorted['urn:ogc:def:parameter:x-istsos:1.0:water:depth'].values.tolist()
                temps = df_filtered_sorted[ob_urn].values.tolist()
                epi_temperature = lake.epi_temperature(temps, depths, bth_a, bth_d)
                values_row = [d, epi_temperature, 100]
                values.append(values_row)
            if values:
                try:
                    if not sensor_registered:
                        try:
                            aid = lake.register_sensor(service_target, new_proc, data)
                            sensor_registered = True
                        except Exception as e:
                            raise Exception(f'Can\'t get procedure {new_proc}.')
                    log.info(unique_dates)
                    if len(unique_dates)>1:
                        time_interval_tmp = [
                            unique_dates[0],
                            unique_dates[-1]
                        ]
                    else:
                        time_interval_tmp = [
                            unique_dates[0],
                            unique_dates[0]
                        ]
                    lake.insert_observation(
                        aid,
                        new_proc,
                        service_target,
                        time_interval_tmp,
                        values
                    )
                except Exception as e:
                    raise e
                begin_position_tmp = end_position_tmp
                end_position_tmp = end_position_tmp + timedelta(minutes=chunk_min)
                if end_position_tmp > end_position:
                    end_position_tmp = end_position
            else:
                begin_position_tmp = end_position_tmp
                end_position_tmp = end_position_tmp + timedelta(minutes=chunk_min)
                if end_position_tmp > end_position:
                    end_position_tmp = end_position
    else:
        url = f'http://istsos/{service}?service=SOS&request=GetObservation&procedure={proc}&offering=temporary&eventTime={time_interval[0]}/{time_interval[1]}&observedProperty=depth,{ob.replace("-", ":")}&responseFormat=text/plain&version=1.0.0&qualityIndex=False'
        req = requests.get(
            url,
        )
        df = pd.read_csv(StringIO(req.text), sep=",")
        unique_dates = df['urn:ogc:def:parameter:x-istsos:1.0:time:iso8601'].unique()
        values = []
        for d in unique_dates:
            df_filtered = df.loc[df['urn:ogc:def:parameter:x-istsos:1.0:time:iso8601']==d]
            df_filtered_sorted = df_filtered.sort_values(by='urn:ogc:def:parameter:x-istsos:1.0:water:depth', ascending=True)
            depths = df_filtered_sorted['urn:ogc:def:parameter:x-istsos:1.0:water:depth'].values.tolist()
            temps = df_filtered_sorted[ob_urn].values.tolist()
            epi_temperature = lake.epi_temperature(temps, depths, bth_a, bth_d)
            values_row = [d, epi_temperature, 100]
            values.append(values_row)
        # create procedure
        try:
            try:
                aid = lake.register_sensor(service_target, new_proc, data)
                sensor_registered = True
            except Exception as e:
                raise Exception(f'Can\'t get procedure {new_proc}.')
            if len(unique_dates)>1:
                time_interval = [
                    unique_dates[0],
                    unique_dates[-1]
                ]
            else:
                time_interval = [
                    unique_dates[0],
                    unique_dates[0]
                ]
            lake.insert_observation(
                aid,
                new_proc,
                service_target,
                time_interval,
                values
            )
        except Exception as e:
            raise e
    if sensor_registered:
        req = requests.put(
            'http://orchestrator:8888/istsos/process',
            json={
                "progress": 100,
                "status": "completed",
                "message": "The process has been successfully completed.",
                "id": process_id
            }
        )
    else:
        req = requests.put(
            'http://orchestrator:8888/istsos/process',
            json={
                "progress": 100,
                "status": "completed",
                "message": "Unfortunately no data and procedure has been created. Time series with no data or not enough data?",
                "id": process_id
            }
        )