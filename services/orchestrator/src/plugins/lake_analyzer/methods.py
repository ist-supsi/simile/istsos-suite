##
# @author Daniele Strigaro <daniele.strigaro@supsi.ch>
 # @file Description
 # @desc Created on 2021-09-28 3:32:16 pm
 # @copyright 2021 IST-SUPSI (www.supsi.ch/ist)
 #
import numpy as np
import requests
from dateutil import parser
import json
import logging
import math

log = logging.getLogger('tornado.application')


def register_sensor(service, new_proc, data):
    req = requests.post(
        'http://istsos/wa/istsos/services/{}/procedures'.format(service),
        data=json.dumps(data)
    )
    req = req.json()
    if not req['success']:
        if 'already exist' not in req['message']:
            raise Exception(f'Cannot create the new procedure {new_proc}')
    req = requests.get(
        'http://istsos/wa/istsos/services/{}/procedures/{}'.format(service, new_proc),
    )

    data = req.json()['data']
    aid = data['assignedSensorId']
    return aid

def get_sensor_aid(service, new_proc, data):
    # req = requests.post(
    #     'http://istsos/wa/istsos/services/{}/procedures'.format(service),
    #     data=json.dumps(data)
    # )
    # req = req.json()
    
    # if not req['success']:
    #     if 'already exists' not in req['message']:
    #         raise Exception(f'Cannot create the new procedure {new_proc}')
    try:
        req = requests.get(
            'http://istsos/wa/istsos/services/{}/procedures/{}'.format(service, new_proc),
        )

        data = req.json()['data']
        aid = data['assignedSensorId']
    except Exception as e:
        raise e
    return aid

def get_sensor_data(
        service, proc, op,
        form, outputs=[],
        system_type='insitu-fixed-point',
        water_depth=True, filter_on_op=False):
    req = requests.get(
        'http://istsos/wa/istsos/services/{}/procedures/{}'.format(
            service,
            proc
        ),
    )
    try:
        data = req.json()['data']
    except Exception as e:
        raise Exception(f'Cannnot get procedure {proc}.')
    data['assignedSensorId'] = ''
    ob_urn = list(filter(lambda x: x['name']==op, data['outputs']))[0]['definition']
    if water_depth:
        data['outputs'] = list(filter(lambda x: x['name']=='water-depth' or x['name']=='Time' , data['outputs']))
    else:
        if filter_on_op:
            data['outputs'] = list(filter(lambda x: x['name']=='Time' or x['name']==op , data['outputs']))
        else:
            data['outputs'] = list(filter(lambda x: x['name']=='Time' , data['outputs']))
    if outputs:
        data['outputs'] = data['outputs'] + outputs

    time_interval = data['outputs'][0]['constraint']['interval']
    begin_position = parser.parse(time_interval[0]).replace(minute=0, second=0)
    end_position = parser.parse(time_interval[1])
    proc_interval = end_position - begin_position
    proc_interval_min = proc_interval.total_seconds() / 60
    data['outputs'][0]['constraint'] = {}
    data['system_id'] = form['name']['value']
    data['system'] = form['name']['value']
    data['description'] = form['description']['value']
    data['keywords'] = form['keywords']['value']
    data['classification'][0]['value'] = system_type
    data['classification'][1]['value'] = 'calculated'
    data['identification'][0]['value'] = ':'.join(data['identification'][0]['value'].split(':')[0:-1]) + ':' + form['name']['value']
    return [
        ob_urn, data, time_interval,
        begin_position, end_position,
        proc_interval, proc_interval_min
    ]

def insert_observation(aid, proc, service, time_interval, values):
    req = requests.get(
        f"http://istsos/wa/istsos/services/{service}/operations/getobservation/offerings/temporary/procedures/{proc}/observedproperties/:/eventtime/last",
        verify=False
    )
    go_data = req.json()
    go_data = go_data['data'][0]
    go_data['result']['DataArray']['values'] = values
    go_data["samplingTime"] = {
        "beginPosition": time_interval[0],
        "endPosition":  time_interval[-1]
    }
    res = requests.post(
        "http://istsos/wa/istsos/services/%s/operations/"
        "insertobservation" % (
            service
        ),
        verify=False,
        data=json.dumps({
            "ForceInsert": "true",
            "AssignedSensorId": aid,
            "Observation": go_data
        })
    )
    res.raise_for_status()

def water_density_2(wtr, cond):
    # from list to np.array
    wtr = np.array(wtr)
    cond = np.array(cond)

    if len(wtr)!=len(cond):
        raise Exception("Water temperature array must be the same length as the conducibility array")
    val = 999.8429 + ((10**-3) * ((0.059385*(wtr**3)) - (8.56272*(wtr**2)) + (65.4891*wtr)))  + cond
    return list(val)

def water_density(wtr, sal=0):
    # from list to np.array
    wtr = np.array(wtr)

    if type(sal) != list:
        sal = wtr * sal
    else:
        sal =np.array(sal)

    # check arrays are float or int
    if not np.can_cast(wtr, float):
        raise Exception("Not all water temperature values can be casted to float")
        return None
    
    if len(wtr)!=len(sal):
        raise Exception("Water temperature array must be the same length as the salinity array")
        return None

    # Determine which method we want to use, initially set both methods to false
    MM = False # Martin & McCutcheon
    UN = False # UNESCO

    # specify temperature and salinity range for the calculations
    t_range = [0,40] # temperature range
    s_range = [0.5,43] # salinity range

    rho = None
    # check to see if all values lie within the ranges specified
    if np.all(sal < s_range[0]):
        MM = True
    elif not (
                np.sum(wtr < t_range[0]) or np.sum(wtr > t_range[1])
            ) and not (
                np.sum(sal < s_range[0]) or np.sum(sal > s_range[1])
            ):
        UN = True

    # if MM is true we use method of Martin and McCutcheon (1999)
    if MM:
        rho = (1000*(1-(wtr+288.9414)*(wtr-3.9863)**2/(508929.2*(wtr+68.12963))))

    if UN:
        # -- equation 1:
        rho_0 = 999.842594 + 6.793952*(10**(-2))*wtr - 9.095290*(10**(-3))*(wtr**2) + 1.001685*(10**(-4))*(wtr**3) - 1.120083*(10**(-6))*(wtr**4) + 6.536335e-9*(wtr**5)
        
        # -- equation 2:
        A = 8.24493*(10**(-1)) - 4.0899e-3*wtr + 7.6438*(10**(-5))*(wtr**2) - 8.2467*(10**(-7))*(wtr**3) + 5.3875*(10**(-9))*(wtr**4)
        
        # -- equation 3:
        B = -5.72466*(10**(-3)) + 1.0227*(10**(-4))*wtr - 1.6546*(10**(-6))*(wtr**2)
        
        # -- equation 4:
        C = 4.8314*(10**(-4))
        
        # -- equation 5:
        rho = rho_0 + A*sal + B*(sal**(3/2)) + C*(sal**2)
    
    # if there is a combination of fresh and saline water we need to use a combination of MM and UN
    if MM==False and UN==False:
        rho = wtr * 0
        for j in range(len(rho)-1):
            rho[j] = water_density(
                [wtr[j]],
                [sal[j]]
            )[0]
        # dim(rho) <- dim(wtr) # ensure same dimension as input array 
    # if UN is true we use method of Martin and McCutcheon (1999)
    return rho.tolist()

def find_peaks(data, treshold=0):
    data = np.array(data)
    var_l = len(data)
    locs = np.repeat(False, var_l)
    peaks = np.repeat(None, var_l)

    for i in range(var_l-1)[1:]:
        pk_i = data[i-1:i+2].argmax(axis=0)
        pos_peak = max(data[i-1:i+2])
        if pk_i==1:
            peaks[i] = pos_peak
            locs[i] = True
    inds = np.arange(0, var_l, 1)
    locs = inds[locs]
    peaks = peaks[locs]
    use_i = peaks > treshold
    locs = locs[use_i]
    return locs.tolist()

def thermo_depth(
        wtr,
        depths,
        s_min=0.1,
        seasonal=True,
        index=False,
        mixed_cutoff=1):

    # from list to np.array36789
    wtr = np.array(wtr)
    depths = np.array(depths)

    # check arrays are float or int
    if not np.can_cast(wtr, float):
        raise Exception("Not all water temperature values can be casted to float")
        return None
    
    wtr_checks = abs(np.diff(wtr)) > mixed_cutoff

    if True not in wtr_checks:
        return None

    if not np.can_cast(depths, float):
        raise Exception("Not all depths values can be casted to float")
        return None

    # We can't determine anything with less than 3 measurements
    # just return lake bottom
    if len(wtr) < 3:
        print("At least 3 measurements are required")
        return None

    # check depths are not duplicates
    if len(depths)!=len(np.unique(depths)):
        print("All depths must be unique")
        return None

    # calculate water density from temperature
    rho = water_density(wtr.tolist())
    d_rho_perc = 0.15 # in percentage max for unique thermocline step
    num_depths = len(depths)
    d_rho_dz = np.repeat(None, num_depths-1)

    # Calculate the first derivative of density
    for i in range(num_depths-1):
        d_rho_dz[i] = ( rho[i+1]-rho[i] )/( depths[i+1] - depths[i] )

    #look for two distinct maximum slopes, lower one assumed to be seasonal
    thermo_ind = np.argmax(d_rho_dz)  #Find max slope
    m_d_rho_z = d_rho_dz[thermo_ind]
    thermo_d = np.mean(
        depths[thermo_ind:(thermo_ind+2)]
    )
    if thermo_ind > 0 and thermo_ind < (num_depths-2):
        s_dn = -(depths[thermo_ind+1] - depths[thermo_ind])/ (d_rho_dz[thermo_ind+1] - d_rho_dz[thermo_ind])

        s_up = (depths[thermo_ind]-depths[thermo_ind-1])/ (d_rho_dz[thermo_ind]-d_rho_dz[thermo_ind-1])

        up_d = depths[thermo_ind]
        dn_d = depths[thermo_ind+1]
        if np.isfinite(s_up) and np.isfinite(s_dn):
            thermo_d = dn_d*(s_dn/(s_dn+s_up))+up_d*(s_up/(s_dn+s_up))

    

    d_rho_cut = np.max(
        np.append(d_rho_perc*m_d_rho_z, s_min)
    )
    locs = find_peaks(d_rho_dz, d_rho_cut)
    pks = d_rho_dz[locs]
    if len(pks)==0:
        s_thermo_d = thermo_d
        s_thermo_ind = thermo_ind
    else:
        s_thermo_ind = locs[len(pks)-1]
        if s_thermo_ind > (thermo_ind+1):
            s_thermo_d = np.mean(
                depths[s_thermo_ind:(s_thermo_ind+1)]
            )
            if s_thermo_ind > 0 and s_thermo_ind < (num_depths - 2):
                s_dn = -(depths[s_thermo_ind+1] - depths[s_thermo_ind]) / (d_rho_dz[s_thermo_ind+1] - d_rho_dz[s_thermo_ind])
                s_up = (depths[s_thermo_ind] - depths[s_thermo_ind-1]) / (d_rho_dz[s_thermo_ind] - d_rho_dz[s_thermo_ind-1])

                up_d = depths[s_thermo_ind]
                dn_d = depths[s_thermo_ind+1]

                if np.isfinite(s_up) and np.isfinite(s_dn):
                    s_thermo_d = dn_d*(s_dn/(s_dn+s_up))+up_d*(s_up/(s_dn+s_up))
        else:
            s_thermo_d = thermo_d
            s_thermo_ind = thermo_ind
    if s_thermo_d < thermo_d:
        s_thermo_d = thermo_d
        s_thermo_ind = thermo_ind
    if index:
        if seasonal:
            return s_thermo_ind
        else:
            return thermo_ind
    else:
        if seasonal:
            return s_thermo_d
        else:
            return thermo_d
    return 0

def buoyancy_freq(wtr, depths):
    rho_var = water_density(wtr)
    rho_var = np.array(rho_var)
    num_depths = len(depths)
    n2 = np.repeat(None, num_depths-1)
    n2_depths = np.repeat(None, num_depths-1)

    # calculate first derivate of density
    for i in range(num_depths-1):
        n2[i] = 9.81/rho_var[i]*( rho_var[i+1]-rho_var[i] )/( depths[i+1] - depths[i] )
        n2_depths[i] = (depths[i+1] + depths[i])/2
    return n2.tolist(), n2_depths.tolist()

def wedderburn_number(delta_rho, meta_t, u_st, a_o, av_hyp_rho):
    delta_rho = np.array(delta_rho)
    meta_t = np.array(meta_t)
    u_st = np.array(u_st)
    a_o = np.array(a_o)
    av_hyp_rho = np.array(av_hyp_rho)
    g = 9.81
    l_o = 2 * np.sqrt(a_o/math.pi)
    go = g*delta_rho/av_hyp_rho
    W = go*(meta_t**2)/((u_st**2)*l_o)
    return W

def volume_aggregation(values, depths, volumes, volumes_d):
    if len(values)!=len(depths):
        raise Exception("Values must be the same legth as the depth array")
    
    # create numpy array
    values = np.array(values)
    depths = np.array(depths)
    volumes = np.array(volumes)
    volumes_d = np.array(volumes_d)

    # volumes interpolated
    volumes_interp = np.interp(
        depths, volumes_d, volumes
    )
    volumes_total = volumes_interp.sum()
    res = (
        (values*volumes_interp).sum()
    )/volumes_total
    return res

def schmidt_stability(wtr, depths, bth_a, bth_d, sal=0):
    if len(wtr)!=len(depths):
        raise Exception("Water temperature must be the same legth as the depth array")
    
    # create numpy array
    wtr = np.array(wtr)
    depths = np.array(depths)
    bth_a = np.array(bth_a)
    bth_d = np.array(bth_d)
    sal = np.repeat(sal, len(wtr))

    g = 9.81
    dz = 0.1

    # if bathymetry has negative values, drop and interpolate to 0
    if np.min(bth_d) < 0:
        use_i = bth_d >= 0

        if np.any(bth_d==0):
            dep_t = [bth_d[use_i]]
        else:
            dep_t = [0] + bth_d[use_i]
        bth_a = np.interp(dep_t, bth_d.tolist(), bth_a.tolist())
        bth_d = dep_t
    num_d = len(wtr)
    if np.max(bth_d) > depths[num_d-1]:
        wtr = np.append(wtr, wtr[num_d-1])
        sal = np.append(sal, sal[num_d-1])
        depths = np.append(depths, np.max(bth_d))
    elif np.max(bth_d) < depths[num_d-1]:
        bth_d = np.append(bth_d, depths[num_d-1])
        bth_a = np.append(bth_a, 0)

    if np.min(bth_d) < depths[0]:
        wtr = np.append(wtr[0], wtr)
        sal = np.append(sal[0], sal)
        depths = np.append(np.min(bth_d), depths)

    z_o = np.min(depths)
    i_o = np.argmin(depths)
    a_o = bth_a[i_o]

    if a_o==0:
        raise Exception('Surface area cannot be zero')

    # calculate water density
    rho_l = water_density(wtr.tolist(), sal.tolist())

    # linear interpolation
    layer_d = np.arange(np.min(depths), np.max(depths)+dz, step=dz)
    layer_p = np.interp(layer_d, depths, rho_l)
    layer_a = np.interp(layer_d, bth_d, bth_a)
    z_cv = np.sum(
        layer_d * layer_a
    ) / np.sum(layer_a)
    s_t = np.sum(
        layer_p * ((layer_d - z_cv)*layer_a)
    ) * dz * g / a_o
    return s_t

def layer_temperature(top, bottom, wtr, depths, bth_a, bth_d):
    wtr = np.array(wtr)
    depths = np.array(depths)
    bth_a = np.array(bth_a)
    bth_d = np.array(bth_d)
    if top > bottom:
        raise Exception("Bottom depth must be greater than top")
    elif len(wtr)!=len(depths):
        raise Exception("Water temperature vector must be same length as depth vector")
    elif np.any(wtr==None) or np.any(depths==None) or np.any(bth_d==None) or np.any(bth_a==None):
        raise Exception("Input arguments must be numbers")

    # if bathymetry has negative values, interpolate to 0 
    if np.min(bth_d) < 0:
        use_i = bth_d >= 0
        if np.any(bth_d==0):
            dep_t = bth_d[use_i]
        else:
            dep_t = np.append(0, bth_d[use_i])
        bth_a = np.interp(dep_t, bth_d, bth_a)
        bth_d = dep_t

    dz = 0.1 # meters

    num_d = len(wtr)
    if depths[num_d-1] < bottom:
        wtr[num_d] = wtr[num_d-1]
        depths =[num_d] = bottom
        num_d+=1
    if np.max(bth_d) < bottom:
        bth_d = np.append(bth_d, bottom)
        bth_a = np.append(bth_a, 0)
    if np.min(bth_d) < depths[0]:
        wtr = np.append(wtr[0], wtr)
        depths = np.append(np.min(bth_d), depths)

    i_o = np.argmin(depths)
    a_o = bth_a[i_o]

    if a_o==0:
        raise Exception("Surface area cannot be zero, check bathymetry file")

    # interpolate the bathymetry data
    layer_d = np.arange(top, bottom+dz, step=dz)
    layer_t = np.interp(layer_d, depths, wtr)
    layer_a = np.interp(layer_d, bth_d, bth_a)

    weighted_t = layer_a*layer_t*dz

    avg_temp = (np.sum(weighted_t) / np.sum(layer_a)) / dz

    return avg_temp

def mixed_depth(wdt, depths, start_depth=0.1):
    wtd = np.array(wdt)
    depths = np.array(depths)


    if None in wtd:
        return None
    
    if len(wtd) != len(depths):
        raise Exception("The water density lenght of the list must be equal to depths' len list")

    if len(wtd) < 3:
        return None

    r1 = 0.025
    r2 = 0.2

    depths = list(filter(lambda x: x > start_depth, depths))
    if len(depths) != len(wtd):
        wtd = wtd[(len(wtd)-len(depths)):len(wtd)]

    num_depths = len(depths)

    for i in range(num_depths-1):
        d_wtd = abs(wtd[i] - wtd[0])
        if d_wtd > r2:
            z_e = depths[i] + (((wtd[0]+d_wtd)-wtd[i])*((depths[i+1]-depths[i])/(wtd[i+1]-wtd[i])))
            print(z_e)
            return float(z_e)
    return float(depths[-1])

    

def meta_depths(
        wtr, depths, slope=0.1, seasonal=True,
        mixed_cutoff=1):
    wtr = np.array(wtr)
    depths = np.array(depths)

    if None in wtr:
        return [None, None]
    
    if len(wtr) < 3:
        return [np.max(depths), np.max(depths)]

    depths_idx_sorted = depths.argsort()
    depths = np.sort(depths)

    wtr = wtr[depths_idx_sorted]

    thermo_d = thermo_depth(
        wtr, depths,
        seasonal=seasonal,
        mixed_cutoff=mixed_cutoff
    )
    
    # if no thermo depth, then there can be no meta depths
    if thermo_d==None:
        return [float(depths[-1]), float(depths[-1])]

    rho_var = water_density(wtr.tolist())
    d_rho_perc = 0.15
    num_depths = len(depths)

    d_rho_dz = np.repeat(0.0, num_depths-1)

    # Calculate the first derivative of density
    for i in range(num_depths-1):
        d_rho_dz[i] = ( rho_var[i+1]-rho_var[i] )/( depths[i+1] - depths[i] )

    # initiate metalimnion bottom as last depth, this is returned if we can't
	# find a bottom
    meta_bot_depth = depths[num_depths-1]
    meta_top_depth = depths[0]
    t_depth = np.repeat(None, num_depths-1)
    for i in range(num_depths-1):
        t_depth[i] = np.mean(depths[i:i+2])
    t_depth = t_depth.astype(np.float)
    d_all = np.append(t_depth, thermo_d)
    tmp = np.unique(d_all, return_index=True)
    sort_depth = tmp[0].astype(np.float)
    sort_ind = tmp[1]
    num_depths = len(sort_depth)
    d_rho_dz = np.interp(sort_depth, t_depth, d_rho_dz)
    # find the thermocline index
	# this is where we will start our search for meta depths
    
    # TO DO
    # find a better way to find index
    thermo_index = np.where(sort_depth==thermo_d)[0]
    if len(thermo_index)==0:
        raise Exception("Something is wrong in thermocline depth calculation")
    else:
        thermo_index = thermo_index[0]
    for i in range(thermo_index, num_depths):
        if d_rho_dz[i] is not None and d_rho_dz[i] < slope:
            meta_bot_depth = sort_depth[i]
            break
    
    if (i-thermo_index) >=1 and d_rho_dz[thermo_index] is not None and d_rho_dz[thermo_index] > slope:
        d_rho_dz_tmp = np.array(d_rho_dz[thermo_index:i+1])
        sort_depth_tmp = np.array(sort_depth[thermo_index:i+1])
        arg_sorted = d_rho_dz_tmp.argsort()
        meta_bot_depth = np.interp(slope, d_rho_dz_tmp[arg_sorted], sort_depth_tmp[arg_sorted])
    if meta_bot_depth is None:
        meta_bot_depth = depths[num_depths-1]
    
    for i in np.arange(thermo_index, -1, step=-1):
        if d_rho_dz[i] is not None and d_rho_dz[i] < slope:
            meta_top_depth = sort_depth[i]
            break
    if thermo_index-i >=1 and d_rho_dz[thermo_index] is not None and d_rho_dz[thermo_index] > slope:
        d_rho_dz_tmp = np.array(d_rho_dz[i:thermo_index+1])
        sort_depth_tmp = np.array(sort_depth[i:thermo_index+1])
        arg_sorted = d_rho_dz_tmp.argsort()
        meta_top_depth = np.interp(slope, d_rho_dz_tmp[arg_sorted], sort_depth_tmp[arg_sorted])
    if meta_top_depth is None:
        meta_top_depth = depths[i]
    return [float(meta_top_depth), float(meta_bot_depth)]

def epi_temperature(wtr, depths, bth_a, bth_d):
    md = meta_depths(wtr, depths)
    if md[0] is None:
        avg_temp = layer_temperature(0, np.max(depths), wtr, depths, bth_a, bth_d)
    else:
        avg_temp = layer_temperature(0, md[0], wtr, depths, bth_a, bth_d)
    return avg_temp

def hypo_temperature(wtr, depths, bth_a, bth_d):
    md = meta_depths(wtr, depths)

    if md[0] is None:
        avg_temp = None
    else:
        avg_temp = layer_temperature(md[1],max(depths), wtr=wtr, depths=depths, bth_a=bth_a, bth_d=bth_d)
    return avg_temp