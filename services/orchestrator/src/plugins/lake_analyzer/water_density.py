import requests
import sys
import logging
from datetime import timedelta
from dateutil import parser
import pandas as pd
import plugins.lake_analyzer.methods as lake
if sys.version_info[0] < 3: 
    from StringIO import StringIO
else:
    from io import StringIO


def execute(form, service, new_proc, sensor_registered, process_id, indicator_id, et=None):
    log = logging.getLogger('tornado.application')
    proc = form['procedure']['value']
    chunk_min = int(form['chunk']['value'])
    if chunk_min <= 0:
        chunk_min = 5000
    service_target = form['service']['value']
    proc_sal = form['salinity']['value']
    ob_sal = form['salinity-observedproperty']['value']
    sal = int(form['salinity_constant']['value'])
    outputs = [{
        'name': 'water-density',
        'definition': 'urn:ogc:def:parameter:x-istsos:1.0:water:density',
        'description': '',
        'uom': 'Kg/m^3',
        'constraint': {}
    }]

    ob = form['procedure-observedproperty']['value']
    (
        ob_urn, data, time_interval, begin_position,
        end_position, proc_interval, proc_interval_min
    ) = lake.get_sensor_data(service, proc, ob, form, outputs, 'profile')
    if et:
        b_et, e_et = et.split('/')
        begin_position = parser.parse(b_et)
        end_position = parser.parse(e_et)
    if begin_position==end_position:
        begin_position = begin_position - timedelta(seconds=1)
    if proc_interval_min > chunk_min:
        end_position_tmp = begin_position + timedelta(minutes=chunk_min)
        begin_position_tmp = begin_position
        while begin_position_tmp < end_position:
            proc_interval_tmp = end_position - begin_position_tmp
            proc_interval_min_tmp = proc_interval_tmp.total_seconds() / 60
            progress = ((proc_interval_min - proc_interval_min_tmp)/proc_interval_min)*100
            try:
                req = requests.put(
                    'http://orchestrator:8888/istsos/process',
                    json={
                        "progress": round(progress, 2),
                        "status": "running",
                        "message": "The process is running.",
                        "id": process_id
                    }
                )
            except Exception as e:
                log.info(str(e))
            url = f'http://istsos/{service}?service=SOS&request=GetObservation&procedure={proc}&offering=temporary&eventTime={begin_position_tmp.isoformat()}/{end_position_tmp.isoformat()}&observedProperty=depth,{ob.replace("-", ":")}&responseFormat=text/plain&version=1.0.0&qualityIndex=False'
            req = requests.get(
                url,
            )
            df = pd.read_csv(StringIO(req.text), sep=",")
            unique_dates = df['urn:ogc:def:parameter:x-istsos:1.0:time:iso8601'].unique()
            values = []

            # salinity
            if sal<0:

                req = requests.get(
                    'http://istsos/wa/istsos/services/{}/procedures/{}'.format(
                        service,
                        proc_sal
                    ),
                )
                try:
                    data_sal = req.json()['data']
                except Exception as e:
                    raise Exception(f'Can\'t get procedure {proc_sal}.')
                ob_urn_sal = list(
                    filter(lambda x: x['name']==ob_sal, data_sal['outputs'])
                )[0]['definition']
                url = f'http://istsos/{service}?service=SOS&request=GetObservation&procedure={proc_sal}&offering=temporary&eventTime={begin_position_tmp.isoformat()}/{end_position_tmp.isoformat()}&observedProperty=depth,{ob_sal.replace("-", ":")}&responseFormat=text/plain&version=1.0.0&qualityIndex=False'
                req_sal = requests.get(
                    url,
                )
                df_sal = pd.read_csv(StringIO(req_sal.text), sep=",")
                unique_dates_sal = df['urn:ogc:def:parameter:x-istsos:1.0:time:iso8601'].unique()
            for d in unique_dates:
                # temperature
                df_filtered = df.loc[df['urn:ogc:def:parameter:x-istsos:1.0:time:iso8601']==d]
                df_filtered_sorted = df_filtered.sort_values(by='urn:ogc:def:parameter:x-istsos:1.0:water:depth', ascending=True)
                depths = df_filtered_sorted['urn:ogc:def:parameter:x-istsos:1.0:water:depth'].values.tolist()
                temps = df_filtered_sorted[ob_urn].values.tolist()

                # salinity
                if sal<0:
                    df_filtered_sal = df_sal.loc[df_sal['urn:ogc:def:parameter:x-istsos:1.0:time:iso8601']==d]
                    df_filtered_sorted_sal = df_filtered_sal.sort_values(by='urn:ogc:def:parameter:x-istsos:1.0:water:depth', ascending=True)
                    depths_sal = df_filtered_sorted_sal['urn:ogc:def:parameter:x-istsos:1.0:water:depth'].values.tolist()
                    sal = df_filtered_sorted_sal[ob_urn_sal].values.tolist()
                #schmidt_stability = lake.schmidt_stability(temps, depths, bth_a, bth_d, sal=float(form['salinity']['value']) )
                water_density = lake.water_density(temps, sal)
                d_ = list(
                    zip(
                        [d]*len(depths), depths, [100]*len(depths), water_density, [100]*len(depths)
                    )
                )
                if values:
                    values = values + d_
                else:
                    values = d_
            # create procedure
            if values:
                try:
                    if not sensor_registered:
                        try:
                            aid = lake.register_sensor(service_target, new_proc, data)
                            sensor_registered = True
                        except Exception as e:
                            raise Exception(f'Can\'t get procedure {new_proc}.')
                    if len(unique_dates)>1:
                        time_interval_tmp = [
                            unique_dates[0],
                            unique_dates[-1]
                        ]
                    else:
                        time_interval_tmp = [
                            unique_dates[0],
                            unique_dates[0]
                        ]
                    lake.insert_observation(
                        aid,
                        new_proc,
                        service_target,
                        time_interval_tmp,
                        values
                    )
                except Exception as e:
                    raise e
                begin_position_tmp = end_position_tmp
                end_position_tmp = end_position_tmp + timedelta(minutes=chunk_min)
                if end_position_tmp > end_position:
                    end_position_tmp = end_position
            else:
                begin_position_tmp = end_position_tmp
                end_position_tmp = end_position_tmp + timedelta(minutes=chunk_min)
                if end_position_tmp > end_position:
                    end_position_tmp = end_position

    else:

        # water-temp
        url = f'http://istsos/{service}?service=SOS&request=GetObservation&procedure={proc}&offering=temporary&eventTime={time_interval[0]}/{time_interval[1]}&observedProperty=depth,{ob.replace("-", ":")}&responseFormat=text/plain&version=1.0.0&qualityIndex=False'
        req = requests.get(
            url,
        )
        df = pd.read_csv(StringIO(req.text), sep=",")
        unique_dates = df['urn:ogc:def:parameter:x-istsos:1.0:time:iso8601'].unique()
        values = []

        # salinity
        if sal<0:

            req = requests.get(
                'http://istsos/wa/istsos/services/{}/procedures/{}'.format(service, proc_sal),
            )
            try:
                data_sal = req.json()['data']
            except Exception as e:
                raise Exception(f'Can\'t get procedure {proc_sal}.')
            ob_urn_sal = list(filter(lambda x: x['name']==ob_sal, data_sal['outputs']))[0]['definition']
            url = f'http://istsos/{service}?service=SOS&request=GetObservation&procedure={proc_sal}&offering=temporary&eventTime={time_interval[0]}/{time_interval[1]}&observedProperty=depth,{ob_sal.replace("-", ":")}&responseFormat=text/plain&version=1.0.0&qualityIndex=False'
            req_sal = requests.get(
                url,
            )
            df_sal = pd.read_csv(StringIO(req_sal.text), sep=",")
            unique_dates_sal = df['urn:ogc:def:parameter:x-istsos:1.0:time:iso8601'].unique()

        for d in unique_dates:
            # temperature
            df_filtered = df.loc[df['urn:ogc:def:parameter:x-istsos:1.0:time:iso8601']==d]
            df_filtered_sorted = df_filtered.sort_values(by='urn:ogc:def:parameter:x-istsos:1.0:water:depth', ascending=True)
            depths = df_filtered_sorted['urn:ogc:def:parameter:x-istsos:1.0:water:depth'].values.tolist()
            temps = df_filtered_sorted[ob_urn].values.tolist()

            # salinity
            if sal<0:
                df_filtered_sal = df_sal.loc[df_sal['urn:ogc:def:parameter:x-istsos:1.0:time:iso8601']==d]
                df_filtered_sorted_sal = df_filtered_sal.sort_values(by='urn:ogc:def:parameter:x-istsos:1.0:water:depth', ascending=True)
                depths_sal = df_filtered_sorted_sal['urn:ogc:def:parameter:x-istsos:1.0:water:depth'].values.tolist()
                sal = df_filtered_sorted_sal[ob_urn_sal].values.tolist()
            #schmidt_stability = lake.schmidt_stability(temps, depths, bth_a, bth_d, sal=float(form['salinity']['value']) )
            water_density = lake.water_density(temps, sal)
            d_ = list(
                zip(
                    [d]*len(depths), depths, [100]*len(depths), water_density, [100]*len(depths)
                )
            )
            if values:
                values = values + d_
            else:
                values = d_
        # create procedure
        try:
            try:
                aid = lake.register_sensor(service_target, new_proc, data)
            except Exception as e:
                raise Exception(f'Can\'t get procedure {new_proc}.')
            
            lake.insert_observation(
                aid,
                new_proc,
                service_target,
                [
                    unique_dates[0],
                    unique_dates[-1]
                ],
                values
            )
        except Exception as e:
            raise e
    if sensor_registered:
        req = requests.put(
            'http://orchestrator:8888/istsos/process',
            json={
                "progress": 100,
                "status": "completed",
                "message": "The process has been successfully completed.",
                "id": process_id
            }
        )
    else:
        req = requests.put(
            'http://orchestrator:8888/istsos/process',
            json={
                "progress": 100,
                "status": "completed",
                "message": "Unfortunately no data and procedure has been created. Time series with no data or not enough data?",
                "id": process_id
            }
        )