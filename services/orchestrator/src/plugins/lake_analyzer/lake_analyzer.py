from datetime import datetime, timezone, timedelta
import requests
import logging
import yaml
import pandas as pd
import numpy as np
import json
import sys
from dateutil import parser
import isodate


if sys.version_info[0] < 3: 
    from StringIO import StringIO
else:
    from io import StringIO
import plugins.lake_analyzer.methods as lake
import plugins.lake_analyzer.mixed_depth as mixed_depth
import plugins.lake_analyzer.meta_depths as meta_depths
import plugins.lake_analyzer.water_density_2 as water_density_2
import plugins.lake_analyzer.water_density as water_density
import plugins.lake_analyzer.buoyancy_freq as buoyancy_freq
import plugins.lake_analyzer.schmidt_stability as schmidt_stability
import plugins.lake_analyzer.epi_temperature as epi_temperature
import plugins.lake_analyzer.thermo_depth as thermo_depth
import plugins.lake_analyzer.volume_aggregation as volume_aggregation


class Indicator:
    def __init__(self):
        self.app_log = logging.getLogger('tornado.application')

    def check_params(self, method, form):
        plugin = None
        with open('./plugins/lake_analyzer/metadata.yml') as f:
            data = yaml.load(f)
            plugin = data
        
        if plugin:
            # check if form has all values set
            for k in plugin['indicators'][method]['form']:
                if form[k]['value'] == '':
                    v = form[k]['label']
                    raise Exception(f'{v} is not set.')
                if form[k]['type'] == 'procedure-ob' or form[k]['type'] == 'profile-ob':
                    if k+'-observedproperty' not in form:
                        v = form[k]['value']
                        raise Exception(f'Observed property of {v} is not set.')
                    else:
                        if form[k+'-observedproperty']['value'] == '':
                            v = form[k]['value']
                            raise Exception(f'Observed property of {v} is not set.')
        else:
            raise Exception('Plugin is not loaded correctly')

    def update(self, service, method, form, eventtime, process_id, indicator_id):
        try:
            plugin = None
            proc = form['name']['value']
            with open('./plugins/lake_analyzer/metadata.yml') as f:
                data = yaml.load(f)
                plugin = data
            try:
                req = requests.put(
                    'http://orchestrator:8888/istsos/process',
                    json={
                        "progress": 1,
                        "status": "running",
                        "message": "The process is running.",
                        "id": process_id
                    }
                )
            except Exception as e:
                self.app_log.info(str(e))
                raise e
            # check if form has all values set
            sensor_registered = False
            if method=="thermo_depth":
                thermo_depth.execute(
                    form, service, proc,
                    sensor_registered, process_id,
                    indicator_id, et=eventtime
                )
            elif method=="epi_temperature":
                epi_temperature.execute(
                    form, service, proc,
                    sensor_registered, process_id,
                    indicator_id, et=eventtime
                )
            elif method=="volume_aggregation":
                volume_aggregation.execute(
                form, service, proc,
                sensor_registered, process_id,
                indicator_id, et=eventtime
            )
            elif method=="schmidt_stability":
                schmidt_stability.execute(
                    form, service, proc,
                    sensor_registered, process_id,
                    indicator_id, et=eventtime
                )
            elif method=="buoyancy_freq":
                buoyancy_freq.execute(
                    form, service, proc,
                    sensor_registered, process_id,
                    indicator_id, et=eventtime
                )
            elif method=="water_density":
                water_density.execute(
                    form, service, proc,
                    sensor_registered, process_id,
                    indicator_id, et=eventtime
                )
            elif method=='water_density_2':
                water_density_2.execute(
                    form, service, proc,
                    sensor_registered, process_id,
                    indicator_id, et=eventtime
                )
            elif method=='meta_depth':
                meta_depths.execute(
                    form, service, proc,
                    sensor_registered, process_id,
                    indicator_id, et=eventtime
                )
            elif method=='mixed_depth':
                mixed_depth.execute(
                    form, service, proc,
                    sensor_registered, process_id,
                    indicator_id, et=eventtime
                )
            else:
                v = plugin['indicators'][method]['title']
                raise Exception(f'Indicator {v} is not implemented yet.')
        except Exception as e:
            try:
                msg = "The process has been terminated due to an error: {}".format(
                    str(e).replace("'", " ")
                )
                req = requests.put(
                    'http://orchestrator:8888/istsos/process',
                    json={
                        "progress": 100,
                        "status": "error",
                        "message": msg,
                        "id": process_id
                    }
                )
            except Exception as e:
                self.app_log.info(str(e))

    def create(self, service, method, form, process_id, indicator_id):
        try:
            new_proc = form['name']['value']
            with open('./plugins/data_analyzer/metadata.yml') as f:
                data = yaml.load(f)
                plugin = data
            try:
                req = requests.put(
                    'http://orchestrator:8888/istsos/process',
                    json={
                        "progress": 1,
                        "status": "running",
                        "message": "The process is running.",
                        "id": process_id
                    }
                )
            except Exception as e:
                self.app_log.info(str(e))
                raise e
            
            sensor_registered = False
            if method=="thermo_depth":
                thermo_depth.execute(
                    form, service, new_proc,
                    sensor_registered, process_id,
                    indicator_id
                )
            elif method=="epi_temperature":
                epi_temperature.execute(
                    form, service, new_proc,
                    sensor_registered, process_id,
                    indicator_id
                )
            elif method=="volume_aggregation":
                volume_aggregation.execute(
                form, service, new_proc,
                sensor_registered, process_id,
                indicator_id
            )
            elif method=="schmidt_stability":
                schmidt_stability.execute(
                    form, service, new_proc,
                    sensor_registered, process_id,
                    indicator_id
                )
            elif method=="buoyancy_freq":
                buoyancy_freq.execute(
                    form, service, new_proc,
                    sensor_registered, process_id,
                    indicator_id
                )
            elif method=="water_density":
                water_density.execute(
                    form, service, new_proc,
                    sensor_registered, process_id,
                    indicator_id
                )
            elif method=='water_density_2':
                water_density_2.execute(
                    form, service, new_proc,
                    sensor_registered, process_id,
                    indicator_id
                )
            elif method=='meta_depth':
                meta_depths.execute(
                    form, service, new_proc,
                    sensor_registered, process_id,
                    indicator_id
                )
            elif method=='mixed_depth':
                mixed_depth.execute(
                    form, service, new_proc,
                    sensor_registered, process_id,
                    indicator_id
                )
            else:
                v = plugin['indicators'][method]['title']
                raise Exception(f'Indicator {v} is not implmented yet.')
        except Exception as e:
            try:
                req = requests.put(
                    'http://orchestrator:8888/istsos/process',
                    json={
                        "progress": 100,
                        "status": "error",
                        "message": "The has been terminated due to an error: {}".format(
                            str(e)
                        ),
                        "id": process_id
                    }
                )
            except Exception as e:
                self.app_log.info(str(e))