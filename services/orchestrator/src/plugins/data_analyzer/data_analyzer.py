from datetime import datetime, timedelta, timezone
import requests
import logging
import yaml
import pandas as pd
import numpy as np
import json
import sys
from dateutil import parser
import isodate


if sys.version_info[0] < 3: 
    from StringIO import StringIO
else:
    from io import StringIO
import plugins.lake_analyzer.methods as lake


def register_sensor(service, new_proc, data):
    req = requests.post(
        'http://istsos/wa/istsos/services/{}/procedures'.format(service),
        data=json.dumps(data)
    )
    req = req.json()
    if not req['success']:
        raise Exception(f'Can\'t create the new procedure {new_proc}')
    req = requests.get(
        'http://istsos/wa/istsos/services/{}/procedures/{}'.format(service, new_proc),
    )

    data = req.json()['data']
    aid = data['assignedSensorId']
    return aid

def insert_observation(aid, proc, service, time_interval, values):
    req = requests.get(
        f"http://istsos/wa/istsos/services/{service}/operations/getobservation/offerings/temporary/procedures/{proc}/observedproperties/:/eventtime/last",
        verify=False
    )
    go_data = req.json()
    go_data = go_data['data'][0]
    go_data['result']['DataArray']['values'] = values
    go_data["samplingTime"] = {
        "beginPosition": time_interval[0],
        "endPosition":  time_interval[-1]
    }
    res = requests.post(
        "http://istsos/wa/istsos/services/%s/operations/"
        "insertobservation" % (
            service
        ),
        verify=False,
        data=json.dumps({
            "ForceInsert": "true",
            "AssignedSensorId": aid,
            "Observation": go_data
        })
    )
    res.raise_for_status()

def insert_observation_simple(service, aid, go_data):
    res = requests.post(
        "http://istsos/wa/istsos/services/%s/operations/"
        "insertobservation" % (
            service
        ),
        verify=False,
        data=json.dumps({
            "ForceInsert": "true",
            "AssignedSensorId": aid,
            "Observation": go_data
        })
    )
    res.raise_for_status()

class Indicator:
    def __init__(self):
        self.app_log = logging.getLogger('tornado.application')

    def update(self, service, method, form, eventtime, process_id, indicator_id):
        try:
            plugin = None
            target_proc = form['name']['value']
            target_service = form['service']['value']
            with open('./plugins/lake_analyzer/metadata.yml') as f:
                data = yaml.load(f)
                plugin = data
            try:
                req = requests.put(
                    'http://orchestrator:8888/istsos/process',
                    json={
                        "progress": 1,
                        "status": "running",
                        "message": "The process is running.",
                        "id": process_id
                    }
                )
            except Exception as e:
                self.app_log.error(str(e))
                raise e
            # check if form has all values set
            sensor_registered = False
            if method=="aggregation":
                proc = form['procedure']['value']
                chunk_min = int(form['chunk']['value'])
                if chunk_min <= 0:
                    chunk_min = 5000
                service_target = form['service']['value']
                ob = form['procedure-observedproperty']['value']
                func = form['function']['value']
                t_interval = form['interval']['value']
                t_interval_parsed = isodate.parse_duration(t_interval)
                # return
                no_data = form['nodata']['value']
                no_data_qi = form['nodataqi']['value']
                if func not in ['AVG', 'SUM', 'COUNT', 'MAX', 'MIN']:
                    raise Exception(f"Function {func} not supported.")
                req = requests.get(
                    'http://istsos/wa/istsos/services/{}/procedures/{}'.format(target_service, target_proc),
                )
                try:
                    # self.app_log.info(req.json())
                    data = req.json()['data']
                except Exception as e:
                    try:
                        req = requests.put(
                            'http://orchestrator:8888/istsos/process',
                            json={
                                "progress": 100,
                                "status": "error",
                                "message": f'Can not get metadata from procedure {target_proc}.',
                                "id": process_id
                            }
                        )
                    except Exception as e:
                        self.app_log.info(str(e))
                    raise Exception(f'Can\'t get metadata from procedure {proc}.')
                # data['assignedSensorId'] = ''
                data['outputs'] = list(filter(lambda x: x['name']==ob or x['name']=='Time' , data['outputs']))
                if not data['outputs'][0]['constraint']:
                    req = requests.put(
                        'http://orchestrator:8888/istsos/process',
                        json={
                            "progress": 100,
                            "status": "error",
                            "message": f'Can not get data from procedure {proc}.',
                            "id": process_id
                        }
                    )
                    raise Exception(f'Can not get data from procedure {proc}.')
                io_time_interval = eventtime.split('/')
                # io_begin_position = parser.parse(io_time_interval[0]).replace(minute=0, second=0)
                io_end_position = parser.parse(io_time_interval[1])
                time_interval = data['outputs'][0]['constraint']['interval']
                # begin_position = parser.parse(time_interval[0]).replace(minute=0, second=0)
                end_position = parser.parse(time_interval[1])
                # proc_interval = end_position - begin_position
                # proc_interval_min = proc_interval.total_seconds() / 60
                io_interval = io_end_position - end_position
                data['outputs'][0]['constraint'] = {}
                data['system_id'] = form['name']['value']
                data['system'] = form['name']['value']
                data['description'] = form['description']['value']
                data['keywords'] = form['keywords']['value']
                data['classification'][0]['value'] = 'insitu-fixed-point'
                data['classification'][1]['value'] = 'calculated'
                data['identification'][0]['value'] = ':'.join(data['identification'][0]['value'].split(':')[0:-1]) + ':' + form['name']['value']
                ob_replaced = ob.replace("-", ":")
                
                if io_interval > t_interval_parsed:
                    url = (
                        'http://istsos/wa/istsos/services/{}/operations/getobservation/'
                        'offerings/temporary/procedures/{}/observedproperties/{}'
                        '/eventtime/{}/{}?aggregatefunction={}&aggregateinterval={}'
                        '&aggregatenodata={}&aggregatenodataqi={}'
                    ).format(
                        service, proc, ob_replaced,
                        end_position.replace(minute=0, second=0).isoformat(),
                        io_end_position.replace(minute=0, second=0).isoformat(),
                        func, t_interval, no_data, no_data_qi
                    )
                    req = requests.get(
                        url,
                    )
                    try:
                        data_insert = req.json()['data'][0]
                    except Exception as e:
                        self.app_log.error(str(e))
                        raise Exception(f'Can\'t get data from procedure {proc}.')
                    try:
                        # data_insert = req.json()['data'][0]
                        data_insert['name'] = form['name']['value']
                        d_array = data_insert['result']['DataArray']['values']
                        if len(d_array)!=0 and len(d_array)==1:
                            data_insert["samplingTime"] = {
                                "beginPosition": (
                                    parser.parse(
                                        d_array[0][0]
                                    ) - timedelta(seconds=1)
                                ).isoformat(),
                                "endPosition":  parser.parse(
                                    d_array[0][0]
                                ).isoformat()
                            }
                        elif len(d_array)!=0 and len(d_array)>1:
                              data_insert["samplingTime"] = {
                                "beginPosition": (
                                    parser.parse(
                                        d_array[0][0]
                                    ) - timedelta(seconds=1)
                                ).isoformat(),
                                "endPosition":  parser.parse(
                                    d_array[len(d_array)-1][0]
                                ).isoformat()
                            }
                        procedure_splitted = data_insert['procedure'].split(':')[:-1]
                        procedure_splitted.append(form['name']['value'])
                        data_insert['procedure'] = ':'.join(
                            procedure_splitted
                        )
                    except Exception as e:
                        self.app_log.error(str(e))
                        raise Exception(f'Can\'t get data from procedure {proc}.')
                    try:
                        # self.app_log.info(data_insert)
                        insert_observation_simple(
                            service_target,
                            data["assignedSensorId"],
                            data_insert
                        )
                        req = requests.put(
                            'http://orchestrator:8888/istsos/process',
                            json={
                                "progress": 100,
                                "status": "completed",
                                "message": "The process has been successfully completed.",
                                "id": process_id
                            }
                        )
                    except Exception as e:
                        raise e
                # else:
                #     io_begin_position = parser.parse(io_time_interval[0]).replace(minute=0, second=0)
                #     io_end_position = parser.parse(io_time_interval[1])
                #     if io_begin_position==io_end_position:
                #         io_begin_position = io_begin_position - timedelta(seconds=1)
                #     url = (
                #         'http://istsos/wa/istsos/services/{}/operations/getobservation/'
                #         'offerings/temporary/procedures/{}/observedproperties/{}'
                #         '/eventtime/{}/{}?aggregatefunction={}&aggregateinterval={}'
                #         '&aggregatenodata={}&aggregatenodataqi={}'
                #     ).format(
                #         service, proc, ob_replaced,
                #         io_begin_position.replace(minute=0, second=0).isoformat(),
                #         io_end_position.replace(minute=0, second=0).isoformat(),
                #         func, t_interval, no_data, no_data_qi
                #     )
                #     req = requests.get(
                #         url,
                #     )
                #     try:
                #         data_insert = req.json()['data'][0]
                #         self.app_log.info(data_insert)
                #     except Exception as e:
                #         self.app_log.error(str(e))
                #         raise Exception(f'Can\'t get data from procedure {proc}.')
                #     try:
                #         # data_insert = req.json()['data'][0]
                #         data_insert['name'] = form['name']['value']
                #         data_insert["samplingTime"] = {
                #             "beginPosition": io_begin_position.replace(minute=0, second=0).isoformat(),
                #             "endPosition":  io_end_position.replace(minute=0, second=0).isoformat()
                #         }
                #         procedure_splitted = data_insert['procedure'].split(':')[:-1]
                #         procedure_splitted.append(form['name']['value'])
                #         data_insert['procedure'] = ':'.join(
                #             procedure_splitted
                #         )
                #     except Exception as e:
                #         self.app_log.error(str(e))
                #         raise Exception(f'Can\'t get data from procedure {proc}.')
                #     try:
                #         # self.app_log.info(data_insert)
                #         insert_observation_simple(
                #             service_target,
                #             data["assignedSensorId"],
                #             data_insert
                #         )
                #         req = requests.put(
                #             'http://orchestrator:8888/istsos/process',
                #             json={
                #                 "progress": 100,
                #                 "status": "completed",
                #                 "message": "The process has been successfully completed.",
                #                 "id": process_id
                #             }
                #         )
                #     except Exception as e:
                #         raise e
                # self.app_log.info(data_insert)
                req = requests.put(
                    'http://orchestrator:8888/istsos/process',
                    json={
                        "progress": 100,
                        "status": "completed",
                        "message": "The process has been successfully completed.",
                        "id": process_id
                    }
                )
            else:
                v = plugin['indicators'][method]['title']
                raise Exception(
                    f'Indicator {v} is not implemented yet.'
                )
        except Exception as e:
            try:
                msg = "The process has been terminated due to an error: {}".format(
                    str(e).replace("'", " ")
                )
                req = requests.put(
                    'http://orchestrator:8888/istsos/process',
                    json={
                        "progress": 100,
                        "status": "error",
                        "message": msg,
                        "id": process_id
                    }
                )
            except Exception as e:
                self.app_log.info(str(e))

    def check_params(self, method, form):
        plugin = None
        with open('./plugins/data_analyzer/metadata.yml') as f:
            data = yaml.load(f)
            plugin = data
        if plugin:
            # check if form has all values set
            for k in plugin['indicators'][method]['form']:
                if form[k]['value'] == '':
                    v = form[k]['label']
                    raise Exception(f'{v} is not set.')
                if form[k]['type'] == 'procedure-ob' or form[k]['type'] == 'profile-ob':
                    if k+'-observedproperty' not in form:
                        v = form[k]['value']
                        raise Exception(f'Observed property of {v} is not set.')
                    else:
                        if form[k+'-observedproperty']['value'] == '':
                            v = form[k]['value']
                            raise Exception(f'Observed property of {v} is not set.')
        else:
            raise Exception('Plugin is not loaded correctly')
    
    def create(self, service, method, form, process_id, indicator_id):
        plugin = None
        new_proc = form['name']['value']
        with open('./plugins/data_analyzer/metadata.yml') as f:
            data = yaml.load(f)
            plugin = data
        try:
            req = requests.put(
                'http://orchestrator:8888/istsos/process',
                json={
                    "progress": 1,
                    "status": "running",
                    "message": "The process is running.",
                    "id": process_id
                }
            )
        except Exception as e:
            self.app_log.info(str(e))
        
        if method=="aggregation":
            proc = form['procedure']['value']
            chunk_min = int(form['chunk']['value'])
            if chunk_min <= 0:
                chunk_min = 5000
            service_target = form['service']['value']
            ob = form['procedure-observedproperty']['value']
            func = form['function']['value']
            t_interval = form['interval']['value']
            t_interval_parsed = isodate.parse_duration(t_interval)
            # return
            no_data = form['nodata']['value']
            no_data_qi = form['nodataqi']['value']
            if func not in ['AVG', 'SUM', 'COUNT', 'MAX', 'MIN']:
                raise Exception(f"Function {func} not supported.")
            req = requests.get(
                'http://istsos/wa/istsos/services/{}/procedures/{}'.format(service, proc),
            )
            try:
                # self.app_log.info(req.json())
                data = req.json()['data']
            except Exception as e:
                self.app_log.info(req.text)
                try:
                    req = requests.put(
                        'http://orchestrator:8888/istsos/process',
                        json={
                            "progress": 100,
                            "status": "error",
                            "message": f'Can not get metadata from procedure {proc}.',
                            "id": process_id
                        }
                    )
                except Exception as e:
                    self.app_log.info(str(e))
                raise Exception(f'Can\'t get metadata from procedure {proc}.')
            data['assignedSensorId'] = ''
            data['outputs'] = list(filter(lambda x: x['name']==ob or x['name']=='Time' , data['outputs']))
            if not data['outputs'][0]['constraint']:
                req = requests.put(
                    'http://orchestrator:8888/istsos/process',
                    json={
                        "progress": 100,
                        "status": "error",
                        "message": f'Can not get data from procedure {proc}.',
                        "id": process_id
                    }
                )
                raise Exception(f'Can not get data from procedure {proc}.')
            time_interval = data['outputs'][0]['constraint']['interval']
            begin_position = parser.parse(time_interval[0]).replace(minute=0, second=0)
            end_position = parser.parse(time_interval[1])
            proc_interval = end_position - begin_position
            proc_interval_min = proc_interval.total_seconds() / 60
            data['outputs'][0]['constraint'] = {}
            data['system_id'] = form['name']['value']
            data['system'] = form['name']['value']
            data['description'] = form['description']['value']
            data['keywords'] = form['keywords']['value']
            data['classification'][0]['value'] = 'insitu-fixed-point'
            data['classification'][1]['value'] = 'calculated'
            data['identification'][0]['value'] = ':'.join(data['identification'][0]['value'].split(':')[0:-1]) + ':' + form['name']['value']
            ob_replaced = ob.replace("-", ":")
            if proc_interval_min > chunk_min:
                end_position_tmp = begin_position + timedelta(minutes=chunk_min)
                begin_position_tmp = begin_position
                sensor_registered = False
                while begin_position_tmp < end_position:
                    proc_interval_tmp = end_position - begin_position_tmp
                    proc_interval_min_tmp = proc_interval_tmp.total_seconds() / 60
                    progress = ((proc_interval_min - proc_interval_min_tmp)/proc_interval_min)*100
                    try:
                        req = requests.put(
                            'http://orchestrator:8888/istsos/process',
                            json={
                                "progress": round(progress, 2),
                                "status": "running",
                                "message": "The process is running.",
                                "id": process_id
                            }
                        )
                    except Exception as e:
                        self.app_log.info(str(e))
                    url = (
                        'http://istsos/wa/istsos/services/{}/operations/getobservation/'
                        'offerings/temporary/procedures/{}/observedproperties/{}'
                        '/eventtime/{}/{}?aggregatefunction={}&aggregateinterval={}'
                        '&aggregatenodata={}&aggregatenodataqi={}'
                    ).format(
                        service, proc, ob_replaced,
                        begin_position_tmp.isoformat(),
                        end_position_tmp.isoformat(),
                        func, t_interval, no_data, no_data_qi
                    )
                    req = requests.get(
                        url,
                    )
                    try:
                        try:
                            data_insert = req.json()['data'][0]
                            data_insert['name'] = form['name']['value']
                            data_insert["samplingTime"] = {
                                "beginPosition": begin_position_tmp.isoformat(),
                                "endPosition":  end_position_tmp.isoformat()
                            }
                            procedure_splitted = data_insert['procedure'].split(':')[:-1]
                            procedure_splitted.append(form['name']['value'])
                            data_insert['procedure'] = ':'.join(
                                procedure_splitted
                            )
                        except Exception as e:
                            self.app_log.error(str(e))
                            raise Exception(f'Can\'t get data from procedure {proc}.')
                        try:
                            if not sensor_registered:
                                try:
                                    aid = register_sensor(service_target, new_proc, data)
                                    sensor_registered = True
                                except Exception as e:
                                    raise Exception(f'Can\'t get procedure {new_proc}.')
                            insert_observation_simple(
                                service_target,
                                aid,
                                data_insert
                            )
                        except Exception as e:
                            raise e
                        begin_position_tmp = end_position_tmp
                        end_position_tmp = end_position_tmp + timedelta(minutes=chunk_min)
                        if end_position_tmp > end_position:
                            end_position_tmp = end_position
                    except Exception as e:
                        self.app_log.error(str(e))
                        raise Exception(f'Can\'t get data from procedure {proc}.')
            else:
                url = (
                    'http://istsos/wa/istsos/services/{}/operations/getobservation/'
                    'offerings/temporary/procedures/{}/observedproperties/{}'
                    '/eventtime/{}/{}?aggregatefunction={}&aggregateinterval={}'
                    '&aggregatenodata={}&aggregatenodataqi={}'
                ).format(
                    service, proc, ob_replaced,
                    time_interval[0], time_interval[1],
                    func, t_interval, no_data, no_data_qi
                )
                req = requests.get(
                    url,
                )
                try:
                    data_insert = req.json()['data'][0]
                except Exception as e:
                    logging.error(str(e))
                    raise Exception(f'Can\'t get data from procedure {proc}.')
                try:
                    # data_insert = req.json()['data'][0]
                    data_insert['name'] = form['name']['value']
                    data_insert["samplingTime"] = {
                        "beginPosition": begin_position.isoformat(),
                        "endPosition":  end_position.isoformat()
                    }
                    procedure_splitted = data_insert['procedure'].split(':')[:-1]
                    procedure_splitted.append(form['name']['value'])
                    data_insert['procedure'] = ':'.join(
                        procedure_splitted
                    )
                except Exception as e:
                    logging.error(str(e))
                    raise Exception(f'Can\'t get data from procedure {proc}.')
                try:
                    try:
                        aid = register_sensor(service_target, new_proc, data)
                    except Exception as e:
                        raise Exception(f'Can\'t get procedure {new_proc}.')
                    insert_observation_simple(
                        service_target,
                        aid,
                        data_insert
                    )
                    req = requests.put(
                        'http://orchestrator:8888/istsos/process',
                        json={
                            "progress": 100,
                            "status": "completed",
                            "message": "The process has been successfully completed.",
                            "id": process_id
                        }
                    )
                except Exception as e:
                    raise e
            req = requests.put(
                'http://orchestrator:8888/istsos/process',
                json={
                    "progress": 100,
                    "status": "completed",
                    "message": "The process has been successfully completed.",
                    "id": process_id
                }
            )
        else:
            v = plugin['indicators'][method]['title']
            raise Exception(f'Indicator {v} is not implmented yet.')