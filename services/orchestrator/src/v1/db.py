import logging


class Db:
    def __init__(self, pool):
        self.pool = pool

    async def new_process(self, name, service, module, indicatorname, procs, form, username):
        indicator_id = ''
        process_id = ''
        try:
            async with self.pool.acquire() as conn:
                try:
                    await conn.execute("BEGIN;")
                    indicator_id = await conn.fetchval("""
                        INSERT INTO orchestrator.indicator(
                        indicatorname, indicatorservice, indicatormodule, indicatormethod, indicatorprocs, indicatorform)
                        VALUES ('%s', '%s', '%s', '%s', ARRAY[%s], '%s') RETURNING indicatorid;
                    """ % (name, service, module, indicatorname, procs, form))
                    process_id = await conn.fetchval("""
                        INSERT INTO orchestrator.process(
                            processusername, indicatorid, processprogress, processstatus, processmessage)
                            VALUES ('%s', %s, '%s', '%s', 'Just started') RETURNING processid;
                    """ % (username,indicator_id,'0','created'))
                #     # await conn.execute("""
                #     #     UPDATE orchestrator.process
                #     #         SET processprogress='%s', processstatus='%s', processmessage='The process is runnnig.'
                #     #         WHERE processid=%s;
                #     # """ % (progress, status, message, id))
                    await conn.execute("COMMIT;")
                except Exception as e:
                    logging.info(str(e))
                    await conn.execute("ROLLBACK;")
        except Exception as e:
            logging.info(str(e))
        finally:
            return [process_id, indicator_id]
        
    async def get_indicators(self, user=None):
        try:
            res = []
            async with self.pool.acquire() as conn:
                try:
                    if user:
                        records = await conn.fetch(
                            """SELECT * FROM orchestrator.indicator
                            INNER JOIN orchestrator.process
                            ON indicator.indicatorid=process.indicatorid
                            WHERE processusername = '%s';""" % (user)
                        )
                    else:
                        records = await conn.fetch(
                            "SELECT * FROM orchestrator.indicator "
                            "INNER JOIN orchestrator.process ON indicator.indicatorid=process.indicatorid;"
                        )
                    return [dict(record) for record in records]
                except Exception as e:
                    await conn.execute("ROLLBACK;")
                    logging.info(str(e))
        except Exception as e:
            logging.info(str(e))

    async def get_process(self, user=None):
        try:
            res = []
            async with self.pool.acquire() as conn:
                try:
                    if user:
                        records = await conn.fetch(
                            """SELECT *
                            FROM orchestrator.process
                            INNER JOIN orchestrator.indicator ON indicator.indicatorid = process.indicatorid
                            WHERE processusername = '%s'
                            ORDER BY processid DESC""" % (user)
                        )
                    else:
                        records = await conn.fetch(
                            """SELECT *
                            FROM orchestrator.process
                            INNER JOIN orchestrator.indicator ON indicator.indicatorid = process.indicatorid
                            ORDER BY processid DESC"""
                        )
                    return [dict(record) for record in records]
                except Exception as e:
                    await conn.execute("ROLLBACK;")
                    logging.info(str(e))
        except Exception as e:
            logging.info(str(e))

    async def update_process(self, body):
        try:
            async with self.pool.acquire() as conn:
                try:
                    await conn.execute("BEGIN;")
                    await conn.execute("""
                        UPDATE orchestrator.process
                        SET processprogress='%s',
                            processstatus='%s',
                            processmessage='%s'
                        WHERE processid=%s;
                    """ % (
                        body['progress'],
                        body['status'],
                        body['message'],
                        body['id'])
                    )
                    await conn.execute("COMMIT;")
                except Exception as e:
                    await conn.execute("ROLLBACK;")
        except Exception as e:
            logging.info(str(e))

    async def delete_process(self, id):
        try:
            async with self.pool.acquire() as conn:
                try:
                    await conn.execute("BEGIN;")
                    await conn.execute("""
                        DELETE FROM orchestrator.process
                        WHERE processid=%s;
                    """ % (id))
                    await conn.execute("COMMIT;")
                except Exception as e:
                    await conn.execute("ROLLBACK;")
        except Exception as e:
            logging.info(str(e))