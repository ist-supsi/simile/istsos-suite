# -*- coding: utf-8 -*-

import asyncio
from tornado.options import options
from tornado.ioloop import IOLoop
from tornado import concurrent
import asyncpg as pg
import json
import importlib
import logging
import requests
# from bms.v1.feedback import ForwardFeedback

executor = concurrent.futures.ThreadPoolExecutor()

# PG DB CONNECTION
async def get_conn():
    try:
        return await pg.connect(
            user=options.pg_user,
            password=options.pg_password,
            database=options.pg_database,
            host=options.pg_host,
            port=options.pg_port
        )
    except Exception as x:
        raise x

async def get_pool():
    try:
        return await pg.create_pool(
            user=options.pg_user,
            password=options.pg_password,
            database=options.pg_database,
            host=options.pg_host,
            port=options.pg_port
        )
    except Exception as x:
        raise x

class InsertObservation():
    def __init__(self, payload, log=None):
        self.idx = 0
        self.log = log
        self.payload = payload
    async def execute(self):
        try:
            conn = await get_conn()
            records = await conn.fetch(
                "SELECT * FROM orchestrator.indicator "
                "INNER JOIN orchestrator.process ON indicator.indicatorid=process.indicatorid;"
            )
            await conn.close()
            for record in records:
                if 'indicatorprocs' in record:
                    indicatorprocs = record['indicatorprocs']
                    fields = self.payload.split(',')
                    eventtime = fields[2].replace(" ", "T") + "/" + fields[3].replace(" ", "T")
                    plugin = record['indicatormodule']
                    method = record['indicatormethod']
                    service = record['indicatorservice']
                    form = record['indicatorform']
                    process_id = record['processid']
                    indicator_id = record['indicatorid']
                    if fields[1] in indicatorprocs:
                        lib = importlib.import_module(f'plugins.{plugin}.{plugin}') 
                        indicator = lib.Indicator()
                        self.log.info(lib)
                        body_json = json.loads(form)
                        executor.submit(
                            indicator.update,
                            service, method,
                            body_json, eventtime,
                            process_id, indicator_id
                        )
        except Exception as e:
            self.log.info(str(e))


class BackgroundProcess():
    def __init__(self, payload, log=None):
        self.idx = 0
        self.log = log
        self.payload = payload
        self.ids = self.payload.split(',')
    async def execute(self):
        try:
            ids = self.payload.split(',')
            conn = await get_conn()
            
            records = await conn.fetch(
                "SELECT * FROM orchestrator.indicator "
                "INNER JOIN orchestrator.process ON indicator.indicatorid=process.indicatorid;"
            )
            await conn.close()
            for record in records:
                if 'indicatorprocs' in record:
                    indicatorprocs = record['indicatorprocs']
                    plugin = record['indicatormodule']
                    method = record['indicatormethod']
                    service = record['indicatorservice']
                    form = record['indicatorform']
                    lib = importlib.import_module(f'plugins.{plugin}.{plugin}') 
                    indicator = lib.Indicator()
                    self.log.info(lib)
                    body_json = json.loads(form)
                    indicator.create(service, method, body_json)
        except Exception as e:
            self.log.info(str(e))


class EventListener():

    actions = [
        "INSERT_OBSERVATION",
        "BACKGROUND_PROCESS"
    ]

    def __init__(self):
        self.log = logging.getLogger(
            'tornado.application'
        )
        self.pool = get_pool()

    async def start(self):
        self.pool = await self.pool
        for action in self.actions:
            conn = await self.pool.acquire()
            await conn.add_listener(action, self.callback)

    async def stop(self):
        for action in self.actions:
            async with self.conn.acquire() as conn:
                await conn.remove_listener(action, self.callback)
        await self.pool.realese()

    def callback(self, conn, pid, action, payload):
        if action in self.actions:
            exe = None
            request = {}
            if action == 'INSERT_OBSERVATION':
                try:
                    io = InsertObservation(payload, log=self.log)
                    asyncio.create_task(io.execute())
                except Exception as ex:
                    self.log.error(ex)
            elif action == "BACKGROUND_PROCESS":
                try:
                    io = BackgroundProcess(payload, log=self.log)
                    asyncio.create_task(io.execute())
                except Exception as ex:
                    self.log.error(ex)
            else:
                self.log.warning(f"Action unknown: {action}")
        else:
            print(f"Unknown event action: {action}")
