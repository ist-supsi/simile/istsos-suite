# Copyright (C) 2021 ciao
# 
# This file is part of istsos-suite.
# 
# istsos-suite is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# istsos-suite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with istsos-suite.  If not, see <http://www.gnu.org/licenses/>.
import jwt
from jwt import PyJWKClient
import logging


def get_userinfo(remote_ip, token):
    app_log = logging.getLogger('tornado.application')
    try:
        url = "http://keycloak:8080/auth/realms/istsos/protocol/openid-connect/certs"
        jwks_client = PyJWKClient(url)
        options = {"verify_signature": True, "verify_aud": False, "exp": True}
        signing_key = jwks_client.get_signing_key_from_jwt(token)
        userinfo = jwt.decode(token, signing_key.key, algorithms=["RS256"], options=options)
        # app_log.info(userinfo)
        return {
            "success": True,
            "data": userinfo
        }
    except Exception as e:
        return {
            "success": False,
            "message": str(e)
        }

def check_permission(userinfo, uri, method):
    app_log = logging.getLogger('tornado.application')
    # app_log.info(userinfo['realm_access'])
    roles = userinfo['realm_access']['roles']
    if 'admin' in roles:
        return True
    uri_splitted = uri.split('/')
    if len(uri_splitted) > 6:
        service = uri_splitted[5]
    else:
        service = None
    if 'viewer' in roles:
        if method=='GET':
            if 'groups' in userinfo:
                if userinfo['groups']:
                    app_log.info(service)
                    # app_log.info("service")
                    if not service or service=='default':
                        if '/istsos/wa/istsos/operations/status' in uri:
                            return True
                    if f'/{service}' in userinfo['groups'] or userinfo['groups']=='*':
                        return True
                    else:
                        return False
                else:
                    return False
            else:
                return False
        else:
            return False
    if 'editor' in roles:
        if 'groups' in userinfo:
            if userinfo['groups']:
                if not service or service=='default':
                    if '/istsos/wa/istsos/operations/status' in uri:
                        return True
                if f'/{service}' in userinfo['groups'] or userinfo['groups']=='*':
                    return True
                else:
                    return False
            else:
                return False
        else:
            return False
    else:
        return False