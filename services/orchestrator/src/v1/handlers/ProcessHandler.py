import tornado
from jsonschema import validate
import json
import logging
from ..db import Db
from ..utils import get_userinfo, check_permission 


update_schema = {
    "type" : "object",
    "properties" : {
        "progress" : {"type" : "number"},
        "status" : {"type" : "string"},
        "message" : {"type" : "string"},
        "id" : {"type" : "number"},
    },
}

log = logging.getLogger('tornado.application')


class ProcessHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

    def options(self):
        self.set_status(204)
        self.finish()

    async def get(self):
        plugins = []
        uri_splitted = self.request.uri.split('/')
        service = uri_splitted[3]
        token = ''
        if self.request.headers.get('Authorization'):
            token = self.request.headers.get('Authorization').split(" ")[1]
        userinfo = get_userinfo(self.request.remote_ip, token)
        username = userinfo['data']['preferred_username']
        if userinfo['success']:
            allowed = check_permission(
                userinfo['data'],
                self.request.uri,
                'GET'
            )
        try:
            db = Db(self.application.pool)
            data = await db.get_process(user=username)
            data_filtered = []
            for d in data:
                form = json.loads(d['indicatorform'])
                d['indicatorform'] = form
                if d['indicatorservice']==service:
                    data_filtered.append(d)
                elif 'service' in form.keys():
                    if form['service']['value']==service:
                        data_filtered.append(d)
            self.write({
                "success": True,
                "data": data_filtered
            })
        except Exception as e:
            self.write({
                "success": False,
                "message": str(e)
            })

    async def put(self):
        try:
            body = json.loads(self.request.body)
            validate(instance=body, schema=update_schema)
            db = Db(self.application.pool)
            await db.update_process(body)
            self.write({
                "success": True,
                "message": ''
            })
        except Exception as e:
            self.write({
                "success": False,
                "message": str(e)
            })

    async def delete(self):
        try:
            process_id = self.request.uri.split('/')[-1]
            db = Db(self.application.pool)
            await db.delete_process(process_id)
            self.write({
                "success": True,
                "message": ''
            })
        except Exception as e:
            self.write({
                "success": False,
                "message": str(e)
            })

    async def post(self):
        self.write("ciao")
