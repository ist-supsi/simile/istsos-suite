#BUILD WITH: 'docker build -t istsos-web:v1.0.0 -f Dockerfile .'
#RUN WITH: 'docker run --rm -d --name istsos-web-c -p 80:80 istsos-web:v1.0.0'
#RUN TO DEBUG: 'docker run --rm -it --name istsos-web-c -p 80:80 istsos-web:v1.0.0 bash'

# Build Stage
FROM debian:buster as build-stage

RUN apt-get update && \
    apt-get install -y git

WORKDIR /app

COPY ./ /app/

ENV ISTSOS_REPOSITORY https://github.com/danistrigaro/istsos2.git

ENV ISTSOS_BRANCH master

RUN rm -rf _build && \
    mkdir -p _build && \
    cd _build && \
    git clone -b ${ISTSOS_BRANCH} ${ISTSOS_REPOSITORY} istsos && \
    cd .. && \
    mkdir -p _build/istsos/logs && \
    mv _build/istsos/interface/admin/www/* _build/istsos/interface/admin && \
    mv _build/istsos/interface/modules/requests/src/xml _build/istsos/interface/modules/requests && \
    cp /app/services/default.cfg _build/istsos/services/default.cfg && \
    rm -rf `find . -type d -name .svn`

FROM httpd:2.4.41-alpine

WORKDIR /usr/share

COPY --from=build-stage /app/_build/ /usr/share/

RUN apk update && \
    chmod 755 -R /usr/share/istsos && \
    chown -R daemon:daemon /usr/share/istsos/services && \
    chown -R daemon:daemon /usr/share/istsos/logs

RUN apk update && \
    set -ex \   
    # && apk add --no-cache --virtual .build-deps \
    && apk add --no-cache \
        py3-pip \
        musl \
        xz-libs \
        zlib \
        libxml2-dev \
        libxslt \
        libc-dev \
        py3-lxml \
        libxslt-dev \
        postgresql-dev \
        gcc \
        python3-dev \
        musl-dev

WORKDIR /usr/share/istsos
RUN pip3 install gunicorn
RUN pip3 install gunicorn[gevent]
RUN pip3 install -r /usr/share/istsos/requirements.txt

EXPOSE 8000

CMD ["gunicorn"  , "-b", "istsos:80", '-k', 'gevent' "application:application"]