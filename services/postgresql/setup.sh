#!/usr/bin/env bash
psql "postgres://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_HOST/$POSTGRES_DB?sslmode=disable" <<-EOSQL
CREATE USER docker;
CREATE DATABASE istsos;
GRANT ALL PRIVILEGES ON DATABASE istsos TO istsos;
CREATE DATABASE keycloak;
\connect istsos;
CREATE EXTENSION postgis;
CREATE SCHEMA orchestrator;

CREATE TABLE orchestrator.indicator
(
    indicatorid serial NOT NULL,
    indicatorname character varying NOT NULL,
    indicatorservice character varying NOT NULL,
    indicatormodule character varying NOT NULL,
    indicatormethod character varying NOT NULL,
    indicatorprocs text[] NOT NULL,
    indicatorform json NOT NULL,
    PRIMARY KEY (indicatorid)
);

CREATE TABLE orchestrator.process
(
    processid serial NOT NULL,
    processusername character varying NOT NULL,
    indicatorid integer NOT NULL,
    processprogress character varying NOT NULL,
    processstatus character varying NOT NULL,
    processmessage character varying,
    PRIMARY KEY (processid),
    CONSTRAINT process_indicatorid
      FOREIGN KEY(indicatorid) 
	  REFERENCES orchestrator.indicator(indicatorid)
	  ON DELETE CASCADE
);

EOSQL