import tornado.ioloop
import tornado.web
import requests
import json
import binascii
from oauthlib.oauth2 import LegacyApplicationClient
from requests_oauthlib import OAuth2Session
from datetime import datetime, date
import time
import os
import logging
import yaml

from keycloak.realm import KeycloakRealm




__abspath__ = os.path.abspath(os.getcwd())
__plugins__ = os.path.join(
    __abspath__,
    'plugins'
)

oauth_client_id = os.environ['KEYCLOAK_ISTSOS_CLIENT_ID']
oauth_client_secret = os.environ['KEYCLOAK_ISTSOS_SECRET']
oauth_realm = os.environ['KEYCLOAK_ISTSOS_REALM']

realm = KeycloakRealm(
    server_url='{}://{}:10001'.format(
        'http',
        # os.environ['KEYCLOAK_PROTOCOL'],
        os.environ['GN_DOMAIN']
    ),
    realm_name=oauth_realm
)
oidc_client = realm.open_id_connect(
    client_id=oauth_client_id,
    client_secret=oauth_client_secret
)


class IstsosHandler(tornado.web.RequestHandler):
    def get(self):
        app_log = logging.getLogger('tornado.application')
        status = 200
        message=''
        app_log.info(self.request)
        if self.request.headers.get('Authorization'):
            token = self.request.headers.get('Authorization').split(" ")[1]
        else:
            token=''
        try:
            userinfo = oidc_client.userinfo(token)
        except Exception as e:
            self.clear()
            message =str(e)
            status = 401
        if status==401:
            self.clear()
            self.set_status(status)
            self.write({
                "success": False,
                "message": message
            })
        else:
            # with open(r'E:\data\fruits.yaml') as file:
            print(self.request.path)
            self.write(userinfo)

class IndicatorsHandler(tornado.web.RequestHandler):
    def get(self):
        app_log = logging.getLogger('tornado.application')
        status = 200
        message=''
        if self.request.headers.get('Authorization'):
            token = self.request.headers.get('Authorization').split(" ")[1]
        else:
            token=''
        try:
            userinfo = oidc_client.userinfo(token)
        except Exception as e:
            self.clear()
            message =str(e)
            status = 401
        if status==401:
            self.clear()
            self.set_status(status)
            self.write({
                "success": False,
                "message": message
            })
        else:
            # with open(r'E:\data\fruits.yaml') as file:
            print(os.path.dirname())
            self.write(userinfo)


    def post(self):
        print('ciao post')


if __name__ == "__main__":
    settings = {
        "debug": True
    }
    app_log = logging.getLogger('tornado.application')
    acc_log = logging.getLogger('tornado.access')
    gen_log = logging.getLogger('tornado.general')
    if settings['debug']:
        app_log.setLevel(logging.DEBUG)
        acc_log.setLevel(logging.DEBUG)
        gen_log.setLevel(logging.DEBUG)
    else:
        app_log.setLevel(logging.INFO)
        acc_log.setLevel(logging.INFO)
        gen_log.setLevel(logging.INFO)
    gen_log.info('### START ###')

    app = tornado.web.Application([
        (r"/istsos", IstsosHandler)
        (r"/indicators", IndicatorsHandler )
    ], **settings)
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
